import { Router, Request, Response } from 'express';
import * as express from 'express';
import * as path from 'path';
var favicon = require('serve-favicon');
import * as logger from 'morgan';
var cookieParser = require('cookie-parser');
import * as bodyParser from 'body-parser';

import * as routes from './routes/index';


const app: express.Application = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes.router);
//app.use('/docs', docs.router);

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res: Response, next) {
	res.status(err.status || 500);
	res.json('Error ' + err);
  //res.send('error', {
  //  message: err.message,
  //  error: {}
  //});
});


export const App: express.Application = app;
