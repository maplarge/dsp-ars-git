# Dashboard ARS Shared


# To Begin:

## Install required packages

### Install Node
Install [Nodejs](https://nodejs.org/en/)

### Install packages

Navigate to the project.
In a command prompt or terminal, run the command:

`npm install`

And Gulp

`npm install gulp-cli -g`
`npm install gulp -D`

## Build project
In your IDE, compile the typescript to JS
In visual studio, you can do this by simply building the project.

(if you see errors in the build log, do not worry about it for now. It is just the IDE thinking it is missing references, but the typescript definitions will still be present.)

## Run the project
In the command prompt/terminal, run

`gulp build`

This will compile the JS (including any custom built dashboard controls) into the main script file.

```
[13:40:14] Using gulpfile ~\Documents\Code\dsp-ars\gulpfile.js
[13:40:14] Starting 'compile'...
Info: 2018-10-11 01:40:14 gulp - compiling TypeScript
[13:40:19] Finished 'compile' after 4.78 s
[13:40:19] Starting 'build'...
Info: 2018-10-11 01:40:19 gulp - compiling Scripts
Info: 2018-10-11 01:40:19 gulp - compiling Styles
[13:40:19] Finished 'build' after 10 ms
```

Then you can run the server with

`node server.js`

or

`nodemon`
if you have nodemon installed

## Open the browser
Open your browser to 
[http://localhost:8000](http://localhost:8000)
