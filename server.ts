
import * as app from "./app";
import * as http from 'http';
import * as log from 'node-pretty-log';
import * as fs from "fs";

let port = process.env.PORT || 8000;
let mlJSUrl = "http://qa.maplarge.com/js";

//var file = fs.createWriteStream("public/lib/MapLargeAPI.js");
//var request = http.get(mlJSUrl, function(response) {
//    log('Info', 'Getting TS from MapLarge');
//    response.pipe(file);
//    file.on('finish', () => log("Info", 'Finished Writting ML file'));
//});

http.createServer(app.App).listen(port, function () {
	log('info', 'Server Listening at ' + port);
});
