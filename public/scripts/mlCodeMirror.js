
var insertTemplate = () => { };


function CodeMirrorInit($el) {
	//var codemirror = ($el.find('.CodeMirror')[0]).CodeMirror;

	var iframe = $el.find('iframe').get(0);

	var $run = $('<button>Run</button>');

	$run.attr('title', 'NOTE: Controls you have built custom may not work here because the code from the left is placed in an iframe that does NOT contain your custom code');

	$el.find('.code-header').after($run);

	var htmlEditor =  CodeMirror.fromTextArea(document.getElementById("code"), {
		mode: {
			name: "htmlmixed",
			scriptTypes: [{
				matches: /\/x-handlebars-template|\/x-mustache/i,
				mode: null
			},
			{
				matches: /(text|application)\/(x-)?vb(a|script)/i,
				mode: "vbscript"
			}]
		},
		theme: 'neat',
		styleActiveLine: true
	});


	htmlEditor.setOption("theme", "neat");

	var lastTime = new Date();

	var contents = "";

	var run = (result) => {
		if (iframe) {
			writeResult(result);
		}
	}

	$run.on('click', (e) => {
		run(contents);
	});

	htmlEditor.on('change', (result) => {
		contents = result.getValue();
	});

	$el.find('#code').on('change', run);

	var writeResult = (result) => {
		var doc;

		if (iframe.contentDocument)
			doc = iframe.contentDocument;
		else if (iframe.contentWindow)
			doc = iframe.contentWindow.document;
		else
			doc = iframe.document;

		doc.open();
		doc.writeln(result);
		doc.close();
	}

	insertTemplate = function(insides){
		if (insides) {
			htmlEditor.setOption('mode', "text/typescript");
			return htmlEditor.getDoc().setValue(insides);
			
		}
		htmlEditor.getDoc().setValue(
		`<!DOCTYPE html>
		<html>
			<head>
				<script type="text/javascript" src="http://qa.maplarge.com/js"></script>
			</head>
			<body>
				<script type="text/javascript">
						${scriptText}
				</script>
				<div id="ml-output" style="width:100%;height:100%;"></div>
			</body>
		</html>`
		);
	}.bind(this);

	insertTemplate();

}

if ($('.basic-control').length) {
	CodeMirrorInit($('.basic-control'));
}