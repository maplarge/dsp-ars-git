function tryControlsPage($el) {
	if (!$el.length) {
		return;
	}

	ml.onload(() => {

		var _ml = ml.ui.dashboard.controls;

		var dashboard = _ml.ScopeDefaults.builder().options({
			defaults: [
				{ path: "button_label", value: "Button Label" },
				{ path: "pw_disabled", value: false },
			]
		}).children([
			_ml.Flexbox.builder()
				.options({
					justify: "Space Between"
				}).children([

					db.examples.controls.TextboxSample.builder().name('button_label')
						.options({
							label: "Text Box",
							placeholder: "Type here"
						}),

					db.examples.controls.TextboxPWSample.builder().name('password')
						.options({
							label: "Password textbox",
							placeholder: "Disabled if label > 3",
							disabled: {
								$scopePath: (scope) => scope.button_label().length > 3
							}
						}),

					db.examples.controls.BPButton.builder().options({
						label: "{{button_label}}"
					}),
				]),
			_ml.Flexbox.builder()
				.options({
					justify: "Space Between"
				}).children([
					db.examples.controls.TextboxGroup.builder().options({
						tbData: [
							{ label: "Textbox 1", placeholder: "Type Here" },
							{ label: "Textbox 2", placeholder: "Type Here" },
							{ label: "Textbox 3", placeholder: "Type Here" },
							{ label: "Textbox 4", placeholder: "Type Here" },
						]
					}),
				])

		]).toJSON();

		ml.ui.dashboard.create(dashboard, '#ml-output');

	});
}

$(window).bind('load', () => {
	tryControlsPage($('.experimenting-with-controls'));
});