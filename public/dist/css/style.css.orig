/* BASICS */
.CodeMirror {
  /* Set height, width, borders, and global font properties here */
  font-family: monospace;
  height: 300px;
  color: black;
  direction: ltr;
  font-size: 12px;
  height: auto;
  min-height: 530px;
  position: relative;
  overflow: hidden;
  background: #f5f5f5;
  border-radius: 5px;
}
/* PADDING */
.CodeMirror-lines {
  padding: 4px 0;
  /* Vertical padding around content */
}
.CodeMirror pre {
  padding: 0 4px;
  /* Horizontal padding of content */
}
.CodeMirror-scrollbar-filler,
.CodeMirror-gutter-filler {
  background-color: white;
  /* The little square between H and V scrollbars */
}
/* GUTTER */
.CodeMirror-gutters {
  border-right: 1px solid #ddd;
  background-color: #f7f7f7;
  white-space: nowrap;
}
.CodeMirror-linenumber {
  padding: 0 3px 0 5px;
  min-width: 20px;
  text-align: right;
  color: #999;
  white-space: nowrap;
}
.CodeMirror-guttermarker {
  color: black;
}
.CodeMirror-guttermarker-subtle {
  color: #999;
}
/* CURSOR */
.CodeMirror-cursor {
  border-left: 1px solid black;
  border-right: none;
  width: 0;
}
/* Shown when moving in bi-directional text */
.CodeMirror div.CodeMirror-secondarycursor {
  border-left: 1px solid silver;
}
.cm-fat-cursor .CodeMirror-cursor {
  width: auto;
  border: 0 !important;
  background: #7e7;
}
.cm-fat-cursor div.CodeMirror-cursors {
  z-index: 1;
}
.cm-fat-cursor-mark {
  background-color: rgba(20, 255, 20, 0.5);
  -webkit-animation: blink 1.06s steps(1) infinite;
  -moz-animation: blink 1.06s steps(1) infinite;
  animation: blink 1.06s steps(1) infinite;
}
.cm-animate-fat-cursor {
  width: auto;
  border: 0;
  -webkit-animation: blink 1.06s steps(1) infinite;
  -moz-animation: blink 1.06s steps(1) infinite;
  animation: blink 1.06s steps(1) infinite;
  background-color: #7e7;
}
@-moz-keyframes blink {
  50% {
    background-color: transparent;
  }
}
@-webkit-keyframes blink {
  50% {
    background-color: transparent;
  }
}
@keyframes blink {
  50% {
    background-color: transparent;
  }
}
/* Can style cursor different in overwrite (non-insert) mode */
.cm-tab {
  display: inline-block;
  text-decoration: inherit;
}
.CodeMirror-rulers {
  position: absolute;
  left: 0;
  right: 0;
  top: -50px;
  bottom: -20px;
  overflow: hidden;
}
.CodeMirror-ruler {
  border-left: 1px solid #ccc;
  top: 0;
  bottom: 0;
  position: absolute;
}
/* DEFAULT THEME */
.cm-s-default .cm-header {
  color: blue;
}
.cm-s-default .cm-quote {
  color: #090;
}
.cm-negative {
  color: #d44;
}
.cm-positive {
  color: #292;
}
.cm-header,
.cm-strong {
  font-weight: bold;
}
.cm-em {
  font-style: italic;
}
.cm-link {
  text-decoration: underline;
}
.cm-strikethrough {
  text-decoration: line-through;
}
.cm-s-default .cm-keyword {
  color: #708;
}
.cm-s-default .cm-atom {
  color: #219;
}
.cm-s-default .cm-number {
  color: #164;
}
.cm-s-default .cm-def {
  color: #00f;
}
.cm-s-default .cm-variable-2 {
  color: #05a;
}
.cm-s-default .cm-variable-3,
.cm-s-default .cm-type {
  color: #085;
}
.cm-s-default .cm-comment {
  color: #a50;
}
.cm-s-default .cm-string {
  color: #a11;
}
.cm-s-default .cm-string-2 {
  color: #f50;
}
.cm-s-default .cm-meta {
  color: #555;
}
.cm-s-default .cm-qualifier {
  color: #555;
}
.cm-s-default .cm-builtin {
  color: #30a;
}
.cm-s-default .cm-bracket {
  color: #997;
}
.cm-s-default .cm-tag {
  color: #170;
}
.cm-s-default .cm-attribute {
  color: #00c;
}
.cm-s-default .cm-hr {
  color: #999;
}
.cm-s-default .cm-link {
  color: #00c;
}
.cm-s-default .cm-error {
  color: #f00;
}
.cm-invalidchar {
  color: #f00;
}
.CodeMirror-composing {
  border-bottom: 2px solid;
}
/* Default styles for common addons */
div.CodeMirror span.CodeMirror-matchingbracket {
  color: #0b0;
}
div.CodeMirror span.CodeMirror-nonmatchingbracket {
  color: #a22;
}
.CodeMirror-matchingtag {
  background: rgba(255, 150, 0, 0.3);
}
.CodeMirror-activeline-background {
  background: #e8f2ff;
}
/* STOP */
/* The rest of this file contains styles related to the mechanics of
   the editor. You probably shouldn't touch them. */
.CodeMirror {
  position: relative;
  overflow: hidden;
  background: #f5f5f5;
  border-radius: 5px;
}
.CodeMirror-scroll {
  overflow: scroll !important;
  /* Things will break if this is overridden */
  /* 30px is the magic margin used to hide the element's real scrollbars */
  /* See overflow: hidden in .CodeMirror */
  margin-bottom: -30px;
  margin-right: -30px;
  padding-bottom: 30px;
  height: 100%;
  outline: none;
  /* Prevent dragging from highlighting the element */
  position: relative;
}
.CodeMirror-sizer {
  position: relative;
  border-right: 30px solid transparent;
}
/* The fake, visible scrollbars. Used to force redraw during scrolling
   before actual scrolling happens, thus preventing shaking and
   flickering artifacts. */
.CodeMirror-vscrollbar,
.CodeMirror-hscrollbar,
.CodeMirror-scrollbar-filler,
.CodeMirror-gutter-filler {
  position: absolute;
  z-index: 6;
  display: none;
}
.CodeMirror-vscrollbar {
  right: 0;
  top: 0;
  overflow-x: hidden;
  overflow-y: scroll;
}
.CodeMirror-hscrollbar {
  bottom: 0;
  left: 0;
  overflow-y: hidden;
  overflow-x: scroll;
}
.CodeMirror-scrollbar-filler {
  right: 0;
  bottom: 0;
}
.CodeMirror-gutter-filler {
  left: 0;
  bottom: 0;
}
.CodeMirror-gutters {
  position: absolute;
  left: 0;
  top: 0;
  min-height: 100%;
  z-index: 3;
}
.CodeMirror-gutter {
  white-space: normal;
  height: 100%;
  display: inline-block;
  vertical-align: top;
  margin-bottom: -30px;
}
.CodeMirror-gutter-wrapper {
  position: absolute;
  z-index: 4;
  background: none !important;
  border: none !important;
}
.CodeMirror-gutter-background {
  position: absolute;
  top: 0;
  bottom: 0;
  z-index: 4;
}
.CodeMirror-gutter-elt {
  position: absolute;
  cursor: default;
  z-index: 4;
}
.CodeMirror-gutter-wrapper ::selection {
  background-color: transparent;
}
.CodeMirror-gutter-wrapper ::-moz-selection {
  background-color: transparent;
}
.CodeMirror-lines {
  cursor: text;
  min-height: 1px;
  /* prevents collapsing before first draw */
}
.CodeMirror pre {
  /* Reset some styles that the rest of the page might have set */
  -moz-border-radius: 0;
  -webkit-border-radius: 0;
  border-radius: 0;
  border-width: 0;
  background: transparent;
  font-family: inherit;
  font-size: inherit;
  margin: 0;
  white-space: pre;
  word-wrap: normal;
  line-height: inherit;
  color: inherit;
  z-index: 2;
  position: relative;
  overflow: visible;
  -webkit-tap-highlight-color: transparent;
  -webkit-font-variant-ligatures: contextual;
  font-variant-ligatures: contextual;
}
.CodeMirror-wrap pre {
  word-wrap: break-word;
  white-space: pre-wrap;
  word-break: normal;
}
.CodeMirror-linebackground {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  z-index: 0;
}
.CodeMirror-linewidget {
  position: relative;
  z-index: 2;
  padding: 0.1px;
  /* Force widget margins to stay inside of the container */
}
.CodeMirror-rtl pre {
  direction: rtl;
}
.CodeMirror-code {
  outline: none;
}
/* Force content-box sizing for the elements where we expect it */
.CodeMirror-scroll,
.CodeMirror-sizer,
.CodeMirror-gutter,
.CodeMirror-gutters,
.CodeMirror-linenumber {
  -moz-box-sizing: content-box;
  box-sizing: content-box;
}
.CodeMirror-measure {
  position: absolute;
  width: 100%;
  height: 0;
  overflow: hidden;
  visibility: hidden;
}
.CodeMirror-cursor {
  position: absolute;
  pointer-events: none;
}
.CodeMirror-measure pre {
  position: static;
}
div.CodeMirror-cursors {
  visibility: hidden;
  position: relative;
  z-index: 3;
}
div.CodeMirror-dragcursors {
  visibility: visible;
}
.CodeMirror-focused div.CodeMirror-cursors {
  visibility: visible;
}
.CodeMirror-selected {
  background: #d9d9d9;
}
.CodeMirror-focused .CodeMirror-selected {
  background: #d7d4f0;
}
.CodeMirror-crosshair {
  cursor: crosshair;
}
.CodeMirror-line::selection,
.CodeMirror-line > span::selection,
.CodeMirror-line > span > span::selection {
  background: #d7d4f0;
}
.CodeMirror-line::-moz-selection,
.CodeMirror-line > span::-moz-selection,
.CodeMirror-line > span > span::-moz-selection {
  background: #d7d4f0;
}
.cm-searching {
  background-color: #ffa;
  background-color: rgba(255, 255, 0, 0.4);
}
/* Used to force a border model for a node */
.cm-force-border {
  padding-right: 0.1px;
}
@media print {
  /* Hide the cursor when printing */
  .CodeMirror div.CodeMirror-cursors {
    visibility: hidden;
  }
}
/* See issue #2901 */
.cm-tab-wrap-hack:after {
  content: '';
}
/* Help users use markselection to safely style text background */
span.CodeMirror-selectedtext {
  background: none;
}

.cm-s-elegant span.cm-number,
.cm-s-elegant span.cm-string,
.cm-s-elegant span.cm-atom {
  color: #762;
}
.cm-s-elegant span.cm-comment {
  color: #262;
  font-style: italic;
  line-height: 1em;
}
.cm-s-elegant span.cm-meta {
  color: #555;
  font-style: italic;
  line-height: 1em;
}
.cm-s-elegant span.cm-variable {
  color: black;
}
.cm-s-elegant span.cm-variable-2 {
  color: #b11;
}
.cm-s-elegant span.cm-qualifier {
  color: #555;
}
.cm-s-elegant span.cm-keyword {
  color: #730;
}
.cm-s-elegant span.cm-builtin {
  color: #30a;
}
.cm-s-elegant span.cm-link {
  color: #762;
}
.cm-s-elegant span.cm-error {
  background-color: #fdd;
}
.cm-s-elegant .CodeMirror-activeline-background {
  background: #e8f2ff;
}
.cm-s-elegant .CodeMirror-matchingbracket {
  outline: 1px solid grey;
  color: black !important;
}

/**
    Name:       IDEA default theme
    From IntelliJ IDEA by JetBrains
 */
.cm-s-idea span.cm-meta {
  color: #808000;
}
.cm-s-idea span.cm-number {
  color: #0000FF;
}
.cm-s-idea span.cm-keyword {
  line-height: 1em;
  font-weight: bold;
  color: #000080;
}
.cm-s-idea span.cm-atom {
  font-weight: bold;
  color: #000080;
}
.cm-s-idea span.cm-def {
  color: #000000;
}
.cm-s-idea span.cm-variable {
  color: black;
}
.cm-s-idea span.cm-variable-2 {
  color: black;
}
.cm-s-idea span.cm-variable-3,
.cm-s-idea span.cm-type {
  color: black;
}
.cm-s-idea span.cm-property {
  color: black;
}
.cm-s-idea span.cm-operator {
  color: black;
}
.cm-s-idea span.cm-comment {
  color: #808080;
}
.cm-s-idea span.cm-string {
  color: #008000;
}
.cm-s-idea span.cm-string-2 {
  color: #008000;
}
.cm-s-idea span.cm-qualifier {
  color: #555;
}
.cm-s-idea span.cm-error {
  color: #FF0000;
}
.cm-s-idea span.cm-attribute {
  color: #0000FF;
}
.cm-s-idea span.cm-tag {
  color: #000080;
}
.cm-s-idea span.cm-link {
  color: #0000FF;
}
.cm-s-idea .CodeMirror-activeline-background {
  background: #FFFAE3;
}
.cm-s-idea span.cm-builtin {
  color: #30a;
}
.cm-s-idea span.cm-bracket {
  color: #cc7;
}
.cm-s-idea {
  font-family: Consolas, Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace, serif;
}
.cm-s-idea .CodeMirror-matchingbracket {
  outline: 1px solid grey;
  color: black !important;
}
.CodeMirror-hints.idea {
  font-family: Menlo, Monaco, Consolas, 'Courier New', monospace;
  color: #616569;
  background-color: #ebf3fd !important;
}
.CodeMirror-hints.idea .CodeMirror-hint-active {
  background-color: #a2b8c9 !important;
  color: #5c6065 !important;
}

.cm-s-neat span.cm-comment {
  color: #a86;
}
.cm-s-neat span.cm-keyword {
  line-height: 1em;
  font-weight: bold;
  color: #708;
}
.cm-s-neat span.cm-string {
  color: #a22;
}
.cm-s-neat span.cm-builtin {
  line-height: 1em;
  font-weight: bold;
  color: #077;
}
.cm-s-neat span.cm-special {
  line-height: 1em;
  font-weight: bold;
  color: #0aa;
}
.cm-s-neat span.cm-variable {
  color: black;
}
.cm-s-neat span.cm-number,
.cm-s-neat span.cm-atom {
  color: #3a3;
}
.cm-s-neat span.cm-meta {
  color: #555;
}
.cm-s-neat span.cm-link {
  color: #3a3;
}
.cm-s-neat span.cm-tag {
  color: #170;
}
.cm-s-neat span.cm-def {
  color: #00f;
}
.cm-s-neat span.cm-attribute {
  color: #00c;
}
.cm-s-neat .CodeMirror-activeline-background {
  background: #e8f2ff;
}
.cm-s-neat .CodeMirror-matchingbracket {
  outline: 1px solid grey;
  color: black !important;
}

.cm-s-twilight.CodeMirror {
  background: #141414;
  color: #f7f7f7;
}
/**/
.cm-s-twilight div.CodeMirror-selected {
  background: #323232;
}
/**/
.cm-s-twilight .CodeMirror-line::selection,
.cm-s-twilight .CodeMirror-line > span::selection,
.cm-s-twilight .CodeMirror-line > span > span::selection {
  background: rgba(50, 50, 50, 0.99);
}
.cm-s-twilight .CodeMirror-line::-moz-selection,
.cm-s-twilight .CodeMirror-line > span::-moz-selection,
.cm-s-twilight .CodeMirror-line > span > span::-moz-selection {
  background: rgba(50, 50, 50, 0.99);
}
.cm-s-twilight .CodeMirror-gutters {
  background: #222;
  border-right: 1px solid #aaa;
}
.cm-s-twilight .CodeMirror-guttermarker {
  color: white;
}
.cm-s-twilight .CodeMirror-guttermarker-subtle {
  color: #aaa;
}
.cm-s-twilight .CodeMirror-linenumber {
  color: #aaa;
}
.cm-s-twilight .CodeMirror-cursor {
  border-left: 1px solid white;
}
.cm-s-twilight .cm-keyword {
  color: #f9ee98;
}
/**/
.cm-s-twilight .cm-atom {
  color: #FC0;
}
.cm-s-twilight .cm-number {
  color: #ca7841;
}
/**/
.cm-s-twilight .cm-def {
  color: #8DA6CE;
}
.cm-s-twilight span.cm-variable-2,
.cm-s-twilight span.cm-tag {
  color: #607392;
}
/**/
.cm-s-twilight span.cm-variable-3,
.cm-s-twilight span.cm-def,
.cm-s-twilight span.cm-type {
  color: #607392;
}
/**/
.cm-s-twilight .cm-operator {
  color: #cda869;
}
/**/
.cm-s-twilight .cm-comment {
  color: #777;
  font-style: italic;
  font-weight: normal;
}
/**/
.cm-s-twilight .cm-string {
  color: #8f9d6a;
  font-style: italic;
}
/**/
.cm-s-twilight .cm-string-2 {
  color: #bd6b18;
}
/*?*/
.cm-s-twilight .cm-meta {
  background-color: #141414;
  color: #f7f7f7;
}
/*?*/
.cm-s-twilight .cm-builtin {
  color: #cda869;
}
/*?*/
.cm-s-twilight .cm-tag {
  color: #997643;
}
/**/
.cm-s-twilight .cm-attribute {
  color: #d6bb6d;
}
/*?*/
.cm-s-twilight .cm-header {
  color: #FF6400;
}
.cm-s-twilight .cm-hr {
  color: #AEAEAE;
}
.cm-s-twilight .cm-link {
  color: #ad9361;
  font-style: italic;
  text-decoration: none;
}
/**/
.cm-s-twilight .cm-error {
  border-bottom: 1px solid red;
}
.cm-s-twilight .CodeMirror-activeline-background {
  background: #27282E;
}
.cm-s-twilight .CodeMirror-matchingbracket {
  outline: 1px solid grey;
  color: white !important;
}

/* PrismJS 1.15.0
https://prismjs.com/download.html#themes=prism&languages=markup+css+clike+javascript+less+typescript */
/**
 * prism.js default theme for JavaScript, CSS and HTML
 * Based on dabblet (http://dabblet.com)
 * @author Lea Verou
 */
code[class*="language-"],
pre[class*="language-"] {
  color: black;
  background: none;
  text-shadow: 0 1px white;
  font-family: Consolas, Monaco, 'Andale Mono', 'Ubuntu Mono', monospace;
  text-align: left;
  white-space: pre;
  word-spacing: normal;
  word-break: normal;
  word-wrap: normal;
  line-height: 1.5;
  -moz-tab-size: 4;
  -o-tab-size: 4;
  tab-size: 4;
  -webkit-hyphens: none;
  -moz-hyphens: none;
  -ms-hyphens: none;
  hyphens: none;
}
pre[class*="language-"]::-moz-selection,
pre[class*="language-"] ::-moz-selection,
code[class*="language-"]::-moz-selection,
code[class*="language-"] ::-moz-selection {
  text-shadow: none;
  background: #b3d4fc;
}
pre[class*="language-"]::selection,
pre[class*="language-"] ::selection,
code[class*="language-"]::selection,
code[class*="language-"] ::selection {
  text-shadow: none;
  background: #b3d4fc;
}
@media print {
  code[class*="language-"],
  pre[class*="language-"] {
    text-shadow: none;
  }
}
/* Code blocks */
pre[class*="language-"] {
  padding: 1em;
  margin: 0.5em 0;
  overflow: auto;
}
:not(pre) > code[class*="language-"],
pre[class*="language-"] {
  background: #f5f2f0;
}
/* Inline code */
:not(pre) > code[class*="language-"] {
  padding: 0.1em;
  border-radius: 0.3em;
  white-space: normal;
}
.token.comment,
.token.prolog,
.token.doctype,
.token.cdata {
  color: slategray;
}
.token.punctuation {
  color: #999;
}
.namespace {
  opacity: 0.7;
}
.token.property,
.token.tag,
.token.boolean,
.token.number,
.token.constant,
.token.symbol,
.token.deleted {
  color: #905;
}
.token.selector,
.token.attr-name,
.token.string,
.token.char,
.token.builtin,
.token.inserted {
  color: #690;
}
.token.operator,
.token.entity,
.token.url,
.language-css .token.string,
.style .token.string {
  color: #9a6e3a;
  background: hsla(0, 0%, 100%, 0.5);
}
.token.atrule,
.token.attr-value,
.token.keyword {
  color: #07a;
}
.token.function,
.token.class-name {
  color: #DD4A68;
}
.token.regex,
.token.important,
.token.variable {
  color: #e90;
}
.token.important,
.token.bold {
  font-weight: bold;
}
.token.italic {
  font-style: italic;
}
.token.entity {
  cursor: help;
}

body {
  padding: 50px;
  font: 14px "Lucida Grande", Helvetica, Arial, sans-serif;
}
a {
  color: #00B7FF;
}
nav.navbar-fixed-top {
  position: sticky !important;
  border-bottom: 1px solid #e7e7e7;
}
.demo-window {
  -webkit-box-shadow: inset 0px 0px 4px 1px rgba(168, 168, 168, 0.48);
  -moz-box-shadow: inset 0px 0px 4px 1px rgba(168, 168, 168, 0.48);
  box-shadow: inset 0px 0px 4px 1px rgba(168, 168, 168, 0.48);
  padding: 10px;
  border-radius: 10px;
}
nav .container {
  display: flex;
  align-items: center;
}
a:hover::after {
  cursor: pointer;
}
.ml-content-blocks {
  visibility: hidden;
}
.mlboxcontainer {
  height: 100%;
}
.mlboxcontainer iframe {
  height: 100%;
  width: 100%;
}

.main-test-extension {
  width: 80%;
  margin-left: 10%;
}


.namespace-custom-button button {
  background: #1AAB8A;
  color: #fff;
  border: none;
  position: relative;
  height: 30px;
  font-size: 1.6em;
  padding: 0 2em;
  cursor: pointer;
  transition: 800ms ease all;
  outline: none;
}
.namespace-custom-button button:hover {
  background: #fff;
  color: #1AAB8A;
}
.namespace-custom-button button:before,
.namespace-custom-button button:after {
  content: '';
  position: absolute;
  top: 0;
  right: 0;
  height: 2px;
  width: 0;
  background: #1AAB8A;
  transition: 400ms ease all;
}
.namespace-custom-button button:after {
  right: inherit;
  top: inherit;
  left: 0;
  bottom: 0;
}
.namespace-custom-button button:hover:before,
.namespace-custom-button button:hover:after {
  width: 100%;
  transition: 800ms ease all;
}

.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-container input.ml-blueprint-numeric-input {
  padding-left: 23px;
  height: inherit;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-container input.ml-blueprint-numeric-input:disabled,
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn:disabled,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn:disabled {
  background: rgba(190, 190, 190, 0.5);
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-container input.ml-blueprint-numeric-input:disabled:hover {
  cursor: not-allowed !important;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-fill {
  width: 100%;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-large {
  font-size: 1.1rem;
  padding: 1em;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-large,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-large {
  font-size: 2.25rem;
  width: 45px;
  height: 45px;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn:disabled:hover,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn:disabled:hover {
  cursor: not-allowed !important;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-container {
  position: relative;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-container-large {
  height: 50px;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-flex-container {
  display: inline-flex;
  flex-direction: column;
  position: absolute;
  height: inherit;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-icon {
  position: absolute;
  top: 8px;
  left: 8px;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input.ml-blueprint-numeric-input-primary {
  border: 0.1rem solid #137CBD;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input.ml-blueprint-numeric-input-success {
  border: 0.1rem solid #0F9960;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input.ml-blueprint-numeric-input-warning {
  border: 0.1rem solid #D9822B;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input.ml-blueprint-numeric-input-danger {
  border: 0.1rem solid #DB3737;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn {
  color: #fff;
  border: none;
  padding: 0;
  width: 30px;
  flex: 1 1 14px;
  min-height: 0;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-primary {
  background: #137CBD;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-success {
  background: #0F9960;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-warning {
  background: #D9822B;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-danger {
  background: #DB3737;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-icon-primary {
  color: #137CBD;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-icon-success {
  color: #0F9960;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-icon-warning {
  color: #D9822B;
}
.namespace-blueprint-numeric-input .ml-blueprint-numeric-input-icon-danger {
  color: #DB3737;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-primary:hover,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-primary:hover {
  background: #0e5e8f;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-primary:active,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-primary:active {
  background: #0a3f60;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-primary:disabled,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-primary:disabled {
  background: rgba(19, 124, 189, 0.5);
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-success:hover,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-success:hover {
  background: #0a6b43;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-success:active,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-success:active {
  background: #063c26;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-success:disabled,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-success:disabled {
  background: rgba(15, 153, 96, 0.5);
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-warning:hover,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-warning:hover {
  background: #b16920;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-warning:active,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-warning:active {
  background: #864f18;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-warning:disabled,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-warning:disabled {
  background: rgba(217, 130, 43, 0.5);
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-danger:hover,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-danger:hover {
  background: #bd2222;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-danger:active,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-danger:active {
  background: #921a1a;
}
.namespace-blueprint-numeric-input .ml-blueprint-increment-btn.ml-blueprint-numeric-input-danger:disabled,
.namespace-blueprint-numeric-input .ml-blueprint-decrement-btn.ml-blueprint-numeric-input-danger:disabled {
  background: rgba(219, 55, 55, 0.5);
}

.namespace-blueprint-tagComp .blueprint-tagComp {
  padding: 7px 7px 4px 7px;
  color: #fff;
  border-radius: 4px;
  background-color: #5c7080;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-large {
  padding: 12px 10px 8px 10px;
  font-size: 15px;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal {
  background-color: rgba(138, 155, 168, 0.2);
  color: #182026;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-interactive:hover {
  background-color: rgba(92, 112, 128, 0.85);
  cursor: pointer;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-interactive-minimal:hover {
  background-color: rgba(92, 112, 128, 0.3);
  cursor: pointer;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-round {
  border-radius: 30px;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-primary,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-primary.ml-blueprint-tagComp-interactive:hover {
  background-color: #137CBD;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-success,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-success.ml-blueprint-tagComp-interactive:hover {
  background-color: #0F9960;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-warning,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-warning.ml-blueprint-tagComp-interactive:hover {
  background-color: #D9822B;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-danger,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-danger.ml-blueprint-tagComp-interactive:hover {
  background-color: #DB3737;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-primary,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-primary .ml-blueprint-left-icon,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-primary .ml-blueprint-right-icon {
  background-color: #deecf6;
  color: #106ba3;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-primary.ml-blueprint-tagComp-interactive-minimal:hover {
  background-color: rgba(19, 124, 189, 0.25);
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-primary .ml-blueprint-left-icon:hover,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-primary .ml-blueprint-right-icon:hover {
  color: #106ba3;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-primary .ml-blueprint-remove {
  color: #7badcd;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-primary .ml-blueprint-remove:hover {
  color: #106ba3;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-success,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-success .ml-blueprint-left-icon,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-success .ml-blueprint-right-icon {
  background-color: #def1e8;
  color: #0d7f51;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-success.ml-blueprint-tagComp-interactive-minimal:hover {
  background-color: rgba(15, 153, 96, 0.25);
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-success .ml-blueprint-left-icon:hover,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-success .ml-blueprint-right-icon:hover {
  color: #0d7f51;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-success .ml-blueprint-remove {
  color: #7bb99e;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-success .ml-blueprint-remove:hover {
  color: #0d7f51;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-warning,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-warning .ml-blueprint-left-icon,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-warning .ml-blueprint-right-icon {
  background-color: #f9ede1;
  color: #bf7325;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-warning.ml-blueprint-tagComp-interactive-minimal:hover {
  background-color: rgba(217, 130, 43, 0.25);
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-warning .ml-blueprint-left-icon:hover,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-warning .ml-blueprint-right-icon:hover {
  color: #bf7325;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-warning .ml-blueprint-remove {
  color: #e0b894;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-warning .ml-blueprint-remove:hover {
  color: #bf7325;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-danger,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-danger .ml-blueprint-left-icon,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-danger .ml-blueprint-right-icon {
  background-color: #fae2e3;
  color: #c23030;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-danger.ml-blueprint-tagComp-interactive-minimal:hover {
  background-color: rgba(219, 55, 55, 0.25);
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-danger .ml-blueprint-left-icon:hover,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-danger .ml-blueprint-right-icon:hover {
  color: #c23030;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-danger .ml-blueprint-remove {
  color: #de8a8d;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal.ml-blueprint-tagComp-danger .ml-blueprint-remove:hover {
  color: #c23030;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-primary.ml-blueprint-tagComp-interactive:hover,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-success.ml-blueprint-tagComp-interactive:hover,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-warning.ml-blueprint-tagComp-interactive:hover,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-danger.ml-blueprint-tagComp-interactive:hover {
  opacity: 0.9;
  cursor: pointer;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-primary .ml-blueprint-remove {
  color: #7badcd;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-success .ml-blueprint-remove {
  color: #7bb99e;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-warning .ml-blueprint-remove {
  color: #e0b894;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-danger .ml-blueprint-remove {
  color: #de8a8d;
}
.namespace-blueprint-tagComp .ml-blueprint-remove {
  font-weight: bold;
  color: #aab5be;
  position: relative;
  cursor: pointer;
  background: none;
  border: none;
  left: 4px;
  font-size: 13px;
  height: 16px;
  top: -5px;
  padding-left: 5px;
}
.namespace-blueprint-tagComp .ml-blueprint-remove:hover {
  color: #e6eaee;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal .ml-blueprint-remove {
  color: #83898c;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal .ml-blueprint-remove:hover {
  color: #182026;
}
.namespace-blueprint-tagComp .ml-blueprint-right-icon:hover,
.namespace-blueprint-tagComp .ml-blueprint-left-icon:hover {
  color: unset;
}
.namespace-blueprint-tagComp .ml-blueprint-right-icon {
  color: #fff;
  padding-left: 5px;
}
.namespace-blueprint-tagComp .ml-blueprint-left-icon {
  padding-right: 5px;
  color: #fff;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal .ml-blueprint-left-icon,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal .ml-blueprint-right-icon {
  color: #000;
}
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal .ml-blueprint-left-icon:hover,
.namespace-blueprint-tagComp .ml-blueprint-tagComp-minimal .ml-blueprint-right-icon:hover {
  color: #000;
}
