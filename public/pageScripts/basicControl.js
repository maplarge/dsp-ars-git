var scriptText = 
`
		ml.onload(function () {
			var dashboard = ml.ui.dashboard.controls.Textbox
				.builder().options({
				label: "Text Box",
				placeholder: "Text Goes Here",
			}).toJSON();

			ml.ui.dashboard.create(dashboard, '#ml-output')
		});
`