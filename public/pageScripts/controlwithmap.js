
var scriptText =
	`
			ml.onload(function () {

				var dashboard = ml.ui.dashboard.controls.Map.builder()
					.name("Map")
					.options({
						"layers": {}
					})
					.children([
						ml.ui.dashboard.controls.Panel.builder()
							.options({
								heading: "Hello World",
							}).layoutOptions({ position: "Right" })
							.children([
								ml.ui.dashboard.controls.Textbox.builder()
									.options({
										label: "Textbox",
										placeholder: "Type here"
									})
							])
					]).toJSON();

				ml.ui.dashboard.create(dashboard, '#ml-output');

			});
`;