var scriptText =
`
	ml.onload(function () {

			var _ml = ml.ui.dashboard.controls;

			var dashboard = _ml.Panel.builder()
				.options({
					showHeader: "false"
				}).children([
					_ml.Repeater.builder()
						.options({
							data: [1, 2, 3, 4, 5]

						}).children([
							_ml.Flexbox.builder()
								.options({
									justifyContent: "Space Between",
								}).children([
								_ml.Html.builder()
									.options({
										html: "row {{item}}"
										}),
								_ml.SingleCheckbox.builder()
									.options({
										label: "",
										initialValue: { $scopePath: (scope) => scope.item() == 3 }
									})
								])
						]),
				]).toJSON();

			ml.ui.dashboard.create(dashboard, '#ml-output')

	});

`