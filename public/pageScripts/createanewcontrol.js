var scriptText = '';

$.ajax({
	url: "/ts/examples/controls/genericControl.ts",
	method: "GET",
	dataType: "text",
}).done((data) => {
	var count = 50;
	var i = setInterval(() => {
		if (typeof (insertTemplate) != "undefined") {
			insertTemplate(data);
			clearInterval(i);
		}
		if (count-- <= 0) {
			clearInterval(i);
		}
	}, 500);
});