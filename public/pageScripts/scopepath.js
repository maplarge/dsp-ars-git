
var scriptText =
	`
		ml.onload(function () {

			var _ml = ml.ui.dashboard.controls;

			var dashboard = _ml.ScopeDefaults.builder().options({
				defaults: [
					{path: "text_value", value: "" }
				]
			}).children([
				_ml.Flexbox.builder()
					.options({
						justify: "Space Between"
					}).children([

						_ml.Textbox.builder().name('text_value')
							.options({
								updateImmediately: true,
								label: "Text Box",
								placeholder: "Type here"
							}),

						_ml.Flexbox.builder().options({
							orientation: "Vertical"
						}).children([

						_ml.Html.builder()
							.options({
								html: "<h3>Output</h3>"
							}),

							_ml.Html.builder()
								.options({
									html: "<p>{{text_value}}</p>"
							}),
						])
					])
				]).toJSON();

			ml.ui.dashboard.create(dashboard, '#ml-output');

		});
`;