﻿namespace statics {
	export enum operators {
		greater = "Greater", //results are greaterThan
		greaterOr = "GreaterOR", //resulters are greater than OR equal to
		between = "Between", //results are between (formatted as "floor/ceiling")
		nBetween = "NotBetween", //results are not between (formatted as "floor/ceiling")
		less = "Less", //results are less than
		lessOR = "LessOR", //results are less than or equal to
		eq = "Equal", //results are equal to
		neq = "EqualNot", //results are NOT equal to
		eqAny = "EqualAny", //results are equal to any in a list of values (formated as "val1,val2,val3...")
		eqNone = "EqualNone", //results are NOT equal to any in a list of values (formated as "val1,val2,val3...")
		contains = "Contains", //results contain part or all of string value
		startsW = "StartsWith", //results start with value of string,

	}

}