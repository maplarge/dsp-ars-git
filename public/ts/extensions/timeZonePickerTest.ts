namespace dashboard.extensions.timeZonePicker {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	// slider is under construction

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.toast',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing new Custom Controls'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([

				bp.BPTimeZonePicker.builder().options({
					standartDisplay: bp.BPTimeZonePicker.TimeZoneStandart.Offset,
					showLocalTimeZone:true,
					disabled:false
				}),

			]).toJSON();
	}
}