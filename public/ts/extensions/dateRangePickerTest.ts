namespace dashboard.extensions.date {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	// slider is under construction

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.date',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing new Custom Controls'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([

				bp.BPDateRangePicker.builder().options({
					// min_date: new Date(1999, 10 - 1, 25),
					// year_range: "1998:2018",
					// showActionsBar: true,
					// canClearSelection: true,
					// reverseMonthAndYearMenus: false,
					// // defaultValue: new Date('01.01.2012'),
					// Precision: bp.BPBase.Precision.Minute					

				}),

				// bp.BPTooltip.builder().options({
				// 	indicator: true,
				// }),

				// bp.BPControlGroup.builder().options({
				// 	btnData: [
				// 		{left: ctrl.BPBase.ControlGroup.Button, buttonText: 'Filter', icon: 'filter', placeholder: 'Find filters...'},
				// 		{left: ctrl.BPBase.ControlGroup.Select,inputIcon: 'x16Search', inputValue: 'from:ggray to:allorca', selectOption: ['Filter', 'Name - ascending', 'Name - descending', 'Price - ascending', 'Price - descending']},
				// 		{right: ctrl.BPBase.ControlGroup.Button, buttonText: 'Add', inputIcon: 'user', placeholder: 'Find collaborators...', intent: ctrl.BPBase.Intent.Success, selectOption: ['Filter', 'Name - ascending', 'Name - descending', 'Price - ascending', 'Price - descending']},
				// 	],
				// }),

				// bp.BPPanelStack.builder().options({
				// 	intent: ctrl.BPBase.Intent.Success,
				// 	panelNumber: 1,
				// })
				
				// bp.BPDialog.builder().options({
				// 	icon: 'info28',
				// 	heading: 'Palantir Foundry',
				// 	canOutsideClickClose: true,
				// 	button1Text: 'Close',
				// 	button2Text: 'Visit the Foundry website',
				// 	canEscapeKeyClose: true,
				// 	autoFocus: true,
				// 	text: `
				// 	<strong>Data integration is the seminal problem of the digital age. For over ten years, we’ve helped the world’s premier organizations rise to the challenge.</strong>
				// 	<br>
				// 	Palantir Foundry radically reimagines the way enterprises interact with data by amplifying and extending the power of data integration. With Foundry, anyone can source, fuse, and transform data into any shape they desire. Business analysts become data engineers — and leaders in their organization’s data revolution.
				// 	<br>
				// 	Foundry’s back end includes a suite of best-in-class data integration capabilities: data provenance, git-style versioning semantics, granular access controls, branching, transformation authoring, and more. But these powers are not limited to the back-end IT shop.
				// 	<br>
				// 	In Foundry, tables, applications, reports, presentations, and spreadsheets operate as data integrations in their own right. Access controls, transformation logic, and data quality flow from original data source to intermediate analysis to presentation in real time. Every end product created in Foundry becomes a new data source that other users can build upon. And the enterprise data foundation goes where the business drives it.
				// 	<br>
				// 	Start the revolution. Unleash the power of data integration with Palantir Foundry.
				// 	`
				// }),

			]).toJSON();
	}
}