namespace dashboard.extensions.popover {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.popover',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing new Custom Controls'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([
                // bp.BPPopover.builder().options({
				// 	disabled: false,
				// 	openOnTargetFocus: false,
				// 	canEscapeKeyClose: true,
				// 	intent: bp.BPBase.Intent.Primary,
				// 	position: bp.BPPopover.Position.BOTTOM,
				// 	label: 'Popover target',
				// 	preventOverflow: true,
				// 	preventOverflowType: bp.BPPopover.PreventOverflowTypes.WINDOW,
				// 	arrow: true,
				// 	interactionKind:bp.BPPopover.PopoverInteractionKind.CLICK,
				// 	flip: true
				// }).children([
				// 	bp.BPSlider.builder().options({
				// 		labelPercentages: [10,20,30,40,50,60,70,80,90],
				// 		min: 0,
				// 		max: 10,
				// 		value: 3,
				// 		showTrackFill: true
				// 	})
				// ]),
				bp.BPTooltip.builder().options({
					intent: bp.BPBase.Intent.Danger,
				}).children([
					bp.BPSlider.builder().options({
						labelPercentages: [10,20,30,40,50,60,70,80,90],
						min: 0,
						max: 10,
						showTrackFill: true
					})
				])
			]).toJSON();
	}
}