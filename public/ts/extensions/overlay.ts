namespace dashboard.extensions.overlay {
    import mdash = ml.ui.dashboard;
    import bp = dashboard.components.controls;
    import BPBase = dashboard.components.controls.BPBase;



    //mdash.registerPublicDashboard({
    //    id: 'dashboard.extensions.overlay',
    //    name: 'Main Custom DB test',
    //    description: 'Main page for testing new Custom Controls'
    //});

    export function getJSON() {

        return mdash.controls.ScopeDefaults.builder()
            .options({
                defaults: [
                ]
            })
            .children([
                bp.BPOverlay.builder().options({
                   headerText:"I'm an Overlay!!!!!!!!!!!",
                  esc_key : true,
                  // backdrop:true,
                 //    modal_body_text_two:"Lorem Ipsum is simply dummy text " +
                 //   "of the printing and typesetting industry. Lorem Ipsum " +
                 //   "has been the industry's standard dummy text ever since " +
                 //   "the 1500s, when an unknown printer took a galley of type" +
                 //   " and scrambled it to make a type specimen book. It has ",
                   modal_body_text_one:"survived not only five centuries, but also" +
                   " the leap into electronic typesetting, remaining essentially unchanged." +
                   " It was popularised in the 1960s with the release of Letraset sheets " +
                   "containing Lorem Ipsum passages, and more recently with desktop publishing " +
                   "software like Aldus PageMaker including versions of Lorem Ipsum.",
                   transitionDuration:100

                })
            ]).toJSON();
    }
}