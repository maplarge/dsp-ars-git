namespace dashboard.extensions.input {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	// slider is under construction

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.input',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing new Custom Controls'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([
				// bp.BPInputGroup.builder().options({
				// 	large: true,
				// 	vertical: true,
				// 	fill: true,
					// disabled: true,
					// readonly: true,
				// 	btnData: [
				// 		{type: 'password', placeholder: 'password', iconRight: 'lock'},
				// 		{type: 'text', placeholder: 'text', intent: bp.BPBase.Intent.Success, iconRight: 'arrow-right', iconLeft: 'search',},
				// 		{type: 'search', placeholder: 'search', intent: bp.BPBase.Intent.Danger, iconLeft: 'search', round: true},
				// 		{type: 'text', placeholder: 'filter histogram...', iconLeft: 'filter', value: true, spin: true}
				// 	]
				// }),

				bp.BPDatePicker.builder().options({
					min_date: new Date(1999, 10 - 1, 25),
					year_range: "1998:2018",
					showActionsBar: true,
					canClearSelection: true,
					reverseMonthAndYearMenus: false,
					// defaultValue: new Date('01.01.2012'),
					Precision: bp.BPBase.Precision.Minute					
				}),
				bp.BPNavBar.builder().options({
					btnData: [
						{label: 'Home', icon: 'square234', divider: true},
						{label: 'Files', icon: 'MapLarge-icons_file', divider: false},
						{label: 'Files', icon: 'MapLarge-icons_file', divider: false},
						{label: 'Files', icon: 'MapLarge-icons_file', divider: false},
						{label: 'Files', icon: 'MapLarge-icons_file', divider: false},
						{label: 'Files', icon: 'MapLarge-icons_file', divider: false},
						{icon: 'user', divider: true, },
						{icon: 'marker', divider: false},
						{icon: 'gear1', divider: false},						
					],
					align: ctrl.BPBase.Alignment.Around,
					fixedToTop: false,
					dark: true,
					input: true
				}),
				
				// bp.BPTimePicker.builder().options({
				// 	Precision: bp.BPBase.Precision.Milisecond,
				// 	showArrowButtons: true,
				// 	// selectAllOnFocus: true,
				// 	// useAmPm: true,
				// 	// defaultValue: new Date('December 17, 1995 0:11:11:111'),
				// 	minTime: 5,
				// 	maxTime: 10
				// 	// disabled: true,
				// }),

				// bp.BPHTMLSelect.builder().options({
				// 	// fill: true,
				// 	// large: true,
				// 	// minimal: true,
				// 	options: [
				// 		{label: 'Option 1', value: '1'},
				// 		{label: 'Option 2', value: '2'},
				// 		{label: 'Option 3', value: '3'},
				// 		{label: 'Option 4', value: '4'}			
				// 	],
				// 	// onChange: (value: string) => {
				// 	// 	console.log(value);
				// 	// }					
				// }),

				// bp.BPFormGroup.builder().options({
				// 	// inline: true,
				// 	// disabled: false,
				// 	// helper: {show: true, text: 'Helper text with details...'},
				// 	// showLabel: {show: true, text: 'Label'},
				// 	// showLabelInfo: {show: true, text: ' (required)'},
				// 	// switchText: {show: true, text: 'Engage the hyperdrive'}
				// }),
 
				// bp.BPTextInput.builder().options({
				// 	// disabled: true,
				// 	// fill: true,
				// 	// round: true,
				// 	// large: true,
				// 	intent: bp.BPBase.Intent.Danger,
				// 	// readonly: true,
				// 	iconRight: 'lock',
				// 	iconLeft: 'search',
				// 	placeholder: 'Search',
				// 	type: 'text',
				// }),
				// bp.BPTagInput.builder().options({
				// 	icon: 'user',
				// 	intent: bp.BPBase.Intent.Danger,
				// 	// minimal: true,
				// 	// disabled: true,
				// 	placeholder: 'placeholder',
				// 	large: true,
				// 	// fill: true,
				// 	onRemove: function(){
				// 		console.log('remove')
				// 	},
				// 	addOnBlur: function(){
				// 		console.log('blur')
				// 	},
				// 	onAdd: [
				// 		{value: 'Option 1',	onRemove: function(){console.log('remove')}},
				// 		{value: 'Option 1',	onRemove: function(){console.log('asfasf')}},
				// 	],
				// }),
				// bp.BPTextArea.builder().options({
				// 	// disabled: true,
				// 	// fill: true,
				// 	large: true,
				// 	intent: bp.BPBase.Intent.Danger,
				// 	// readonly: true,				
				// }),
				// bp.BPTag.builder().options({
				// 	label: 'London',
				// 	large: true,
				// 	interactive: true,
				// 	minimal: true,
				// 	iconRight: 'envelope',
				// 	iconLeft: 'cloud',
				// 	intent: bp.BPBase.Intent.Success,
				// 	// round: true,
				// 	onRemove: function(){
				// 		console.log('remove')
				// 	},
				// }),
			]).toJSON();
	}
}