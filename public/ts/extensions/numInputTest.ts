namespace dashboard.extensions.num {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.num',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing new Custom Controls'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([
				bp.BPNumericInput.builder().options({
                    majorStepSize: 10,
                    minorStepSize: 0.1,
					stepSize: 1,
					min: -1000,
					max: 500000,
					numericOnly: true,
					clampValueOnBlur: true,
					selectAllOnIncrement: true,
					intent: bp.BPBase.Intent.Success,
					position: bp.BPNumericInput.Position.Right,
					leftIcon: "money"
				})
			]).toJSON();
	}
}