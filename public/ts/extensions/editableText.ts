namespace dashboard.extensions.editable {


    import mdash = ml.ui.dashboard;
    import bp = dashboard.components.controls;
    import BPBase = dashboard.components.controls.BPBase;



    //mdash.registerPublicDashboard({

    //    id: 'dashboard.extensions.editable',
    //    name: 'Main Custom DB test',
    //    description: 'Main page for testing new Custom Controls'

    //});

    export function getJSON() {

        return mdash.controls.ScopeDefaults.builder()
            .options({
                defaults: [
                ]
            })
            .children([
                bp.BPEditableText.builder().options({
                    placeholder_input:"Edit text...",
                    intent:BPBase.Intent.Danger,
                    selected_value:false,
                    breakNewMultiLine:false,
                }),

                // bp.BPOverlay.builder().options({
                //     click_outsitte:false,
                //     headerText:"I'm an Overlay!!!!!!!!!!!",
                //     esc_key : true,
                //     // backdrop:true,
                //     //    modal_body_text_two:"Lorem Ipsum is simply dummy text " +
                //     //   "of the printing and typesetting industry. Lorem Ipsum " +
                //     //   "has been the industry's standard dummy text ever since " +
                //     //   "the 1500s, when an unknown printer took a galley of type" +
                //     //   " and scrambled it to make a type specimen book. It has ",
                //     modal_body_text_one:"survived not only five centuries, but also" +
                //     " the leap into electronic typesetting, remaining essentially unchanged." +
                //     " It was popularised in the 1960s with the release of Letraset sheets " +
                //     "containing Lorem Ipsum passages, and more recently with desktop publishing " +
                //     "software like Aldus PageMaker including versions of Lorem Ipsum.",
                //     transitionDuration:100
                // }),

                // bp.BPDivider.builder().options({
                //
                //     button_data: [
                //         {label: 'Delete',divider:true},
                //         {label: 'Create',divider:false},
                //         {label: 'Edit',divider:true  },
                //         {label: 'File',divider:false  },
                //     ],
                //     vertical:true
                // })

                // bp.BPOverflow.builder().options({
                //     position: ctrl.BPBase.Position.End,
                //     icon:"mlicon-moreoptions",
                //     width:60,
                //     link_data: [
                //         {label: 'image.jpg'},
                //         {label: 'Wednesday'},
                //         {label: 'Photos'},
                //         {label: 'Janet'},
                //         {label: 'Users'},
                //         {label: 'All files'},
                //     ],

                // })


            ]).toJSON();
    }
}