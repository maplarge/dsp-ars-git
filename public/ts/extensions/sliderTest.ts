namespace dashboard.extensions.slider {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	// slider is under construction

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.slider',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing Slider'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([
				mdash.controls.Flexbox.builder().options({
					justify: "Center"
				}).children([

					// bp.BPSlider.builder().options({
					// 	showTrackFill: true,
					// 	vertical: true,
					// 	value: 71,
					// 	min: -50,
					// 	max: 150,
					// 	step: 1,
					// 	labelPercentages: [12,24,36,48,60,72,84,96]
					// }),
				]),
				bp.BPMultiSlider.builder().options({
					handles: [
						{ value: 0, intentAfter: bp.BPBase.Intent.Primary },
						{ value: 100 },
					],
					step: 1,
					min: 0,
					max: 250,
					handleInteraction: bp.BPMultiSlider.HandleInteraction.Lock,
					labelPercentages: [10,20,30,40,50,60,70,80,90]
				}),

			]).toJSON();
	}
}