/// <reference path="../../_references.ts"/>
/// <reference path="../../_statics.ts"/>


namespace dashboard.extensions.tigran_newDashboard{

    import BPBase = dashboard.components.controls.BPBase;
    mdash.registerPublicDashboard({
        id: 'dashboard.extensions.tigran_newDashboard',
        name: 'textControlsTest',
        description: 'A way to test the text based controls'
    });

    var title = "Text Controls";
    var i;
    export function getJSON() {

        registerFormatFunctions();
        return mdash.controls.ScopeDefaults.builder()
            .cssClass("mixed-controls")
            .options({
                defaults:  [
                    {path: "interactive",value:true},
                    {path: "fillColor",value:'red'},
                    {path: "size",value:1},
                    {path: "zoomData", value: [
                            {label:"-",divider:false, onClick:({$rootScope})=>{
                                    i=$rootScope.Map.zoom(),
                                    i--,
                                    $rootScope.Map.zoom(i);
                                }},
                                {label:"+",divider:false, onClick:({$rootScope})=>{
                                    i=$rootScope.Map.zoom(),
                                        i++,
                                        $rootScope.Map.zoom(i);
                                }},

                        ]},

                ]
            })
            .transforms([

            ])
            .children([
                mdash.controls.DataStore.builder()
                    .options({
                        "sources": [

                            {
                                "name": "hotels",
                                "label": "Hotels",
                                "dataSource": "hms/hotels"
                            },
                            {
                                "name": "hotels2",
                                "label": "Hotels",
                                "dataSource": "hms/hotels"
                            },
                        ],
                        "hidden": true
                    })

                    .children([
                        mdash.controls.LayerStore.builder()
                            .name("LayerStore")
                            .options({

                                "hidden": true,
                                "layers": [
                                    <mdash.controls.LayerStore.ILayerOptionsDefinition>{
                                        id: "hotels",

                                        dataSource: {
                                            "$scopePath": "hotels"
                                        },
                                        baseLayerOpts: {
                                            "name": "hotels",
                                            visible: "{{displayDataOnMap}}",
                                            style: {
                                                method: "rules",
                                                rules: [
                                                    {
                                                        style: {
                                                            fillColor: {$scopePath: scope => scope.fillColor()},
                                                            size: {$scopePath: scope => scope.size()},
                                                        },
                                                        where: "CatchAll"
                                                    }
                                                ]
                                            },
                                            onHover: "template",
                                            hoverFieldsCommaDel: "CountryName,HotelName,HotelID,PricingRating,Address,StarRating,LATITUDE,LONGITUDE",
                                            hoverTemplate: {
                                                 $scopePath: scope => {
                                                    return `<div style="width:200px">
                                                                <dl>
																	<dt>Hotel Name</dt>	<dd>{HotelName}</dd>
																	<dt>Address</dt>	<dd>{Address}</dd>
																	<dt>Price Ratinmg</dt>	<dd>{PricingRating}</dd>
													 			</dl>
                                                              
                                                             <div>`;
                                                }
                                            }
                                        }
                                    },

                                ]
                            })
                            .cssClass("ml-dash-spacing-none")
                            .children([
                                mdash.controls.Map.builder()
                                    .name("Map")
                                    .options({
                                        "layerSources": {
                                            "$scopePath": "LayerStore.allLayers",

                                        },
                                        autoZoomExtents: true,
                                        "layers": {}
                                    })
                                    .children([
                                        mdash.controls.FloatingPanel.builder().options({
                                            heading: title,
                                            size: "Fixed"

                                        }).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{position: 'Right'})
                                            .children([
                                                mdash.controls.StackPanel.builder().children([
                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Dots color</h3>"
                                                    }),
                                                    ctrl.BPButtonGroup.builder()

                                                        .options({
                                                            btnData: [
                                                                { label: "Success", intent: ctrl.BPBase.Intent.Success,
                                                                    onClick({ $rootScope }){
                                                                        $rootScope.fillColor("#0F9960")
                                                                    }
                                                                },
                                                                { label: "Warning", intent: ctrl.BPBase.Intent.Warning,
                                                                    onClick({ $rootScope }){
                                                                        $rootScope.fillColor("#D9822B")
                                                                    }
                                                                },
                                                                { label: "Danger", intent: ctrl.BPBase.Intent.Danger,
                                                                    onClick({ $rootScope }){
                                                                        $rootScope.fillColor("#DB3737")
                                                                    }
                                                                },
                                                                { label: "Primary", intent: ctrl.BPBase.Intent.Primary,
                                                                    onClick({ $rootScope }){
                                                                        $rootScope.fillColor("#137CBD")
                                                                    }
                                                                },
                                                            ]

                                                        }),
                                                    ///////////////////////////////////
                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Number Input</h3>"
                                                    }),
                                                    ctrl.BPNumericInput.builder()
                                                        .name("size")
                                                        .options({
                                                            min: 1,
                                                            max: 25,
                                                            clampValueOnBlur: true,
                                                            numericOnly: true,
                                                            minorStepSize: 0.1,
                                                            majorStepSize: 10,
                                                            stepSize: 1,
                                                            leftIcon:"information",
                                                            intent: ctrl.BPBase.Intent.Warning,
                                                            position: ctrl.BPNumericInput.Position.Right
                                                            // disabled: {$scopePath: (scope) => !scope.progress_rangeDisable()}
                                                        }),

                                                    ///////////////////////////////////
                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Divider</h3>"
                                                    }),
                                                    ctrl.BPDivider.builder()
                                                        .options({
                                                            button_data: {$scopePath: "zoomData"},

                                                        }),


                                                ])
                                            ])
                                    ])
                            ])
                    ])
            ]).toJSON();
    }

    function registerFormatFunctions() {


    }


}