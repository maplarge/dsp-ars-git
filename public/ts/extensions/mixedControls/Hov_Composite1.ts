/// <reference path="../../_references.ts"/>

namespace dashboard.extensions.HM_Composite1 {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.HM_Composite1',
		name: 'Main Custom DB test for All dashboards',
		description: 'Switch,Radio,Button,Slider,RangeSlider'
	});

	var title = "Inventorys";
	let latitude: number;
	let longitude: number;

	function getLatLng() {
		return {
			latitude: latitude,
			longitude: longitude
		};
	}

	navigator.geolocation.getCurrentPosition(position => {
		latitude = position.coords.latitude; 
		longitude = position.coords.longitude;
	});

	export function getJSON() {

		registerFormatFunctions();

		return mdash.controls.ScopeDefaults.builder()
			.cssClass("mixed-controls")
			.options({
				defaults: [
					{ path: "latitude", value: latitude },
					{ path: "longitude", value: longitude },
					{ path: "size", value: 10 },
					{ path: "shape", value: 'round' },
					{ path: "rating", value: [0,6] },
					{ path: "displayDataOnMap", value: true },
				]
			}).transforms([
			
			])
			.children([
				mdash.controls.DataStore.builder()
					.options({
						"sources": [
							{
								"name": "hotels",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							{
								"name": "hotels2",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							
						],
						"hidden": true
					})
					.transforms([
						mdash.transforms.RunQuery.builder().options({
							source: "hotels.dataSource",
							dest: "HotelCountryListPretty",
							transposeMethod: "TableTranspose",
							take: 1000
						})
					])
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({

								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},
										baseLayerOpts: {
											"name": "hotels",
											visible: "{{displayDataOnMap}}",
											style: {
												method: "rules",
												rules: [
													{
														style: {
															fillColor: "#000000",
															size: { $scopePath: scope => scope.size() },
															shape: { $scopePath: scope => scope.shape() }, 
														},
														where: [[
															{col:"StarRating",test:"GreaterOR",value:0},
															{col:"StarRating",test:"Less",value:1},
														]]
													},
													{
														style: {
															fillColor: "#303300",
															size: { $scopePath: scope => scope.size() },											
															shape: { $scopePath: scope => scope.shape() }, 
														},
														where: [[
															{col:"StarRating",test:"GreaterOR",value:1},
															{col:"StarRating",test:"Less",value:2}
														]]
													},
													{
														style: {
															fillColor: "#606601",
															size: { $scopePath: scope => scope.size() },
															shape: { $scopePath: scope => scope.shape() }, 
														},
														where: [[
															{col:"StarRating",test:"GreaterOR",value:2},
															{col:"StarRating",test:"Less",value:3}
														]]
													},
													{
														style: {
															fillColor: "#909901",
															size: { $scopePath: scope => scope.size() },
															shape: { $scopePath: scope => scope.shape() }, 
														},
														where: [[
															{col:"StarRating",test:"GreaterOR",value:3},
															{col:"StarRating",test:"Less",value:4}
														]]
													},
													{
														style: {
															fillColor: "#c0cc02",
															size: { $scopePath: scope => scope.size() },
															shape: { $scopePath: scope => scope.shape() }, 
														},
														where: [[
															{col:"StarRating",test:"GreaterOR",value:4},
															{col:"StarRating",test:"Less",value:5}
														]]
													},
													{
														style: {
															fillColor: "#f0ff03",
															size: { $scopePath: scope => scope.size() },
															shape: { $scopePath: scope => scope.shape() }, 
														},
														where: [[
															{col:"StarRating",test:"GreaterOR",value:5},
															{col:"StarRating",test:"Less",value:6}
														]]
													},
													{
														style: {
															fillColor: "#ff0000",
															size: { $scopePath: scope => scope.size() },
															shape: { $scopePath: scope => scope.shape() }, 
														},
														where: [[
															{col:"StarRating",test:"GreaterOR",value:6},
															{col:"StarRating",test:"Less",value:7}
														]]
													},
												]
											},
											onHover: "template",
											hoverFieldsCommaDel: "CountryName,CityName,HotelName,HotelID,Address,StarRating,LATITUDE,LONGITUDE",
											hoverTemplate: {
												$scopePath: scope => {
													return `<div style="width:250px; padding:10px;border-radius:5px;">
																<dl>
																	${"CountryName,CityName,HotelName,HotelID,Address,StarRating,LATITUDE,LONGITUDE".split(',').map(c => {
																	return `<dt style="float:left; clear:both;padding-bottom:3px;">${c}:</dt>	<dd style="float:right">{${c}}</dd >`;
																	}).join('')}
																</dl>
															</div>`;
													
												}
											}
										}
									},
									
								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
								.name("Map")
								.options({
									"layerSources": {
										"$scopePath": "LayerStore.allLayers",
										
									},
									autoZoomExtents: true,
									"layers": {} ,
									mapOpts: {
										lat: latitude,
										lng: longitude,
										z: 13
									}
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: "Mixing External, Internal Controls with Transform",
											size:"Fixed"
											
										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "right" })
										.children([
											mdash.controls.StackPanel.builder().children([

												mdash.controls.Html.builder().options({
													html: "<h3>Show Hotels</h3>"
												}),
												ctrl.BPSwitch.builder().name('displayDataOnMap')
													.options({
														label: "Show Hotels"
													}),

												mdash.controls.Html.builder().options({
													html: "<h3>Choose Shape Style</h3>"
												}),
												ctrl.BPRadioGroup.builder().name('shape')
													.options({
														align: ctrl.BPBase.Alignment.Right,
														radioData: [
															{label: "Shape Round", checked:true, value: 'round'},
															{label: "Shape Rectangle", value: 'rectangle'}
														]
													}),
												
												mdash.controls.Html.builder().options({
													html: "<h3>Star Raitings Filtration</h3>"
												}),

												ctrl.BPRangeSlider.builder()
												.options({
													min:0,
													max: 6,
													step:1,
												}).name("rating")
												.transforms([
													mdash.transforms.Filter.builder().options({
														source: "rating:toBetween()",
														column: "StarRating",
														dest: "hotels",
														test: statics.operators.between.toString(),
													})
												]),

												mdash.controls.Html.builder().options({
													html: "<h3>Dot Size (External Control)</h3>"
												}),

												ctrl.BPSlider.builder()
													.options({
														min: 5,
														max: 15,
														step: 1,
														showTrackFill: true,
													})
													.name("size"),
									
												mdash.controls.Html.builder().options({
													html: "<h3>Zoom Map 15%</h3>"
												}),

												ctrl.BPButton.builder()
												.options({
													label: 'Zoom',
													onClick: (scope) => scope.Map.zoom(15)
												}).name("zoom"),
											
												mdash.controls.Html.builder().options({
													html: "<h3>GPS</h3>"
												}),

												ctrl.BPButton.builder()
													.options({
														label: 'GPS',
														onClick: (scope) => {
															scope.Map.center({ lat: getLatLng().latitude, lng: getLatLng().longitude });
															scope.Map.zoom(8);
														}
														}).name("longlat"),	
																						

											])
										])
									])
							])
					])
			]).toJSON()
	}
	
	function registerFormatFunctions() {

		ml.ui.dashboard.registerFormatFunction("toBetween",
		(data: any) => data.join('/'),
		ml.ui.dashboard.ScopeDataType.STRING, ml.ui.dashboard.ScopeDataType.STRING);
		
		// ml.ui.dashboard.registerFormatFunction("first",
		// (data: any[]) => data && data[0] ? data[0] : null,
		// ml.ui.dashboard.ScopeDataType.STRING, ml.ui.dashboard.ScopeDataType.STRING);
		
		// ml.ui.dashboard.registerFormatFunction("last",
		// (data: any[]) =>  data && data[data.length - 1] ? data[data.length - 1] : null,
		// ml.ui.dashboard.ScopeDataType.STRING, ml.ui.dashboard.ScopeDataType.STRING);

	}
			
}