/// <reference path="../../_references.ts"/>
/// <reference path="../../_statics.ts"/>

namespace dashboard.extensions.composites1 {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.composites1',
		name: 'CompositeControls1',
		description: 'Tesitng ColumnValuePicker',
	});

	var title = "Text Controls";

	export function getJSON() {

		registerFormatFunctions();


		return mdash.controls.ScopeDefaults.builder()
			.cssClass("mixed-controls")
			.options({
				defaults: [
					{ path: "name_starts_with", value: "Days In" },
					{ path: "countries", value: [] }
				]
			})
			.transforms([
				ml.ui.dashboard.transforms.RunQuery.builder().options({
					dest: "countries",
					source: "HotelCountries.dataSource",
					transposeMethod: "TableTranspose",
					take:1000
				})
				])
			.children([
				mdash.controls.DataStore.builder()
					.options({
						"sources": [
							{
								"name": "hotels",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							{
								"name": "hotels2",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							{
								name: "HotelCountries",
								label: "Hotel Countries",
								dataSource: {
									"withgeo": true,
									"start": 0,
									"take": 100,
									"table": {
										"name": "hms/hotels"
									},
									"sqlselect": [
										"CountryCode as value",
										"CountryFileName as label"
									],
									"groupby": [
										"CurrencyCode"
									]
								}
							}
						],
						"hidden": true
					})
					.transforms([
						mdash.transforms.RunQuery.builder().options({
							source: "hotels.dataSource",
							dest: "HotelCountryListPretty",
							transposeMethod: "TableTranspose",
							take: 1000
						})
					])
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({

								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},
										baseLayerOpts: {
											"name": "hotels",
											//visible: "{{displayDataOnMap}}",
											style: {
												method: "rules",
											},
											onHover: "template",
											hoverFieldsCommaDel: "HotelName,HotelID,Address,StarRating",
											hoverTemplate: {
												$scopePath: scope => {
													return `<div style="width:200px;">
																<dl>
																	<dt>Hotel Name</dt>	<dd>{HotelName}</dd>
																	<dt>Address</dt>	<dd>{Address}</dd>
																	<dt>Star Rating</dt><dd>{StarRating}</dd>
																</dl>
															<div>`;
												}
											}
										}
									},

								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
									.name("Map")
									.options({
										"layerSources": {
											"$scopePath": "LayerStore.allLayers",
										},
										autoZoomExtents: true,
										"layers": {}
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: title,
											size: "Fixed"

										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "Right" })
											.children([
												mdash.controls.StackPanel.builder().children([

													mdash.controls.Html.builder().options({
														html: "<h3>Text input that filters hotels with names that start with value</h3>"
													}),

													ctrl.BPOmnibar.builder().options({
														items: { $scopePath: "countries" },
														isShow: true
													}),

													ctrl.ColumnPicker2.builder().options({
														datasource: { $scopePath: "hotels2.dataSource" },
														closeOnSelect: true,
														
													}).name('selected_column'),

													ctrl.CVP.builder().name('cvp').options({
														datasource: { $scopePath: "hotels2.dataSource" },
														column: "{{selected_column}}",
														includeCount: false,
														selectMode: ctrl.CVP.SelectMode.AutoComplete

													}).visible({ $scopePath: "selected_column:isNotNullUndefinedOrEmpty"})
													.transforms([
														mdash.transforms.Filter.builder().options({
															source: "cvp:exludeSearch()",
															column: "{{selected_column}}",
															dest: "hotels",
															test: "Equal",
															
														})
													])
												])
											])
									])
							])
					])
			]).toJSON();
	}

	function registerFormatFunctions() {
		ml.ui.dashboard.registerFormatFunction("exludeSearch",
			(value: any) => value == "Search..." ? "" : value,
			ml.ui.dashboard.ScopeDataType.STRING, ml.ui.dashboard.ScopeDataType.STRING);
	}
}
