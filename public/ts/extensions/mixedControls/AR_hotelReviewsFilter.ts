/// <reference path="../../_references.ts"/>
/// <reference path="../../_statics.ts"/>

namespace dashboard.extensions.AR_hotelReviews {
	
	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.AR_hotelReviews',
		name: 'Hotel Reviews Filter',
		description: 'Multi Slider,Checkbox,Dialog,Button,ButtonGroup,Slider'
	});
	
	var title = "Hotel Reviews Filter";

	function setTrackIntent(intent, scope){
		let handles = [...scope.handles()];
		handles[0].intentAfter = intent;
		scope.handles(handles);
	}

	export function getJSON() {

		registerFormatFunctions();

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
					{ path: "reviews_range", value: [0, 1800] },
					{ path: "handles", value: [
						{ value: 0, intentAfter: ctrl.BPBase.Intent.Success },
						{ value: 1800 },
					] },
					{ path: "hotels_arr", value: [] },
					{ path: "avg_star", value: 0 },
					{ path: "dialog_btn", value: false }
				]
			}).transforms([
				mdash.transforms.RunQuery.builder().options({
					source: "hotels.dataSource",
					dest: "hotels_arr",
					transposeMethod: "TableTranspose",
					take: 7777,
				}),
			])
			.children([
				mdash.controls.DataStore.builder()
					.options({
						"sources": [
                            {
								"name": "hotels",
								"label": "Hotels", 
								"dataSource": "hms/hotels"
							},
							{
								"name": "hotels2",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
						],
						"hidden": true
					}).children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({

								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},
										baseLayerOpts: {
											"name": "hotels",
											style: {
												method: "rules",
												rules: [
													{
														style: {
															fillColor: "darkblue",
														},
														where: [[
															{ col: "NumberOfReviews", test: statics.operators.eq.toString(), value: 0 },
														]]
													},
													{
														style: {
															fillColor: "royalblue",
														},
														where: [[
															{ col: "NumberOfReviews", test: statics.operators.greater.toString(), value: 0 },
															{ col: "NumberOfReviews", test: statics.operators.less.toString(), value: 100 },
														]]
													},
													{
														style: {
															fillColor: "skyblue",
														},
														where: [[
															{ col: "NumberOfReviews", test: statics.operators.greaterOr.toString(), value: 100 },
															{ col: "NumberOfReviews", test: statics.operators.less.toString(), value: 250 },
														]]
													},
													{
														style: {
															fillColor: "cyan",
														},
														where: [[
															{ col: "NumberOfReviews", test: statics.operators.greaterOr.toString(), value: 250 },
															{ col: "NumberOfReviews", test: statics.operators.less.toString(), value: 500 },
														]]
													},
													{
														style: {
															fillColor: "lime",
														},
														where: "CatchAll"
													},
													
												]
											},
											onHover: "template",
											hoverFieldsCommaDel: "HotelName,NumberOfReviews",
											hoverTemplate: `<div style="width:200px; box-shadow: 0 0 15px #000; height: 100%; padding: 10px;">
																<p>Hotel Name: <strong>{HotelName}</strong></p>
                                                                <p style="margin-bottom: 0;">Number of reviews: <strong>{NumberOfReviews}</strong></p>
															<div>`
										}
									}
								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
									.name("Map")
									.options({
										"layerSources": {
											"$scopePath": "LayerStore.allLayers",
										},
										autoZoomExtents: true,
										"layers": {} 
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: title,
											size:"Fixed"

										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "Right" })
											.children([
												mdash.controls.StackPanel.builder().children([
													ctrl.BPCheckbox.builder().name('toggle').options({
														label: "Toggle Multi Slider/Hotel Info Dialog",
														align: ctrl.BPBase.Alignment.Right
													}).visible({ $scopePath: scope => scope.hotels.dataSource() }),

													mdash.controls.Html.builder().options({
														html: "<h3>Reviews Slider filter</h3>"
													}).visible({ $scopePath: scope => !scope.toggle() && scope.hotels.dataSource() }),
													
													ctrl.BPMultiSlider.builder().name('reviews_range').options({
														min: 0,
														max: 1800,
														handles: { $scopePath: "handles" },
														labelPercentages: [25,50,75],
														handleInteraction: ctrl.BPMultiSlider.HandleInteraction.Push,
													}).visible({ $scopePath: scope => !scope.toggle() && scope.hotels.dataSource() })
													.transforms([
														mdash.transforms.Filter.builder().options({
															source: "reviews_range:toBetween",
															dest: 'hotels',
															column: "NumberOfReviews",
															test: statics.operators.between.toString()
														}),

														mdash.transforms.Set.builder().options({
															source: "reviews_range",
															dest: "dialog_btn",
															value: false,
															setOnInit: true,
														})
													]),

													ctrl.BPSpinner.builder().options({
														circleSize: 20
													}).visible({ $scopePath: scope => !scope.hotels.dataSource() }),

													ctrl.BPSpinner.builder().options({
														circleSize: 20
													}).visible({ $scopePath: scope => !scope.hotels.dataSource() }),

													ctrl.BPButtonGroup.builder().options({
														btnData: [
															{ 
																label: "Set MultiSlider track intent to Primary", 
																intent: ctrl.BPBase.Intent.Primary,
																loading: { $scopePath: scope => !scope.toggle() && !scope.hotels.dataSource() },
																disabled: { $scopePath: scope => scope.handles()[0].intentAfter == ctrl.BPBase.Intent.Primary },
																onClick: ({ $rootScope }) => setTrackIntent(ctrl.BPBase.Intent.Primary, $rootScope)
															},
															{ 
																label: "Set MultiSlider track intent to Success", 
																intent: ctrl.BPBase.Intent.Success,
																loading: { $scopePath: scope => !scope.toggle() && !scope.hotels.dataSource() }, 
																disabled: { $scopePath: scope =>  scope.handles()[0].intentAfter == ctrl.BPBase.Intent.Success },
																onClick: ({ $rootScope }) => setTrackIntent(ctrl.BPBase.Intent.Success, $rootScope)
															},
															{ 
																label: "Set MultiSlider track intent to Danger", 
																intent: ctrl.BPBase.Intent.Danger,
																loading: { $scopePath: scope => !scope.toggle() && !scope.hotels.dataSource() },
																disabled: { $scopePath: scope => scope.handles()[0].intentAfter == ctrl.BPBase.Intent.Danger },
																onClick: ({ $rootScope }) => setTrackIntent(ctrl.BPBase.Intent.Danger, $rootScope)
															},
															{ 
																label: "Set MultiSlider track intent to Warning", 
																intent: ctrl.BPBase.Intent.Warning,
																loading: { $scopePath: scope => !scope.toggle() && !scope.hotels.dataSource() }, 
																disabled: { $scopePath: scope => scope.handles()[0].intentAfter == ctrl.BPBase.Intent.Warning }, 
																onClick: ({ $rootScope }) => setTrackIntent(ctrl.BPBase.Intent.Warning, $rootScope)
															},
															{ 
																label: "Set MultiSlider track intent to Default", 
																intent: ctrl.BPBase.Intent.Default,
																loading: { $scopePath: scope => !scope.toggle() && !scope.hotels.dataSource() }, 
																disabled: { $scopePath: scope => scope.handles()[0].intentAfter == ctrl.BPBase.Intent.Default },
																onClick: ({ $rootScope }) => setTrackIntent(ctrl.BPBase.Intent.Default, $rootScope)
															}
														]
													}).visible({ $scopePath: scope => !scope.toggle() }),

													ctrl.BPButton.builder().options({
														intent: ctrl.BPBase.Intent.Success,
														label: "Calculate Average Star Rating of hotels",
														onClick: scope => {
															const hotels = [...scope.hotels_arr()];
															let starRatings: number = 0;

															hotels.forEach(hotel => starRatings += hotel["StarRating"]);
															scope.avg_star((+starRatings / hotels.length).toFixed(2));
															scope.dialog_btn(true);
														}
													}).visible({ $scopePath: scope => scope.toggle() && scope.hotels.dataSource() }),

													ctrl.BPDialog.builder().options({
														buttonText: "Display calculation info",
														heading: "Calculation info",
														canEscapeKeyClose: true,
														canOutsideClickClose: true,
														closeButton: 'Close',
														icon: "information3",
														text: { $scopePath: scope => {
															return `<p>Average star rating of hotels with ${scope.reviews_range()[0]}-${scope.reviews_range()[1]} reviews: ${+scope.avg_star()}</p>`;
														} }
													}).visible({ $scopePath: scope => scope.toggle() && scope.hotels.dataSource() && scope.dialog_btn() })

												])
											])
									])
							])
					])
			]).toJSON();
	}

	function registerFormatFunctions() {
		ml.ui.dashboard.registerFormatFunction("toBetween", (data: any) => data.join('/'),
			ml.ui.dashboard.ScopeDataType.STRING, ml.ui.dashboard.ScopeDataType.STRING);
	}

}