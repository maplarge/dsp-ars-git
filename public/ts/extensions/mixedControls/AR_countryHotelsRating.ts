/// <reference path="../../_references.ts"/>
/// <reference path="../../_statics.ts"/>

namespace dashboard.extensions.AR_countryHotelsRating {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.AR_countryHotelsRating',
		name: 'Country Hotels Rating',
		description: 'Select with country list,Radio Group,Button,Tabs components'
	});

	var title = "Country Hotels Rating";

	enum Ratings{
		HotelRating = "HotelRating",
		CleanlinessRating = "CleanlinessRating",
		RoomsRating = "RoomsRating",
		FacilitiesRating = "FacilitiesRating",
		StarRating = "StarRating",
		LocationRating = "LocationRating",
		PricingRating = "PricingRating",
		ServiceRating = "ServiceRating"
	}

	function calculateHotelRating(data, selected_country){
		let pointsGathered: number = 0;
		
		data.forEach(hotel => {
			let ratings: number[] = [];
			({ 
				CleanlinessRating: ratings[0],
				RoomsRating: ratings[1],
				FacilitiesRating: ratings[2],
				LocationRating: ratings[3],
				PricingRating: ratings[4],
				ServiceRating: ratings[5],
				StarRating: ratings[6]
			} = hotel);
			const sum: number = ratings.reduce((a,b) => a + b);
			pointsGathered += sum;
		});

		return [{
			name: `Rating of Hotels in ${selected_country}`,
			body: {
				title: `Rating of ${data.length} hotels`,
				text: `${+(pointsGathered / data.length).toFixed(2)}/36`
			}
		}];
	}

	function calculateAverageRating(data, selected_country, rating_type){
		let pointsGathered: number = 0;

		data.forEach(hotel => pointsGathered += hotel[rating_type]);

		return [{
			name: `Average ${rating_type.split('Rating').join(' Rating')} of Hotels in ${selected_country}`,
			body: {
				title: `Average ${rating_type.split('Rating').join(' Rating')} of ${data.length} hotels`,
				text: `${+(pointsGathered / data.length).toFixed(2)}/${rating_type == 'StarRating' ? '6' : '5'}`
			}
		}];
	}

	export function getJSON() {
		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
					{ path: "hotels_arr", value: [] },
					{ path: "hotels2_arr", value: [] },
					{ path: "countries_arr", value: [] },
					{ path: "selected_country", value: '' },
					{ path: "hotel_rating", value: 0 },
					{ path: "rating_calculated", value: false },
					{ path: "tabs_tabList", value: [] },
					{ path: "rating_choice", value: Ratings.HotelRating }
				]
			}).transforms([
				mdash.transforms.RunQuery.builder().options({
					source: "hotels.dataSource",
					dest: "hotels_arr",
					transposeMethod: "TableTranspose",
					take: 7777
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "hotels2.dataSource",
					dest: "hotels2_arr",
					transposeMethod: "TableTranspose",
					take: 7777
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "countries_ds.dataSource",
					dest: "countries_arr",
					transposeMethod: "TableTranspose",
					take: 250
				})
			])
			.children([
				mdash.controls.DataStore.builder()
					.options({
						"sources": [
                            {
								name: "hotels",
								label: "Hotels",
								dataSource: {
									"withgeo": true,
									"start": 0,
									"take": 7777,
									"table": {
										"name": "hms/hotels"
									},
									"sqlselect": [
										"CountryName",
										"HotelName",
										"CleanlinessRating",
										"FacilitiesRating",
										"LocationRating",
										"PricingRating",
										"RoomsRating",
										"ServiceRating",
										"StarRating"
									],
								}
							},
							{
								name: "hotels2",
								label: "Hotels",
								dataSource: {
									"table": {
										"name": "hms/hotels"
									},
									"sqlselect": [
										"CountryName",
										"HotelName",
										"CleanlinessRating",
										"FacilitiesRating",
										"LocationRating",
										"PricingRating",
										"RoomsRating",
										"ServiceRating",
										"StarRating"
									],
								}
							},
							{
								name: "countries_ds",
								label: "Hotel Countries",
								dataSource: {
									withgeo: true,
									start: 0,
									take: 100,
									table: {
										name: "hms/hotels"
									},
									sqlselect: [
										"CountryCode",
										"CountryName",
									],
									orderby: ["CountryName.asc"],
									groupby: [
										"CountryCode"
									]
								}
							}
						],
						"hidden": true
					})
					.transforms([
						
					])
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({

								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},
										baseLayerOpts: {
											"name": "hotels",
											style: {
												method: "rules",
												rules: 
												[{
													style: {
														fillColor: "cyan",
														size: 15,
													},

													where: [[
														{ col: "CountryName", test: statics.operators.eq.toString(), value: "{{selected_country}}" },
													]]
												},
												{
													style: {
														color: "#FF0000"
													},
													where: "CatchAll"
												}]
											},
										}
									}
								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
									.name("Map")
									.options({
										"layerSources": {
											"$scopePath": "LayerStore.allLayers",
										},
										autoZoomExtents: true,
										"layers": {} 
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: title,
											size:"Fixed"

										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "Right" })
											.children([
												mdash.controls.StackPanel.builder().children([
													mdash.controls.Html.builder().options({
                                                        html: "<h3>Country dropdown list</h3>"
													}),
													
													ctrl.BPSelect.builder().name('selected_country').options({
														intent: ctrl.BPBase.Intent.Primary,
														items: { $scopePath: "countries_arr" },
														labelField: "CountryName",
														valueField: "CountryCode",
														filterable: true
													}).transforms([
														mdash.transforms.Filter.builder().options({
															source: "selected_country",
															dest: "hotels2",
															column: "CountryName",
															test: statics.operators.eq.toString()
														}),

														mdash.transforms.Set.builder().options({
															source: "selected_country",
															dest: "rating_calculated",
															value: false,
															setOnInit: true
														}),

														mdash.transforms.Set.builder().options({
															source: "selected_country",
															dest: "rating_choice",
															value: Ratings.HotelRating,
															setOnInit: true
														})
													]).visible( { $scopePath: scope => scope.countries_ds.dataSource() } ),

													mdash.controls.Html.builder().options({
                                                        html: "<h3>Calculate Ratings</h3>"
													}).visible( { $scopePath: scope => scope.hotels2.dataSource() && scope.selected_country() } ),

													ctrl.BPRadioGroup.builder().name('rating_choice').options({
														align: ctrl.BPBase.Alignment.Right,
														radioData: [
															{ label: "Total Hotel Rating", value: Ratings.HotelRating },
															{ label: "Average Star Rating", value: Ratings.StarRating },
															{ label: "Average Rooms Rating", value: Ratings.RoomsRating },
															{ label: "Average Facilities Rating", value: Ratings.FacilitiesRating },
															{ label: "Average Pricing Rating", value: Ratings.PricingRating },
															{ label: "Average Cleanliness Rating", value: Ratings.CleanlinessRating },
															{ label: "Average Location Rating", value: Ratings.LocationRating },
															{ label: "Average Service Rating", value: Ratings.ServiceRating },
														]
													}).visible( { $scopePath: scope => scope.hotels2.dataSource() && scope.selected_country() } ),

													ctrl.BPButton.builder().options({
														label: "Calculate it for {{selected_country}}",
														intent: ctrl.BPBase.Intent.Danger,
														onClick: scope => {
															const data: any[] = [...scope.hotels2_arr()];
															const selected_country: string = scope.selected_country();
															const choice: string = scope.rating_choice();
															let tabData: any[] = choice == Ratings.HotelRating ? 
																				calculateHotelRating(data,selected_country) : 
																				calculateAverageRating(data,selected_country,choice);

															scope.tabs_tabList(tabData);
															scope.rating_calculated(true);
														}
													}).visible( { $scopePath: scope => scope.hotels2.dataSource() && scope.selected_country() } ),

													mdash.controls.Html.builder().options({
														html: "<h3>Hotel rating for {{selected_country}}</h3>"
													}).visible( { $scopePath: scope => scope.rating_calculated() && scope.hotels2.dataSource() } ),

													ctrl.BPTabs.builder().options({
														animate: true,
														tabList: { $scopePath: "tabs_tabList" },
														activePanelOnly: true,
														searchInput: false
													}).visible( { $scopePath: scope => scope.rating_calculated() && scope.hotels2.dataSource() && scope.tabs_tabList() } )
												])
											])
									])
							])
					])
			]).toJSON();
	}

}