/// <reference path="../../_references.ts"/>
/// <reference path="../../_statics.ts"/>


namespace dashboard.extensions.tigran_testControls {

    import BPBase = dashboard.components.controls.BPBase;
    mdash.registerPublicDashboard({
        id: 'dashboard.extensions.tigran_testControls',
        name: 'textControlsTest',
        description: 'A way to test the text based controls'
    });

    var title = "Text Controls";
    let latitude: number;
    let longitude: number;

    function getLatLng() {
        return {
            latitude: latitude,
            longitude: longitude
        };
    }

    navigator.geolocation.getCurrentPosition(position => {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
    });

    export function getJSON() {

        registerFormatFunctions();
        return mdash.controls.ScopeDefaults.builder()
            .cssClass("mixed-controls")
            .options({
                defaults:  [
                    {path: "displayDataOnMap", value: true},
                    {path: "fillColor", value: "red"},
                    {path: "size", value: 10},
                    {path: "hover_text", value: ''},
                    {path: "hover_template_texts", value: ['Row 1']},
                    {path: "hover_footer_text", value: ""},
                    {path: "country", value: ''},
                    {path: "password", value: ''},
                    {path: "notes", value: ''},
                    {path: "editable", value: ['', '']},
                    {path: "suggest", value: ""},
                    {path: "dataList", value: []},
                    {path: "hotels_by_name", value: []},
                    {path: "hotels_in_country", value: null},
                    {path: "selected_hotel", value: null},
                    {path: "plusZoom", value: "+" },
                    {path: "minusZoom", value: "-" },
                    {path: "startCount", value: 0},
                    {path: "priceRaiting", value: [2, 4]},
                    {path: "headerText", value: "Help"},
                    {path: "overlayText", value: "Help me"},
                    {path:"CloseButton",value:"Close"},
                    {path:"SubmitButton",value:"Submit"},
                    { path: "selectedCity", value:'Yerevan'},
                    { path: "selectedCountry", value:'ArmEniA'},
                    { path: "cityStartCount", value: 0 },
                    { path: "countries", value: [] },
                    { path: "cities", value: [] },
                    { path: "hotels_by_name", value: [] },
                    { path: "hotels_in_country", value: null },
                    { path: "cities_in_country", value: null },
                    { path: "selected_hotel", value: null },
                     {path: "backdrop", value: false},
                ]
            })
            .transforms([
                mdash.transforms.RunQuery.builder().options({
                    source: "countries_ds.dataSource",
                    dest: "countries",
                    transposeMethod: "TableTranspose",
                    take: 1000,
                }),
                mdash.transforms.RunQuery.builder().options({
                    source: "cities_by_name_ds.dataSource",
                    dest: "cities",
                    transposeMethod: "TableTranspose",
                    take: 1000,
                }),
                mdash.transforms.RunQuery.builder().options({
                    source: "hotels_by_name_ds.dataSource",
                    dest: "hotels_by_name",
                    transposeMethod: "TableTranspose",
                    take: 1000,
                }),
                mdash.transforms.RunQuery.builder().options({
                    source: "selected_hotel_ds.dataSource",
                    dest: "selected_hotel",
                    transposeMethod: "TableTranspose",
                    take: 1,
                })
            ])
            .children([
                mdash.controls.DataStore.builder()
                    .options({
                        "sources": [

                            {
                                "name": "hotels",
                                "label": "Hotels",
                                "dataSource": "hms/hotels"
                            },
                            {
                                "name": "hotels2",
                                "label": "Hotels",
                                "dataSource": "hms/hotels"
                            },
                            {
                                name: "hotels_by_name_ds",
                                dataSource: {
                                    withgeo: true,
                                    start: <any>"{{startCount}}",
                                    take: 1000,
                                    table: "{{hotels:asSubQuery()}}",
                                    sqlselect: [
                                        "HotelID as value",
                                        "HotelName as label",
                                        "CityName",
                                        "CityID",
                                        "CityFileName",
                                        "CountryName",
                                        "CountryCode",
                                        "LATITUDE as lat",
                                        "LONGITUDE as lng",
                                        "Address",
                                        "StarRating",
                                        "ChainID",
                                    ],
                                    orderby: ["HotelName.asc"]
                                }
                            },
                            {
                                name: 'cities_by_name_ds',
                                dataSource: {
                                    withgeo: true,
                                    start: <any>"{{cityStartCount}}",
                                    take: 1000,
                                    table: "{{hotels2:asSubQuery()}}",
                                    sqlselect: [
                                        "CountryName",
                                        "CityID as value",
                                        "CityName as label",
                                        "CityFileName",
                                    ],
                                    orderby: ["CityName.asc"],
                                    groupby: [
                                        'CityFileName'
                                    ]
                                }
                            },
                            {
                                name: "selected_hotel_ds",
                                dataSource: {
                                    withgeo: true,
                                    start: 0,
                                    take: 1,
                                    table: {
                                        name: "hms/hotels"
                                    },
                                }
                            },
                            {
                                name: "countries_ds",
                                label: "Hotel Countries",
                                dataSource: {
                                    withgeo: true,
                                    start: 0,
                                    take: 100,
                                    table: {
                                        name: "hms/hotels"
                                    },
                                    sqlselect: [
                                        "CountryCode as value",
                                        "CountryName as label",
                                    ],
                                    orderby: ["CountryName.asc"],
                                    groupby: [
                                        "CountryCode"
                                    ]
                                }
                            }                        ],
                        "hidden": true
                    })

                    .children([
                        mdash.controls.LayerStore.builder()
                            .name("LayerStore")
                            .options({

                                "hidden": true,
                                "layers": [
                                    <mdash.controls.LayerStore.ILayerOptionsDefinition>{
                                        id: "hotels",

                                        dataSource: {
                                            "$scopePath": "hotels"
                                        },
                                        baseLayerOpts: {
                                            "name": "hotels",
                                            visible: "{{displayDataOnMap}}",
                                            style: {
                                                method: "rules",
                                                rules: [
                                                {
                                                    style: {
                                                        fillColor: "blue",
                                                    },
                                                    where: [[
                                                        { col: "HotelID", test: statics.operators.eq.toString(), value: "{{selected_hotel.0.HotelID}}" }
                                                    ]]
                                                },
                                                    {

                                               style: {
                                                        size: {$scopePath: scope => scope.size()},
                                                        fillColor: {$scopePath: scope => scope.fillColor()},
                                                    },
                                                    where: "CatchAll"
                                                }
                                                ]
                                            },
                                            onHover: "template",
                                            hoverFieldsCommaDel: "CountryName,HotelName,HotelID,PricingRating,Address,StarRating,LATITUDE,LONGITUDE",
                                            hoverTemplate: {
                                                $scopePath: scope => {


                                                    let rows = scope.hover_template_texts();
                                                    let template = '';
                                                    rows.forEach(element => {
                                                        template += `<li>${element}</li>`;
                                                    });


                                                    return `<div style="width:200px">
                                                                <dl>
																	<dt>Hotel Name</dt>	<dd>{HotelName}</dd>
																	<dt>Address</dt>	<dd>{Address}</dd>
																	<dt>Price Ratinmg</dt>	<dd>{PricingRating}</dd>
													 			</dl>
                                                                <h2><b>${scope.editable()[0]}</b></h2>
                                                                <p>${scope.editable()[1]}</p>
                                                             <div>`;
                                                }
                                            }
                                        }
                                    },

                                ]
                            })
                            .cssClass("ml-dash-spacing-none")
                            .children([
                                mdash.controls.Map.builder()
                                    .name("Map")
                                    .options({
                                        "layerSources": {
                                            "$scopePath": "LayerStore.allLayers",
                                        },
                                        autoZoomExtents: true,
                                        "layers": {}
                                    })
                                    .children([
                                        mdash.controls.FloatingPanel.builder().options({
                                            heading: title,
                                            size: "Fixed"

                                        }).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{position: 'Right'})
                                            .children([
                                                mdash.controls.StackPanel.builder().children([
                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Switch (External Control)</h3>"
                                                    }),
                                                    ctrl.BPSwitch.builder().name('displayDataOnMap')
                                                        .options({
                                                            label: "Display Hotels"
                                                        }),
                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Editable text</h3>"
                                                    }),
                                                    ctrl.BPEditableText.builder()
                                                        .options({}).name("editable"),

                                                    ctrl.BPRadioGroup.builder()
                                                        .options({
                                                            radioData: [
                                                                {value: "5", label: "5"},
                                                                {value: "10", label: "10"},
                                                                {value: "15", label: "15"},
                                                                {value: "20", label: "20"},
                                                            ]
                                                        })
                                                        .name('size'),
                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Dot Size (External Control)</h3>"
                                                    }),
                                                    ctrl.BPRangeSlider.builder().name('priceRaiting').options({
                                                        min: 0,
                                                        max: 6,
                                                        step: 1,

                                                    })
                                                        .transforms([
                                                            mdash.transforms.Filter.builder().options({
                                                                source: "priceRaiting:toBetween()",
                                                                column: "PricingRating",
                                                                dest: "hotels",
                                                                test: statics.operators.between.toString()
                                                            }),

                                                        ]),


                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Fill Color (External Control)</h3>"
                                                    }),

                                                    ctrl.BPSelect.builder().name('fillColor').options({
                                                        items: [
                                                            {label: "Red", value: '#ff0000'},
                                                            {label: "Green", value: '#00ff00'},
                                                            {label: "Blue", value: '#0000ff'},
                                                            {label: "Orange", value: '#C87A2D'},
                                                        ]
                                                    }),

                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Zoom Button</h3>"
                                                    }),

                                                    ctrl.BPButton.builder()
                                                        .options({
                                                            label:"+",
                                                            onClick:(scope)=>{
                                                                scope.Map.zoom(10)
                                                            }
                                                        }).name("plusZoom")
                                                        .transforms([
                                                            mdash.transforms.Filter.builder().options({
                                                                source: "plusZoom",
                                                                column: "HotelName",
                                                                dest: "hotels",
                                                                test :statics.operators.startsW.toString()
                                                            })
                                                        ]),
                                                    ctrl.BPButton.builder()
                                                        .options({
                                                            label:"-",
                                                            onClick:(scope)=>{
                                                                scope.Map.zoom(-20)
                                                            }
                                                        }).name("minusZoom")
                                                        .transforms([
                                                            mdash.transforms.Filter.builder().options({
                                                                source: "minusZoom",
                                                                column: "HotelName",
                                                                dest: "hotels",
                                                                test: statics.operators.startsW.toString()
                                                            })
                                                        ]),
                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Select</h3>"
                                                    }),


                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Select Countries</h3>"
                                                    }).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.countries()) }),
                                                    ctrl.BPSuggest.builder().name('selectedCountry').options({
                                                        items: { $scopePath: "countries" },
                                                        placeholder:"Search...",
                                                        minimal:true,
                                                        intent: ctrl.BPBase.Intent.Warning
                                                    }).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.countries()) })
                                                        .transforms([
                                                            mdash.transforms.Filter.builder().options({
                                                                source: "selectedCountry",
                                                                column: "CountryName",
                                                                dest: "hotels",
                                                                test: "Equal"
                                                            }),

                                                            mdash.transforms.Filter.builder().options({
                                                                source: "selectedCountry",
                                                                column: "CountryName",
                                                                dest: "hotels2",
                                                                test: "Equal"
                                                            }),

                                                            mdash.transforms.Set.builder().options({
                                                                source: "selectedCountry",
                                                                dest: "startCount",
                                                                value: 0,
                                                                setOnInit: true
                                                            }),

                                                            mdash.transforms.Set.builder().options({
                                                                source: "selectedCountry",
                                                                dest: "selectedCity",
                                                                value: "",
                                                                setOnInit: true
                                                            }),

                                                            mdash.transforms.Set.builder().options({
                                                                source: "selectedCountry",
                                                                dest: "startCount",
                                                                value: 0,
                                                                setOnInit: true
                                                            }),

                                                            mdash.transforms.BuildObject.builder().options({
                                                                dest: "hotels_in_country",
                                                                template: {
                                                                    length: "{{hotels_by_name_ds:count}}"
                                                                },
                                                            }),
                                                        ]),

                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Select City</h3>"
                                                    }).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) }),

                                                    ctrl.BPSuggest.builder().name('selectedCity').options({
                                                        items: { $scopePath: "cities" },
                                                        placeholder:'Search...',
                                                        minimal:true,
                                                        intent: ctrl.BPBase.Intent.Warning
                                                    }).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) })
                                                        .transforms([
                                                            mdash.transforms.Filter.builder().options({
                                                                source: "selectedCity",
                                                                column: "CityName",
                                                                dest: "hotels_by_name_ds",
                                                                test: "Equal"
                                                            }),

                                                            mdash.transforms.Filter.builder().options({
                                                                source: "selectedCity",
                                                                column: "CityName",
                                                                dest: "hotels",
                                                                test: "Equal"
                                                            }),

                                                            mdash.transforms.Set.builder().options({
                                                                source: "selectedCity",
                                                                dest: "startCount",
                                                                value: 0,
                                                                setOnInit: true
                                                            }),

                                                            mdash.transforms.Set.builder().options({
                                                                source: "selectedCity",
                                                                dest: "selectedHotelList",
                                                                value: '',
                                                                setOnInit: true
                                                            }),

                                                            mdash.transforms.BuildObject.builder().options({
                                                                dest: "cities_in_country",
                                                                template: {
                                                                    length: "{{cities_by_name_ds:count}}"
                                                                },
                                                            }),
                                                        ]),

                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Select Hotel</h3>"
                                                    })
                                                        .visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) && ml.util.isNotNullUndefinedOrEmpty(scope.selectedCity())}),

                                                    ctrl.BPSuggest.builder().options({
                                                        items: { $scopePath: "hotels_by_name" },
                                                        placeholder:"Search...",
                                                        minimal:true,
                                                        intent: ctrl.BPBase.Intent.Warning,
                                                    })
                                                        .name('selectedHotelList')
                                                        .visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) && ml.util.isNotNullUndefinedOrEmpty(scope.selectedCity())
                                                        })
                                                        .transforms([
                                                            mdash.transforms.Filter.builder().options({
                                                                source: "selectedHotelList",
                                                                dest: "selected_hotel_ds",
                                                                test: statics.operators.eq.toString(),
                                                                column: "HotelName",
                                                            }),

                                                            mdash.transforms.BuildObject.builder().options({
                                                                dest: "Map.center",
                                                                template: {
                                                                    lat: {
                                                                        $scopePath: scope => scope.selected_hotel() && ml.util.isNotNullUndefinedOrEmpty(scope.selectedHotelList())
                                                                            ? scope.selected_hotel()[0].LATITUDE : getLatLng().latitude
                                                                    },
                                                                    lng: {
                                                                        $scopePath: scope => scope.selected_hotel() && ml.util.isNotNullUndefinedOrEmpty(scope.selectedHotelList())
                                                                            ? scope.selected_hotel()[0].LONGITUDE : getLatLng().longitude
                                                                    }
                                                                }
                                                            }),
                                                            mdash.transforms.Set.builder().options({
                                                                source: "selectedHotelList",
                                                                dest: "Map.zoom",
                                                                value: 18,
                                                                setOnInit: true
                                                            }),
                                                        ]),
                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Overlay</h3>"
                                                    }),

                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3> Number Input-</h3>"
                                                    }),

                                                ])
                                            ])
                                    ])
                            ])
                    ])
            ]).toJSON();
    }

    function registerFormatFunctions() {

        ml.ui.dashboard.registerFormatFunction("toBetween",
            (data: any) => data.join('/'),
            ml.ui.dashboard.ScopeDataType.STRING, ml.ui.dashboard.ScopeDataType.STRING);

    }


}