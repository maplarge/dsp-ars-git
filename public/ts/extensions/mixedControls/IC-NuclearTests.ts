/// <reference path="../../_references.ts"/>
/// <reference path="../../_statics.ts"/>

namespace dashboard.extensions.nuclear_tests {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.nuclear_tests',
		name: 'Nuclear Tests',
		description: 'Showing all nuclear tests',
	});

	var title = "Text Controls";

	export function getJSON() {

		registerFormatFunctions();

		return mdash.controls.ScopeDefaults.builder()
			.cssClass("mixed-controls")
			.options({
				defaults: [
					{ path: "name_starts_with", value: "Days In" },
					{ path: "size", value: 8 }
				]
			})
			.children([
				mdash.controls.DataStore.builder()
					.options({
						"sources": [
							{
								name: "tests",
								label: "Nuclear Tests",
								dataSource: {
									table: {
										name: "test/nuclear_tests"
									}
								}
							},

						],
						"hidden": true
					})
					.transforms([

					])
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({

								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "tests"
										},
										baseLayerOpts: {
											"onClick": "debug",
											"query": {
												"select": {
													"type": "geo.heat2"
												},
												"table": {
													"name": "test/nuclear_tests"
												}
											},
											"style": {
												"method": "rules",
												"iconSize": 64,
												"densityThreshold": 1,
												"shadeBy": "",
												"gradient": "#0000ff,#00ff00,#ffff00,#ff0000",
												"colorTransform": {
													"alpha": 0.83
												}
											},
											"opacity": "0.83",
											"visible": true,
											"type": "layer",
											"onHover": "template",
											"creator": {
												"class": "ui.HeatMap",
												"id": "47be7e34-9490-41d6-9c57-bc6ff15eefff",
												"scale": {
													"colors": [
														"blue",
														"lime",
														"yellow",
														"red"
													],
													"colorSpace": null,
													"useBezier": false,
													"correctLightness": false
												}
											},
											"zIndex": "17",
											"id": "layer_ml_62",
											"clickDraggable": true,
											"clickPins": [],
											"clickCustomDisplays": [],
											"hashcode": "db822b94378ddf6f5d298f29e7336303",
											"hoverDefault": "",
											"hoverFieldsCommaDel": "",
											"linkedCharts": []
										}
									},

								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
									.name("Map")
									.options({
										"layerSources": {
											"$scopePath": "LayerStore.allLayers",
										},
										autoZoomExtents: true,
										"layers": {}
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: title,
											size: "Fixed"

										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "Right" })
											.children([
												mdash.controls.StackPanel.builder().children([
													ctrl.BPRangeSlider.builder().name('year_slider').options({
														min: { $scopePath: 'tests_years.min' },
														max: { $scopePath: 'tests_years.max' },
														step: 1
													})
												])

											])

									])

							])

					])
			]).toJSON();

	}


	function registerFormatFunctions() {
	}
}