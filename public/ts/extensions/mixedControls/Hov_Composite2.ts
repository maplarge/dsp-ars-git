/// <reference path="../../_references.ts"/>

namespace dashboard.extensions.HM_Composite2 {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.HM_Composite2',
		name: 'Main Custom DB test for All dashboards',
		description: 'Select,Card,Dialog'
	});

	var title = "Inventorys";
	let latitude: number;
	let longitude: number;

	function getLatLng() {
		return {
			latitude: latitude,
			longitude: longitude
		};
	}

	navigator.geolocation.getCurrentPosition(position => {
		latitude = position.coords.latitude; 
		longitude = position.coords.longitude;
	});

	export function getJSON() {

		registerFormatFunctions();

		return mdash.controls.ScopeDefaults.builder()
			.cssClass("mixed-controls")
			.options({
				defaults: [
					{ path: "latitude", value: latitude },
					{ path: "longitude", value: longitude },
					{ path: "suggestData", value:[]},
					{ path: "startCount", value: 0 },
					{ path: "size", value: 10 },
					{ path: "shape", value: 'round' },
					{ path: "omni_country", value: "" },
					{ path: "rating", value: [0,6] },
					{ path: "displayDataOnMap", value: true },
					{ path: "selectedCity", value:''},
					{ path: "selectedCountry", value: ''},
					{ path: "cityStartCount", value: 0 },
					{ path: "countries", value: [] },
					{ path: "cities", value: [] },
					{ path: "hotels_by_name", value: [] },
					{ path: "hotels_in_country", value: null },
					{ path: "cities_in_country", value: null },
					{ path: "selected_hotel", value: null },
					
				]
			}).transforms([
			
				mdash.transforms.RunQuery.builder().options({
					source: "countries_ds.dataSource",
					dest: "countries",
					transposeMethod: "TableTranspose",
					take: 1000,
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "cities_by_name_ds.dataSource",
					dest: "cities",
					transposeMethod: "TableTranspose",
					take: 1000,
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "hotels_by_name_ds.dataSource",
					dest: "hotels_by_name",
					transposeMethod: "TableTranspose",
					take: 1000,
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "selected_hotel_ds.dataSource",
					dest: "selected_hotel",
					transposeMethod: "TableTranspose",
					take: 1,
				})
			])
			.children([
				mdash.controls.DataStore.builder()
					.options({
						"sources": [
							{
								"name": "hotels",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							{
								"name": "hotels2",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							{
								name: "hotels_by_name_ds",
								dataSource: {
									withgeo: true,
									start: <any>"{{startCount}}",
									take: 1000,
									table: "{{hotels:asSubQuery()}}",
									sqlselect: [
										"HotelID as value",
										"HotelName as label",
										"CityName",
										"CityID",
										"CityFileName",
										"CountryName",
										"CountryCode",
										"LATITUDE as lat",
										"LONGITUDE as lng",
										"Address",
										"StarRating",
										"ChainID",
									],
									orderby: ["HotelName.asc"]
								}
							},
							{
								name: 'cities_by_name_ds',
								dataSource: {
									withgeo: true,
									start: <any>"{{cityStartCount}}",
									take: 1000,
									table: "{{hotels2:asSubQuery()}}",
									sqlselect: [
										"CountryName",
										"CityID as value",
										"CityName as label",
										"CityFileName",
									],
									orderby: ["CityName.asc"],
									groupby: [
										'CityFileName'
									]
								}
							},
							{
								name: "selected_hotel_ds",
								dataSource: {
									withgeo: true,
									start: 0,
									take: 1,
									table: {
										name: "hms/hotels"
									},
								}
							},
							{
								name: "countries_ds",
								label: "Hotel Countries",
								dataSource: {
									withgeo: true,
									start: 0,
									take: 100,
									table: {
										name: "hms/hotels"
									},
									sqlselect: [
										"CountryCode as value",
										"CountryName as label",
									],
									orderby: ["CountryName.asc"],
									groupby: [
										"CountryCode"
									]
								}
							}
						],
						"hidden": true
					})
					.transforms([
						mdash.transforms.RunQuery.builder().options({
							source: "hotels.dataSource",
							dest: "HotelCountryListPretty",
							transposeMethod: "TableTranspose",
							take: 1000
						})
					])
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({

								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},
										baseLayerOpts: {
											"name": "hotels",
											visible: "{{displayDataOnMap}}",
											style: {
												method: "rules",
												rules: [
													{
														style: {
															fillColor: "blue",
															shape: {$scopePath: (scope)=>{
																if(scope.shape() == 'round'){
																	return "rectangle"
																}
																else{
																	return "round"
																}
															}},
															size: {$scopePath: scope => scope.size() + 5}
														},
														where: [[
															{ col: "HotelID", test: statics.operators.eq.toString(), value: "{{selected_hotel.0.HotelID}}" }
														]]
													},
													{
														style: {
															fillColor: "red",
														},
														where: "CatchAll"
													},
												]
											},
											onHover: "template",
											hoverFieldsCommaDel: "CountryName,CityName,HotelName,HotelID,Address,StarRating,LATITUDE,LONGITUDE",
											hoverTemplate: {
												$scopePath: scope => {
													return `<div style="width:250px; padding:10px;border-radius:5px;">
																<dl>
																	${"CountryName,CityName,HotelName,HotelID,Address,StarRating,LATITUDE,LONGITUDE".split(',').map(c => {
																	return `<dt style="float:left; clear:both;padding-bottom:3px;">${c}:</dt>	<dd style="float:right">{${c}}</dd >`;
																	}).join('')}
																</dl>
															</div>`;
													
												}
											}
										}
									},
									
								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
								.name("Map")
								.options({
									"layerSources": {
										"$scopePath": "LayerStore.allLayers",
									},
									autoZoomExtents: true,
									"layers": {} ,
									mapOpts: {
										lat: latitude,
										lng: longitude,
										z: 10
									},
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: "Mixing External, Internal Controls with Transform",
											size:"Fixed"
											
										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "right" })
										.children([
											mdash.controls.StackPanel.builder().children([

											
												mdash.controls.Html.builder().options({
													html: "<h3>Select Countries</h3>"
												}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.countries()) }),
												ctrl.BPSelect.builder().name('selectedCountry').options({
													items: { $scopePath: "countries" },
													placeholder:"Search...",
													minimal:true,
													filterable:true,
													intent: ctrl.BPBase.Intent.Warning
												})
												.visible({ $scopePath: (scope) => ml.util.isNotNullUndefinedOrEmpty(scope.countries())})
												.transforms([
														mdash.transforms.Filter.builder().options({
															source: "selectedCountry",
															column: "CountryName",
															dest: "hotels",
															test: "Equal"
														}),

														mdash.transforms.Filter.builder().options({
															source: "selectedCountry",
															column: "CountryName",
															dest: "hotels2",
															test: "Equal"
														}),
														
														mdash.transforms.Set.builder().options({
															source: "selectedCountry",
															dest: "startCount",
															value: 0,
															setOnInit: true
														}),

														mdash.transforms.Set.builder().options({
															source: "selectedCountry",
															dest: "selectedCity",
															value: "",
															setOnInit: true
														}),

														mdash.transforms.Set.builder().options({
															source: "selectedCountry",
															dest: "startCount",
															value: 0,
															setOnInit: true
														}),

														mdash.transforms.BuildObject.builder().options({
															dest: "hotels_in_country",
															template: {
																length: "{{hotels_by_name_ds:count}}"
															},
														}),
												]),

												mdash.controls.Html.builder().options({
													html: "<h3>Select City</h3>"
												}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) }),

												ctrl.BPSelect.builder().name('selectedCity').options({
													items: { $scopePath: "cities" },
													placeholder:'Search...',
													minimal:true,
													filterable:true,
													intent: ctrl.BPBase.Intent.Warning
												}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) })
												.transforms([
													mdash.transforms.Filter.builder().options({
														source: "selectedCity",
														column: "CityName",
														dest: "hotels_by_name_ds",
														test: "Equal"
													}),

													mdash.transforms.Filter.builder().options({
														source: "selectedCity",
														column: "CityName",
														dest: "hotels",
														test: "Equal"
													}),

													mdash.transforms.Set.builder().options({
														source: "selectedCity",
														dest: "startCount",
														value: 0,
														setOnInit: true
													}),

													mdash.transforms.Set.builder().options({
														source: "selectedCity",
														dest: "selectedHotelList",
														value: '',
														setOnInit: true
													}),

													mdash.transforms.BuildObject.builder().options({
														dest: "cities_in_country",
														template: {
															length: "{{cities_by_name_ds:count}}"
														},
													}),
												]),

												mdash.controls.Html.builder().options({
													html: "<h3>Select Hotel</h3>"
												})
												.visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) && ml.util.isNotNullUndefinedOrEmpty(scope.selectedCity())}),

												ctrl.BPSelect.builder().options({
													items: { $scopePath: "hotels_by_name" },
													placeholder:"Search...",
													minimal:true,
													// filterable:true,
													intent: ctrl.BPBase.Intent.Warning,
												})
												.name('selectedHotelList')
												.visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) && ml.util.isNotNullUndefinedOrEmpty(scope.selectedCity())
												})													
												.transforms([
														mdash.transforms.Filter.builder().options({
															source: "selectedHotelList",
															dest: "selected_hotel_ds",
															test: statics.operators.eq.toString(),
															column: "HotelName",
														}),
														
														mdash.transforms.BuildObject.builder().options({
															dest: "Map.center",
															template: {
																lat: {
																	$scopePath: scope => scope.selected_hotel() && ml.util.isNotNullUndefinedOrEmpty(scope.selectedHotelList())
																		? scope.selected_hotel()[0].LATITUDE : getLatLng().latitude
																},
																lng: {
																	$scopePath: scope => scope.selected_hotel() && ml.util.isNotNullUndefinedOrEmpty(scope.selectedHotelList())
																		? scope.selected_hotel()[0].LONGITUDE : getLatLng().longitude
																}
															}
														}),
														mdash.transforms.Set.builder().options({
															source: "selectedHotelList",
															dest: "Map.zoom",
															value: 18,
															setOnInit: true
														}),
													]),

													ctrl.BPCard.builder()
														.options({
															elevation: ctrl.BPBase.Elevation.Two,
															interactive: true
														})
														.visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedHotelList())})
														.children([
															ctrl.BPDialog.builder()
																.options({
																	icon:"info28",
																	buttonText: "More Information",
																	closeButton: "Close",
																	submitButton: "Submit",
																	onClick: (scope)=>{
																		window.open(`https://www.google.am/search?source=hp&ei=WBojXOfrIcfprgT-3rbYAg&q=${scope.selected_hotel()[0].HotelName}`)
																	},
																	canEscapeKeyClose:true,
																	canOutsideClickClose:true,
																	heading: {$scopePath: scope=> scope.selected_hotel()[0].HotelName },
																	text: {$scopePath: (scope) => {

																		return	`
																			<dl>
																				<dt style="float:left; clear:both;padding-bottom:3px;">${"Address"}</dt>
																				<dd style="float:right">${scope.selected_hotel()[0].Address}</dd >
																				<dt style="float:left; clear:both;padding-bottom:3px;">${"CityID"}</dt>
																				<dd style="float:right">${scope.selected_hotel()[0].CityID}</dd >
																				<dt style="float:left; clear:both;padding-bottom:3px;">${"CountryName"}</dt>
																				<dd style="float:right">${scope.selected_hotel()[0].CountryName}</dd >
																				<dt style="float:left; clear:both;padding-bottom:3px;">${"CountryCode"}</dt>
																				<dd style="float:right">${scope.selected_hotel()[0].CountryCode}</dd >
																				<dt style="float:left; clear:both;padding-bottom:3px;">${"Longitude"}</dt>
																				<dd style="float:right">${scope.selected_hotel()[0].LONGITUDE}</dd >
																				<dt style="float:left; clear:both;padding-bottom:3px;">${"Latitude"}</dt>
																				<dd style="float:right">${scope.selected_hotel()[0].LATITUDE}</dd >
																				<dt style="float:left; clear:both;padding-bottom:3px;">${"StarRating"}</dt>
																				<dd style="float:right">${scope.selected_hotel()[0].StarRating}</dd >
																				<dt style="float:left; clear:both;padding-bottom:3px;">${"ChainID"}</dt>
																				<dd style="float:right">${scope.selected_hotel()[0].ChainID}</dd >
																			</dl>
																				
																			`
																		}																	
																	}
																}),

															ctrl.BPButton.builder()
																.options({
																	label:"Reset Filtration",
																	onClick( scope ){
																		scope.Map.center({ lat: getLatLng().latitude, lng: getLatLng().longitude });
																		scope.selectedCountry("");
																	} 
															})
													]),


											])
										])
									])
							])
					])
			]).toJSON()
	}
	
	function registerFormatFunctions() {

		// ml.ui.dashboard.registerFormatFunction("exludeSearch",
		// (value: any) => value == "Search..." ? "" : value,
		// ml.ui.dashboard.ScopeDataType.STRING, ml.ui.dashboard.ScopeDataType.STRING);
		
	}
			
}