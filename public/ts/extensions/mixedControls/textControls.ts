/// <reference path="../../_references.ts"/>
/// <reference path="../../_statics.ts"/>

namespace dashboard.extensions.text {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.text',
		name: 'textControls',
		description: 'A way to test the text based controls'
	});

	var title = "Text Controls";

	export function getJSON() {
		return mdash.controls.ScopeDefaults.builder()
			.cssClass("mixed-controls")
			.options({
				defaults: [
					{ path: "name_starts_with", value: "Days In" },
					{ path: "size", value: 8}
				]
			})
			.children([
				mdash.controls.DataStore.builder()
					.options({
						"sources": [
							{
								"name": "hotels",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							{
								"name": "hotels2",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							//{
							//	name: "HotelCountries",
							//	label: "Hotel Countries",
							//	dataSource: {
							//		"withgeo": true,
							//		"start": 0,
							//		"take": 100,
							//		"table": {
							//			"name": "hms/hotels"
							//		},
							//		"sqlselect": [
							//			"CountryCode as id",
							//			"CountryFileName as item"
							//		],
							//		"groupby": [
							//			"CurrencyCode"
							//		]
							//	}
							//}
						],
						"hidden": true
					})
					.transforms([
						mdash.transforms.RunQuery.builder().options({
							source: "hotels.dataSource",
							dest: "HotelCountryListPretty",
							transposeMethod: "TableTranspose",
							take: 1000
						})
					])
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({

								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},
										baseLayerOpts: {
											"name": "hotels",
											//visible: "{{displayDataOnMap}}",
											style: {
												method: "rules",
												rules: [{
													style: {
														size: { $scopePath: scope => scope.size() },
													},
													where: "CatchAll"
												}
												]
											},
											onHover: "template",
											hoverFieldsCommaDel: "HotelName,HotelID,Address,StarRating",
											hoverTemplate: {
												$scopePath: scope => {
													return `<div style="width:200px;">
																<dl>
																	<dt>Hotel Name</dt>	<dd>{HotelName}</dd>
																	<dt>Address</dt>	<dd>{Address}</dd>
																	<dt>Star Rating</dt><dd>{StarRating}</dd>
																</dl>
															<div>`;
												}
											}
										}
									},

								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
									.name("Map")
									.options({
										"layerSources": {
											"$scopePath": "LayerStore.allLayers",
										},
										autoZoomExtents: true,
										"layers": {} 
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: title,
											size:"Fixed"

										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "Right" })
											.children([
												mdash.controls.StackPanel.builder().children([
													
													mdash.controls.Html.builder().options({
														html: "<h3>Text input that filters hotels with names that start with value</h3>"
													}),
													ctrl.BPTextInput.builder()
														.options({
															
														}).name("name_starts_with")
														.transforms([
															mdash.transforms.Filter.builder().options({
																source: "name_starts_with",
																column: "HotelName",
																dest: "hotels",
																test: statics.operators.startsW.toString()
															})
														]),
													mdash.controls.Html.builder().options({
														html: "<h3>Dot Size(2-20)</h3>"
													}),
													ctrl.BPNumericInput.builder().options({
														min: 2,
														max: 20,
														clampValueOnBlur: true,
														leftIcon: "ruler",
														numericOnly: true,
														majorStepSize: 2,
														minorStepSize: 0.5,
														placeholder: "Dot Size"
													}).name('size')
												])
											])
									])
							])
					])
			]).toJSON();
	}

	function setFillColor(scope, color) {
		console.log(scope);
		scope.fillColor(color);
	}

}