

/// <reference path="../../_references.ts"/>
/// <reference path="../../_references.ts"/>

namespace dashboard.extensions.Vahe_mixControls {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.Vahe_mixControls',
		name: 'Main Custom DB test for All dashboards',
		description: 'Main page for testing new Custom Controls built by Arman,Vahe,Hovhannes'
	});

	var title = "Inventorys";
	let lat: number;
	let lng: number;
	function getLatLng() {
		return { lat, lng };
	}

	navigator.geolocation.getCurrentPosition(position => {
		lat = position.coords.latitude;
		lng = position.coords.longitude;
	});

	export function getJSON() {
		return mdash.controls.ScopeDefaults.builder()
			.cssClass("mixed-controls")
			.options({
				defaults: [
					{ path: "dotsColorData", value: [
						{ label: "Red",		value: '#ff0000' },
						{ label: "Green",	value: '#00ff00' },
						{ label: "Blue",		value: '#0000ff' },
						{ label: "Orange",	value: '#C87A2D' },
					] },
					{ path: "shape", value: true },
					{ path: "switch", value: true },
					{ path: "select", value: '' },
					{ path: "fillColor", value: 'red' },
					{ path: "hotelData", value: [] },
					{ path: "selectedCountry", value: '' },
					{ path: "selectedCity", value: ''},
					{ path: "country_list", value: [] },
					{ path: "plusZoom", value: "+" },
					{ path: "minusZoom", value: "-" },
					{ path: "selected_column", value: "CountryName" },
					{ path: "selected_hotel", value: [] },
					{ path: "hotels_by_name", value: [] },
					{ path: 'cities', value: []},
					{ path: "cityStartCount", value: 0},
					{ path: "hotels_in_country", value: null },
					{ path: "startCount", value: 0 },
					{ path: "size", value: 10 },

				]
			})
			.transforms([
				mdash.transforms.RunQuery.builder().options({
					source: "countries_ds.dataSource",
					dest: "country_list",
					transposeMethod: "TableTranspose",
					take: 1000,
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "cities_by_name_ds.dataSource",
					dest: "cities",
					transposeMethod: "TableTranspose",
					take: 1000,
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "hotels_by_name_ds.dataSource",
					dest: "hotels_by_name",
					transposeMethod: "TableTranspose",
					take: 1000,
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "selected_hotel_ds.dataSource",
					dest: "selected_hotel",
					transposeMethod: "TableTranspose",
					take: 1,
				}),
			])
			.children([
				mdash.controls.DataStore.builder()
					.options({
						"sources": [
							{
								"name": "hotels",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							{
								name: "hotels_by_name_ds",
								dataSource: {
									withgeo: true,
									start: 0,
									take: 500,
									table: {
										name: "hms/hotels"
									},
									
									sqlselect: [
										"HotelID as value",
										"HotelName as label",
										"CountryName",
										"LATITUDE as lat",
										"LONGITUDE as lng"
									],
									orderby: ["HotelName.asc"]
								}
							},
							{
								name: 'cities_by_name_ds',
								dataSource: {
									withgeo: true,
									start: <any>"{{cityStartCount}}",
									take: 1000,
									table: "{{hotels:asSubQuery()}}",
									sqlselect: [
										"CityID as value",
										"CityName as label",
										"CountryName",
										"CityFileName"
									],
									orderby: ["CityName.asc"],
									groupby: [
										'CityFileName'
									]
								}
							},
							{
								name: "selected_hotel_ds",
								dataSource: {
									withgeo: true,
									start: 0,
									take: 1,
									table: {
										name: "hms/hotels"
									},
								}
							},
							{
								name: "countries_ds",
								label: "Hotel Countries",
								dataSource: {
									withgeo: true,
									table: {
										name: "hms/hotels"
									},
									sqlselect: [
										"CountryCode as value",
										"CountryName as label",
									],
									orderby: ["CountryName.asc"],
									groupby: [
										"CountryCode"
									]
								}
							}
						],
						"hidden": true
					})
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({

								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},

										baseLayerOpts: {
											"name": "hotels",
											visible: "{{switch}}",
											style: {
												method: "rules",
												rules: [
													{
														style: {
															fillColor: "blue",
															shape: { $scopePath: scope => scope.shape() ? "rectangle" : "round" }, 
															size: { $scopePath: scope => scope.size() + 5 },
														},
														where: [[
															{ col: "HotelID", test: statics.operators.eq.toString(), value: "{{selected_hotel.0.HotelID}}" }
														]]
													},
													{
														style: {
															fillColor: { $scopePath: scope => scope.fillColor() }, // "#00ffff",
															color: { $scopePath: scope => scope.fillColor().split(" ")[1]  }, // "#00ffff",
															size: { $scopePath: scope => scope.size() },
														},
														where: 'CatchAll'
													},

												]
											},
											onHover: "template",
											hoverFieldsCommaDel: "CountryName,CityName,CityID,HotelName,HotelID,Address,StarRating,LATITUDE,LONGITUDE",
											hoverTemplate: {
												$scopePath: scope => {

													return `<div style="width:200px;">
																<p>Country: {CountryName}</p>
																<p>City: {CityName}</p>
																<p>Hotel: {HotelName}</p>
																<p>Latitude: {LATITUDE}</p>
																<p>Longitude: {LONGITUDE}</p>
																<p>Address: {Address}</p>
																<p>Star Rating: {StarRating}</p>
															<div>`;
												}
											}
										}
									},
								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
									.name("Map")
									.options({
										"layerSources": {
											"$scopePath": "LayerStore.allLayers",

										},
										autoZoomExtents: true,
										"layers": {},
										mapOpts: {
											lat: lat,
											lng: lng,
											z: 1
										}
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: "Mixing External, Internal Controls with Transform",

										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "Right" })
											.children([
												mdash.controls.StackPanel.builder().children([
												
													ctrl.BPSwitch.builder()
													.options({
														label:"Toggle Dots",
													}).name("switch"),
												
													mdash.controls.Html.builder().options({
														html: "<h1>Countries</h1>"
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.country_list()) }),
													ctrl.BPSelect.builder()
													.name('selectedCountry')
													.options({
														intent: ctrl.BPBase.Intent.Warning,
														filterable: true,
														minimal:true,
														// popoverMinimal: true,
														items: { $scopePath: 'country_list' }
													})

													.visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.country_list()) })
													.transforms([
														mdash.transforms.Filter.builder().options({
															source: "selectedCountry",
															column: "CountryName",
															dest: "hotels",
															test: statics.operators.eqAny.toString()
														}),
														mdash.transforms.Filter.builder().options({
															source: "selectedCountry",
															column: "CountryName",
															dest: "hotels_by_name_ds",
															test: statics.operators.eqAny.toString()
														}),
														mdash.transforms.Set.builder().options({
															source: "selectedCountry",
															dest: "startCount",
															value: 0,
															setOnInit: true
														}),
														mdash.transforms.BuildObject.builder().options({
															dest: "hotels_in_country",
															template: {
																length: "{{hotels_by_name_ds:count}}"
															},
														}),
													]),
													ctrl.BPSpinner.builder().options({
														circleSize: 20
													}).visible({ $scopePath: scope => ml.util.isNullUndefinedOrEmpty(scope.country_list()) }),

													mdash.controls.Html.builder().options({
														html: "<h1>Cities from {{selectedCity}} - {{selectedCountry}}:</h1>"
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) }),
													
													ctrl.BPSpinner.builder().options({
														circleSize: 20
													}).visible({ $scopePath: scope => ml.util.isNullUndefinedOrEmpty(scope.country_list()) }),

													ctrl.BPSuggest.builder().name('selectedCity').options({
														intent: ctrl.BPBase.Intent.Success,
														// filterable: true,
														minimal: true,
														items: { $scopePath: 'cities' }
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) })
													.transforms([
														mdash.transforms.Filter.builder().options({
															source: "selectedCity",
															column: "CityName",
															dest: "cities_by_name_ds",
															test: "Equal"
														}),
													]),


													mdash.controls.Html.builder().options({
														html: "<h1>Hotels from {{selectedCity}}:</h1>"
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCity()) }),

													ctrl.BPSuggest.builder()
													.name('selectedHotelList')
													.options({
														minimal: true,
														intent:ctrl.BPBase.Intent.Success,
														placeholder:"Search...",
														// filterable: true,
														items: { $scopePath: "hotels_by_name" },
														// disabled: { $scopePath: scope => ml.util.isNullUndefinedOrEmpty(scope.selected_hotel()) }
													})
													.visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCity()) })
													.transforms([
														
														mdash.transforms.Filter.builder().options({
															source: "selectedHotelList",
															dest: "selected_hotel_ds",
															test: statics.operators.eq.toString(),
															column: "HotelName",
														}),
														//mdash.transforms.BuildObject.builder().options({
														//	dest: "Map.center",
														//	template: scope => scope &&
														//		scope.selected_hotel &&
														//		scope.selected_hotel() &&
														//		ml.util.isNotNullUndefinedOrEmpty(scope.selectedHotelList()) ?
														//		{
														//			lat: scope.selected_hotel()[0].LATITUDE,
														//			lng: scope.selected_hotel()[0].LONGITUDE
														//		} :
														//		getLatLng()
														//}),
														mdash.transforms.Set.builder().options({
															source: "selectedHotelList",
															dest: "Map.zoom",
															value: 18,
															setOnInit: true
														}),
													]),
													ctrl.BPSpinner.builder().options({
														circleSize: 20
													}).visible({ $scopePath: scope => ml.util.isNullUndefinedOrEmpty(scope.hotels_by_name()) }),
													mdash.controls.Html.builder().options({
														html: "<h3>Zoom Button</h3>"
													}),
													ctrl.BPButtonGroup.builder().options({
														large: true,
														btnData: [
															{
																onClick: (s) => {
																	let scope = s.$rootScope;
																	scope.Map.zoom(Math.max(0, scope.Map.zoom() + 2));
																},
																label: "+"
															},
															{
																onClick: (s) => {
																	let scope = s.$rootScope;
																	scope.Map.zoom(Math.min(20, scope.Map.zoom() - 2));
																},
																label: "-"
															}
														]
													}),
													mdash.controls.Html.builder().options({
														html: "<h3>Dots Size</h3>"
													}),
													ctrl.BPSlider.builder().name('size').options({
														min: 5,
														max: 15,
														step: 1,
														showTrackFill: true,
													}),
													/* mdash.controls.Html.builder().options({
														html: "<h3>Dots Color</h3>"
													}), */
													/* ctrl.BPSelect.builder()
													.name('fillColor')
													.options({
														minimal: true,
														filterable: false,
														intent: ctrl.BPBase.Intent.Warning,
														items: {$scopePath: "dotsColorData"}
														// disabled: { $scopePath: scope => ml.util.isNullUndefinedOrEmpty(scope.selected_hotel()) }
													}),
 */
												/* 	ctrl.BPMultiSelect.builder()
													.name('fillColor')
													.options({
														minimal: true,
														intent: ctrl.BPBase.Intent.Warning,
														items: {$scopePath: "dotsColorData"}
														// disabled: { $scopePath: scope => ml.util.isNullUndefinedOrEmpty(scope.selected_hotel()) }
													}), */
												
													

												])
											])
									])
							])
					])
			]).toJSON();

	}

	// }
	// function setFillColor(scope, color) {
	// 	console.log(scope);
		// scope.fdotsColor(color);
	// }

}

