/// <reference path="../../_references.ts"/>
/// <reference path="../../_statics.ts"/>

namespace dashboard.extensions.AR_hotelFilters {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.AR_hotelFilters',
		name: 'hotelFilters',
		description: 'Display of checkbox,numeric input,slider,tag,callout,progress,spinner and etc.'
	});

	var title = "Hotel Filters";

	function filterSelect(scope,select: string,hotels){
		let arrCp = [...scope[hotels]()];
		arrCp = arrCp.filter(item => item.text === scope[select]());
		return arrCp;
	}

	function makeCalloutLayout(scope, customParam: string){
		let custom: number;
		let city: string,country: string,address: string,starRating: string;

		if(customParam == 'CleanlinessRating')
		{
			custom = scope.selected_cleanliness_hotel()[0].CleanlinessRating;
			country = scope.selected_cleanliness_hotel()[0].CountryName;
			city = scope.selected_cleanliness_hotel()[0].CityName;
			address = scope.selected_cleanliness_hotel()[0].Address;
			starRating = scope.selected_cleanliness_hotel()[0].StarRating;
		}
		else
		{
			custom = scope.selected_reviews_hotel()[0].NumberOfReviews;
			country = scope.selected_reviews_hotel()[0].CountryName;
			city = scope.selected_reviews_hotel()[0].CityName;
			address = scope.selected_reviews_hotel()[0].Address;
			starRating = scope.selected_reviews_hotel()[0].StarRating;
		}

		if((custom || custom == 0) && country && city && address)
		{
			return `
				<strong>Country</strong>: ${country}<br>
				<strong>City</strong>: ${city}<br>
				<strong>Address</strong>: ${address}<br>
				<strong>${customParam == 'CleanlinessRating' ? 'Cleanliness Rating' : 'Number of Reviews'}</strong>: ${custom}<br>
				<strong>Star Rating</strong>: ${starRating}<br>
			`;
		}
		else
		{
			return `
				<strong>No data available</strong>
			`;
		}
	}

	export function getJSON() {
		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
					{ path: "layers_visible", value: false },
					{ path: "cleanliness_rating", value: 0 },
					{ path: "reviews_filter", value: 0 },
					{ path: "hotels_arr", value: null },
					{ path: "hotels2_arr", value: null },
					{ path: "cleanliness_select", value: '' },
					{ path: "reviews_select", value: '' },
					{ path: "selected_cleanliness_hotel", value: [] },
					{ path: "selected_reviews_hotel", value: [] },
				]
			}).transforms([
				mdash.transforms.RunQuery.builder().options({
					source: "hotels.dataSource",
					dest: "hotels_arr",
					transposeMethod: "TableTranspose",
					take: 5000
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "hotels2.dataSource",
					dest: "hotels2_arr",
					transposeMethod: "TableTranspose",
					take: 5000
				}),
			])
			.children([
				mdash.controls.DataStore.builder()
					.options({
						sources: [
							{
								name: "hotels",
								label: "Hotels",
								dataSource: {
									"withgeo": true,
									"start": 0,
									"take": 5000,
									"table": {
										"name": "hms/hotels"
									},
									"sqlselect": [
										"HotelID as id",
										"HotelName as text",
										"CleanlinessRating",
										"CityName",
										"CountryName",
										"Address",
										"StarRating"
									],
								}
							},
							{
								name: "hotels2",
								label: "Hotels",
								dataSource: {
									"withgeo": true,
									"start": 0,
									"take": 5000,
									"table": {
										"name": "hms/hotels"
									},
									"sqlselect": [
										"HotelID as id",
										"HotelName as text",
										"NumberOfReviews",
										"CityName",
										"CountryName",
										"Address",
										"StarRating"
									],
								}
							},
							
						],
						hidden: true
					})
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({
								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},
										baseLayerOpts: {
											"name": "hotels",
											visible: { $scopePath: (scope) => !scope.layers_visible() },
											style: {
												method: "rules",
												rules: [
													{
														style: {
															fillColor: "black",
														},

														where: [[
															{ col: "CleanlinessRating", test: statics.operators.eq.toString(), value: 0 },
														]]
													},
													{
														style: {
															fillColor: "#212121",
														},

														where: [[
															{ col: "CleanlinessRating", test: statics.operators.greater.toString(), value: 0 },
															{ col: "CleanlinessRating", test: statics.operators.lessOR.toString(), value: 2 }
														]]
													},
													{
														style: {
															fillColor: "darkred",
														},

														where: [[
															{ col: "CleanlinessRating", test: statics.operators.greater.toString(), value: 3 },
															{ col: "CleanlinessRating", test: statics.operators.lessOR.toString(), value: 4 }
														]]
													},
													{
														style: {
															fillColor: "red",
														},

														where: [[
															{ col: "CleanlinessRating", test: statics.operators.greater.toString(), value: 4 },
															{ col: "CleanlinessRating", test: statics.operators.less.toString(), value: 5 },
														]]
													},
													{
														style: {
															fillColor: "orange",
														},

														where: [[
															{ col: "CleanlinessRating", test: statics.operators.eq.toString(), value: 5 },
														]]
													},
													{
														style: {
															fillColor: "crimson",
														},

														where: "CatchAll"
													},
												]
											},
											onHover: "template",
											hoverFieldsCommaDel: "CleanlinessRating,text",
											hoverTemplate: `<div style="width:200px;">
																<p>Hotel Name: <strong>{text}</strong></p>
                                                                <p>Cleanliness Rating: <strong>{CleanlinessRating}</strong></p>                                                             
															<div>`
										}
                                    },
                                    <mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels2",

										dataSource: {
											"$scopePath": "hotels2"
										},
										baseLayerOpts: {
											"name": "hotels2",
											visible: { $scopePath: (scope) => scope.layers_visible() },
											style: {
												method: "rules",
												rules: [
													{
														style: {
															fillColor: "darkblue",
														},
														where: [[
															{ col: "NumberOfReviews", test: statics.operators.eq.toString(), value: 0 },
														]]
													},
													{
														style: {
															fillColor: "royalblue",
														},
														where: [[
															{ col: "NumberOfReviews", test: statics.operators.greater.toString(), value: 0 },
															{ col: "NumberOfReviews", test: statics.operators.lessOR.toString(), value: 20 },
														]]
													},
													{
														style: {
															fillColor: "skyblue",
														},
														where: [[
															{ col: "NumberOfReviews", test: statics.operators.greater.toString(), value: 20 },
															{ col: "NumberOfReviews", test: statics.operators.lessOR.toString(), value: 50 },
														]]
													},
													{
														style: {
															fillColor: "cyan",
														},
														where: [[
															{ col: "NumberOfReviews", test: statics.operators.greater.toString(), value: 50 },
															{ col: "NumberOfReviews", test: statics.operators.lessOR.toString(), value: 100 },
														]]
													},
													{
														style: {
															fillColor: "lime",
														},
														where: "CatchAll"
													},
													
												]
											},
											onHover: "template",
											hoverFieldsCommaDel: "text,NumberOfReviews",
											hoverTemplate: `<div style="width:200px;">
																<p>Hotel Name: <strong>{text}</strong></p>
                                                                <p>Number of reviews: <strong>{NumberOfReviews}</strong></p>                                            
															<div>`
										}
									}
								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
									.name("Map")
									.options({
										"layerSources": {
											"$scopePath": "LayerStore.allLayers",
										},
										autoZoomExtents: true,
										"layers": {},
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: title,
											size:"Fixed"

										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "Right" })
											.children([
												mdash.controls.StackPanel.builder().children([
													
													ctrl.BPSpinner.builder().options({
														circleSize: 20
													}).visible({ $scopePath: scope => !scope.hotels.dataSource() }),
													
													ctrl.BPCheckbox.builder().name('layers_visible').options({
														label: "Toggle cleanliness rating/number of reviews layers",
														align: ctrl.BPBase.Alignment.Right,
													}).visible({ $scopePath: scope => scope.hotels.dataSource() }),

													/*
													------------------CLEANLINESS RATING LAYER COMPONENTS-------------------------------------------------
													*/
													mdash.controls.Html.builder().options({
														html: "<h2>Cleanliness Rating Slider</h2>"
													}).visible({ $scopePath: scope => !scope.layers_visible() && scope.hotels.dataSource() }),

													ctrl.BPSlider.builder().name('cleanliness_rating').options({
														min: 0,
														max: 5,
														showTrackFill: true,
														labelPercentages: [20, 40, 60, 80],
														step: 0.1
													}).transforms([
														mdash.transforms.Filter.builder().options({
															source: "cleanliness_rating",
															column: "CleanlinessRating",
															dest: "hotels",
															test: statics.operators.eq.toString()
														}),
													]).visible({ $scopePath: scope => !scope.layers_visible() && scope.hotels.dataSource() }),

													ctrl.BPProgress.builder().options({
														rangeDisable: true,
														intent: ctrl.BPBase.Intent.Primary
													}).visible({ $scopePath: scope => !scope.layers_visible() && scope.hotels.dataSource() && !scope.hotels_arr() }),

													ctrl.BPTag.builder().options({
														label: { $scopePath: scope => {
															const arr_len: number = scope.hotels_arr().length;
															return `Number of hotels with cleanliness rating of ${scope.cleanliness_rating()}: ${arr_len}${arr_len == 5000 ? "+" : ""}`;
														} },
														intent: ctrl.BPBase.Intent.Primary
													}).visible({ $scopePath: scope => !scope.layers_visible() && scope.hotels.dataSource() && scope.hotels_arr() }),

													mdash.controls.Html.builder().options({
														html: "<h2>Hotels that match the filter</h2>"
													}).visible({ $scopePath: scope => !scope.layers_visible() && scope.hotels.dataSource() }),

													ctrl.BPHTMLSelect.builder().name('cleanliness_select').options({
														options: { $scopePath: "hotels_arr" },
														labelField: "text",
														valueField: "id",
													}).transforms([
														mdash.transforms.Set.builder().options({
															source: 'cleanliness_select',
															dest: 'selected_cleanliness_hotel',
															value: { $scopePath: scope => {
																if(scope.hotels_arr())
																{
																	return filterSelect(scope,'cleanliness_select','hotels_arr');
																}
															} },
															setOnInit: true
														})
													]).visible({ $scopePath: scope => !scope.layers_visible() && scope.hotels.dataSource() && scope.hotels_arr() }),

													mdash.controls.Html.builder().options({
														html: "<h2>Further info on selected hotel</h2>"
													}).visible({ $scopePath: scope => !scope.layers_visible() && scope.hotels.dataSource() && scope.selected_cleanliness_hotel() }),

													ctrl.BPCallout.builder().options({
														intent: { $scopePath: scope => {
															return scope.selected_cleanliness_hotel()[0] && scope.selected_cleanliness_hotel()[0].text ? ctrl.BPBase.Intent.Primary : ctrl.BPBase.Intent.Default;
														} },
														headerText: { $scopePath: scope => scope.selected_cleanliness_hotel()[0] && scope.selected_cleanliness_hotel()[0].text },
														hideHeader: { $scopePath: scope => !scope.selected_cleanliness_hotel()[0] || scope.selected_cleanliness_hotel()[0].label == "" },
														contentText: { $scopePath: scope => {
															if(scope.selected_cleanliness_hotel()[0])
															{
																return makeCalloutLayout(scope,'CleanlinessRating');
															}
														} },
													}).visible({ $scopePath: scope => !scope.layers_visible() && scope.hotels.dataSource() && scope.hotels_arr() && scope.selected_cleanliness_hotel().length }),

													/*
													------------------CLEANLINESS RATING LAYER COMPONENTS END---------------------------------------------
													*/

													/*
													------------------NUMBER OF REVIEWS LAYER COMPONENTS--------------------------------------------------
													*/

													mdash.controls.Html.builder().options({
														html: "<h2>Show hotels with reviews greater than or equal to: </h2>"
													}).visible({ $scopePath: scope => scope.layers_visible() && scope.hotels2.dataSource() }),

													ctrl.BPNumericInput.builder().name('reviews_filter').options({
														position: ctrl.BPNumericInput.Position.Right,
														intent: ctrl.BPBase.Intent.Primary,
														min: 0,
														clampValueOnBlur: true,
														majorStepSize: 10,
														numericOnly: true,
														leftIcon: "filter"
													}).transforms([
														mdash.transforms.Filter.builder().options({
															source: "reviews_filter",
															column: "NumberOfReviews",
															dest: "hotels2",
															test: statics.operators.greaterOr.toString()
														})
													]).visible({ $scopePath: scope => scope.layers_visible() && scope.hotels2.dataSource() }),

													mdash.controls.Html.builder().options({
														html: "<h2>Hotels that match the filter</h2>"
													}).visible({ $scopePath: scope => scope.layers_visible() && scope.hotels2.dataSource() }),

													ctrl.BPSuggest.builder().name('reviews_select').options({
														intent: ctrl.BPBase.Intent.Primary,
														items: { $scopePath: "hotels2_arr" },
														labelField: "text",
														valueField: "id",
														closeOnSelect: true,
													}).transforms([
														mdash.transforms.Set.builder().options({
															source: 'reviews_select',
															dest: 'selected_reviews_hotel',
															value: { $scopePath: scope => {
																if(scope.hotels2_arr())
																{
																	return filterSelect(scope,'reviews_select','hotels2_arr');
																}
															} },
															setOnInit: true
														})
													]).visible({ $scopePath: scope => scope.layers_visible() && scope.hotels2.dataSource() && scope.hotels2_arr() }),

													mdash.controls.Html.builder().options({
														html: "<h2>Further info on selected hotel</h2>"
													}).visible({ $scopePath: scope => scope.layers_visible() && scope.hotels.dataSource() && scope.selected_reviews_hotel() }),

													ctrl.BPCallout.builder().options({
														intent: { $scopePath: scope => {
															return scope.selected_reviews_hotel()[0] && scope.selected_reviews_hotel()[0].text ? ctrl.BPBase.Intent.Primary : ctrl.BPBase.Intent.Default;
														} },
														headerText: { $scopePath: scope => scope.selected_reviews_hotel()[0] && scope.selected_reviews_hotel()[0].text },
														hideHeader: { $scopePath: scope => !scope.selected_reviews_hotel()[0] || scope.selected_reviews_hotel()[0].text == "" },
														contentText: { $scopePath: scope => {
															if(scope.selected_reviews_hotel()[0])
															{
																return makeCalloutLayout(scope,'NumberOfReviews');
															}
														} },
													}).visible({ $scopePath: scope => scope.layers_visible() && scope.hotels.dataSource() && scope.hotels_arr() && scope.selected_reviews_hotel().length }),

													/*
													------------------NUMBER OF REVIEWS LAYER COMPONENTS END----------------------------------------------
													*/

												])
											])
									])
							])
					])
			]).toJSON();
	}

}