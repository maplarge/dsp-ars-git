/// <reference path="../../_references.ts"/>
/// <reference path="../../_statics.ts"/>

namespace dashboard.extensions.AR_textControls {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.AR_textControls',
		name: 'textControlsTest',
		description: 'Dashboard with text input,tag input,numeric input,text area'
	});

	var title = "Text Controls";

	export function getJSON() {
		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
					{ path: "size", value: 8 },
					{ path: "hover_template_texts", value: ['Row 1'] },
					{ path: "hover_text", value: '' },
					{ path: "hover_footer_text", value: '' },
				]
			})
			.children([
				mdash.controls.DataStore.builder()
					.options({
						"sources": [
							{
								"name": "hotels",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							{
								"name": "hotels2",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							{
								name: "HotelCountries",
								label: "Hotel Countries",
								dataSource: {
									"withgeo": true,
									"start": 0,
									"take": 100,
									"table": {
										"name": "hms/hotels"
									},
									"sqlselect": [
										"CountryCode as id",
										"CountryFileName as item"
									],
									"groupby": [
										"CurrencyCode"
									]
								}
							}
						],
						"hidden": true
					})
					.transforms([
						mdash.transforms.RunQuery.builder().options({
							source: "hotels.dataSource",
							dest: "HotelCountryListPretty",
							transposeMethod: "TableTranspose",
							take: 1000
						})
					])
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({

								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},
										baseLayerOpts: {
											"name": "hotels",
											style: {
												method: "rules",
												rules: [{
													style: {
														size: { $scopePath: scope => scope.size() },
													},
													where: "CatchAll"
												}
												]
											},
											onHover: "template",
											hoverFieldsCommaDel: "HotelName,Address,StarRating",
											hoverTemplate: {
												$scopePath: scope => {
													let rows = scope.hover_template_texts();
													let template = '';
													rows.forEach(element => {
														template += `<li>${element}</li>`;
													});
													return `<div style="width:200px;">
																${scope.hover_text() ? `<h3 style="text-align: center">${scope.hover_text()}</h3>` : `<h4 style="text-align: center">{HotelName}</h4>`}
																${template ? `<ol>${template}</ol>` : `<p>{Address}</p>`}
																${scope.hover_footer_text() ? `<hr /><p>${scope.hover_footer_text()}</p>` : '<p>Rating: {StarRating}</p>'}
															<div>`;
												}
											}
										}
									},

								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
									.name("Map")
									.options({
										"layerSources": {
											"$scopePath": "LayerStore.allLayers",
										},
										autoZoomExtents: true,
										"layers": {} 
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: title,
											size:"Fixed"

										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "Right" })
											.children([
												mdash.controls.StackPanel.builder().children([
													
													mdash.controls.Html.builder().options({
														html: "<h3>Set hover text</h3>"
													}),
													ctrl.BPTextInput.builder()
														.options({
															
														}).name("hover_text"),
													mdash.controls.Html.builder().options({
														html: "<h3>Hover Subtext Rows</h3>"
													}),
													ctrl.BPTagInput.builder().name('hover_template_texts')
														.options({
															placeholder: "Add hover subtext row",
															onAdd: [{
																value: 'Row 1',
																onRemove: function() {}
															}],
															addOnBlur: function () {},
															onRemove: function () {},
														}).name("hover_template_texts"),
													mdash.controls.Html.builder().options({
														html: "<h3>Hover footer text</h3>"
													}),
													ctrl.BPTextInput.builder()
														.options({
															
														}).name("hover_footer_text"),
													mdash.controls.Html.builder().options({
														html: "<h3>Dot Size(2-20)</h3>"
													}),
													ctrl.BPNumericInput.builder().name('size').options({
														min: 2,
														max: 20,
														clampValueOnBlur: true,
														leftIcon: "ruler",
														numericOnly: true,
														majorStepSize: 2,
														minorStepSize: 0.5,
														placeholder: "Dot Size"
													}).name('size'),
													mdash.controls.Html.builder().options({
														html: "<h3>Show only from country</h3>"
													}),
													ctrl.BPTextInput.builder()
														.options({
															
														}).name("country")
														.transforms([
															mdash.transforms.Filter.builder().options({
																source: "country",
																column: "CountryName",
																dest: "hotels",
																test: "Equal"
														})
													]),
													mdash.controls.Html.builder().options({
														html: "<h3>Password Input (no effect on map)</h3>"
													}),
													ctrl.BPTextInput.builder()
														.options({
															type: 'password',
														}).name("password"),
													mdash.controls.Html.builder().options({
														html: "<h3>Notes (no effect on map)</h3>"
													}),
													ctrl.BPTextArea.builder().options({

													}).name('notes')
												])
											])
									])
							])
					])
			]).toJSON();
	}

	function setFillColor(scope, color) {
		console.log(scope);
		scope.fillColor(color);
	}

}