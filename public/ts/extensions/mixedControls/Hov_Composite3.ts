/// <reference path="../../_references.ts"/>

namespace dashboard.extensions.HM_Composite3 {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.HM_Composite3',
		name: 'Main Custom DB test for All dashboards',
		description: 'Button,Collapse'
	});

	var title = "Inventorys";
	let i;

	export function getJSON() {

		registerFormatFunctions();

		return mdash.controls.ScopeDefaults.builder()
			.cssClass("mixed-controls")
			.options({
				defaults: [
					// {path:"outlineColor", value:"Orange"},
					{path:"fillColor", value:""},
					{path:"r", value:0},
					{path:"g", value:0},
					{path:"b", value:0},
				]
			}).transforms([
			
			])
			.children([
				mdash.controls.DataStore.builder()
					.options({
						"sources": [
							{
								"name": "hotels",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							{
								"name": "hotels2",
								"label": "Hotels",
								"dataSource": "hms/hotels"
							},
							
						],
						"hidden": true
					})
					.transforms([
						mdash.transforms.RunQuery.builder().options({
							source: "hotels.dataSource",
							dest: "HotelCountryListPretty",
							transposeMethod: "TableTranspose",
							take: 1000
						})
					])
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({

								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},
										baseLayerOpts: {
											"name": "hotels",
											style: {
												method: "rules",
												rules: [
													{
														style: {
															fillColor: {$scopePath: scope => scope.fillColor()},
															// borderColor: {$scopePath: scope => scope.outlineColor()},
														},
														where: "CatchAll"
													},
												]
											},
											onHover: "template",
											hoverFieldsCommaDel: "CountryName,CityName,HotelName,HotelID,Address,StarRating,LATITUDE,LONGITUDE",
											hoverTemplate: {
												$scopePath: scope => {
													return `<div style="width:250px; padding:10px;border-radius:5px;">
																<dl>
																	${"CountryName,CityName,HotelName,HotelID,Address,StarRating,LATITUDE,LONGITUDE".split(',').map(c => {
																	return `<dt style="float:left; clear:both;padding-bottom:3px;">${c}:</dt>	<dd style="float:right">{${c}}</dd >`;
																	}).join('')}
																</dl>
															</div>`;
													
												}
											}
										}
									},
									
								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
								.name("Map")
								.options({
									"layerSources": {
										"$scopePath": "LayerStore.allLayers",
									},
									autoZoomExtents: true,
									"layers": {} ,
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: "Mixing External, Internal Controls with Transform",
											size:"Fixed"
											
										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "right" })
										.children([
											mdash.controls.StackPanel.builder().children([

												// ctrl.BPHTMLSelect.builder()
												// 	.name("outlineColor")
												// 	.options({
												// 		options: [
												// 			{label:"Red",value:"Red"},
												// 			{label:"Blue",value:"Blue"},
												// 			{label:"Green",value:"Green"},
												// 			{label:"Orange",value:"Orange"},
												// 		]
												// 	}),

												ctrl.BPCollapse.builder()
													.options({
														hideText:"Zoom OFF",
														showText: "Zoom ON",	
													}).children([
														ctrl.BPButtonGroup.builder()
															.options({
																btnData: [
																	{
																		label:"Plus", intent: ctrl.BPBase.Intent.Success,onClick: ({$rootScope}) => {i=$rootScope.Map.zoom(),i++,$rootScope.Map.zoom(i)}
																	},
																	{
																		label: {$scopePath: scope=>{
																			if(scope.Map.zoom()-1 >= 1 && scope.Map.zoom()-1 <= 20){
																				return scope.Map.zoom()-1
																			}
																			else{
																				scope.Map.zoom(1) 
																				return scope.Map.zoom()
																			}
																		}
																		}, intent: ctrl.BPBase.Intent.Default
																	},
																	{
																		label:"Minus", intent: ctrl.BPBase.Intent.Danger,onClick: ({$rootScope}) => {i=$rootScope.Map.zoom(),i--,$rootScope.Map.zoom(i)}
																	}
																]
															}),
													]),

												ctrl.BPTimeZonePicker.builder().options({
													disabled:false,
													showLocalTimeZone:true,
													standartDisplay: ctrl.BPTimeZonePicker.TimeZoneStandart.Composite,
												}),

												ctrl.BPToast.builder().options({
													modalPosition: ctrl.BPToast.AlignmentSelect.TopCenter,
													escClear:true,
													autoFocus:false,
													timeout: 5000,
													btnData: [
														{name:'Procuar toast', intent: ctrl.BPBase.Intent.Primary, modalText:"One toast created. "},
														{name:'Move files', intent: ctrl.BPBase.Intent.Success, modalText:"Moved 6 files.", replaceItemText:'You cannot undo the past.'},
														{name:'Delete Root', intent: ctrl.BPBase.Intent.Danger, modalText:"You do not have permissions to perform this action. Please contact your system administrator to request the appropriate access rights."},
														{name:'Log Out', intent: ctrl.BPBase.Intent.Warning, modalText:"Goodbye, old friend.",  replaceItemText:"Isn't parting just the sweetest sorrow?"},
													],
												}),

												ctrl.BPCard.builder().options({
												}).children([

													mdash.controls.Html.builder().options({
														html: "<h2>Red</h2>"
													}),
													ctrl.BPSlider.builder().name("r").options({max:255}),
													mdash.controls.Html.builder().options({
														html: "<h2>Green</h2>"
													}),
													ctrl.BPSlider.builder().name("g").options({max:255}),
													mdash.controls.Html.builder().options({
														html: "<h2>Blue</h2>"
													}),
													ctrl.BPSlider.builder().name("b").options({max:255}),
												
													mdash.controls.Html.builder().options({
														html: {$scopePath: scope => {
															scope.$rootScope.fillColor(`${scope.r()},${scope.g()},${scope.b()}`);
															rgbaToHex(scope);
															return `
																<h2 style='float:left'>Color</h2>
																<h2 style='float:right'>${scope.$rootScope.fillColor()}</h2>
																<div style='margin-bottom:15px;width:100%;border-radius:7px;height:10px;
																	background-color:${scope.fillColor()}'>
																</div>
															`
														}}
													}),
												])



											])
										])
									])
							])
					])
			]).toJSON()
	}
	
	function registerFormatFunctions() {
		
	}

	function rgbaToHex(scope){
		let rgb = `rgb(${(scope.fillColor())})`,
			hex;
		function rgb2hex(rgb){
			rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
			return (rgb && rgb.length === 4) ? "#" +
				("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
				("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
				("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
		}
			hex = rgb2hex(rgb)
			scope.$rootScope.fillColor(hex)
	}

	
}