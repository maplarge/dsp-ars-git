/// <reference path="../../_references.ts"/>

namespace dashboard.extensions.mixedExample {

    mdash.registerPublicDashboard({
        id: 'dashboard.extensions.mixedExample',
        name: 'Main Custom DB test for All dashboards',
        description: 'Main page for testing new Custom Controls built by Arman,Vahe,Hovhannes'
    });

    var title = "Inventorys";

    export function getJSON() {

        registerFormatFunctions();

        return mdash.controls.ScopeDefaults.builder()
            .cssClass("mixed-controls")
            .options({
                defaults: [
                    {path: "fillColor", value: "red"},
                    {path: "displayDataOnMap", value: true},
					{path: "size", value: 10},
                    {path: "shape", value: true},
                    {path: "outlineHidden", value: true},
                    {path: "strokeColor", value: "#000000"},
                    {path: "selected_column", value: "CountryName"}
                ]
            })
            .children([
                mdash.controls.DataStore.builder()
                    .options({
                        "sources": [
                            {
                                "name": "hotels",
                                "label": "Hotels",
                                "dataSource": "hms/hotels"
                            },
                            {
                                "name": "hotels2",
                                "label": "Hotels",
                                "dataSource": "hms/hotels"
                            },
                            {
                                name: "HotelCountries",
                                label: "Hotel Countries",
                                dataSource: {
                                    "withgeo": true,
                                    "start": 0,
                                    "take": 100,
                                    "table": {
                                        "name": "hms/hotels"
                                    },
                                    "sqlselect": [
                                        "CountryCode as id",
                                        "CountryFileName as item"
                                    ],
                                    "groupby": [
                                        "CurrencyCode"
                                    ]
                                }
                            }
                        ],
                        "hidden": true
                    })
                    .transforms([
                        mdash.transforms.RunQuery.builder().options({
                            source: "hotels.dataSource",
                            dest: "HotelCountryListPretty",
                            transposeMethod: "TableTranspose",
                            take: 1000
                        })
                    ])
                    .children([
                        mdash.controls.LayerStore.builder()
                            .name("LayerStore")
                            .options({

                                "hidden": true,
                                "layers": [
                                    <mdash.controls.LayerStore.ILayerOptionsDefinition>{
                                        id: "hotels",

                                        dataSource: {
                                            "$scopePath": "hotels"
                                        },
                                        baseLayerOpts: {
                                            "name": "hotels",
                                            visible: "{{displayDataOnMap}}",
                                            style: {
                                                method: "rules",
                                                rules: [{
                                                    style: {
                                                        borderWidth: {$scopePath: scope => !scope.outlineHidden() ? 0 : 1},
                                                        fillColor: {$scopePath: scope => scope.fillColor()}, // "#00ffff",
                                                        color: {$scopePath: scope => scope.fillColor().split(" ")[1]}, // "#00ffff",
                                                        shape: {$scopePath: scope => scope.shape() ? "round" : "rectangle"}, // "#00ffff",
                                                        size: {$scopePath: scope => scope.size()},
                                                        borderColor: {$scopePath: scope => scope.strokeColor()}
                                                    },
                                                    where: "CatchAll"
                                                }
                                                ]
                                            },
                                            onHover: "template",
                                            hoverFieldsCommaDel: "HotelName,HotelID,Address,StarRating",
                                            hoverTemplate: {
                                                $scopePath: scope => {
                                                    return `<div style="width:200px;">
																<dl>
																	<dt>Hotel Name</dt>	<dd>{HotelName}</dd>
																	<dt>Address</dt>	<dd>{Address}</dd>
																	<dt>Star Rating</dt><dd>{StarRating}</dd>
																</dl>
															<div>`;
                                                }
                                            }
                                        }
                                    },

                                ]
                            })
                            .cssClass("ml-dash-spacing-none")
                            .children([
                                mdash.controls.Map.builder()
                                    .name("Map")
                                    .options({
                                        "layerSources": {
                                            "$scopePath": "LayerStore.allLayers",

                                        },
                                        autoZoomExtents: true,
                                        "layers": {}
                                    })
                                    .children([
                                        mdash.controls.FloatingPanel.builder().options({
                                            heading: "Mixing External, Internal Controls with Transform",
                                            size: "Fixed"

                                        }).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{position: "Right"})
                                            .children([
                                                mdash.controls.StackPanel.builder().children([
                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Switch (External Control)</h3>"
                                                    }),
                                                    ctrl.BPSwitch.builder().name('displayDataOnMap')
                                                        .options({
                                                            label: "Display Hotels"
                                                        }),

                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Dot Size (External Control)</h3>"
                                                    }),
                                                    ctrl.BPSlider.builder().name('size').options({
                                                        min: 1,
                                                        max: 15,
                                                        step: 1,
                                                        showTrackFill: true,
                                                    }),

                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Fill Color (External Control)</h3>"
                                                    }),

                                                    ctrl.BPSelect.builder().name('fillColor').options({

                                                        items: [
                                                            {label: "Red", value: '#ff0000'},
                                                            {label: "Green", value: '#00ff00'},
                                                            {label: "Blue", value: '#0000ff'},
                                                            {label: "Orange", value: '#C87A2D'},
                                                        ]
                                                    }),

                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Border Color (External Control)</h3>"
                                                    }),

                                                    ctrl.BPRadioGroup.builder().name('strokeColor').options({
                                                        align: ctrl.BPBase.Alignment.Right,
                                                        radioData: [
                                                            {value: "#ff0000", label: "Red"},
                                                            {value: "#00ff00", label: "Green"},
                                                            {value: "#0000ff", label: "Blue"},
                                                            {value: "#C87A2D", label: "Orange"},
                                                        ],

                                                    }),

                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Column Picker (Internal Control)</h3>"
                                                    }),
                                                    mdash.controls.ColumnPicker.builder()
                                                        .name('selected_column')
                                                        .options({
                                                            label: "",
                                                            datasource: {$scopePath: "hotels2.dataSource"}
                                                        }),

                                                    mdash.controls.Html.builder().options({
                                                        html: "<h3>Column Value Picker (Internal Control w/ Filter transform)</h3>"
                                                    }),
                                                    mdash.controls.ColumnValuePicker.builder()
                                                        .options({
                                                            label: "",
                                                            datasource: {$scopePath: "hotels2.dataSource"},
                                                            column: "{{selected_column}}",
                                                            includeCount: true
                                                        }).name("selected_val")
                                                        .transforms([
                                                            mdash.transforms.Filter.builder().options({
                                                                source: "Text",
                                                                column: "{{selected_column:setMinMax(1,4)}}",
                                                                dest: "hotels",
                                                                test: "Equal"
                                                            })
                                                        ]),

                                                ])
                                            ])
                                    ])
                            ])
                    ])
            ]).toJSON();
    }

    function registerFormatFunctions() {

        ml.ui.dashboard.registerFormatFunction("setMinMax", (data: any, min: number, max: number) => {
            console.log(data, min, max);
            return "";
        }, ml.ui.dashboard.ScopeDataType.STRING, ml.ui.dashboard.ScopeDataType.STRING);
    }

}