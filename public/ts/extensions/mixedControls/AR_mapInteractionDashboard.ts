/// <reference path="../../_references.ts"/>
/// <reference path="../../_statics.ts"/>

namespace dashboard.extensions.AR_mapInteractions {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.AR_mapInteractions',
		name: 'mapInteraction',
		description: 'Display of dropdowns,buttons,spinners,card component,switch and etc.'
	});

	var title = "Map Interaction";
	let latitude: number;
	let longitude: number;

	function getLatLng() {
		return {
			latitude: latitude,
			longitude: longitude
		};
	}

	navigator.geolocation.getCurrentPosition(position => {
		latitude = position.coords.latitude; 
		longitude = position.coords.longitude;
	});

	export function getJSON() {
		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
					{ path: "latitude", value: latitude },
					{ path: "longitude", value: longitude },
					{ path: "displayDataOnMap", value: true },
					{ path: "selectedCity", value: ''},
					{ path: "selectedCountry", value: '' },
					{ path: "startCount", value: 0 },
					{ path: "cityStartCount", value: 0 },
					{ path: "countries", value: [] },
					{ path: 'cities', value: [] },
					{ path: "hotels_by_name", value: [] },
					{ path: "hotels_in_country", value: null },
					{ path: "cities_in_country", value: null },
					{ path: "selected_hotel", value: null },
				]
			}).transforms([
				mdash.transforms.RunQuery.builder().options({
					source: "countries_ds.dataSource",
					dest: "countries",
					transposeMethod: "TableTranspose",
					take: 1000,
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "cities_by_name_ds.dataSource",
					dest: "cities",
					transposeMethod: "TableTranspose",
					take: 1000,
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "hotels_by_name_ds.dataSource",
					dest: "hotels_by_name",
					transposeMethod: "TableTranspose",
					take: 1000,
				}),
				mdash.transforms.RunQuery.builder().options({
					source: "selected_hotel_ds.dataSource",
					dest: "selected_hotel",
					transposeMethod: "TableTranspose",
					take: 1,
				})

			])
			.children([
				mdash.controls.DataStore.builder()
					.options({
						sources: [
							{
								name: "hotels",
								label: "Hotels",
								dataSource: "hms/hotels"
							},
							{
								name: "hotels2",
								label: "Hotels",
								dataSource: "hms/hotels"
							},
							{
								name: "hotels_by_name_ds",
								dataSource: {
									withgeo: true,
									start: <any>"{{startCount}}",
									take: 1000,
									table: "{{hotels2:asSubQuery()}}",
									sqlselect: [
										"HotelID as value",
										"HotelName as label",
										"CityName",
										"CityID",
										"CityFileName",
										"CountryName",
										"CountryCode",
										"LATITUDE as lat",
										"LONGITUDE as lng",
										"Address",
										"StarRating",
									],
									orderby: ["HotelName.asc"]
								}
							},
							{
								name: 'cities_by_name_ds',
								dataSource: {
									withgeo: true,
									start: <any>"{{cityStartCount}}",
									take: 1000,
									table: "{{hotels2:asSubQuery()}}",
									sqlselect: [
										"CountryName",
										"CityID as value",
										"CityName as label",
										"CityFileName",
									],
									orderby: ["CityName.asc"],
									groupby: [
										'CityFileName'
									]
								}
							},
							{
								name: "selected_hotel_ds",
								dataSource: {
									withgeo: true,
									start: 0,
									take: 1,
									table: {
										name: "hms/hotels"
									},
								}
							},
							{
								name: "countries_ds",
								label: "Hotel Countries",
								dataSource: {
									withgeo: true,
									start: 0,
									take: 100,
									table: {
										name: "hms/hotels"
									},
									sqlselect: [
										"CountryCode as value",
										"CountryName as label",
									],
									orderby: ["CountryName.asc"],
									groupby: [
										"CountryCode"
									]
								}
							}
						],
						hidden: true
					})
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({
								"hidden": true,
								"layers": [
									<mdash.controls.LayerStore.ILayerOptionsDefinition>{
										id: "hotels",

										dataSource: {
											"$scopePath": "hotels"
										},
										baseLayerOpts: {
											"name": "hotels",
											visible: "{{displayDataOnMap}}",
											style: {
												method: "rules",
												rules: [
													{
														style: {
															fillColor: "blue",
														},
														where: [[
															{ col: "HotelID", test: statics.operators.eq.toString(), value: "{{selected_hotel.0.HotelID}}" }
														]]
													},
													{
														style: {
															fillColor: "skyblue",
														},
														where: [[
															{ col: "StarRating", test: statics.operators.greaterOr.toString(), value: 0 },
															{ col: "StarRating", test: statics.operators.lessOR.toString(), value: 3 }
														]]
													},
													{
														style: {
															fillColor: "orange",
															size: 16,
															borderColor: "black"
														},
														where: [[
															{ col: "StarRating", test: statics.operators.eq.toString(), value: 6 },
														]]
													},
													{
														style: {
															fillColor: "red",
														},
														where: "CatchAll"
													},
													
												]
											},
											onHover: "template",
											hoverFieldsCommaDel: "CountryName,HotelName,Address,StarRating,LATITUDE,LONGITUDE",
											hoverTemplate: `
															<div style="width:200px; box-shadow: 0 0 15px #000; height: 100%; padding: 10px; transform-style: preserve-3d; animation: rotateAround 0.75s; background: white;">
																<h3 style="text-align:center; margin-top: 0;">Hotel Info</h1>
																<p><strong>Country</strong>: {CountryName}</p>
																<p><strong>Hotel</strong>: {HotelName}</p>
																<p><strong>Latitude</strong>: {LATITUDE}</p>
																<p><strong>Longitude</strong>: {LONGITUDE}</p>
																<p><strong>Address</strong>: {Address}</p>
																<p style="margin-bottom: 0;"><strong>Star Rating</strong>: <span style="color: blue;">{StarRating}</span></p>
															<div>`
										}
									},

								]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
									.name("Map")
									.options({
										"layerSources": {
											"$scopePath": "LayerStore.allLayers",
										},
										autoZoomExtents: true,
										"layers": {},
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: title,
											size:"Fixed"

										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "Right" })
											.children([
												mdash.controls.StackPanel.builder().children([

													mdash.controls.Html.builder().options({
														html: "<h1>Small interactions</h1>"
													}),

													ctrl.BPCollapse.builder().options({
														showText: "Show interactions",
														hideText: "Hide interactions",
													}).children([
														ctrl.BPSwitch.builder().options({
															label: "Toggle Dots"
														}).name('displayDataOnMap').visible({ $scopePath: (scope) => scope.hotels_by_name_ds.dataSource() }),
	
														ctrl.BPButton.builder().options({
															iconsOnly: true,
															icon: "MapLarge-icons_annotations",
															intent: ctrl.BPBase.Intent.Default,
															onClick: scope => {
																scope.Map.center({ lat: getLatLng().latitude, lng: getLatLng().longitude });
																scope.Map.zoom(18);
															},
														}).visible({ $scopePath: scope => scope.hotels_by_name_ds.dataSource() })
													]),

													ctrl.BPSpinner.builder().options({
														circleSize: 20
													}).visible({ $scopePath: scope => { 
															document.getElementById("tooltip0").classList.add('transparent');
															return ml.util.isNullUndefinedOrEmpty(scope.countries());
														} }),

													mdash.controls.Html.builder().options({
														html: "<h1>Countries:</h1>"
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.countries()) }),

													ctrl.BPSuggest.builder().name('selectedCountry').options({
														items: { $scopePath: "countries" },
														intent: ctrl.BPBase.Intent.Primary
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.countries()) })
													  .transforms([
															mdash.transforms.Filter.builder().options({
																source: "selectedCountry",
																column: "CountryName",
																dest: "hotels",
																test: "Equal"
															}),

															mdash.transforms.Filter.builder().options({
																source: "selectedCountry",
																column: "CountryName",
																dest: "hotels2",
																test: "Equal"
															}),
															
															mdash.transforms.Set.builder().options({
																source: "selectedCountry",
																dest: "startCount",
																value: 0,
																setOnInit: true
															}),

															mdash.transforms.Set.builder().options({
																source: "selectedCountry",
																dest: "selectedCity",
																value: "",
																setOnInit: true
															}),

															mdash.transforms.Set.builder().options({
																source: "selectedCountry",
																dest: "startCount",
																value: 0,
																setOnInit: true
															}),

															mdash.transforms.BuildObject.builder().options({
																dest: "hotels_in_country",
																template: {
																	length: "{{hotels_by_name_ds:count}}"
																},
															}),
													]),

													ctrl.BPSpinner.builder().options({
														circleSize: 20
													}).visible({ $scopePath: scope => ml.util.isNullUndefinedOrEmpty(scope.cities()) }),

													mdash.controls.Html.builder().options({
														html: "<h1>Cities in {{selectedCountry}}:</h1>"
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) }),

													ctrl.BPSuggest.builder().name('selectedCity').options({
														items: { $scopePath: "cities" },
														intent: ctrl.BPBase.Intent.Primary
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) })
													.transforms([
														mdash.transforms.Filter.builder().options({
															source: "selectedCity",
															column: "CityName",
															dest: "hotels_by_name_ds",
															test: "Equal"
														}),

														mdash.transforms.Filter.builder().options({
															source: "selectedCity",
															column: "CityName",
															dest: "hotels",
															test: "Equal"
														}),

														mdash.transforms.Set.builder().options({
															source: "selectedCity",
															dest: "startCount",
															value: 0,
															setOnInit: true
														}),

														mdash.transforms.Set.builder().options({
															source: "selectedCity",
															dest: "selectedHotelList",
															value: '',
															setOnInit: true
														}),

														mdash.transforms.BuildObject.builder().options({
															dest: "cities_in_country",
															template: {
																length: "{{cities_by_name_ds:count}}"
															},
														}),

													]),

													ctrl.BPButton.builder().options({
														label: "Show previous 1000 cities from {{selectedCountry}} in the dropdown",
														intent: ctrl.BPBase.Intent.Primary,
														onClick: scope => {
															scope.cityStartCount(scope.cityStartCount() - 1000);
														},
													}).visible({ $scopePath: scope => scope.selectedCountry() && scope.cityStartCount() }),
													
													ctrl.BPButton.builder().options({
														label: "Show the next 1000 cities from {{selectedCountry}} in the dropdown",
														intent: ctrl.BPBase.Intent.Primary,
														onClick: scope => {
															scope.cityStartCount(scope.cityStartCount() + 1000);
														},
													}).visible({ $scopePath: scope => scope.selectedCountry() && scope.cityStartCount() + 1000 < scope.cities_in_country().length }),

													ctrl.BPSpinner.builder().options({
														circleSize: 20
													}).visible({ $scopePath: scope => ml.util.isNullUndefinedOrEmpty(scope.hotels_by_name()) }),

													mdash.controls.Html.builder().options({
														html: "<h1>Hotels from {{selectedCity}},{{selectedCountry}}:</h1>"
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) }),
													
													ctrl.BPSuggest.builder().options({
														items: { $scopePath: "hotels_by_name" },
														intent: ctrl.BPBase.Intent.Primary,
													}).name('selectedHotelList')
													  .visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedCountry()) })
														.transforms([
															mdash.transforms.Filter.builder().options({
																source: "selectedHotelList",
																dest: "selected_hotel_ds",
																test: statics.operators.eq.toString(),
																column: "HotelName",
															}),
															
															mdash.transforms.BuildObject.builder().options({
																dest: "Map.center",
																template: {
																	lat: {
																		$scopePath: scope => scope.selected_hotel() && ml.util.isNotNullUndefinedOrEmpty(scope.selectedHotelList())
																			? scope.selected_hotel()[0].LATITUDE : getLatLng().latitude
																	},
																	lng: {
																		$scopePath: scope => scope.selected_hotel() && ml.util.isNotNullUndefinedOrEmpty(scope.selectedHotelList())
																			? scope.selected_hotel()[0].LONGITUDE : getLatLng().longitude
																	}
																}
															}),

															mdash.transforms.Set.builder().options({
																source: "selectedHotelList",
																dest: "Map.zoom",
																value: 18,
																setOnInit: true
															}),
														]),

													ctrl.BPButton.builder().options({
														label: "Show previous 1000 hotels from {{selectedCity}},{{selectedCountry}} in the dropdown",
														intent: ctrl.BPBase.Intent.Primary,
														onClick: scope => {
															scope.startCount(scope.startCount() - 1000);

														},
													}).visible({ $scopePath: scope => scope.selectedCountry() && scope.selectedCity() && scope.startCount() }),
													
													ctrl.BPButton.builder().options({
														label: "Show the next 1000 hotels from {{selectedCity}},{{selectedCountry}} in the dropdown",
														intent: ctrl.BPBase.Intent.Primary,
														onClick: scope => {
															scope.startCount(scope.startCount() + 1000);
														},
													}).visible({ $scopePath: scope => scope.selectedCountry() && scope.selectedCity() && scope.startCount() + 1000 < scope.hotels_in_country().length }),

													mdash.controls.Html.builder().options({
														html: "<h1>Hotel Info:</h1>"
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selected_hotel()) }),

													ctrl.BPCard.builder().options({
														elevation: ctrl.BPBase.Elevation.Two,
														interactive: true
													}).visible({ $scopePath: scope => ml.util.isNotNullUndefinedOrEmpty(scope.selectedHotelList()) })
														.children([
															
														mdash.controls.Html.builder().options({
															html: { $scopePath: scope => `<h3>Country: ${scope.selected_hotel()[0].CountryName}</h3>` }
														}),

														mdash.controls.Html.builder().options({
															html: { $scopePath: scope => `<h3>City: ${scope.selected_hotel()[0].CityName}</h3>` }
														}),

														mdash.controls.Html.builder().options({
															html: { $scopePath: scope => `<h3>Number of hotels in ${scope.selectedCity()}: ${scope.hotels_in_country().length}</h3>` }
														}),
														
														mdash.controls.Html.builder().options({
															html: { $scopePath: scope => `<h3>Hotel: ${scope.selected_hotel()[0].HotelName}</h3>` } 
														}),
														
														mdash.controls.Html.builder().options({
															html: { $scopePath: scope => `<h3>Star Rating: ${scope.selected_hotel()[0].StarRating}</h3>` }
														}),
														
														mdash.controls.Html.builder().options({
															html: { $scopePath: scope => `<h3>Address: ${scope.selected_hotel()[0].Address}</h3>` }
														}),

													])
												])
											])
									])
							])
					])
			]).toJSON();
			
	}

}