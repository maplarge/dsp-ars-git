/// <reference path="../_references.ts"/>

namespace dashboard.extensions.arman {


	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.arman',
	//	name: 'Main Custom DB test for Arman',
	//	description: 'Main page for testing new Custom Controls built by Arman (or whatever he decides to put in here).'
	//});

	var title = "Arman"
	

	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
					{ path: "text_val", value: "Initial Text Value" }
				]
			})
			.children([
				mdash.controls.DataStore.builder()
					.name('DataStore')
					.options({
						sources: [
							{
								name: "Hotels",
								label: "Hotels",
								dataSource: "hms/hotels"
							},
							{
								name: "HotelVals",
								label: "HotelVals",
								dataSource: {
									table: "{{DataStore.dataSource:asSubQuery}}",
									sqlselect: [
										'CountryName'
									]
								}
							}
						],
						hidden: true
					})
					.children([
						mdash.controls.LayerStore.builder()
							.name("LayerStore")
							.options({
								"hidden": true,
								"layers": []
								//"layers": [
								//	<mdash.controls.LayerStore.ILayerOptionsDefinition>{
								//		id: "Hotels",
								//		dataSource: { $scopePath: "Hotels" },
								//		baseLayerOpts: {
								//			name: "Hotels",
								//			style: {
								//				method: "rules",
								//				rules: [{
								//					style: {
								//						size: "{{size.valueMin}}",
								//						borderWidth: 0
								//					}
								//				}
								//				]
								//			}
								//		}
								//	}

								//]
							})
							.cssClass("ml-dash-spacing-none")
							.children([
								mdash.controls.Map.builder()
									.name("Map")
									.options({
										layerSources: { $scopePath: "LayerStore.allLayers" },
										layers: {}
									})
									.children([
										mdash.controls.FloatingPanel.builder().options({
											heading: title,
											size: "Auto"

										}).layoutOptions(<mdash.controls.Map.IChildOptionsDefinition>{ position: "Right" })
											.children([
												mdash.controls.StackPanel.builder().children([

													mdash.controls.ScopeDefaults.builder().options({
														defaults: [
															{ path: "ti_fill", value: true },
															{ path: "ti_intent", value: ctrl.BPBase.Intent.Success },
															{ path: "ti_minimal", value: true }
														]
													}).children([

														mdash.controls.Grid.builder().options({
															rows: ["1fr", "1fr", "1fr", "1fr"],
															columns: ["200px", "150px"]
														}).children([

															mdash.controls.StackPanel.builder().children([
																ctrl.BPTextInput.builder().options({
																	fill: { $scopePath: "ti_fill" },
																	intent: { $scopePath: "ti_intent" },

																})
															]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 1, rowEnd: 5 }),

															mdash.controls.StackPanel.builder().children([
																ctrl.BPCheckbox.builder().name("ti_fill").options({
																	label: "Fill"
																}),

																mdash.controls.RadioGroup.builder().name('ti_intent').options({
																	options: [
																		{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
																		{ value: ctrl.BPBase.Intent.Default, label: "Default" },
																		{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
																		{ value: ctrl.BPBase.Intent.Success, label: "Success" },
																		{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
																	]
																}),

																ctrl.BPButton.builder().name('ti_minimal').options({
																	label: 'Something',
																	intent: ctrl.BPBase.Intent.Primary,
																	minimal: {$scopePath: "ti_minimal"},
																}),

																ctrl.BPButtonGroup.builder().options({

																	btnData: [

																	]
																}),

																//ctrl.BPRadioGroup.builder().name('ti_intent').options({
																//	radioData: [
																//		{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
																//		{ value: ctrl.BPBase.Intent.Default, label: "Defaul" },
																//		{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
																//		{ value: ctrl.BPBase.Intent.Success, label: "Sucecess" },
																//		{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
																//	]
																//})

															]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 1, rowEnd: 5 })
															]),

														mdash.controls.Html.builder().options({
															html: "{{ti_intent}}"
														}),

														ctrl.BPButton.builder().options({
															label: "button",
															intent: ctrl.BPBase.Intent.Primary
														})
													])
												])

											])
									])
							])
					])
			]).toJSON();


	}
}