namespace dashboard.extensions.progress {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	// slider is under construction

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.progress',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing new Custom Controls'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([
				bp.BPProgress.builder().options({
					intent: bp.BPBase.Intent.Success,
					// bp.BPBase.Intent.Success
					rangeWidth:10,
					rangeDisable:true
				}),

			]).toJSON();
	}
}