namespace dashboard.extensions.toast {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	// slider is under construction

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.toast',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing new Custom Controls'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([

				bp.BPToast.builder().options({
					modalPosition: bp.BPToast.AlignmentSelect.TopCenter,
					btnData: [
						{name:'Procuar toast', intent: bp.BPBase.Intent.Primary, modalText:"One toast created. "},
						{name:'Move files', intent: bp.BPBase.Intent.Success, modalText:"Moved 6 files.", replaceItemText:'You cannot undo the past.'},
						{name:'Delete Root', intent: bp.BPBase.Intent.Danger, modalText:"You do not have permissions to perform this action. Please contact your system administrator to request the appropriate access rights."},
						{name:'Log Out', intent: bp.BPBase.Intent.Warning, modalText:"Goodbye, old friend.",  replaceItemText:"Isn't parting just the sweetest sorrow?"},
					],
					autoFocus:false,
					escClear:true,
					timeout:7000
					
				}),

			]).toJSON();
	}
}