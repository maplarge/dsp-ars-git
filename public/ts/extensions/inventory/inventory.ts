/// <reference path="../../_references.ts"/>

namespace dashboard.extensions.inventory {

	mdash.registerPublicDashboard({
		id: 'dashboard.extensions.inventory',
		name: 'Main Custom DB test for All dashboards',
		description: 'Main page for testing new Custom Controls built by Arman,Vahe,Hovhannes'
	});

	var title = "Inventorys";

	var gridOptions = {
		rows: ["auto", "1fr"],
		columns: ["450px", "400px", "1fr", "200px"],
		columnGap: "20px"
	};

	function buildControlSection(control: mdash.ControlDefinitionBuilder<any>, valueTemplate?: string): mdash.ControlDefinitionBuilder<any> {

		var title = (<any>control).control.replace(/([A-Z])/g, ' $1')
			// uppercase the first character
				.replace(/^./, function (str) { return str.toUpperCase(); }),
			meta: ml.ui.dashboard.IControlMeta = null,
			description: string = '',
			childrenStatus: string = '',
			releaseStatus: string = '',
			noMeta = false,
			options = control.toJSON().options,
			textValue  = '';

		ml.ui.dashboard.setScopeValue((<any>control).name(), JSON.stringify(options, undefined, 2), (<any>dashboard).scope);

		//try {
		//	meta = ml.ui.dashboard.controls[(<any>controllers[0]).control].meta;
		//	switch (meta.childSupport) {
		//		case ml.ui.dashboard.ChildSupport.NONE: childrenStatus = "does <i>not</i> allow children."; break;
		//		case ml.ui.dashboard.ChildSupport.SINGLE: childrenStatus = "can have <i>one</i> declared child."; break;
		//		case ml.ui.dashboard.ChildSupport.MULTIPLE: childrenStatus = "can have declared children."; break;
		//	}

		//	switch (meta.status) {
		//		case ml.ui.dashboard.ControlReleaseStatus.ALPHA: releaseStatus = "Alpha"; break;
		//		case ml.ui.dashboard.ControlReleaseStatus.BETA: releaseStatus = "Beta"; break;
		//		case ml.ui.dashboard.ControlReleaseStatus.DEPRECATED: releaseStatus = "Deprecated"; break;
		//		case ml.ui.dashboard.ControlReleaseStatus.PRODUCTION: releaseStatus = "Production"; break;
		//	}
		//	description = meta.shortDescription;

		//} catch (e) {
		//	if (meta == undefined) {
		//		console.warn(title + " has no meta description.");
		//		noMeta = true;
		//	}
		//	meta = {
		//		status: null,
		//		level: null,
		//		friendlyName: null,
		//		shortDescription: null,
		//		categories: null,
		//		childSupport: null
		//	};
		//}

		return mdash.controls.Grid.builder().options(gridOptions).children([
			mdash.controls.Html.builder().options({
				html: `<h2 class='control-header'>${title}</h2>`
			}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),
			mdash.controls.StackPanel.builder().children([
				control
			]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),


			ctrl.BPTextArea.builder().name((<any>control).name() +"")
				.layoutOptions({ columnStart: 3, columnEnd: 4, rowStart: 2, rowEnd: 3 }),

			mdash.controls.StackPanel.builder()
				.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
				.children([
					mdash.controls.Html.builder().options({
						html: `<h2>Value:</h2><p>Switch is on: {{${(<any>control).name()}}}</p>`
					})
				])
		]);
	}

	export function getJSON() {

		let dashboardControls = [

			//buildControlSection(
			//	ctrl.BPSwitch.builder().name('switch_box').options({
			//		label: "Switch Demo",
			//		checked: { $scopePath: 'switch_checked' },
			//	})
			//	),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Switch</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPSwitch.builder().name('switch_box').options({
						label: "Switch Demo",
						checked: { $scopePath: 'switch_checked' },
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					// ctrl.BPSwitch.builder().name("switch_checked").options({
					// 	label: "Checked",
					// 	inline: true,
					// }),


				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>Switch is on: {{switch_box}}</p>"
						})
					])
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Checkbox</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPCheckbox.builder().name('check_box_val').options({
						label: "Checkbox Demo",
						align: { $scopePath: 'chbox_align' },
						large: { $scopePath: 'chbox_large' },
						checked: { $scopePath: 'chbox_checked' },
						indeterminate: { $scopePath: 'chbox_indeterminate' },
						disabled: { $scopePath: 'chbox_disabled' },
						inline: { $scopePath: 'chbox_inline' }
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("chbox_large").options({
						label: "Large",
						inline: true,
					}),

					// ctrl.BPSwitch.builder().name("chbox_checked").options({
					// 	label: "Checked",
					// 	inline: true,
					// }),

					ctrl.BPSwitch.builder().name("chbox_indeterminate").options({
						label: "Indeterminate",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("chbox_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Alignment</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('chbox_align').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Alignment.Left, label: "Left align" },
							{ value: ctrl.BPBase.Alignment.Right, label: "Right align" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>Check box is checked: {{check_box_val}}</p>"
						})
					])
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Radio Button</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPRadioButton.builder().name('radio_btn').options({
						label: "Radio",
						checked: { $scopePath: 'radio_checked' },
						disabled: { $scopePath: 'radio_disabled' },
						large: { $scopePath: 'radio_large' },
						align: { $scopePath: 'radio_align' },
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("radio_checked").options({
						label: "Checked",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("radio_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("radio_large").options({
						label: "Large",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Alignment</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('radio_align').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Alignment.Left, label: "Left align" },
							{ value: ctrl.BPBase.Alignment.Right, label: "Right align" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Radio Group</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPRadioGroup.builder().name('radio_group').options({
						disabled: { $scopePath: 'radioGroup_disabled' },
						large: { $scopePath: 'radioGroup_large' },
						align: { $scopePath: 'radioGroup_align' },
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("radioGroup_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("radioGroup_large").options({
						label: "Large",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Alignment</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('radioGroup_align').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Alignment.Left, label: "Left align" },
							{ value: ctrl.BPBase.Alignment.Right, label: "Right align" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{radio_group}}</p>"
						})
					])
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Text Input</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPTextInput.builder().name('text_input').options({

						fill: { $scopePath: "ti_fill" },
						disabled: { $scopePath: "ti_disabled" },
						large: { $scopePath: "ti_isLarge" },
						round: { $scopePath: "ti_isRounded" },
						placeholder: { $scopePath: "ti_placeholder" },
						//icon:"", // "mlicon-money369",
						//iconLeft: { $scopePath: "ts_iconLeft" },
						//iconRight: { $scopePath: "ti_iconRight" },
						intent: {
							$scopePath: scope => Array.isArray(scope.ti_intent()) ?
								scope.ti_intent().filter(m => m.checked)[0].value :
								scope.ti_intent()
						},

					})
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("ti_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("ti_isLarge").options({
						label: "Large",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("ti_isRounded").options({
						label: "Rounded Borders",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("ti_fill").options({
						label: "Fill",
						inline: true,
					}),


					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('ti_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{text_input}}</p>"
						})
					])

			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Input Group</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPInputGroup.builder().options({
						large: { $scopePath: 'inputGroup_isLarge' },
						vertical: { $scopePath: 'inputGroup_isVertical' },
						fill: { $scopePath: 'inputGroup_isFill' },
						disabled: { $scopePath: 'inputGroup_isDisabled' },
						readonly: { $scopePath: 'inputGroup_isReadonly' },
						btnData: [
							{type: 'password', placeholder: 'password',intent: ctrl.BPBase.Intent.Warning ,iconRight: 'lock'},
							{type: 'text', placeholder: 'text', intent: ctrl.BPBase.Intent.Success, iconRight: 'arrow-right', iconLeft: 'search',},
							{type: 'search', placeholder: 'search', intent: ctrl.BPBase.Intent.Danger, iconLeft: 'search', round: true},
							{type: 'text', placeholder: 'filter histogram...', iconLeft: 'filter',intent: ctrl.BPBase.Intent.Primary, value: true, spin: true}
						]

					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("inputGroup_isLarge").options({
						label: "Large",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("inputGroup_isVertical").options({
						label: "Vertical",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("inputGroup_isFill").options({
						label: "Fill",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("inputGroup_isDisabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("inputGroup_isReadonly").options({
						label: "ReadOnly",
						inline: true,
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Number Input</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPNumericInput.builder().name('num_input').options({
						placeholder: { $scopePath: "numInput_placeholder" },
						fill: { $scopePath: "numInput_fill" },
						leftIcon: { $scopePath: "numInput_icon" },
						position: { $scopePath: 'numInput_position' },
						disabled: { $scopePath: 'numInput_disabled' },
						large: { $scopePath: 'numInput_large' },
						intent: { $scopePath: 'numInput_intent' },
						selectAllOnFocus: { $scopePath: 'numInput_selOnFocus' },
						selectAllOnIncrement: { $scopePath: 'numInput_selAllOnInc' },
						numericOnly: { $scopePath: 'numInput_numOnly' },
						clampValueOnBlur: { $scopePath: 'numInput_clampOnBlur' },
						minorStepSize: { $scopePath: 'numInput_minorStep' },
						majorStepSize: { $scopePath: 'numInput_majorStep' },
						stepSize: { $scopePath: 'numInput_step' },
						min: { $scopePath: 'numInput_min' },
						max: { $scopePath: 'numInput_max' },
						decimalPrecision: { $scopePath: 'numInput_decPrecision' },
						onButtonClick: (number, numberAsString) => {
							// do something when the mouse events happen
							return [number, numberAsString];
						},
						onValueChange: (number, numberAsString) => {
							// do something when the value changes by some action(mouse and keyboard events)
							return [number, numberAsString];
						}
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("numInput_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("numInput_large").options({
						label: "Large",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("numInput_fill").options({
						label: "Fill",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("numInput_selOnFocus").options({
						label: "Select all on focus",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("numInput_selAllOnInc").options({
						label: "Select all on increment",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("numInput_numOnly").options({
						label: "Numeric only",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("numInput_clampOnBlur").options({
						label: "Clamp value on blur",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Step(Click or Arrow up/down)</h2>"
					}),

					ctrl.BPNumericInput.builder().name('numInput_step').options({
						placeholder: 'Enter the normal step',
						position: ctrl.BPNumericInput.Position.Right,
						intent: { $scopePath: 'numInput_intent' },
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Minor step(Alt + Click or Alt + Arrow up/down)</h2>"
					}),

					ctrl.BPNumericInput.builder().name('numInput_minorStep').options({
						placeholder: 'Enter the minor step',
						position: ctrl.BPNumericInput.Position.Right,
						intent: { $scopePath: 'numInput_intent' },
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Major step(Shift + Click or Shift + Arrow up/down)</h2>"
					}),

					ctrl.BPNumericInput.builder().name('numInput_majorStep').options({
						placeholder: 'Enter the major step',
						position: ctrl.BPNumericInput.Position.Right,
						intent: { $scopePath: 'numInput_intent' },
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Min</h2>"
					}),

					ctrl.BPNumericInput.builder().name('numInput_min').options({
						placeholder: 'Enter the minimum',
						position: ctrl.BPNumericInput.Position.Right,
						intent: { $scopePath: 'numInput_intent' },
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Max</h2>"
					}),

					ctrl.BPNumericInput.builder().name('numInput_max').options({
						placeholder: 'Enter the maximum',
						position: ctrl.BPNumericInput.Position.Right,
						intent: { $scopePath: 'numInput_intent' },
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Amount of numbers after the decimal dot</h2>"
					}),

					ctrl.BPNumericInput.builder().name('numInput_decPrecision').options({
						placeholder: 'Amount of numbers after the decimal dot',
						position: ctrl.BPNumericInput.Position.Right,
						intent: { $scopePath: 'numInput_intent' },
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Placeholder for numeric input</h2>"
					}),

					ctrl.BPTextInput.builder().name('numInput_placeholder').options({
						placeholder: 'Numeric Input placeholder',
						intent: { $scopePath: 'numInput_intent' },
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>MapLarge icon without the 'mlicon-' at start</h2>"
					}),

					ctrl.BPTextInput.builder().name('numInput_icon').options({
						placeholder: "MapLarge icon without the 'mlicon-' at start",
						intent: { $scopePath: 'numInput_intent' },
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('numInput_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Button Position</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('numInput_position').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPNumericInput.Position.Left, label: "Left" },
							{ value: ctrl.BPNumericInput.Position.Right, label: "Right" },
							{ value: ctrl.BPNumericInput.Position.None, label: "None" }
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{num_input}}</p>"
						})
					])
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Button</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPButton.builder().options({
						label: 'Button Text',
						intent: {
							$scopePath: scope => Array.isArray(scope.btn_intent()) ?
								scope.btn_intent().filter(m => m.checked)[0].value :
								scope.btn_intent()
						},
						minimal: { $scopePath: scope => scope.btn_minimal() },
						loading: { $scopePath: "btn_isLoading" },
						large: { $scopePath: 'btn_isLarge' },
						disabled: { $scopePath: 'btn_disabled' },
						small: { $scopePath: 'btn_small' },
						active: { $scopePath: 'btn_active' },
						iconsOnly: { $scopePath: 'btn_iconsOnly' },
						icon: { $scopePath: 'btn_icon'}
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("btn_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("btn_isLarge").options({
						label: "Large",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("btn_isLoading").options({
						label: "Loading",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("btn_minimal").options({
						label: "Minimal",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("btn_active").options({
						label: "Active",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("btn_small").options({
						label: "Small",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("btn_iconsOnly").options({
						label: "Icons Only(Hides text)",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('btn_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Icon</h2>"
					}),

					ctrl.BPTextInput.builder().name('btn_icon').options({
						placeholder: `Button icon without the 'mlicon-' at start`,
						intent: ctrl.BPBase.Intent.Default,
						type: "text"
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })

			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Button Group</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPButtonGroup.builder().options({
						large: { $scopePath: 'btnGroup_isLarge' },
						minimal: { $scopePath: 'btnGroup_minimal' },
						small: { $scopePath: 'btnGroup_small' },
						iconsOnly: { $scopePath: 'btnGroup_iconsOnly' },
						// unfortunately, the layout option of the Repeater doesn't support $scopePath values
						// so can't align it vertically or horizontally when the switch is on/off
						vertical: { $scopePath: 'btnGroup_vertical' },
						btnData: [
							{ label: "Button 1", intent: { $scopePath: 'btnGroup_intent' }, icon: 'checked21' },
							{ label: "Button 2", intent: { $scopePath: 'btnGroup_intent' }, icon: 'balloon' },
							{ label: "Button 3", intent: { $scopePath: 'btnGroup_intent' }, icon: 'social' },
							{ label: "Button 4", intent: { $scopePath: 'btnGroup_intent' }, icon: 'json' },
						]
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("btnGroup_isLarge").options({
						label: "Large",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("btnGroup_minimal").options({
						label: "Minimal",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("btnGroup_small").options({
						label: "Small",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("btnGroup_iconsOnly").options({
						label: "Icons Only(Hides text)",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('btnGroup_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>File Input</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPFileInput.builder().options({
						label: { $scopePath: 'fileInput_label' },
						name: 'somefile',
						disabled: { $scopePath: 'fileInput_disabled' },
						large: { $scopePath: 'fileInput_large' },
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("fileInput_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("fileInput_large").options({
						label: "Large",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Update the label below</h2>"
					}),

					ctrl.BPTextInput.builder().name('fileInput_label').options({
						placeholder: 'Update fileInput label here',
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),


				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Callout</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPCallout.builder().options({
						hideHeader: { $scopePath: 'callout_hideHeader' },
						contentText: { $scopePath: 'callout_contentText' },
						headerText: { $scopePath: 'callout_headerText' },
						intent: { $scopePath: 'callout_intent' }
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("callout_hideHeader").options({
						label: "Hide header",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Update the header text below</h2>"
					}),

					ctrl.BPTextInput.builder().name('callout_headerText').options({
						placeholder: 'Update the callout header here',
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Update the content text below</h2>"
					}),

					ctrl.BPTextInput.builder().name('callout_contentText').options({
						placeholder: 'Update the callout content here',
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('callout_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Card</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPCard.builder().options({
						interactive: { $scopePath: 'card_interactive' },
						elevation: { $scopePath: 'card_elevation' }
					}).children([
						mdash.controls.Html.builder().options({
							html: "<h1>This is an HTML child of a Card</h1>"
						}),

						ctrl.BPButtonGroup.builder().options({
							btnData: [
								{ label: "These", intent: ctrl.BPBase.Intent.Success },
								{ label: "Are", intent: ctrl.BPBase.Intent.Primary },
								{ label: "Buttons in the", intent: ctrl.BPBase.Intent.Danger },
								{ label: "Card component", intent: ctrl.BPBase.Intent.Warning },
							]
						}),
					]),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("card_interactive").options({
						label: "Interactive",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Elevation</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('card_elevation').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Elevation.Zero, label: "Level 0" },
							{ value: ctrl.BPBase.Elevation.One, label: "Level 1" },
							{ value: ctrl.BPBase.Elevation.Two, label: "Level 2" },
							{ value: ctrl.BPBase.Elevation.Three, label: "Level 3" },
							{ value: ctrl.BPBase.Elevation.Four, label: "Level 4" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Slider (Single)</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSlider.builder().name("single_slider").options({
						disabled: { $scopePath: 'slider_disabled' },
						min: { $scopePath: 'slider_min' },
						max: { $scopePath: 'slider_max' },
						labelPercentages: { $scopePath: 'slider_percentages' },
						vertical: { $scopePath: 'slider_vertical' },
						step: { $scopePath: 'slider_step' },
						showTrackFill: { $scopePath: 'slider_showTrackFill' }
					})
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("slider_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("slider_vertical").options({
						label: "Vertical",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("slider_showTrackFill").options({
						label: "Show Track Fill",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Minimum</h2>"
					}),

					ctrl.BPNumericInput.builder().name('slider_min').options({
						placeholder: 'Enter the minimum',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Maximum</h2>"
					}),

					ctrl.BPNumericInput.builder().name('slider_max').options({
						placeholder: 'Enter the maximum',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Increase step by</h2>"
					}),

					ctrl.BPNumericInput.builder().name('slider_step').options({
						placeholder: 'Enter the number increase step',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						min: 0.1,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1,
						clampValueOnBlur: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Initial/Fixed value</h2>"
					}),

					ctrl.BPNumericInput.builder().name('slider_value').options({
						placeholder: 'Set a fixed value/initial value',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Values:</h2><p>{{single_slider}}</p>"
						})
					])
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Range Slider</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPRangeSlider.builder().name('range_slider').options({
						disabled: { $scopePath: 'rangeSlider_disabled' },
						min: { $scopePath: 'rangeSlider_min' },
						max: { $scopePath: 'rangeSlider_max' },
						labelPercentages: { $scopePath: 'rangeSlider_percentages' },
						vertical: { $scopePath: 'rangeSlider_vertical' },
						step: { $scopePath: 'rangeSlider_step' }
					})
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("rangeSlider_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("rangeSlider_vertical").options({
						label: "Vertical",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Minimum</h2>"
					}),

					ctrl.BPNumericInput.builder().name('rangeSlider_min').options({
						placeholder: 'Enter the minimum',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Maximum</h2>"
					}),

					ctrl.BPNumericInput.builder().name('rangeSlider_max').options({
						placeholder: 'Enter the maximum',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Increase step by</h2>"
					}),

					ctrl.BPNumericInput.builder().name('rangeSlider_step').options({
						placeholder: 'Enter the number increase step',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						min: 0.1,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1,
						clampValueOnBlur: true,
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),


				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Values:</h2><p>{{range_slider}}</p>"
						})
					])
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Multi-Slider</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPMultiSlider.builder().name('multi_slider').options({
						disabled: { $scopePath: 'multiSlider_disabled' },
						handles: [
							{value: 20, intentBefore: ctrl.BPBase.Intent.Danger},
							{value: 125, intentBefore: ctrl.BPBase.Intent.Success, intentAfter: ctrl.BPBase.Intent.Danger},
							{value: 73, intentBefore: ctrl.BPBase.Intent.Warning},
							{value: 42, intentBefore: ctrl.BPBase.Intent.Success},
						],
						min: { $scopePath: 'multiSlider_min' },
						max: { $scopePath: 'multiSlider_max' },
						labelPercentages: { $scopePath: 'multiSlider_percentages' },
						vertical: { $scopePath: 'multiSlider_vertical' },
						step: { $scopePath: 'multiSlider_step' },
						handleInteraction: { $scopePath: 'multiSlider_handleInteraction' }
					})
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("multiSlider_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("multiSlider_vertical").options({
						label: "Vertical",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Minimum</h2>"
					}),

					ctrl.BPNumericInput.builder().name('multiSlider_min').options({
						placeholder: 'Enter the minimum',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Maximum</h2>"
					}),

					ctrl.BPNumericInput.builder().name('multiSlider_max').options({
						placeholder: 'Enter the maximum',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Increase step by</h2>"
					}),

					ctrl.BPNumericInput.builder().name('multiSlider_step').options({
						placeholder: 'Enter the number increase step',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						min: 0.1,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1,
						clampValueOnBlur: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Handle Interaction</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('multiSlider_handleInteraction').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPMultiSlider.HandleInteraction.Lock, label: "Lock" },
							{ value: ctrl.BPMultiSlider.HandleInteraction.Push, label: "Push" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Values:</h2><p>{{multi_slider}}</p>"
						})
					])
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Omnibar</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPOmnibar.builder().name('omnibar').options({
						resetOnSelect: { $scopePath: 'omnibar_resetOnSelect' },
						intent: { $scopePath: 'omnibar_intent' },
						isShow: { $scopePath: 'omnibar_isShow' },
						items: inv_consts.selectItems
					}).children([
						mdash.controls.Html.builder().options({
							html: "<h2>This is a child inside a Omnibar</h2>"
						}),
					])

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([


					ctrl.BPSwitch.builder().name("omnibar_isShow").options({
						label: "Is show",
						inline: true,
						checked: true,
					}),

					ctrl.BPSwitch.builder().name("omnibar_resetOnSelect").options({
						label: "Reset On Select",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('omnibar_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{omnibar}}</p>"
						})
					])

			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Text Area</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPTextArea.builder().name('text_area').options({
						disabled: { $scopePath: 'textArea_disabled' },
						large: { $scopePath: 'textArea_large' },
						fill: { $scopePath: 'textArea_fill' },
						readonly: { $scopePath: 'textArea_readonly' },
						intent: { $scopePath: 'textArea_intent' },
					})
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("textArea_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("textArea_readonly").options({
						label: "Read Only",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("textArea_fill").options({
						label: "Fill",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("textArea_large").options({
						label: "Large",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('textArea_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),
				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{text_area}}</p>"
						})
					])
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Bread Crumbs</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPBreadcrumbs.builder().options({
						icon: { $scopePath: 'breadcrumbs_icon' },
						links: [
							{ href: 'https://www.google.com', label: 'Google', disabled: true },
							{ href: 'https://blueprintjs.com/docs/#core/components/breadcrumbs', label: 'Breadcrumbs Docs', target: '_blank' },
							{ href: 'http://qa.maplarge.com/dashboard.html?extension=Style.Guide', label: 'MapLarge Style Guide', target: '_blank' },
						],
						current: { $scopePath: 'breadcrumbs_current' }
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					mdash.controls.Html.builder().options({
						html: "<h2>MapLarge icon without the 'mlicon-'</h2>"
					}),

					ctrl.BPTextInput.builder().name('breadcrumbs_icon').options({
						placeholder: "MapLarge icon class without the 'mlicon-' at start",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Update the last span</h2>"
					}),

					ctrl.BPTextInput.builder().name('breadcrumbs_current').options({
						placeholder: 'Update the span',
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),
				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Collapse</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPCollapse.builder().options({
						hideText: { $scopePath: 'collapse_hideText' },
						showText: { $scopePath: 'collapse_showText' },
						isOpen: { $scopePath: 'collapse_isOpen' },
						transitionDuration: { $scopePath: 'collapse_transitionDuration' }
					}).children([
						mdash.controls.Html.builder().options({
							html: "<h2>This is a child inside a collapse</h2>"
						}),
					])

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPSwitch.builder().name("collapse_isOpen").options({
						label: "Is Open",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Button text for show</h2>"
					}),

					ctrl.BPTextInput.builder().name('collapse_showText').options({
						placeholder: 'Collapse show text',
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Button text for hide</h2>"
					}),

					ctrl.BPTextInput.builder().name('collapse_hideText').options({
						placeholder: 'Collapse hide text',
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Animation duration</h2>"
					}),

					ctrl.BPNumericInput.builder().name('collapse_transitionDuration').options({
						placeholder: 'Animation duration in milliseconds(ms)',
						leftIcon: 'information',
						position: ctrl.BPNumericInput.Position.Right,
						intent: ctrl.BPBase.Intent.Primary,
						min: 50,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1,
						clampValueOnBlur: true,
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>HTML Select</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPHTMLSelect.builder().name('html_select').options({
						fill: { $scopePath: 'htmlSelect_fill' },
						large: { $scopePath: 'htmlSelect_large' },
						minimal: { $scopePath: 'htmlSelect_minimal' },
						disabled: { $scopePath: 'htmlSelect_disabled' },
						options: [
							{ label: 'Option 1', value: '1' },
							{ label: 'Option 2', value: '2' },
							{ label: 'Option 3', value: '3' },
							{ label: 'Option 4', value: '4' }
						],
						onChange: () => {}
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPSwitch.builder().name("htmlSelect_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("htmlSelect_fill").options({
						label: "Fill",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("htmlSelect_large").options({
						label: "Large",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("htmlSelect_minimal").options({
						label: "Minimal",
						inline: true,
					}),


				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{html_select}}</p>"
						})
					])
			]),
			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Label</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPLabel.builder().name('label').options({

						disabled: { $scopePath: 'label_disabled' },
						inline: { $scopePath: 'label_inline' },
						label: { $scopePath: 'label_label' }
					})
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("label_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("label_inline").options({
						label: "Inline",
						inline: true,
					}),

					ctrl.BPTextInput.builder().name('label_label').options({
						placeholder: 'Label component label',
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),
				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{label}}</p>"
						})
					])

			]),

			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Tag</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPTag.builder().name('tag').options({
						label: { $scopePath: 'tag_label' },
						intent: { $scopePath: 'tag_intent' },
						minimal: { $scopePath: 'tag_minimal' },
						large: { $scopePath: 'tag_large' },
						interactive: { $scopePath: 'tag_interactive' },
						round: { $scopePath: 'tag_round' },
						onRemove: function () { },
						iconLeft: { $scopePath: 'tag_iconLeft' },
						iconRight: { $scopePath: 'tag_iconRight' }
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("tag_minimal").options({
						label: "Minimal",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("tag_large").options({
						label: "Large",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("tag_interactive").options({
						label: "Interactive",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("tag_round").options({
						label: "Round",
						inline: true,
					}),

					ctrl.BPTextInput.builder().name('tag_iconLeft').options({
						placeholder: "MapLarge icon class without the 'mlicon-' at start",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					ctrl.BPTextInput.builder().name('tag_iconRight').options({
						placeholder: "MapLarge icon class without the 'mlicon-' at start",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					ctrl.BPTextInput.builder().name('tag_label').options({
						placeholder: "MapLarge icon class without the 'mlicon-' at start",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('tag_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),
				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{tag}}</p>"
						})
					])

			]),
			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Select</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSelect.builder().name('select').options({
						disabled: { $scopePath: 'select_disabled' },
						icon: { $scopePath: 'select_icon' },
						resetOnClose: { $scopePath: 'select_resetOnClose' },
						resetOnSelect: { $scopePath: 'select_resetOnSelect' },
						filterable: { $scopePath: 'select_filterable' },
						minimal: { $scopePath: 'select_minimal' },
						intent: { $scopePath: 'select_intent' },
						placeholder: { $scopePath: 'select_placeholder' },
						items: inv_consts.selectItems
					}).children([
						mdash.controls.Html.builder().options({
							html: "<h2>This is a child inside a select</h2>"
						}),
					])

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPSwitch.builder().name("select_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("select_resetOnSelect").options({
						label: "Reset on select",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("select_resetOnClose").options({
						label: "Reset on close",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("select_minimal").options({
						label: "Minimal",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("select_filterable").options({
						label: "Filterable",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>MapLarge icon class without the 'mlicon-' at start</h2>"
					}),

					ctrl.BPTextInput.builder().name('select_icon').options({
						placeholder: "MapLarge icon class without the 'mlicon-' at start",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('select_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{select}}</p>"
						})
					])
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([


				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Suggest</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSuggest.builder().name('suggest').options({
						openOnKeyDown: { $scopePath: 'suggest_openOnKeyDown' },
						closeOnSelect: { $scopePath: 'suggest_closeOnSelect' },
						resetOnSelect: { $scopePath: 'suggest_resetOnSelect' },
						minimal: { $scopePath: 'suggest_minimal' },
						placeholder: { $scopePath: 'suggest_placeholder' },
						intent: { $scopePath: 'suggest_intent' },
						items: inv_consts.selectItems
					}).children([
						mdash.controls.Html.builder().options({
							html: "<h2>This is a child inside a suggest</h2>"
						}),
					])

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([


					ctrl.BPSwitch.builder().name("suggest_closeOnSelect").options({
						label: "Close on select",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("suggest_resetOnSelect").options({
						label: "Reset on select",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("suggest_minimal").options({
						label: "Minimal",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("suggest_openOnKeyDown").options({
						label: "Open On Key Down",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('suggest_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{suggest}}</p>"
						})
					])

			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Multi Select</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPMultiSelect.builder().name('multiSelect').options({
						openOnKeyDown: { $scopePath: 'multiSelect_openOnKeyDown' },
						resetOnSelect: { $scopePath: 'multiSelect_resetOnSelect' },
						minimal: { $scopePath: 'multiSelect_minimal' },
						intent: { $scopePath: 'multiSelect_intent' },
						popoverMinimal: { $scopePath: 'multiSelect_popoverMinimal' },
						placeholder: { $scopePath: 'multiSelect_placeholder' },
						onremove: { $scopePath: 'multiSelect_onremove' },
						onAdd: [],
						addOnBlur: function () { },
						items: inv_consts.selectItems
					}).children([
						mdash.controls.Html.builder().options({
							html: "<h2>This is a child inside a Multi Select</h2>"
						}),
					])

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([


					ctrl.BPSwitch.builder().name("multiSelect_onremove").options({
						label: "On Remove",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("multiSelect_minimal").options({
						label: "Minimal",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("multiSelect_popoverMinimal").options({
						label: "Popover Minimal",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("multiSelect_resetOnSelect").options({
						label: "Reset on Select",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("multiSelect_openOnKeyDown").options({
						label: "Open On Key Down",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('multiSelect_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{multiSelect}}</p>"
						})
					])

			]),

			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Tag Input</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPTagInput.builder().name('tagInput').options({
						intent: { $scopePath: 'tagInput_intent' },
						minimal: { $scopePath: 'tagInput_minimal' },
						large: { $scopePath: 'tagInput_large' },
						disabled: { $scopePath: 'tagInput_disabled' },
						fill: { $scopePath: 'tagInput_fill' },
						placeholder: { $scopePath: 'tagInput_placeholder' },
						icon: { $scopePath: 'tagInput_icon' },

						onAdd: [
							{
								value: 'Option 1',
								onRemove: function () { },
							},
							{
								value: 'Option 2',
								onRemove: function () { },
							},
						],
						addOnBlur: function () { },
						onRemove: function () { },
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("tagInput_minimal").options({
						label: "Minimal",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("tagInput_large").options({
						label: "Large",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("tagInput_fill").options({
						label: "Fill",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("tagInput_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('tagInput_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),
				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{tagInput}}</p>"
						})
					])

			]),
			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Time Picker</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 4, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPTimePicker.builder().name('timePicker').options({
						disabled: { $scopePath: 'timePicker_disabled' },
						showArrowButtons: { $scopePath: 'timePicker_showArrowButtons' },
						selectAllOnFocus: { $scopePath: 'timePicker_selectAllOnFocus' },
						useAmPm: { $scopePath: 'timePicker_useAmPm' },
						Precision: { $scopePath: 'timePicker_precision' },
						minTime: { $scopePath: 'timePicker_minTime' },
						maxTime: { $scopePath: 'timePicker_maxTime' }
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("timePicker_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("timePicker_showArrowButtons").options({
						label: "ShowArrowButtons",
						inline: true,
					}),


					ctrl.BPSwitch.builder().name("timePicker_selectAllOnFocus").options({
						label: "SelectAllOnFocus",
						inline: true,
					}),


					ctrl.BPSwitch.builder().name("timePicker_useAmPm").options({
						label: "UseAmPm",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Min Time</h2>"
					}),

					ctrl.BPTextInput.builder().name('timePicker_minTime').options({
						placeholder: "Min Time",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Max Time</h2>"
					}),

					ctrl.BPTextInput.builder().name('timePicker_maxTime').options({
						placeholder: "Max time",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Precison</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('timePicker_precision').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Precision.Minute, label: "Minute" },
							{ value: ctrl.BPBase.Precision.Second, label: "Second" },
							{ value: ctrl.BPBase.Precision.Milisecond, label: "Milisecond" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),


				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{timePicker}}</p>"
						})
					])

			]),

			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Date Picker</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 4, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPDatePicker.builder().name('datePicker').options({
						Precision: { $scopePath: 'datePicker_precision' },
						min_date: { $scopePath: 'datePicker_min_date' },
						max_date: { $scopePath: 'datePicker_max_date' },
						year_range: { $scopePath: 'datePicker_year_range' },
						showActionsBar: { $scopePath: 'datePicker_showActionsBar' },
						reverseMonthAndYearMenus: { $scopePath: 'datePicker_reverseMonthAndYearMenus' },
						canClearSelection: { $scopePath: 'datePicker_canClearSelection' },
						defaultValue: { $scopePath: 'datePicker_defaultValue' },
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("datePicker_showActionsBar").options({
						label: "Show actions bar",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("datePicker_reverseMonthAndYearMenus").options({
						label: "Reverse month and year menus",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("datePicker_canClearSelection").options({
						label: "Clear Button",
						inline: true,
					}),

					ctrl.BPRadioGroup.builder().name('datePicker_precision').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Precision.None, label: "None" },
							{ value: ctrl.BPBase.Precision.Minute, label: "Minute" },
							{ value: ctrl.BPBase.Precision.Second, label: "Second" },
							{ value: ctrl.BPBase.Precision.Milisecond, label: "Milisecond" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{datePicker}}</p>"
						})
					])

			]),

			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Context Menu</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 4, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPContextMenu.builder().options({
						icon: { $scopePath: 'contextMenu_icon' },
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					mdash.controls.Html.builder().options({
						html: "<h2>MapLarge icon without the 'mlicon-'</h2>"
					}),

					ctrl.BPTextInput.builder().name('contextMenu_icon').options({
						placeholder: "MapLarge icon class without the 'mlicon-' at start",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),
				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Editable Text</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 4, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPEditableText.builder().name('editable').options({
						placeholder_input: {$scopePath: "placeholderInput"},
						intent:  {$scopePath: "editableText_intent"},
						placeholder_textarea: {$scopePath: "placeholderTextarea"},
						selected_value: {$scopePath: "editableText_selected_value"},
						breakNewMultiLine: {$scopePath: "editableText_breakNewLine_value"},
						MaxLength: {$scopePath: "editableText_MaxLength_value"},
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					mdash.controls.Html.builder().options({
						html: "<h2>Max Length</h2>"
					}),

					ctrl.BPTextInput.builder().name('editableText_MaxLength_value').options({
						placeholder: "Max length",
						intent: ctrl.BPBase.Intent.Primary,
						type: "number"
					}),
					mdash.controls.Html.builder().options({
						html: "<h2>Intents</h2>"
					}),
					ctrl.BPRadioGroup.builder().name('editableText_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

					ctrl.BPSwitch.builder().name("editableText_breakNewLine_value").options({
						label: "Swap keypress for confirm and newline (multiline only)",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("editableText_selected_value").options({
						label: "Selected Value",
						inline: true,
					}),


					// ctrl.BPTextInput.builder().name('editableText_minWidth').options({
					// 	placeholder: "Minimum Width",
					// 	intent: ctrl.BPBase.Intent.Primary,
					// 	type: "number"
					// }),

					// ctrl.BPTextInput.builder().name('editableText_minLine').options({
					// 	placeholder: "Minimum number of lines",
					// 	intent: ctrl.BPBase.Intent.Primary,
					// 	type: "number"
					// }),

					// ctrl.BPTextInput.builder().name('editableText_maxLine').options({
					// 	placeholder: "Maximum Lines",
					// 	intent: ctrl.BPBase.Intent.Primary,
					// 	type: "number"
					// }),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Overlay</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 4, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPOverlay.builder().name('overlay').options({
						headerText: {$scopePath: 'overlay_headerText'},
						esc_key: {$scopePath: 'overlay_esc_key'},
						backdrop:  {$scopePath: 'overlay_backdrop'},
						click_outsitte: {$scopePath: 'overlay_click_outside'},
						modal_body_text_one: {$scopePath: 'overlay_modal_body_text_one'},
						modal_body_text_two: {$scopePath: 'overlay_modal_body_text_two'},
						transitionDuration: {$scopePath: 'overlay_transitionDuration'},
						CloseButton: {$scopePath: 'overlay_closeButton'},
						SubmitButton: {$scopePath: 'overlay_submitButton'}
						// SubmitButton: {$scopePath: 'overlay_submitButton'}
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPTextInput.builder().name('overlay_headerText').options({
						placeholder: "Header Text",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					ctrl.BPSwitch.builder().name("overlay_esc_key").options({
						label: "Esc Key",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("overlay_backdrop").options({
						label: "Backdrop",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("overlay_click_outside").options({
						label: "Click Outside",
						inline: true,
					}),

					ctrl.BPTextInput.builder().name('overlay_modal_body_text_one').options({
						placeholder: "Modal Text 1",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					ctrl.BPTextInput.builder().name('overlay_modal_body_text_two').options({
						placeholder: "Modal Text 2",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					ctrl.BPTextInput.builder().name('overlay_transitionDuration').options({
						placeholder: "Transition",
						intent: ctrl.BPBase.Intent.Primary,
						type: "number"
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Progress Bar</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPProgress.builder().options({
						intent: { $scopePath: 'progress_intent'},
						rangeWidth: { $scopePath: 'progress_rangeWidth'},
						rangeDisable: { $scopePath: 'progress_rangeDisable'}
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([
					mdash.controls.Html.builder().options({
						html: "<h2>Range Width</h2>"
					}),

					ctrl.BPNumericInput.builder().name("progress_rangeWidth").options({
						min: 0,
						max: 100,
						clampValueOnBlur: true,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1,
						disabled: {$scopePath: (scope) => !scope.progress_rangeDisable()}
					}),

					ctrl.BPSwitch.builder().name("progress_rangeDisable").options({
						label: "Range Disable",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('progress_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Divider</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPDivider.builder().options({
						vertical: { $scopePath: 'divider_verdical'},
						button_data: [
							{label: 'Delete',divider:true},

						],
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("divider_verdical").options({
						label: "Vertical Effect",
						inline: true,
					}),



				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Spinner</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPSpinner.builder().options({
						intent: { $scopePath: 'spinner_intent'},
						circleSize: { $scopePath: 'spinner_svgSize'},
						animationStop: { $scopePath: 'spinner_rangeDisable'},
						strokeSize: { $scopePath: 'spinner_strokeSize'}
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([
					mdash.controls.Html.builder().options({
						html: "<h2>Range Size</h2>"
					}),

					ctrl.BPNumericInput.builder().name("spinner_svgSize").options({
						min: 0,
						max: 200,
						clampValueOnBlur: true,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 1
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Range Stroke</h2>"
					}),

					ctrl.BPNumericInput.builder().name("spinner_strokeSize").options({
						min: 0,
						max: 250,
						clampValueOnBlur: true,
						numericOnly: true,
						minorStepSize: 0.1,
						majorStepSize: 10,
						stepSize: 10,
						disabled: {$scopePath: (scope) => !scope.spinner_rangeDisable()}
					}),

					ctrl.BPSwitch.builder().name("spinner_rangeDisable").options({
						label: "Range Disable",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('spinner_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),
				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([


				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Skeleton</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPSkeleton.builder().options({
						skeleton: { $scopePath: 'skeleton_skeleton'},
						icon: { $scopePath: 'skeleton_icon'},
						title: { $scopePath: 'skeleton_title'},
						text: { $scopePath: 'skeleton_text'}
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("skeleton_skeleton").options({
						label: "Skeleton",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Icon</h2>"
					}),

					ctrl.BPTextInput.builder().name('skeleton_icon').options({
						placeholder: "MapLarge icon class without the 'mlicon-' at start",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Title</h2>"
					}),

					ctrl.BPTextInput.builder().name('skeleton_title').options({
						placeholder: "title",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Text</h2>"
					}),

					ctrl.BPTextInput.builder().name('skeleton_text').options({
						placeholder: "text",
						intent: ctrl.BPBase.Intent.Primary,
						type: "text"
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Toast</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPToast.builder().options({
						modalPosition: { $scopePath: 'toast_modalPosition'},
						autoFocus: { $scopePath: 'toast_autoFocus'},
						escClear: { $scopePath: 'toast_escClear'},
						timeout: { $scopePath: 'toast_timeout'},
						btnData: [
							{name:'Procuar toast', intent: ctrl.BPBase.Intent.Primary, modalText:"One toast created. "},
							{name:'Move files', intent: ctrl.BPBase.Intent.Success, modalText:"Moved 6 files.", replaceItemText:'You cannot undo the past.'},
							{name:'Delete Root', intent: ctrl.BPBase.Intent.Danger, modalText:"You do not have permissions to perform this action. Please contact your system administrator to request the appropriate access rights."},
							{name:'Log Out', intent: ctrl.BPBase.Intent.Warning, modalText:"Goodbye, old friend.",  replaceItemText:"Isn't parting just the sweetest sorrow?"},
						],
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("toast_autoFocus").options({
						label: "Auto Focus",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("toast_escClear").options({
						label: "Can Esc key clear",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Timeout</h2>"
					}),

					ctrl.BPTextInput.builder().name('toast_timeout').options({
						placeholder: "Timeout",
						intent: ctrl.BPBase.Intent.Primary,
						type: "number"
					}),

					ctrl.BPRadioGroup.builder().name('toast_modalPosition').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPToast.AlignmentSelect.TopCenter, label: "Top Center" },
							{ value: ctrl.BPToast.AlignmentSelect.TopLeft, label: "Top Left" },
							{ value: ctrl.BPToast.AlignmentSelect.TopRight, label: "Top Right" },
							{ value: ctrl.BPToast.AlignmentSelect.BottomCenter, label: "Bottom Center" },
							{ value: ctrl.BPToast.AlignmentSelect.BottomLeft, label: "Bottom Left" },
							{ value: ctrl.BPToast.AlignmentSelect.BottomRight, label: "Bottom Right" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Dialog</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPDialog.builder().options({
						icon: { $scopePath: 'dialog_icon'},
						heading: { $scopePath: 'dialog_heading'},
						closeButton: { $scopePath: 'dialog_button1Text'},
						submitButton: { $scopePath: 'dialog_button2Text'},
						canOutsideClickClose: { $scopePath: 'dialog_canOutsideClickClose'},
						canEscapeKeyClose: { $scopePath: 'dialog_canEscapeKeyClose'},
						text: { $scopePath: 'dialog_text'},
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("dialog_canOutsideClickClose").options({
						label: "Can Outside Click cCose",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("dialog_canEscapeKeyClose").options({
						label: "Can Esc Key Close",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Heading</h2>"
					}),

					ctrl.BPTextInput.builder().name('dialog_heading').options({
						placeholder: "heading title",
						intent: ctrl.BPBase.Intent.Primary,
						type: "string"
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>MapLarge icon without the 'mlicon-' at start</h2>"
					}),

					ctrl.BPTextInput.builder().name('dialog_icon').options({
						placeholder: "MapLarge icon without the 'mlicon-' at start",
						intent: { $scopePath: 'numInput_intent' },
						type: "text"
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Nav Bar</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPNavBar.builder().options({
						fixedToTop: { $scopePath: 'navBar_fixedToTop'},
						dark: { $scopePath: 'navBar_dark'},
						input: { $scopePath: 'navBar_input'},
						align: { $scopePath: 'navBar_align'},
						btnData: [
							{label: 'Home', icon: 'square234', divider: true},
							{label: 'Files', icon: 'MapLarge-icons_file', divider: false},
							{icon: 'user', divider: true, },
							{icon: 'marker', divider: false},
							{icon: 'gear1', divider: false},
						],
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("navBar_fixedToTop").options({
						label: "Fixed To Top",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("navBar_dark").options({
						label: "Dark",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("navBar_input").options({
						label: "Add Input",
						inline: true,
					}),

					ctrl.BPRadioGroup.builder().name('navBar_align').options({
						radioData: [
							{ value: ctrl.BPBase.Alignment.Around, label: "Around" },
							{ value: ctrl.BPBase.Alignment.Center, label: "Center" },
							{ value: ctrl.BPBase.Alignment.Left, label: "Left" },
							{ value: ctrl.BPBase.Alignment.Right, label: "Right" },
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Control Group</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 4, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPControl.builder().name("control").options({
						fill: {$scopePath: 'controlGroup_fill'},
						vertical: {$scopePath: 'controlGroup_vertical'},
						// btnData: [
						// {left: ctrl.BPBase.ControlGroup.Button, buttonText: 'Filter', icon: 'filter', placeholder: 'Find filters...'},
						// {right: ctrl.BPBase.ControlGroup.Button, buttonText: 'Add', inputIcon: 'user', placeholder: 'Find collaborators...', intent: ctrl.BPBase.Intent.Success },
						// {
						left: ctrl.BPBase.ControlGroup.Select,
						inputIcon: 'x16Search',
						inputValue: 'from:ggray to:allorca',
						selectOption: [
							{label:'Filter',value:"F"},
							{label:'Name - ascending',value:"A"},
							{label:'Name - descending',value:"D"},
							{label:'Price - ascending',value:"P"},
							{label:'Price - descending',value:"PD"}
						]
						// },
						// ],
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{control}}</p>"
						})
					]),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("controlGroup_fill").options({
						label: "Fill",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("controlGroup_vertical").options({
						label: "Vertical",
						inline: true,
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>TimeZone Picker</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPTimeZonePicker.builder().name('timeZone').options({
						standartDisplay: { $scopePath: 'timeZone_standart'},
						showLocalTimeZone: { $scopePath: 'timeZone_showLocal'},
						disabled: { $scopePath: 'timeZone_disabled'}
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("timeZone_showLocal").options({
						label: "Show local timezone",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("timeZone_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Display format</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('timeZone_standart').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPTimeZonePicker.TimeZoneStandart.Abbreviation, label: "Abbreviation" },
							{ value: ctrl.BPTimeZonePicker.TimeZoneStandart.Composite, label: "Composite" },
							{ value: ctrl.BPTimeZonePicker.TimeZoneStandart.Name, label: "Name" },
							{ value: ctrl.BPTimeZonePicker.TimeZoneStandart.Offset, label: "Offset" }
						]
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{timeZone}}</p>"
						})
					])
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Form Group</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPFormGroup.builder().options({
						disabled: { $scopePath: 'formGroup_disabled'},
						inline: { $scopePath: 'formGroup_inline'},
						intent: { $scopePath: 'formGroup_intent'},
						helperShow: { $scopePath: 'formGroup_helperShow'},
						helperText: { $scopePath: 'formGroup_helperText'},
						showLabel: { $scopePath: 'formGroup_showLabel'},
						showLabelText: { $scopePath: 'formGroup_showLabelText'},
						showLabelInfo: { $scopePath: 'formGroup_showLabelInfo'},
						showLabelInfoText: { $scopePath: 'formGroup_showLabelInfoText'},
						switchText: { $scopePath: 'formGroup_switchText'},
						switchTextShow: { $scopePath: 'formGroup_switchTextShow'},
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("formGroup_disabled").options({
						label: "Disabled",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("formGroup_inline").options({
						label: "Inline",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("formGroup_helperShow").options({
						label: "Helper Text",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("formGroup_showLabel").options({
						label: "Label",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("formGroup_showLabelInfo").options({
						label: "Label Info",
						inline: true,
					}),

					ctrl.BPRadioGroup.builder().name('formGroup_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),


				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Overflow List</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPOverflow.builder().options({
						icon: { $scopePath: 'overflow_icon'},
						link_data: { $scopePath: 'overflow_linkData'},
						width: { $scopePath: 'overflow_width'},
						position: { $scopePath: 'overflow_position'}
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPRadioGroup.builder().name('overflow_position').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Position.Start, label: "Start" },
							{ value: ctrl.BPBase.Position.End, label: "End" },
						]
					}),

					ctrl.BPTextInput.builder().name('overflow_width').options({
						placeholder: "Width",
						intent: ctrl.BPBase.Intent.Primary,
						type: "number"
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),


			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Tree</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPTree.builder().options({
						data: inv_consts.treeDataVars,
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([


				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Tabs</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPTabs.builder().options({
						animate: { $scopePath: 'tabs_animate'},
						vertical: { $scopePath: 'tabs_vertical'},
						activePanelOnly: { $scopePath: 'tabs_activePanelOnly'},
						navbarTabsList: { $scopePath: 'tabs_navbarTabsList'},
						tabList: { $scopePath: 'tabs_tabList'},
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPSwitch.builder().name("tabs_animate").options({
						label: "Animate",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("tabs_vertical").options({
						label: "Vertical",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("tabs_activePanelOnly").options({
						label: "Active Panel Only",
						inline: true,
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Date Input</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 4, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPDateInput.builder().name('dateInput').options({
						Precision: { $scopePath: 'dateInput_precision' },
						min_date: { $scopePath: 'dateInput_min_date' },
						max_date: { $scopePath: 'dateInput_max_date' },
						year_range: { $scopePath: 'dateInput_year_range' },
						reverseMonthAndYearMenus: { $scopePath: 'dateInput_reverseMonthAndYearMenus' },
						disabled: { $scopePath: "dateInput_disabled" },
						closeOnSelection: { $scopePath: "dateInput_closeOnSelection" },
						dateFormat: { $scopePath: "dateInput_dateFormat" }
					}),
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([

					ctrl.BPSwitch.builder().name("dateInput_reverseMonthAndYearMenus").options({
						label: "Reverse month and year menus",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("dateInput_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("dateInput_closeOnSelection").options({
						label: "Close on Selection",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Date format</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('dateInput_dateFormat').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPDateInput.DateFormat.JS_DATE, label: "JS Date"},
							{ value: ctrl.BPDateInput.DateFormat.MONTH_DAY_YEAR, label: "MM/DD/YYYY (moment)"},
							{ value: ctrl.BPDateInput.DateFormat.YEAR_MONTH_DAY, label: "YYYY-MM-DD (moment)"},
							{ value: ctrl.BPDateInput.DateFormat.YEAR_MONTH_DAY_TIME, label: "YYYY-MM-DD HH:mm:ss (moment)"},
						]
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Time Precision</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('dateInput_precision').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Precision.None, label: "None" },
							{ value: ctrl.BPBase.Precision.Minute, label: "Minute" },
							{ value: ctrl.BPBase.Precision.Second, label: "Second" },
							{ value: ctrl.BPBase.Precision.Milisecond, label: "Milisecond" },
						]
					}),


				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder()
					.layoutOptions({ columnStart: 4, columnEnd: 5, rowStart: 2, rowEnd: 3 })
					.children([
						mdash.controls.Html.builder().options({
							html: "<h2>Value:</h2><p>{{dateInput}}</p>"
						})
					])

			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Html Table</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPHtmlTable.builder().options({
						condensed: { $scopePath: 'htmlTable_condensed'},
						striped: { $scopePath: 'htmlTable_striped'},
						bordered: { $scopePath: 'htmlTable_bordered'},
						interactive: { $scopePath: 'htmlTable_interactive'},
						dataTable: { $scopePath: 'htmlTable_dataTable'},
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPSwitch.builder().name("htmlTable_condensed").options({
						label: "Condensed",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("htmlTable_striped").options({
						label: "Striped",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("htmlTable_bordered").options({
						label: "Bordered",
						inline: true,
					}),
					ctrl.BPSwitch.builder().name("htmlTable_interactive").options({
						label: "Interactive",
						inline: true,
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([

				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Menu</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 5, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPMenu.builder().options({
						textLarge: { $scopePath: 'menu_textLarge'},
						menuItems: { $scopePath: 'menu_menuItems'},
					}),

				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPSwitch.builder().name("menu_textLarge").options({
						label: "Text Large",
						inline: true,
					}),

				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 })
			]),

			mdash.controls.Grid.builder().options(gridOptions).children([
				mdash.controls.Html.builder().options({
					html: "<h2 class='control-header'>Popover</h2>"
				}).layoutOptions({ columnStart: 1, columnEnd: 4, rowStart: 1, rowEnd: 2 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPPopover.builder().options({
						disabled: { $scopePath: "popover_disabled" },
						arrow: { $scopePath: "popover_arrow" },
						flip: { $scopePath: "popover_flip" },
						preventOverflow: { $scopePath: "popover_preventOverflow" },
						canEscapeKeyClose: { $scopePath: "popover_canEscapeKeyClose" },
						openOnTargetFocus: { $scopePath: "popover_openOnTargetFocus" },
						label: { $scopePath: "popover_label" },
						intent: { $scopePath: "popover_intent" },
						interactionKind: { $scopePath: "popover_popoverInteractionKind" },
						position: { $scopePath: "popover_position" },
						preventOverflowType: { $scopePath: "popover_preventOverflowType" },
					}).children([
						ctrl.BPSlider.builder().options({
							labelPercentages: [10,20,30,40,50,60,70,80,90],
							min: 0,
							max: 10,
							showTrackFill: true
						})
					])
				]).layoutOptions({ columnStart: 1, columnEnd: 2, rowStart: 2, rowEnd: 3 }),

				mdash.controls.StackPanel.builder().children([
					ctrl.BPSwitch.builder().name("popover_disabled").options({
						label: "Disabled",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("popover_arrow").options({
						label: "Arrow",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("popover_flip").options({
						label: "Flip",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("popover_preventOverflow").options({
						label: "Prevent overflow",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("popover_canEscapeKeyClose").options({
						label: "Can close the popover with Escape",
						inline: true,
					}),

					ctrl.BPSwitch.builder().name("popover_openOnTargetFocus").options({
						label: "Open popover when the button is focused",
						inline: true,
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Popover Button Label</h2>"
					}),

					ctrl.BPTextInput.builder().name('popover_label').options({
						placeholder: "Popover button label",
						intent: ctrl.BPBase.Intent.Primary
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Popover Button Intent</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('popover_intent').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPBase.Intent.Danger, label: "Danger" },
							{ value: ctrl.BPBase.Intent.Default, label: "Default" },
							{ value: ctrl.BPBase.Intent.Primary, label: "Primary" },
							{ value: ctrl.BPBase.Intent.Success, label: "Success" },
							{ value: ctrl.BPBase.Intent.Warning, label: "Warning" },
						]
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Popover position</h2>"
					}),

					ctrl.BPHTMLSelect.builder().name('popover_position').options({
						options: [
							{ value: ctrl.BPPopover.Position.AUTO, label: "auto" },
							{ value: ctrl.BPPopover.Position.AUTO_END, label: "auto-end" },
							{ value: ctrl.BPPopover.Position.AUTO_START, label: "auto-start" },
							{ value: ctrl.BPPopover.Position.BOTTOM, label: "bottom" },
							{ value: ctrl.BPPopover.Position.BOTTOM_LEFT, label: "bottom-left" },
							{ value: ctrl.BPPopover.Position.BOTTOM_RIGHT, label: "bottom-right" },
							{ value: ctrl.BPPopover.Position.LEFT, label: "left" },
							{ value: ctrl.BPPopover.Position.LEFT_BOTTOM, label: "left-bottom" },
							{ value: ctrl.BPPopover.Position.LEFT_TOP, label: "left-top" },
							{ value: ctrl.BPPopover.Position.RIGHT, label: "right" },
							{ value: ctrl.BPPopover.Position.RIGHT_BOTTOM, label: "right-bottom" },
							{ value: ctrl.BPPopover.Position.RIGHT_TOP, label: "right-top" },
							{ value: ctrl.BPPopover.Position.TOP, label: "top" },
							{ value: ctrl.BPPopover.Position.TOP_LEFT, label: "top-left" },
							{ value: ctrl.BPPopover.Position.TOP_RIGHT, label: "top-right" }
						]
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Popover target interaction kind</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('popover_popoverInteractionKind').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPPopover.PopoverInteractionKind.CLICK, label: "Click" },
							{ value: ctrl.BPPopover.PopoverInteractionKind.CLICK_TARGET_ONLY, label: "Click (target only)" },
							{ value: ctrl.BPPopover.PopoverInteractionKind.HOVER, label: "Hover" },
							{ value: ctrl.BPPopover.PopoverInteractionKind.HOVER_TARGET_ONLY, label: "Hover (target only)" }

						]
					}),

					mdash.controls.Html.builder().options({
						html: "<h2>Popover prevent overflow method</h2>"
					}),

					ctrl.BPRadioGroup.builder().name('popover_preventOverflowType').options({
						align: ctrl.BPBase.Alignment.Right,
						radioData: [
							{ value: ctrl.BPPopover.PreventOverflowTypes.SCROLL_PARENT, label: "scrollParent" },
							{ value: ctrl.BPPopover.PreventOverflowTypes.VIEWPORT, label: "viewport" },
							{ value: ctrl.BPPopover.PreventOverflowTypes.WINDOW, label: "window" }

						]
					}),
				]).layoutOptions({ columnStart: 2, columnEnd: 3, rowStart: 2, rowEnd: 3 }),
			]),

		];

		//dashboardControls.forEach(c => {

		//	var control = c.children()[1].children[0];

		//	var html = "";

		//	var options = control.toJSON().options;


		//	if (Object.keys(options).length) {
		//		html = "<p class='h3'><xmp style='white-space: pre-wrap;'>controls." + (<any>control).control + ".builder()\n.options(" + JSON.stringify(options, undefined, 2) + ")</xmp></p>";
		//	} else {
		//		html = "<p class='h3'>No Required Options</p>";
		//	}


		//});

		return mdash.controls.ResponsiveGrid.builder().options({
			size: "sm",
			offset: 2
		}).cssClass('ext-inventory')
			.children([
				mdash.controls.Heading.builder().options({
					text: "Blueprint Component Display",
					autoLevel: true
				}).cssClass('heading'),

				mdash.controls.StackPanel
					.builder()

					.layoutOptions({ span: 10 })
					.children([

						mdash.controls.ScopeDefaults.builder().options({
							defaults: [
								// creates copies of objects declared above
								...inv_consts.textInputVars,
								...inv_consts.inputGroupVars,
								...inv_consts.btnVars,
								...inv_consts.btnGroupVars,
								...inv_consts.checkboxVars,
								...inv_consts.switchVars,
								...inv_consts.radioVars,
								...inv_consts.radioGroupVars,
								...inv_consts.fileInputVars,
								...inv_consts.numInputVars,
								...inv_consts.calloutVars,
								...inv_consts.cardVars,
								...inv_consts.sliderVars,
								...inv_consts.rangeSliderVars,
								...inv_consts.breadcrumbVars,
								...inv_consts.textAreaVars,
								...inv_consts.collapseVars,
								...inv_consts.multiSliderVars,
								...inv_consts.htmlSelectVars,
								...inv_consts.labelVars,
								...inv_consts.tagVars,
								...inv_consts.selectVars,
								...inv_consts.suggestVars,
								...inv_consts.multiSelectVars,
								...inv_consts.tagInputVars,
								...inv_consts.omnibarVars,
								...inv_consts.timePickerVars,
								...inv_consts.datePickerVars,
								...inv_consts.contextMenuVars,
								...inv_consts.editableTextVars,
								...inv_consts.overlayVars,
								...inv_consts.progressVars,
								...inv_consts.spinnerVars,
								...inv_consts.skeletonVars,
								...inv_consts.toastVars,
								...inv_consts.dialogVars,
								...inv_consts.navBarVars,
								...inv_consts.controlGroupVars,
								...inv_consts.dividerVars,
								...inv_consts.timeZoneVars,
								...inv_consts.formGroupVars,
								...inv_consts.tabsVars,
								...inv_consts.dateInputVars,
								...inv_consts.htmlTableVars,
								...inv_consts.overflowVars,
								...inv_consts.menuVars,
								...inv_consts.popoverVars
							]
						}).children(dashboardControls),


					])


			]).toJSON();

	}
}