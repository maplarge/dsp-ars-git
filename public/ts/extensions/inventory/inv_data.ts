namespace dashboard.extensions.inventory.inv_consts {
	export const selectItems = [
		{ label: "The Shawshank Redemption", value: 1994, },
		{ label: "The Godfather", value: 1972 },
		{ label: "The Godfather: Part II", value: 1974 },
		{ label: "The Dark Knight", value: 2008 },
		{ label: "12 Angry Men", value: 1957 },
		{ label: "Shindler's List", value: 1993 },
		{ label: "Pulp Fiction", value: 1994 },
		{ label: "The Lord of Rings: The Return of the King", value: 2003 },
		{ label: "The Good, the Bad and the Ugly", value: 1966 },
		{ label: "Fight CLub", value: 1999 },
		{ label: "The Lord of the Rings: The Fellowship of the Ring", value: 2001 },
		{ label: "Star Wars: Episode V - The Empire Strikes Back", value: 1980 },
		{ label: "Forrest Gump", value: 1994 },
		{ label: "Inception", value: 2010 },
		{ label: "The Lord of the Rings: The Two Towers", value: 2002 },
		{ label: "One Flew Over the Cuckoo's Nest", value: 1975 },
		{ label: "Goodfellas", value: 1990 },
		{ label: "The Matrix", value: 1999 },
		{ label: "Seven Samurai", value: 1974 },
		{ label: "Star Wars: Episode IV - A New Hope", value: 1974 },
		{ label: "City of God", value: 2002 },
		{ label: "Se7en", value: 1995 },
		{ label: "The Silence of the Lambs", value: 1991 },
		{ label: "It's a Wonderful Life", value: 1946 },
		{ label: "Life is Beautiful", value: 1997 },
		{ label: "The Usual Suspects", value: 1995 },
		{ label: "Leon: The Professional", value: 1994 },
		{ label: "Spirited Away", value: 2001 },
		{ label: "Saving Private Ryan", value: 1998 },
		{ label: "Once Upon a Time in the West", value: 1968 },
		{ label: "American History X", value: 1998 },
		{ label: "Intersteller", value: 2014 },
		{ label: "Casablanca", value: 1948 },
		{ label: "City Lights", value: 1931 },
		{ label: "Psycho", value: 1960 },
		{ label: "The Green Mile", value: 1999 },
		{ label: "The Intouchables", value: 2011 },
		{ label: "Modern Times", value: 1936 },
		{ label: "Raiders of the Lost Ark", value: 1981 },
		{ label: "Rear Window", value: 1954 },
		{ label: "The Pianist", value: 2002 },
		{ label: "The Departed", value: 2006 },
		{ label: "Terminator 2: Judgment Day", value: 1991 },
		{ label: "Back to the Future", value: 1985 },
		{ label: "Whiplash", value: 2014 },
		{ label: "Gladiator", value: 2000 },
		{ label: "Memento", value: 2000 },
		{ label: "The Prestige", value: 2006 },
		{ label: "The Lion King", value: 1994 },
		{ label: "Apocalypse Now", value: 1979 },
		{ label: "Alien", value: 1979 },
		{ label: "Sunset Bolevard", value: 1950 },
		{ label: "Dr. Strangelove or: How I Learned to Stor Worrying and love the Bomb", value: 1964 },
		{ label: "The Freay Dictator", value: 1940 },
		{ label: "Cinema Paradiso", value: 1988 },
		{ label: "The Lives of Others", value: 2006 },
		{ label: "Grave of the Fireflies", value: 1988 },
		{ label: "Paths of Glory", value: 1957 },
		{ label: "Django Unchaired", value: 2012 },
		{ label: "The Shining", value: 1980 },
		{ label: "WALL-E", value: 2008 },
		{ label: "American Beauty", value: 1999 },
		{ label: "The Dark Knight Rises", value: 2012 },
		{ label: "Princess Mononoke", value: 1997 },
		{ label: "Aliens", value: 1986 },
		{ label: "Oldboy", value: 2003 },
		{ label: "Once Upon a Time in America", value: 1984 },
		{ label: "Witness of the Prosecultion", value: 1974 },
		{ label: "Das Boot", value: 1981 },
		{ label: "Citizen Kane", value: 1941 },
		{ label: "North by Northwest", value: 1959 },
		{ label: "Vertigo", value: 1958 },
		{ label: "Star Wars: Episode VI - E|Return of the Jedi", value: 1983 },
		{ label: "Reservoir Dogs", value: 1992 },
		{ label: "Bravheart", value: 1995 },
		{ label: "M", value: 1931 },
		{ label: "Requiem for a Dream", value: 2000 },
		{ label: "Amelie", value: 2001 },
		{ label: "A Clockwork Orange", value: 1971 },
		{ label: "Like Stars on Earth", value: 2007 },
		{ label: "Taxi Driver", value: 1976 },
		{ label: "Lawrence of Arabia", value: 1962 },
		{ label: "Double Indemnity", value: 1944 },
		{ label: "Eternal Sunshine of the Sportless Mind", value: 2004 },
		{ label: "Amadeus", value: 1984 },
		{ label: "To Kill a Mockingbird", value: 1962 },
		{ label: "Toy Story 3", value: 2010 },
		{ label: "Logan", value: 2017 },
		{ label: "Full Metal Jacket", value: 1987 },
		{ label: "Dangal", value: 2016 },
		{ label: "The Sting", value: 1973 },
		{ label: "2001: A Space Odyssey", value: 1968 },
		{ label: "Singin' in the Rain", value: 1952 },
		{ label: "Toy Story", value: 1995 },
		{ label: "Bicycle Thieves", value: 1948 },
		{ label: "The Kid", value: 1921 },
		{ label: "Ingourious Basterds", value: 2009 },
		{ label: "Snatch", value: 2000 },
		{ label: "3 Idiots", value: 2009 },
		{ label: "Monty Python and the Holy Grail", value: 1975 }
	];

	export const treeDataVars = [
		{
			eyeTextToolTip:"",
			eye:"",
			type:"folder",
			isOpen:false,
			name:"Folder 0",
			iconCaretClass:"mlicon-arrowdown",
			iconClass:"mlicon-load",
			hasIcon:Function,
			subMenu:[
				{
					name:"Item 0",
					iconCaretClass:"mlicon-arrowdown",
					iconClass:"mlicon-document162",
					eyeTextToolTip:"",
					eye:"",
					type:"file",
				},
				{
					eyeTextToolTip:"I am foo!",
					eye:"mlicon-hide",
					type:"folder",
					isOpen:false,
					name:"Folder 5",
					iconCaretClass:"mlicon-arrowdown",
					iconClass:"mlicon-load",
					hasIcon:Function,
					subMenu:[
						{
							name:"item 1",
							iconCaretClass:"mlicon-arrowdown",
							iconClass:"mlicon-document162",
							hasIcon:Function,
							eyeTextToolTip:"",
							eye:"",
							type:"file",
						},
						{
							name:"item 2",
							iconCaretClass:"mlicon-arrowdown",
							iconClass:"mlicon-drawingtools-label",
							hasIcon:Function,
							eyeTextToolTip:"",
							eye:"",
							type:"file",
						}
					]
				}
			]
		},
		{
			eyeTextToolTip:"I am foo2!",
			eye:"mlicon-hide",
			type:"folder",
			isOpen:true,
			name:"Folder 1",
			iconCaretClass:"mlicon-arrowdown",
			iconClass:"mlicon-load",
			hasIcon:Function,
			subMenu:[
				{
					name:"Item 0",
					iconCaretClass:"mlicon-arrowdown",
					iconClass:"mlicon-document162",
					hasIcon:Function,
					eyeTextToolTip:"",
					eye:"",
					type:"file"
				},
				{
					name:"Organic meditation gluten-free, sriracha VHS drinking vinegar beard man.",
					iconCaretClass:"mlicon-arrowdown",
					iconClass:"mlicon-drawingtools-label",
					hasIcon:Function,
					eyeTextToolTip:"",
					eye:"",
					type:"file"
				},
				{
					isOpen:true,
					name:"Folder 2",
					iconCaretClass:"mlicon-arrowdown",
					iconClass:"mlicon-load",
					hasIcon:Function,
					eyeTextToolTip:"",
					eye:"",
					type:"folder",
					subMenu:[
						{
							name:'no icon item',
							iconCaretClass:"mlicon-arrowdown",
							iconClass:"",
							hasIcon:Function,
							eyeTextToolTip:"",
							eye:"",
							type:"file"
						},
						{
							name:'item 1',
							iconCaretClass:"mlicon-arrowdown",
							iconClass:"mlicon-drawingtools-label",
							hasIcon:Function,
							eyeTextToolTip:"",
							eye:"",
							type:"file"
						},
						{
							name:'Folder 3',
							isOpen:true,
							iconCaretClass:"mlicon-arrowdown",
							iconClass:"mlicon-load",
							hasIcon:Function,
							eyeTextToolTip:"",
							eye:"",
							type:"folder",
							subMenu:[
								{
									name:"item 0",
									iconCaretClass:"mlicon-arrowdown",
									iconClass:"mlicon-document162",
									hasIcon:Function,
									eyeTextToolTip:"",
									eye:"",
									type:"file",
								},
								{
									name:"item 1",
									iconCaretClass:"mlicon-arrowdown",
									iconClass:"mlicon-load",
									hasIcon:Function,
									eyeTextToolTip:"",
									eye:"",
									isOpen:true,
									type:"folder",
									subMenu:[
										{
											name:"item 1",
											iconCaretClass:"mlicon-arrowdown",
											iconClass:"mlicon-document162",
											hasIcon:Function,
											eyeTextToolTip:"",
											eye:"",
											type:"file",
										},
										{
											eyeTextToolTip:"I am foo!",
											eye:"mlicon-hide",
											type:"folder",
											isOpen:false,
											name:"Folder 5",
											iconCaretClass:"mlicon-arrowdown",
											iconClass:"mlicon-load",
											hasIcon:Function,
											subMenu:[
												{
													name:"item 1",
													iconCaretClass:"mlicon-arrowdown",
													iconClass:"mlicon-document162",
													hasIcon:Function,
													eyeTextToolTip:"",
													eye:"",
													type:"file",
												},
												{
													name:"item 2",
													iconCaretClass:"mlicon-arrowdown",
													iconClass:"mlicon-drawingtools-label",
													hasIcon:Function,
													eyeTextToolTip:"",
													eye:"",
													type:"file",
												}
											]
										}
									]
								}
							]
						}
					]
				}
			]
		}
	]

	export const selectList = [
		{ text: "Red",		value: '#ff0000' },
		{ text: "Green",	value: '#00ff00' },
		{ text: "Blue",		value: '#0000ff' },
		{ text: "Orange",	value: '#C87A2D' },
	]

	export const textInputVars = [
		{ path: "ti_fill", value: false },
		{ path: "ti_isLarge", value: false },
		{ path: "ti_isRounded", value: false },
		{ path: "ti_disabled", value: false },
		{ path: "ti_placeholder", value: 'Placeholder' },
		{ path: "ti_intent", value: ctrl.BPBase.Intent.Success },
	]

	export const btnGroupVars = [
		{ path: "btnGroup_isLarge", value: false },
		{ path: "btnGroup_iconsOnly", value: false },
		{ path: "btnGroup_small", value: false },
		{ path: "btnGroup_minimal", value: false },
		{ path: "btnGroup_intent", value: ctrl.BPBase.Intent.Warning },
	]

	export const btnVars = [
		{ path: "btn_minimal", value: false },
		{ path: "btn_disabled", value: false },
		{ path: "btn_isLarge", value: false },
		{ path: "btn_iconsOnly", value: false },
		{ path: "btn_small", value: false },
		{ path: "btn_active", value: false },
		{ path: "btn_isLoading", value: false },
		{ path: "btn_intent", value: ctrl.BPBase.Intent.Primary },
		{ path: "btn_icon", value: ''}
	]

	export const checkboxVars = [
		{ path: "chbox_large", value: false },
		{ path: "chbox_checked", value: false },
		{ path: "chbox_indeterminate", value: false },
		{ path: "chbox_disabled", value: false },
		{ path: "chbox_align", value: ctrl.BPBase.Alignment.Right },
	]

	export const switchVars = [
		{ path: "switch_checked", value: false },
	]

	export const radioVars = [
		{ path: 'radio_checked', value: false },
		{ path: 'radio_disabled', value: false },
		{ path: 'radio_large', value: false },
		{ path: "radio_align", value: ctrl.BPBase.Alignment.Right },
	]

	export const radioGroupVars = [
		{ path: 'radioGroup_disabled', value: false },
		{ path: 'radioGroup_large', value: false },
		{ path: "radioGroup_align", value: ctrl.BPBase.Alignment.Right },
	]

	export const fileInputVars = [
		{ path: 'fileInput_large', value: false },
		{ path: 'fileInput_disabled', value: false },
		{ path: 'fileInput_label', value: 'Choose file' },
	]

	export const numInputVars = [
		{ path: 'numInput_disabled', value: false },
		{ path: 'numInput_large', value: false },
		{ path: 'numInput_selOnFocus', value: false },
		{ path: 'numInput_selAllOnInc', value: false },
		{ path: 'numInput_clampOnBlur', value: false },
		{ path: 'numInput_numOnly', value: false },
		{ path: 'numInput_fill', value: false },
		{ path: "numInput_intent", value: ctrl.BPBase.Intent.Primary },
		{ path: "numInput_step", value: 1 },
		{ path: "numInput_minorStep", value: 0.1 },
		{ path: "numInput_majorStep", value: 10 },
		{ path: "numInput_min", value: 25 },
		{ path: "numInput_max", value: 100 },
		{ path: "numInput_position", value: ctrl.BPNumericInput.Position.Right },
		{ path: "numInput_decPrecision", value: 2 },
		{ path: "numInput_placeholder", value: 'Enter a number here' },
		{ path: "numInput_icon", value: 'information' },
	]

	export const calloutVars = [
		{ path: "callout_hideHeader", value: false },
		{ path: "callout_headerText", value: 'Header' },
		{ path: 'callout_contentText', value: 'Content' },
		{ path: 'callout_intent', value: ctrl.BPBase.Intent.Default }
	]

	export const cardVars = [
		{ path: 'card_interactive', value: false },
		{ path: 'card_elevation', value: ctrl.BPBase.Elevation.Zero }
	]

	export const sliderVars = [
		{ path: 'slider_disabled', value: false },
		{ path: 'slider_vertical', value: false },
		{ path: 'slider_value', value: 15 },
		{ path: 'slider_min', value: 0 },
		{ path: 'slider_max', value: 250 },
		{ path: 'slider_showTrackFill', value: false },
		{ path: 'slider_step', value: 1 },
		{ path: 'slider_percentages', value: [25, 50, 75] }
	]

	export const rangeSliderVars = [
		{ path: 'rangeSlider_disabled', value: false },
		{ path: 'rangeSlider_vertical', value: false },
		{ path: 'rangeSlider_min', value: 0 },
		{ path: 'rangeSlider_max', value: 250 },
		{ path: 'rangeSlider_step', value: 1 },
		{ path: 'rangeSlider_percentages', value: [20, 40, 60, 80] }
	]

	export const textAreaVars = [
		{ path: 'textArea_disabled', value: false },
		{ path: 'textArea_large', value: false },
		{ path: 'textArea_fill', value: false },
		{ path: 'textArea_readonly', value: false },
		{ path: 'textArea_intent', value: ctrl.BPBase.Intent.Primary },
	]

	export const breadcrumbVars = [
		{ path: "breadcrumbs_current", value: 'Inventory' },
		{ path: "breadcrumbs_icon", value: 'MapLarge-icons_bullet-list' }
	]

	export const collapseVars = [
		{ path: 'collapse_isOpen', value: false },
		{ path: 'collapse_showText', value: 'Show' },
		{ path: 'collapse_hideText', value: 'Hide' },
		{ path: 'collapse_transitionDuration', value: 500 }
	]

	export const multiSliderVars = [
		{ path: 'multiSlider_disabled', value: false },
		{ path: 'multiSlider_vertical', value: false },
		{ path: 'multiSlider_min', value: 0 },
		{ path: 'multiSlider_max', value: 300 },
		{ path: 'multiSlider_step', value: 1 },
		{ path: 'multiSlider_percentages', value: [20, 40, 60, 80] },
		{ path: 'multiSlider_handleInteraction', value: ctrl.BPMultiSlider.HandleInteraction.Lock }
	]

	export const htmlSelectVars = [
		{ path: 'htmlSelect_disabled', value: false },
		{ path: 'htmlSelect_fill', value: false },
		{ path: 'htmlSelect_large', value: false },
		{ path: 'htmlSelect_minimal', value: false },
	]

	export const labelVars = [
		{ path: 'label_disabled', value: false },
		{ path: 'label_inline', value: false },
		{ path: 'label_label', value: 'Label 1' }
	]

	export const tagVars = [
		{ path: 'tag_round', value: false },
		{ path: 'tag_minimal', value: false },
		{ path: 'tag_large', value: false },
		{ path: 'tag_interactive', value: false },
		{ path: 'tag_label', value: 'New York' },
		{ path: 'tag_iconRight', value: 'MapLarge-icons_maps-public' },
		{ path: 'tag_iconLeft', value: 'MapLarge-icons_logo' },
		{ path: 'tag_intent', value: ctrl.BPBase.Intent.Success }
	]

	export const selectVars = [
		{ path: 'select_filterable', value: false },
		{ path: 'select_disabled', value: false },
		{ path: 'select_minimal', value: false },
		{ path: 'select_resetOnSelect', value: false },
		{ path: 'select_resetOnClose', value: false },
		{ path: 'select_intent', value: ctrl.BPBase.Intent.Primary },
		{ path: 'select_placeholder', value: 'Filter...' },
		{ path: 'select_icon', value: 'MapLarge-icons_bullet-list' },
	]

	export const suggestVars = [
		{ path: 'suggest_minimal', value: false },
		{ path: 'suggest_resetOnSelect', value: false },
		{ path: 'suggest_closeOnSelect', value: false },
		{ path: 'suggest_openOnKeyDown', value: false },
		{ path: 'suggest_placeholder', value: 'Search...' },
		{ path: 'suggest_intent', value: ctrl.BPBase.Intent.Primary },
	]

	export const multiSelectVars = [
		{ path: 'multiSelect_onremove', value: true },
		{ path: 'multiSelect_minimal', value: false },
		{ path: 'multiSelect_popoverMinimal', value: false },
		{ path: 'multiSelect_resetOnSelect', value: false },
		{ path: 'multiSelect_openOnKeyDown', value: false },
		{ path: 'multiSelect_icon', value: 'MapLarge-icons_close-delete' },
		{ path: 'multiSelect_placeholder', value: 'Search...' },
		{ path: 'multiSelect_intent', value: ctrl.BPBase.Intent.Primary },
	]

	export const tagInputVars = [
		{ path: 'tagInput_intent', value: ctrl.BPBase.Intent.Success },
		{ path: 'tagInput_icon', value: 'user' },
		{ path: 'tagInput_minimal', value: false },
		{ path: 'tagInput_large', value: false },
		{ path: 'tagInput_fill', value: false },
		{ path: 'tagInput_disabled', value: false },
		{ path: 'tagInput_placeholder', value: 'placeholder' },
	]

	export const omnibarVars = [
		{ path: 'omnibar_isShow', value: true },
		{ path: 'omnibar_resetOnSelect', value: false },
		{ path: 'omnibar_intent', value: ctrl.BPBase.Intent.Primary },
	]

	export const timePickerVars = [
		{ path: 'timePicker_precision', value: ctrl.BPBase.Precision.Milisecond },
		{ path: 'timePicker_disabled', value: false },
		{ path: 'timePicker_showArrowButtons', value: false },
		{ path: 'timePicker_selectAllOnFocus', value: false },
		{ path: 'timePicker_useAmPm', value: false },
		{ path: 'timePicker_minTime', value: '' },
		{ path: 'timePicker_maxTime', value: '' }
	]

	export const datePickerVars = [
		{ path: 'datePicker_precision', value: ctrl.BPBase.Precision.Milisecond },
		{ path: 'datePicker_min_date', value: new Date(1999, 10 - 1, 25) },
		{ path: 'datePicker_max_date', value: '' },
		{ path: 'datePicker_year_range', value: '1998:2018' },
		{ path: 'datePicker_showActionsBar', value: false },
		{ path: 'datePicker_reverseMonthAndYearMenus', value: true },
		{ path: 'datePicker_canClearSelection', value: false },
		{ path: 'datePicker_defaultValue', value: '' }
	]

	export const contextMenuVars = [
		{ path: 'contextMenu_icon', value: 'load-clear' },
	]

	export const inputGroupVars = [
		{ path: "inputGroup_isLarge", value: false },
		{ path: "inputGroup_isVertical", value: true },
		{ path: "inputGroup_isFill", value: false },
		{ path: "inputGroup_isDisabled", value: false },
		{ path: "inputGroup_isReadonly", value: false },
	]

	export  const editableTextVars = [
		{ path: 'placeholderInput', value: 'Edit text...' },
		{ path: 'placeholderTextarea', value: 'Edit report... (controlled, multiline)' },
		{ path: 'editableText_intent', value: ctrl.BPBase.Intent.Primary },
		{ path: 'editableText_selected_value', value: false},
		{ path: 'editableText_breakNewLine_value', value: false},
		{ path: 'editableText_MaxLength_value', value: 50},
	]

	export const overlayVars = [
		{ path: 'overlay_headerText' , value: 'Header Text' },
		{ path: 'overlay_click_outside' , value:true },
		{ path: 'overlay_esc_key' , value: false },
		{ path: 'overlay_backdrop' , value: false },
		{ path: 'overlay_modal_body_text_one' , value: 'This is a simple container with some inline styles to position it on the screen. Its CSS transitions are customized for this example only to demonstrate how easily custom transitions can be implemented.' },
		{ path: 'overlay_modal_body_text_two' , value: 'Click the right button below to transfer focus to the "Show overlay" trigger button outside of this overlay. If persistent focus is enabled, focus will be constrained to the overlay. Use the tab key to move to the next focusable element to illustrate this effect.' },
		{ path: 'overlay_transitionDuration' , value: 100 },
		{ path: 'overlay_closeButton' , value: "Close"},
		{ path: 'overlay_submitButton' , value: "Submit"},
	]

	export const progressVars = [
		{ path: 'progress_intent', value: ctrl.BPBase.Intent.Success},
		{ path: 'progress_rangeWidth', value: 10},
		{ path: 'progress_rangeDisable', value: false},
	]

	export const spinnerVars = [
		{ path: 'spinner_intent', value: ctrl.BPBase.Intent.Default},
		{ path: 'spinner_svgSize', value: 100},
		{ path: 'spinner_rangeDisable', value: false},
		{ path: 'spinner_strokeSize', value: 60},
	]

	export const skeletonVars = [
		{ path: 'skeleton_skeleton', value: false},
		{ path: 'skeleton_icon', value: 'add210' },
		{ path: 'skeleton_title', value: 'Card heading' },
		{ path: 'skeleton_text', value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque eget tortor felis. Fusce dapibus metus in dapibus mollis. Quisque eget ex diam.' },
	]

	export const toastVars = [
		{ path: 'toast_modalPosition', value: ctrl.BPToast.AlignmentSelect.TopCenter},
		{ path: 'toast_autoFocus', value: false },
		{ path: 'toast_escClear', value: false },
		{ path: 'toast_timeout', value: 7000 },
	]
	export const timeZoneVars = [
		{ path: 'timeZone_standart', value: ctrl.BPTimeZonePicker.TimeZoneStandart.Composite},
		{ path: 'timeZone_showLocal', value: true },
		{ path: 'timeZone_disabled', value: false },
	]

	export const dialogVars = [
		{ path: 'dialog_icon', value: 'info28'},
		{ path: 'dialog_heading', value: 'Palantir Foundry' },
		{ path: 'dialog_button1Text', value: 'Close' },
		{ path: 'dialog_button2Text', value: 'Visit the Foundry website' },
		{ path: 'dialog_canOutsideClickClose', value: true },
		{ path: 'dialog_canEscapeKeyClose', value: true },
		{ path: 'dialog_text', value: `
			<strong>Data integration is the seminal problem of the digital age. For over ten years, we’ve helped the world’s premier organizations rise to the challenge.</strong>
			<br>
			Palantir Foundry radically reimagines the way enterprises interact with data by amplifying and extending the power of data integration. With Foundry, anyone can source, fuse, and transform data into any shape they desire. Business analysts become data engineers — and leaders in their organization’s data revolution.
			<br>
			Foundry’s back end includes a suite of best-in-class data integration capabilities: data provenance, git-style versioning semantics, granular access controls, branching, transformation authoring, and more. But these powers are not limited to the back-end IT shop.
			<br>
			In Foundry, tables, applications, reports, presentations, and spreadsheets operate as data integrations in their own right. Access controls, transformation logic, and data quality flow from original data source to intermediate analysis to presentation in real time. Every end product created in Foundry becomes a new data source that other users can build upon. And the enterprise data foundation goes where the business drives it.
			<br>
			Start the revolution. Unleash the power of data integration with Palantir Foundry.
		`},
	]

	export const navBarVars = [
		{ path: 'navBar_align', value: ctrl.BPBase.Alignment.Around},
		{ path: 'navBar_fixedToTop', value: false },
		{ path: 'navBar_dark', value: false },
		{ path: 'navBar_input', value: false },
	]

	export const controlGroupVars = [
		{ path: 'controlGroup_vertical', value: false },
		{ path: 'controlGroup_fill', value: false },
	]

	export const dividerVars = [
		{ path: 'divider_verdical', value: false},
	]

	export const formGroupVars = [
		{ path: 'formGroup_inline', value: true},
		{ path: 'formGroup_disabled', value: false},
		{ path: 'formGroup_intent', value: ctrl.BPBase.Intent.Success},
		{ path: 'formGroup_helperShow', value: true},
		{ path: 'formGroup_helperText', value:  'Helper text with details...'},
		{ path: 'formGroup_showLabel', value: true},
		{ path: 'formGroup_showLabelText', value: 'Label'},
		{ path: 'formGroup_showLabelInfo', value: true},
		{ path: 'formGroup_showLabelInfoText', value: ' (required)'},
		{ path: 'formGroup_switchTextShow', value: true},
		{ path: 'formGroup_switchText', value: 'Engage the hyperdrive'},
	]

	export const overflowVars = [
		{ path: 'overflow_icon', value: "mlicon-moreoptions"},
		{ path: 'overflow_width', value: 50},
		{ path: 'overflow_position', value: ctrl.BPBase.Position.Start},
		{ path: 'overflow_linkData', value: [
				{label: 'image.jpg'},
				{label: 'Wednesday',href:"#"},
				{label: 'Photos',href:"#"},
				{label: 'Janet'},
				{label: 'Users'},
				{label: 'All files'},
			]},
	]



	export const tabsVars = [
		{ path: 'tabs_animate', value: false},
		{ path: 'tabs_vertical', value: false},
		{ path: 'tabs_activePanelOnly', value: false},
		{ path: 'tabs_navbarTabsList', value: [{name:'Home'}, {name:'Files'}, {name:'Builds'}],},
		{ path: 'tabs_tabList', value: [
				{
					name:'Angular',
					body: {
						title: 'Example panel: Angular',
						text:  "HTML is great for declaring static documents, but it falters when we try to use it for declaring dynamic views in web-applications. AngularJS lets you extend HTML vocabulary for your application. The resulting environment is extraordinarily expressive, readable, and quick to develop.",
					},
				},
				{
					name:'React',
					body:{
						title: 'Example panel: React',
						text:  "Lots of people use React as the V in MVC. Since React makes no assumptions about the rest of your technology stack, it's easy to try it out on a small feature in an existing project.",
					},
				},
				{
					name:'Ember',
					body:{
						title: 'Example panel: Ember',
						text:  "Ember.js is an open-source JavaScript application framework, based on the model-view-controller (MVC) pattern. It allows developers to create scalable single-page web applications by incorporating common idioms and best practices into the framework. What is your favorite JS framework? <p><input class='ml-blueprint-input' /></p>",
					},
				},
				{
					name:'Backbone',
					body:{
						title: 'Example panel: Backbone',
						text:  'Nothing',
					},
					disabled: true,
				},
			],
		},
	]

	export const dateInputVars = [
		{ path: 'dateInput_precision', value: ctrl.BPBase.Precision.Milisecond },
		{ path: 'dateInput_min_date', value: new Date(1999, 10 - 1, 25) },
		{ path: 'dateInput_max_date', value: '' },
		{ path: 'dateInput_year_range', value: '1998:2018' },
		{ path: 'dateInput_showActionsBar', value: false },
		{ path: 'dateInput_reverseMonthAndYearMenus', value: true },
		{ path: 'dateInput_canClearSelection', value: false },
		{ path: 'dateInput_disabled', value: false},
		{ path: 'dateInput_dateFormat', value: ctrl.BPDateInput.DateFormat.JS_DATE},
		{ path: 'dateInput_closeOnSelection', value: false}
	]

	export const htmlTableVars = [
		{ path: 'htmlTable_condensed', value: false},
		{ path: 'htmlTable_striped', value: false},
		{ path: 'htmlTable_bordered', value: false},
		{ path: 'htmlTable_interactive', value: false},
		{ path: 'htmlTable_dataTable', value: {
				thead:['Project','Description','Technologies'],
				tbody:[
					['Blueprint','CSS framework and UI toolkit','Sass, TypeScript, React'],
					['TSLint','Static analysis linter for TypeScript','TypeScript'],
					['Plottable','Composable charting library built on top of D3','SVG, TypeScript, D3'],
				]
			}},
	]

	export const menuVars = [
		{ path: 'menu_textLarge', value: false},
		{ path: 'menu_menuItems', value: [
				{
					leftIcon:"mlicon-x64AdvancedMapEditor",
					rightIcon:"",
					shortCuts:"",
					text:"Custom SVG icon",
					disabled:false,
					onClick:false,
					isTitle:false,
					urlHref:"",
					targetBlank:false,
					isDivider:false,
					intent:ctrl.BPBase.Intent.Default
				},
				{
					isDivider:true
				},
				{
					leftIcon:'mlicon-file242',
					rightIcon:'mlicon-MapLarge-icons_data-source',
					shortCuts:"",
					text:"New text box",
					disabled:false,
					onClick:false,
					isTitle:false,
					urlHref:"",
					targetBlank:false,
					isDivider:false,
					intent:ctrl.BPBase.Intent.Default
				},
				{
					leftIcon:'mlicon-earth-globe',
					rightIcon:'',
					shortCuts:"",
					text:"New project",
					disabled:true,
					onClick:false,
					isTitle:false,
					urlHref:"",
					targetBlank:false,
					isDivider:false,
					intent:ctrl.BPBase.Intent.Default
				},
				{
					leftIcon:'mlicon-MapLarge-icons_share-link',
					rightIcon:'',
					shortCuts:"",
					text:"New link",
					disabled:false,
					onClick:false,
					isTitle:false,
					urlHref:"#",
					targetBlank:true,
					isDivider:false,
					intent:ctrl.BPBase.Intent.Default
				},
				{
					isDivider:true
				},
				{
					leftIcon:'mlicon-gear1',
					rightIcon:'',
					shortCuts:"",
					text:"Settings",
					disabled:false,
					onClick:false,
					isTitle:false,
					urlHref:"",
					targetBlank:false,
					isDivider:false,
					intent:ctrl.BPBase.Intent.Default,
					children:[
						{
							leftIcon:"",
							rightIcon:"",
							shortCuts:"",
							text:"Edit",
							disabled:false,
							onClick:false,
							isTitle:true,
							urlHref:"",
							targetBlank:false,
							isDivider:false,
							intent:ctrl.BPBase.Intent.Default
						},
						{
							leftIcon:'mlicon-MapLarge-icons_mask',
							rightIcon:'mlicon-wind-lines',
							shortCuts:"X",
							text:"Cut",
							disabled:false,
							onClick:false,
							isTitle:false,
							urlHref:"",
							targetBlank:false,
							isDivider:false,
							intent:ctrl.BPBase.Intent.Default
						},
						{
							leftIcon:'mlicon-MapLarge-icons_duplicate',
							rightIcon:'mlicon-wind-lines',
							shortCuts:"C",
							text:"Copy",
							disabled:true,
							onClick:true,
							isTitle:false,
							urlHref:"",
							targetBlank:false,
							isDivider:false,
							intent:ctrl.BPBase.Intent.Default
						},
						{
							leftIcon:'mlicon-documents7',
							rightIcon:'mlicon-wind-lines',
							shortCuts:"V",
							text:"Paste",
							disabled:true,
							onClick:true,
							isTitle:false,
							urlHref:"#",
							targetBlank:true,
							isDivider:false,
							intent:ctrl.BPBase.Intent.Default
						},
						{
							isDivider:true
						},
						{
							leftIcon:'',
							rightIcon:'',
							shortCuts:"",
							text:"Text",
							disabled:false,
							onClick:true,
							isTitle:true,
							urlHref:"",
							targetBlank:false,
							isDivider:false,
							intent:ctrl.BPBase.Intent.Default
						},
						{
							leftIcon:'mlicon-MapLarge-icons_align-left',
							rightIcon:'',
							shortCuts:"",
							text:"Alignment",
							disabled:true,
							onClick:true,
							isTitle:false,
							urlHref:"",
							targetBlank:false,
							isDivider:false,
							intent:ctrl.BPBase.Intent.Default,
							children:[
								{
									leftIcon:"mlicon-archive18",
									rightIcon:"",
									shortCuts:"",
									text:"Something",
									disabled:false,
									onClick:false,
									isTitle:false,
									urlHref:"",
									targetBlank:false,
									isDivider:false,
									intent:ctrl.BPBase.Intent.Default
								},
							]
						},
						{
							leftIcon:'mlicon-style',
							rightIcon:'',
							shortCuts:"",
							text:"Style",
							disabled:false,
							onClick:false,
							isTitle:false,
							urlHref:"",
							targetBlank:false,
							isDivider:false,
							intent:ctrl.BPBase.Intent.Default,
							children:[
								{
									leftIcon:"mlicon-MapLarge-icons_align-center",
									rightIcon:"",
									shortCuts:"",
									text:"Center",
									disabled:false,
									onClick:true,
									isTitle:false,
									urlHref:"",
									targetBlank:false,
									isDivider:false,
									intent:ctrl.BPBase.Intent.Default
								},
								{
									leftIcon:"mlicon-MapLarge-icons_align-left",
									rightIcon:"",
									shortCuts:"",
									text:"Left",
									disabled:false,
									onClick:true,
									isTitle:false,
									urlHref:"",
									targetBlank:false,
									isDivider:false,
									intent:ctrl.BPBase.Intent.Default
								},
								{
									leftIcon:"mlicon-MapLarge-icons_align-right",
									rightIcon:"",
									shortCuts:"",
									text:"Right",
									disabled:false,
									onClick:true,
									isTitle:false,
									urlHref:"",
									targetBlank:false,
									isDivider:false,
									intent:ctrl.BPBase.Intent.Default
								},

							]
						},
						{
							leftIcon:'mlicon-shining-sun',
							rightIcon:'',
							shortCuts:"",
							text:"Miscelladeous",
							disabled:false,
							onClick:false,
							isTitle:false,
							urlHref:"",
							targetBlank:false,
							isDivider:false,
							intent:ctrl.BPBase.Intent.Default,
							children:[
								{
									leftIcon:"mlicon-light",
									rightIcon:"",
									shortCuts:"",
									text:"Light",
									disabled:false,
									onClick:true,
									isTitle:false,
									urlHref:"",
									targetBlank:false,
									isDivider:false,
									intent:ctrl.BPBase.Intent.Default
								},
								{
									leftIcon:"mlicon-speedometer26",
									rightIcon:"",
									shortCuts:"",
									text:"Spped Test",
									disabled:false,
									onClick:true,
									isTitle:false,
									urlHref:"",
									targetBlank:false,
									isDivider:false,
									intent:ctrl.BPBase.Intent.Default
								},

								{
									leftIcon:"mlicon-MapLarge-icons_options",
									rightIcon:"",
									shortCuts:"",
									text:"Long text is access for developers",
									disabled:false,
									onClick:false,
									isTitle:false,
									urlHref:"",
									targetBlank:false,
									isDivider:false,
									intent:ctrl.BPBase.Intent.Default,
									children:[
										{
											leftIcon:"mlicon-MapLarge-icons_save-layer",
											rightIcon:"",
											shortCuts:"",
											text:"Star",
											disabled:false,
											onClick:true,
											isTitle:false,
											urlHref:"",
											targetBlank:false,
											isDivider:false,
											intent:ctrl.BPBase.Intent.Default
										},
										{
											leftIcon:"mlicon-MapLarge-icons_share-link",
											rightIcon:"",
											shortCuts:"",
											text:"Link",
											disabled:false,
											onClick:true,
											isTitle:false,
											urlHref:"",
											targetBlank:false,
											isDivider:false,
											intent:ctrl.BPBase.Intent.Default
										},

										{
											leftIcon:"mlicon-balloon",
											rightIcon:"",
											shortCuts:"",
											text:"Chat Hangouts",
											disabled:false,
											onClick:true,
											isTitle:false,
											urlHref:"",
											targetBlank:false,
											isDivider:false,
											intent:ctrl.BPBase.Intent.Default
										},
										{
											leftIcon:"mlicon-social",
											rightIcon:"",
											shortCuts:"",
											text:"Social Shares",
											disabled:false,
											onClick:false,
											isTitle:false,
											urlHref:"",
											targetBlank:false,
											isDivider:false,
											intent:ctrl.BPBase.Intent.Default,
											children:[
												{
													leftIcon:"mlicon-MapLarge-icons_data-source",
													rightIcon:"",
													shortCuts:"",
													text:"Map Large",
													disabled:false,
													onClick:true,
													isTitle:false,
													urlHref:"",
													targetBlank:false,
													isDivider:false,
													intent:ctrl.BPBase.Intent.Default
												},
												{
													leftIcon:"mlicon-basemap",
													rightIcon:"",
													shortCuts:"",
													text:"Internet",
													disabled:false,
													onClick:true,
													isTitle:false,
													urlHref:"",
													targetBlank:false,
													isDivider:false,
													intent:ctrl.BPBase.Intent.Default
												},
												{
													leftIcon:"mlicon-compass66",
													rightIcon:"",
													shortCuts:"",
													text:"Safari",
													disabled:false,
													onClick:true,
													isTitle:false,
													urlHref:"",
													targetBlank:false,
													isDivider:false,
													intent:ctrl.BPBase.Intent.Default
												},
												{
													isDivider:true
												},
												{
													leftIcon:"mlicon-delete",
													rightIcon:"",
													shortCuts:"",
													text:"Delete",
													disabled:false,
													onClick:true,
													isTitle:false,
													urlHref:"",
													targetBlank:false,
													isDivider:false,
													intent:ctrl.BPBase.Intent.Danger
												},
											]
										},
									]
								},
							]
						},
					]
				},

			]},
	]

	export const popoverVars = [
		{ path: "popover_disabled", value: false },
		{ path: "popover_canEscapeKeyClose", value: false },
		{ path: "popover_openOnTargetFocus", value: false },
		{ path: "popover_preventOverflow", value: false },
		{ path: "popover_arrow", value: false },
		{ path: "popover_flip", value: false },
		{ path: "popover_label", value: 'Popover target' },
		{ path: "popover_intent", value: ctrl.BPBase.Intent.Primary },
		{ path: "popover_position", value: ctrl.BPPopover.Position.AUTO },
		{ path: "popover_popoverInteractionKind", value: ctrl.BPPopover.PopoverInteractionKind.CLICK },
		{ path: "popover_preventOverflowType", value: ctrl.BPPopover.PreventOverflowTypes.SCROLL_PARENT },
	]
}