namespace dashboard.extensions.spinner {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	// slider is under construction

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.spinner',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing new Custom Controls'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([
				bp.BPSpinner.builder().options({
					intent: bp.BPBase.Intent.Success,
					circleSize:70,
					animationStop:false,
					strokeSize:60
					
				}),

			]).toJSON();
	}
}