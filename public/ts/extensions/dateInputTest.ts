namespace dashboard.extensions.dateinput {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	// slider is under construction

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.dateinput',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing new Custom Controls'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([
                bp.BPDateInput.builder().options({
					min_date: new Date(1999, 10 - 1, 25),
					year_range: "1998:2018",
					showActionsBar: true,
					canClearSelection: true,
					reverseMonthAndYearMenus: true,
					Precision: bp.BPBase.Precision.Milisecond,
					dateFormat: bp.BPDateInput.DateFormat.YEAR_MONTH_DAY_TIME,
					closeOnSelection: true
				})
			]).toJSON();
	}
}