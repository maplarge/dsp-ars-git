namespace dashboard.extensions.tree {
	import mdash = ml.ui.dashboard;
	import bp = dashboard.components.controls;

	// tree is under construction

	//mdash.registerPublicDashboard({
	//	id: 'dashboard.extensions.tree',
	//	name: 'Main Custom DB test',
	//	description: 'Main page for testing Slider'
	//});
	
	export function getJSON() {

		return mdash.controls.ScopeDefaults.builder()
			.options({
				defaults: [
				]
			})
			.children([

				bp.BPTree.builder().options({
					data: [
						{
							eyeTextToolTip:"",
							eye:"",
							type:"folder",
							isOpen:true,
							name:"Folder 0",
							iconCaretClass:"mlicon-arrowdown",
							iconClass:"mlicon-load",
							hasIcon:Function,
							subMenu:[
								{
									name:"Item 0",
									iconCaretClass:"mlicon-arrowdown",
									iconClass:"mlicon-document162",
									eyeTextToolTip:"",
									eye:"",
									type:"file",
								},
								{
									eyeTextToolTip:"I am foo!",
									eye:"mlicon-hide",
									type:"folder",
									isOpen:false,
									name:"Folder 5",
									iconCaretClass:"mlicon-arrowdown",
									iconClass:"mlicon-load",
									hasIcon:Function,
									subMenu:[
										{
											name:"item 1",
											iconCaretClass:"mlicon-arrowdown",
											iconClass:"mlicon-document162",
											hasIcon:Function,
											eyeTextToolTip:"",
											eye:"",
											type:"file",
										},
										{
											name:"item 2",
											iconCaretClass:"mlicon-arrowdown",
											iconClass:"mlicon-drawingtools-label",
											hasIcon:Function,
											eyeTextToolTip:"",
											eye:"",
											type:"file",
										}
									] 
								}
							]
						},
						{
							eyeTextToolTip:"I am foo2!",
							eye:"mlicon-hide",
							type:"folder",
							isOpen:true,
							name:"Folder 1",
							iconCaretClass:"mlicon-arrowdown",
							iconClass:"mlicon-load",
							hasIcon:Function,
							subMenu:[
								{
									name:"Item 0",
									iconCaretClass:"mlicon-arrowdown",
									iconClass:"mlicon-document162",
									hasIcon:Function,
									eyeTextToolTip:"",
									eye:"",
									type:"file"
								}, 
								{
									name:"Organic meditation gluten-free, sriracha VHS drinking vinegar beard man.",
									iconCaretClass:"mlicon-arrowdown",
									iconClass:"mlicon-drawingtools-label",
									hasIcon:Function,
									eyeTextToolTip:"",
									eye:"",
									type:"file"
								},
								{
									isOpen:true,
									name:"Folder 2",
									iconCaretClass:"mlicon-arrowdown",
									iconClass:"mlicon-load",
									hasIcon:Function,
									eyeTextToolTip:"",
									eye:"",
									type:"folder",
									subMenu:[
										{
											name:'no icon item',
											iconCaretClass:"mlicon-arrowdown",
											iconClass:"",
											hasIcon:Function,
											eyeTextToolTip:"",
											eye:"",
											type:"file"
										},
										{
											name:'item 1',
											iconCaretClass:"mlicon-arrowdown",
											iconClass:"mlicon-drawingtools-label",
											hasIcon:Function,
											eyeTextToolTip:"",
											eye:"",
											type:"file"
										},
										{
											name:'Folder 3',
											isOpen:true,
											iconCaretClass:"mlicon-arrowdown",
											iconClass:"mlicon-load",
											hasIcon:Function,
											eyeTextToolTip:"",
											eye:"",
											type:"folder",
											subMenu:[
												{
													name:"item 0",
													iconCaretClass:"mlicon-arrowdown",
													iconClass:"mlicon-document162",
													hasIcon:Function,
													eyeTextToolTip:"",
													eye:"",
													type:"file",
												},
												{
													name:"item 1",
													iconCaretClass:"mlicon-arrowdown",
													iconClass:"mlicon-load",
													hasIcon:Function,
													eyeTextToolTip:"",
													eye:"",
													isOpen:true,
													type:"folder",
													subMenu:[
														{
															name:"item 1",
															iconCaretClass:"mlicon-arrowdown",
															iconClass:"mlicon-document162",
															hasIcon:Function,
															eyeTextToolTip:"",
															eye:"",
															type:"file",
														},
														{
															eyeTextToolTip:"I am foo!",
															eye:"mlicon-hide",
															type:"folder",
															isOpen:false,
															name:"Folder 5",
															iconCaretClass:"mlicon-arrowdown",
															iconClass:"mlicon-load",
															hasIcon:Function,
															subMenu:[
																{
																	name:"item 1",
																	iconCaretClass:"mlicon-arrowdown",
																	iconClass:"mlicon-document162",
																	hasIcon:Function,
																	eyeTextToolTip:"",
																	eye:"",
																	type:"file",
																},
																{
																	name:"item 2",
																	iconCaretClass:"mlicon-arrowdown",
																	iconClass:"mlicon-drawingtools-label",
																	hasIcon:Function,
																	eyeTextToolTip:"",
																	eye:"",
																	type:"file",
																}
															] 
														}
													]
												}
											]
										}
									]
								}
							]
						}
					]
					
				}),

			]).toJSON();
	}
}