﻿The MapLarge Javascript API is a client side Javascript library used to create maps, layers graphs and other big data visualizations.  It interacts with MapLarge web services that are drop in compatible with Google Maps, Leaflet, ESRI and other mapping API's.
* [More Quick Start Guides](http://maplarge.com/api/examples)
* [Documentation](http://maplarge.com/developer/js)
---
#Quick Start Using the MapLarge Javascript API

### Add the Javascript API to the Web Page
To use the MapLarge Javascript API, add the following script tag at the top of your HTML page in the &lt;head&gt; section:
```javascript
<script type="text/javascript" src="//e.maplarge.com/JS"></script>
```
### Three Steps to Display a Map on a Web Page
* [Create a map](http://maplarge.com/developer/js/createmaps) instance within a DOM element (Technical reference at [[ml.map]]).
* [Create and display data layers on the map](http://maplarge.com/developer/js/createmaplayers) (Technical reference at [[ml.layer]]).

#### Example Source Code
[Live Example](http://e.maplarge.com/example.html?id=150016053043164125175104151129146170231153051006)
```
<html>
    <head>
        <script type="text/javascript" src="/JS"></script>
        <script type="text/javascript">
            ml.onload(function () {

                // Create a map zoomed in on Atlanta, GA (34,-84)
                var map = new ml.map('mapDiv', { lat: 33.709, lng: -84.404, z: 9 });

                // Add a layer with no style: default is "red dots"
                var layer = new ml.layer(map, {
                    query: {
                        table: 'hms/hotels',  //table name = hotels
                        select: 'geo.dot' //geography = dots
                    },
                });

                //show the layer
                layer.show();
            });

        </script>
    </head>
    <body>
        <div id="mapDiv" style="width:100%;height:100%;"></div>
    </body>
</html>
```
---

Jump to the documentation for creating a MapLarge Map using [[ml.map]] or Layer using [[ml.layer]] methods.

---
# Copyright and License
Copyright © 2017 [MapLarge, Inc.](http://maplarge.com)