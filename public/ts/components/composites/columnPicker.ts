namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class ColumnPicker2<T> extends mdash.controls.CompositeInputControl<ColumnPicker2.IOptionsDefinition, ColumnPicker2.IOptions> {

		public static type: string = 'ColumnPicker2';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.PRODUCTION,
			level: mdash.ControlLevel.ADVANCED,
			friendlyName: 'Column Picker',
			shortDescription: "Provides a column picker based on a chosen datasource",
			categories: ['Input'],
			childSupport: mdash.ChildSupport.NONE
		};

		public static getInitialConfiguration() {
			return ColumnPicker2.builder()
				.options({ items: [], datasource: null })
				.toJSON();
		}

		protected definition: mdash.IControlDefinition<ColumnPicker2.IOptionsDefinition>;

		protected internalScope: ColumnPicker2.IInternalScope;

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'label', label: 'Label', type: mdash.IComponentConfigSettingTypes.TEXT },
				{ path: 'datasource', label: 'Datasource', type: mdash.IComponentConfigSettingTypes.DATASOURCE, acceptScopePath: true },
				{ path: 'autofocus', label: 'Auto Focus', type: mdash.IComponentConfigSettingTypes.BOOLEAN },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text', 'ColumnType'] },
			]
		}

		private dataSource: ml.data.IDataSource;
		private firstNonNullValueBeforeDataSource: string = null;
		private sourceArrayWatch: mdash.IDisposable;

		constructor(id: string, definition: mdash.IControlDefinition<ColumnPicker2.IOptionsDefinition>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

			this.internalScope = ml.$.extend(true, this.internalScope, {
				columnList: ml.ko.observableArray<ColumnPicker2.IInternalScope>([])
			});
		}

		protected validateDefinition(): boolean {
			if (!super.validateDefinition())
				return false;

			if (!this.definition.options.datasource)
				return false;

			return true;
		}

		protected init() {
			var value = this.registerScopeObservable(this.definition.name, "", false);

			super.init();

			this.watchScopePath(this.definition.name, (newValue: string) => {
				if (newValue != value()) {
					this.internalScope[this.definition.name](newValue);
				}
			});

			this.internalScope[this.definition.name].subscribe(nv => {
				if (nv != value()) {
					this.setScopeValue(this.definition.name, nv);
				}
			});
		}

		protected onOptionsChanged(options: ColumnPicker2.IOptions) {

			if (options.datasource == null) {
				return this.internalScope['columnList']([])
			}
			let colNames = (<any>options.datasource).source.columns.names.split(","),
				colTypes = (<any>options.datasource).source.columns.types.split(','),
				items = [],
				type = "";

			for (var i in colNames) {
				type = options.includeTypes ? `<span style="float:right; clear:left;">${colTypes[i]}</span>` : "";
				items.push({
					label: `<span style="font-weight:bold">${colNames[i]}</span>${type}`,
					value: colNames[i]
				});
			}
			this.internalScope['columnList'](items)
		}

		protected cleanupTransformDefinition(definition: mdash.ITransformDefinition<any>) {
			switch (definition.options.source) {
				case 'Text':
					definition.options.source = this.definition.name;
					break;
				case 'ColumnType':
					definition.options.source = this.definition.name + "_column_type";
					break;
			}
		}

		protected onDestroy(suppressNotification: boolean) {
			if (this.sourceArrayWatch)
				this.sourceArrayWatch.dispose();
		}

		protected getControlLayout(options: ColumnPicker2.IOptions): mdash.IControlDefinition<any> {

			return ctrl.BPSuggest.builder()
				.name(this.definition.name)
				.options({
					items: { $scopePath: "columnList"  },
					closeOnSelect: true,

				}).toJSON();

			//if (options.selectMode == CVP.SelectMode.Dropdown) {
			//	return ctrl.BPSelect.builder()
			//		.name("SelectedValue")
			//		.options({
			//			disabled: { $scopePath: 'options.disabled' },
			//			items: { $scopePath: 'values' }
			//		})
			//		.toJSON();
			//} else if (options.selectMode == CVP.SelectMode.DropdownSelect) {
			//	return ctrl.BPMultiSelect.builder()
			//		.name('SelectedValue')
			//		.options({
			//			//disabled: { $scopePath: 'options.disabled' },
			//			items: { $scopePath: "values" }
			//		}).toJSON();
			//} else {
			//	return ctrl.BPSuggest.builder()
			//		.name("SelectedValue")
			//		.options({
			//			items: { $scopePath: "values" },
			//			closeOnSelect: true,

			//		}).toJSON();
			//}
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<ColumnPicker2.IOptionsDefinition>(this.type);
		}

		public static typedBuilder<TScope extends mdash.ITypedScope>() {
			return new mdash.TypedControlDefinitionBuilderWithChildren<ColumnPicker2.ITypedOptionsDefinition<TScope>, TScope>(this.type);
		}
	}


	export namespace ColumnPicker2 {

		export interface IInternalScope extends mdash.controls.ICompositeInputInternalScope<IOptions> {
			values: KnockoutComputed<{ label: string, value: string }[]>
		}

		export type IScopeData = string | number | string[];

		export interface ITypedOptionsDefinition<TScope extends mdash.ITypedScope> extends ctrl.BPSuggest.IOptionsDefinition {
			datasource: mdash.DataSourceDefinition | mdash.ITypedScopePath<TScope, mdash.DataSourceDefinition>;
			columnTypes?: ml.data.enums.ColumnTypes[] | mdash.ITypedScopePath<TScope, ml.data.enums.ColumnTypes[]>;
			includeTypes?: boolean
		}

		export interface IOptionsDefinition extends ITypedOptionsDefinition<mdash.IScope> { }

		export interface IOptions extends ctrl.BPSuggest.IOptions {
			datasource: mdash.DataSourceDefinition;
			columnTypes?: ml.data.enums.ColumnTypes[],
			includeTypes?: boolean
		}
	}

	mdash.registerControl(ColumnPicker2, 'dashboard.components.controls', ColumnPicker2.meta);
}