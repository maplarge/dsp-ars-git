﻿namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;


	export class CVP extends mdash.controls.CompositeInputControl<CVP.IOptionsDefinition, CVP.IOptions> {
		public static type: string = 'CVP';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.INTERNAL,
			friendlyName: 'Column Value Picker',
			shortDescription: "Creates a list selecteable elements from distinct values in a data source",
			categories: ['Input'],
			childSupport: mdash.ChildSupport.NONE
		};

		protected internalScope: CVP.IInternalScope;
		protected definition: mdash.IControlDefinition<CVP.IOptionsDefinition>;

		private options: CVP.IOptions;
		private lastOptionsChangedTimeoutId: number;
		private lastColumnName: string;
		private hasSentDataInternally: boolean;
		private filterMode: CVP.FilterMode;
		private takeLimit: number;
		private lastDataSource: ml.data.IDataSource;
		private queryResult: KnockoutObservable<CVP.IQueryResult>;
		private lastSearchId: string = null;
		private queryRunner: ml.data.query.OrderedQueryRunner;
		private outputSubscription: KnockoutSubscription;

		constructor(id: string, definition: mdash.IControlDefinition<CVP.IOptionsDefinition>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
			this.queryRunner = new ml.data.query.OrderedQueryRunner();
			this.filterMode = CVP.FilterMode.CLIENT
			this.queryResult = ml.ko.observable(null);
			this.lastColumnName = null;
			this.lastOptionsChangedTimeoutId = null;

			this.internalScope.values = ml.ko.pureComputed(() => {
				if (this.definition.options.dataFormatter != null) {
					return this.getValueList(this.queryResult())
						.map(this.definition.options.dataFormatter)
						.sort((a, b) => {
							a = a.label.trim ? a.label.trim().toLowerCase() : a.label;
							b = b.label.trim ? b.label.trim().toLowerCase() : b.label;
							//always place "All" in first place
							if (a == 'all' && b != 'all') return -1;
							if (a != 'all' && b == 'all') return 1;
							return a > b ? 1 : a < b ? -1 : 0;
						});
				}
				return this.getValueList(this.queryResult());
			});
		}

		protected init() {
			var controlValue = this.registerScopeObservable<CVP.IScopeData>(this.definition.name, '', true, this.scope, false, false);

			var includeAll = ml.util.isNullOrUndefined(this.definition.options.includeAll) ? true : this.definition.options.includeAll;
			this.registerScopeObservable('includeAll', includeAll, true, this.internalScope);

			// This value won't be empty string if there's already something on the scope, which is 
			// why we are querying it again.
			var currentValue = this.getScopeValue(this.definition.name);
			if (this.definition.options.allowMultipleSelect) {
				currentValue = ml.util.isNullUndefinedOrEmpty(currentValue) ? [] : Array.isArray(currentValue) ? currentValue : [currentValue]
			}
			// We need to register SelectedValue here so that when the ColumnValuePicker gets a value 
			// from the ScopeRecorder that forces a layout change, the SelectedValue on the scope 
			// doesn't get lost in forceNewLayout().
			this.registerScopeObservable('SelectedValue', [], false, this.internalScope, false, false);

			this.takeLimit = this.definition.options.takeLimit;
			if (ml.util.isNullOrUndefined(this.takeLimit)) this.takeLimit = CVP.TAKE_LIMIT;

			this.outputSubscription = this.watchScopePath(this.definition.name, (newScopeData: CVP.IScopeData | string) => {
				this.updateInternalScope(newScopeData);
			}, true);

			super.init();

			this.objectsToDispose.push(
				this.internalScope.values.subscribe(valueList => {
					// We need to wait on the data to be sent before populating the selected value
					var shouldSend = this.hasSentDataInternally,
						saveValue = this.getScopeValue(this.definition.name);
					this.hasSentDataInternally = true;
					if (!shouldSend)
						(<any>ml.ko).tasks.schedule(() => {
							this.updateInternalScope(saveValue);
						});
				})
			);

			this.internalScope.SelectedValue.subscribe(nv => {
				if (nv != controlValue()) {
					this.setScopeValue(this.definition.name, nv);
				}
			});


			this.initTransforms();
		}

		protected getControlLayout(options: CVP.IOptions): mdash.IControlDefinition<any> {
			if (options.selectMode == CVP.SelectMode.Dropdown) {
				return ctrl.BPSelect.builder()
					.name("SelectedValue")
					.options({
						disabled: { $scopePath: 'options.disabled' },
						items: { $scopePath: 'values' }
					})
					.toJSON();
			} else if (options.selectMode == CVP.SelectMode.DropdownSelect) {
				return ctrl.BPMultiSelect.builder()
					.name('SelectedValue')
					.options({
						//disabled: { $scopePath: 'options.disabled' },
						items: { $scopePath: "values" }
					}).toJSON();
			} else {
				return ctrl.BPSuggest.builder()
					.name("SelectedValue")
					.options({
						items: { $scopePath: "values" },
						closeOnSelect: true,
						
					}).toJSON();
			}
		}

		protected onOptionsChanged(options: CVP.IOptions) {
			var clearedSelection = false;

			//var columnChanged = this.options && this.options.column != options.column;
			this.options = options;

			if (this.lastColumnName != options.column) {
				this.updateInternalScope('');
			}

			if (this.lastOptionsChangedTimeoutId) {
				clearTimeout(this.lastOptionsChangedTimeoutId);
			}
			this.lastOptionsChangedTimeoutId = setTimeout(() => {
				this.lastOptionsChangedTimeoutId = null;

				var columnChanged = ml.util.isNotNullUndefinedOrEmpty(this.lastColumnName) && ml.util.isNotNullOrUndefined(options.column) && this.lastColumnName !== options.column;
				this.lastColumnName = options.column;

				if (columnChanged && options.clearOnColumnChange) {
					this.updateInternalScope("");
					clearedSelection = true;
				}

				this.internalScope['includeAll'](ml.util.isNullOrUndefined(options.includeAll) ? true : options.includeAll);

				this.getDataSource(options.datasource, (ds) => {
					var didDSChange = !this.lastDataSource || !ds || this.lastDataSource.baseTableName != ds.baseTableName || !ml.data.dataSource.areEquivalent(this.lastDataSource, ds);

					var wasLastDSNull = this.lastDataSource == null;
					this.lastDataSource = ds;

					this.executeSearch(null, (queryResult, validColumn) => {
						// This is to stop re-layout if the control is destroyed
						if (this.isDestroyed) return;
						// This is to clear out the value if our datasource has changed but our column
						// has not and the new datasource does not have the new column.
						if (!wasLastDSNull && didDSChange && !clearedSelection && !validColumn) {
							this.updateInternalScope('');
							clearedSelection = true;
						}
						var newFilterMode = options.selectMode == CVP.SelectMode.Dropdown ||
							options.selectMode == CVP.SelectMode.DropdownSelect ||
							queryResult.totalCount <= this.takeLimit ? CVP.FilterMode.CLIENT : CVP.FilterMode.SERVER;

						var didFilterModeChange = newFilterMode != this.filterMode;

						this.filterMode = newFilterMode;

						if (newFilterMode == CVP.FilterMode.CLIENT) {
							if (queryResult.totalCount > this.takeLimit)
								console.warn("Too many results to display in drop down list. Limiting to first " + this.takeLimit);


							//update count for currently selected value
							var currentValue = this.getScopeValue<CVP.IScopeData>(this.definition.name);
							if (ml.util.isNotNullUndefinedOrEmpty(currentValue)) {
								if (Array.isArray(currentValue)) {
									for (var i = currentValue.length - 1; i >= 0; i--) {
										var insertVal = currentValue[i];
										var currentValueIndex = queryResult.values.indexOf(insertVal);

										if (currentValueIndex == -1) {
											//add placeholder for the current selection
											queryResult.values.unshift(insertVal);
											queryResult.counts.unshift(0);
										}
									}
								}
								else {
									var currentValueIndex = queryResult.values.indexOf(currentValue);

									if (currentValueIndex == -1) {
										//add placeholder for the current selection
										queryResult.values.unshift(currentValue);
										queryResult.counts.unshift(0);
									}
								}
							}
						}



						if (!didFilterModeChange)
							this.queryResult(queryResult);
						else {
							if (newFilterMode == CVP.FilterMode.CLIENT) {
								this.queryResult(queryResult);
								this.forceNewLayout();
							}
							else {
								this.forceNewLayout();
								this.queryResult(queryResult);
							}
						}
					}, false);

					/*if (options.selectMode == "Dropdown")
						this.executeSearch(!didDSChange);
					else {
						//check if selected value is still valid
						var currentValue = this.getScopeValue<CVP.IScopeData>(this.definition.name);
							
						if (didDSChange && !ml.util.isNullUndefinedOrEmpty(currentValue) && !Array.isArray(currentValue)) {
							this.executeSearch(false, "" + currentValue, (values) => {
								if (values.length == 0)
									this.updateScope(null); //selected value is no longer valid
							});
						}
					}*/
				});
			});
		}

		private updateInternalScope(value: any) {
			var setValue = this.definition.options.allowMultipleSelect
				? ml.util.orderedStringify(value) != ml.util.orderedStringify(this.internalScope["SelectedValue"]())
				: true;

			if (setValue && this.hasSentDataInternally) {
				var val;
				if (Array.isArray(value))
					val = this.definition.options.includeNullAndBlank ? value.slice().map(d => d == "" ? "(Empty String)" : d) : value.slice();
				else if (value == "" && this.definition.options.includeNullAndBlank) val = "(Empty String)";
				else val = value;
				this.internalScope["SelectedValue"](val);
				if (this.filterMode == CVP.FilterMode.SERVER && (this.options.selectMode == CVP.SelectMode.AutoComplete || this.options.allowMultipleSelect)) {
					this.executeSearch(null, (result, validColumn) => {
						this.queryResult(result);
					});
				}
			}
		}

		private executeServerSearch(term: string, callback?: (values: any[]) => void) {
			if (ml.util.isNullUndefinedOrEmpty(term)) {
				callback(this.getValueList(this.queryResult()));
			}
			else {
				this.executeSearch(term, (queryResult, validColumn) => {
					callback(this.getValueList(queryResult));
				});
			}
		}

		private executeSearch(term?: string, callback?: (result: CVP.IQueryResult, validColumn: boolean) => void, excludeCurrentlySelected: boolean = true) {
			var ds = this.lastDataSource;

			var validColumn = !!(ds && this.options.column && ds.columns[this.options.column]);
			if (validColumn) {
				var query: ml.data.query.Query;

				if (this.isSimpleDataSource(ds))
					query = ml.data.query.getQueryFromJSON(ml.data.dataSource.getQuery(ds)); //potential unsafe client-side optimization?
				else
					query = ml.query().from(ds);

				query
					.select(this.options.column + ".count")
					.select(this.options.column)
					.groupby(this.options.column)
					.take(this.takeLimit);

				if (this.options.orderByValue)
					query.orderby(this.options.column);
				else
					query.orderby(this.options.column + ".count.desc");

				if (ml.util.isNullOrUndefined(this.options.includeNullAndBlank) || this.options.includeNullAndBlank === false) {
					query.where(this.options.column, "IsNotNull", "");
					if (ds.columns[this.options.column].colType == ml.data.enums.ColumnTypes.STRING)
						query.where(this.options.column, "IsNotEmpty", "");
				}

				if (ml.util.isNotNullUndefinedOrEmpty(term))
					query.where(this.options.column, this.definition.options.searchMode || "Contains", term);
				var val = this.getScopeValue<string>(this.definition.name);
				if (excludeCurrentlySelected &&
					this.options.allowMultipleSelect &&
					(ml.util.isNotNullOrUndefined(this.options.selectMode) ||
						this.options.selectMode == CVP.SelectMode.AutoComplete) &&
					!(ml.util.isNullOrUndefined(val) ||
						(this.options.includeNullAndBlank && ml.util.isNullUndefinedOrEmpty(val)))) {
					if (Array.isArray(val)) query.where(this.options.column, "EqualNone", val);
					else query.where(this.options.column, "EqualNot", val);
				}

				if (this.options.filterEmptyString && ds.columns[this.options.column].colType == ml.data.enums.ColumnTypes.STRING) {
					query.where({ col: this.options.column, test: "IsNotEmpty", value: null });
				}
				var searchId = ml.util.uuid();
				this.lastSearchId = searchId;


				this.queryRunner.run(query, (result) => {
					if (this.lastSearchId !== searchId) return;
					this.lastSearchId = null;

					var queryResult: CVP.IQueryResult = {
						values: result.data[this.options.column] || [],
						counts: result.data[this.options.column + "_Count"] || [],
						totalCount: result.totals.Records
					};

					callback(queryResult, validColumn);
				}, null, true);
			} else {
				var queryResult: CVP.IQueryResult = {
					values: [],
					counts: [],
					totalCount: 0
				};

				callback(queryResult, validColumn);
			}
		}

		private getValueList(queryResult: CVP.IQueryResult) {
			var options: { label: string, value: string }[] = [];

			if (queryResult) {
				if ((this.options.selectMode == CVP.SelectMode.Dropdown ||
					this.options.selectMode == CVP.SelectMode.DropdownSelect) &&
					(this.internalScope['includeAll'] && this.internalScope['includeAll']())) {
					options.push({ label: "All", value: "" });
				}

				if (queryResult.values) {
					for (var i = 0; i < queryResult.values.length; i++) {
						var value = queryResult.values[i];
						var count = queryResult.counts[i];

						var label = value;

						//Adding to make empty string values more clear
						if (ml.util.isNullUndefinedOrEmpty(label)) {
							label = '(Empty String)';
							value = label;
						}
						else if (/^\s+$/.test(label))
							label = `"${label}"`;

						if (this.options.includeCount)
							label += ' - (' + ml.util.addCommas(count) + ')';

						options.push({ label: label, value: value });
					}
				}
			}

			return options;
		}

		private isSimpleDataSource(ds: ml.data.IDataSource) {
			var query = ml.data.dataSource.getQuery(ds);

			if (!query.sqlselect && !query.groupby && !query.join && !query.orderby)
				return true;
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilder<CVP.IOptionsDefinition>(this.type);
		}

		public static typedBuilder<TScope extends mdash.ITypedScope>() {
			return new mdash.TypedControlDefinitionBuilder<CVP.ITypedOptionsDefinition<TScope>, TScope>(this.type);
		}
	}

	export namespace CVP {

		export const TAKE_LIMIT = 200;


		export interface IInternalScope extends mdash.controls.ICompositeInputInternalScope<IOptions> {
			values: KnockoutComputed<{ label: string, value: string }[]>
		}

		export type IScopeData = string | number | string[];

		export interface ICVP extends BPBase.IAppearance, mdash.IControlOptionsWithTransforms {
		}

		export enum SelectMode {
			AutoComplete,
			Dropdown,
			DropdownSelect
		}

		export enum FilterMode {
			CLIENT,
			SERVER
		}

		export interface IQueryResult {
			values: any[]
			counts: number[]
			totalCount: number
		}

		export interface ITypedOptionsDefinition<TScope extends mdash.ITypedScope> extends mdash.IControlOptionsWithTransforms {
			selectMode?: SelectMode,
			disabled?: boolean | mdash.IScopePath,
			//datasource_Available: DataSourceDefinition,
			datasource: mdash.DataSourceDefinition | mdash.ITypedScopePath<TScope, mdash.DataSourceDefinition>,
			column: string | mdash.ITypedScopePath<TScope, string>,
			includeCount: boolean,
			orderByValue?: boolean | mdash.ITypedScopePath<TScope, boolean>,
			clearOnColumnChange?: boolean,
			filterEmptyString?: boolean,
			allowMultipleSelect?: boolean | mdash.ITypedScopePath<TScope, boolean>,
			searchMode?: statics.operators,
			includeNullAndBlank?: boolean,
			takeLimit?: number,
			includeAll?: boolean,
			dataFormatter?: (d: any, i?: number) => any,
			doNotRequireListMatch?: boolean
		}

		export interface IOptionsDefinition extends ITypedOptionsDefinition<mdash.IScope> { }

		export interface IOptions extends ICVP {
			selectMode?: SelectMode,
			disabled?: boolean,
			datasource_Available: mdash.DataSourceDefinition,
			datasource: mdash.DataSourceDefinition,
			column: string,
			includeCount: boolean,
			orderByValue?: boolean,
			clearOnColumnChange?: boolean,
			filterEmptyString?: boolean,
			allowMultipleSelect?: boolean,
			searchMode?: statics.operators,
			includeNullAndBlank?: boolean,
			takeLimit?: number,
			includeAll?: boolean,
			dataFormatter?: (d: any, i?: number) => any,
			doNotRequireListMatch?: boolean
		}
	}
	mdash.registerControl(CVP, 'dashboard.components.controls', CVP.meta);
}