namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPCard extends mdash.BaseControl<BPCard.IOptionsDefinition> {
		public static type: string = 'BPCard';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Card',
			shortDescription: "A simple card that wraps content in a border and allows for simple hover/box shadow interactivity.",
			categories: ["Layout"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPCard.builder().children(children).toJSON();
		}

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		constructor(id: string, definition: mdash.IControlDefinition<BPCard.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		/**
		 * @inheritDoc
		 */
		public build() {
			super.build();

			this.contentDiv[0].classList.add('ml-blueprint-card');
			this.container.classList.add('namespace-blueprint-card');
		}
		/**
		 * @inheritDoc
		 */
		protected validateDefinition(): boolean {
			if (!super.validateDefinition())
				return false;

			return true;
		}
		/**
		 * @inheritDoc
		 */
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, null, false);

			if (this.isDOMLoaded) {
				this.recreateChildren();
				this.watchDefinitionOptions((options: BPCard.IOptions) => {
                    
					this.contentDiv[0].className = 'ml-blueprint-card';

					// adds a box shadow on card hover
					if(options.interactive)
					{
						this.contentDiv[0].classList.add('ml-blueprint-card-interactive')
					} 

					// Elevation is the intensity of the box shadow of the card
                    if(options.elevation)
                    {
						if(options.elevation == BPBase.Elevation.One)
						{
							this.contentDiv[0].classList.add('ml-blueprint-elevation-1');
						}
						else if(options.elevation == BPBase.Elevation.Two)
						{
							this.contentDiv[0].classList.add('ml-blueprint-elevation-2');
						}
						else if(options.elevation == BPBase.Elevation.Three)
						{
							this.contentDiv[0].classList.add('ml-blueprint-elevation-3');
						}
						else if(options.elevation == BPBase.Elevation.Four)
						{
							this.contentDiv[0].classList.add('ml-blueprint-elevation-4');
						}
						else
						{
							this.contentDiv[0].classList.add('ml-blueprint-elevation-0');                   
						}
                    }
				});
			}
		}
		/**
		 * @inheritDoc
		 */
		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPCard.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPCard {

		/**
		 * Property interface for properties used inside the card component
		 * @typedef ICardProps
		 * @interface
		 * @readonly
		 */
		export interface ICardProps extends BPBase.IProps{
			/**
			 * Box shadow intensity of the card
			 * @type {BPBase.Elevation}
			 */
			elevation?: BPBase.Elevation,
			/**
			 * Add box shadow on hover of the card
			 * @type {boolean}
			 */
            interactive?: boolean
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ICardProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            interactive?: boolean | mdash.IScopePath,
            elevation?: BPBase.Elevation | mdash.IScopePath,    
		}
	}

	mdash.registerControl(BPCard,'dashboard.components.controls', BPCard.meta);
}