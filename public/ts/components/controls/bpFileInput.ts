namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPFileInput extends mdash.BaseControl<BPFileInput.IOptionsDefinition> {
		public static type: string = 'BPFileInput';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'File Input',
			shortDescription: "FileInput is a uploading component, which is uploaded any images or files",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPFileInput.builder().options(BPFileInput.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPFileInput.IOptions> = {
			'BPFileInput': {
				options: {
					outputTransforms: []
				}
			}
		}

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		constructor(id: string, definition: mdash.IControlDefinition<BPFileInput.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
        }
        
        private fileInput: HTMLElement;
		private fancyInput: HTMLElement;
		private fileName: string;

		public build() {
			super.build();
            
            this.fileInput = ml.create('input','ml-blueprint-file-input',this.contentDiv[0]);
            this.fancyInput = ml.create('label','ml-blueprint-file-input-fancy',this.contentDiv[0]);
			
			this.fileInput.setAttribute('type','file');

			this.fileInput.classList.add('ml-blueprint-file-input');
			this.fancyInput.classList.add('ml-blueprint-file-input-fancy');

			this.contentDiv[0].appendChild(this.fileInput);
			this.contentDiv[0].appendChild(this.fancyInput);
			
			this.container.classList.add('namespace-blueprint-file-input');
		}


		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		private initChangeListener(obs: mdash.RefCountedObservable<any>): void{
			ml.$(this.fileInput).off('change');
			this.fileInput.onchange = (e: Event) => {
				let files = (<HTMLInputElement>e.target).files;
				this.fancyInput.textContent = files[0].name;
				this.fileName = files[0].name;

				if (this.fileName != obs() || obs() == null) {
					obs(this.fileName);
				}
				
				this.objectsToDispose.push(
					obs.subscribe(newValue => {
						if (obs() == null || newValue != obs()) {
							this.fancyInput.textContent = newValue;
						}
					})
				);
			}
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, this.fileName, false);
			this.initChangeListener(obs);
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPFileInput.IOptions) => {
					
					this.fancyInput.textContent = obs() || this.fileName || options.label;
					this.fancyInput.className = 'ml-blueprint-file-input-fancy';

					if(options.name)
					{
						this.fileInput.setAttribute('name', options.name);
						this.fileInput.setAttribute('id', options.name);
						this.fancyInput.setAttribute('for', options.name);
					}
					else
					{
						this.fileInput.removeAttribute('name');
						this.fileInput.removeAttribute('id');
						this.fancyInput.removeAttribute('for');
					}

					options.disabled ? this.fileInput.setAttribute('disabled', 'disabled') :
										this.fileInput.removeAttribute('disabled');

					if(options.large){
						this.fancyInput.classList.add('ml-blueprint-file-input-large');
					}
					
					if(options.fill){
						this.fancyInput.classList.add('ml-blueprint-file-input-fill');
					}
				});
			}

		}

		protected cleanup() {
			super.cleanup();
			this.fileInput.onchange = null;
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPFileInput.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPFileInput {

		/**
		 * Property list for File Input component
		 * @typedef IFileProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IProps
		 */
		export interface IFileProps extends BPBase.IProps{
			/**
			 * Disables interaction with the file input
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Fill the whole width of the parent container
			 * @type {boolean}
			 */
			fill?: boolean,
			/**
			 * Make the file input larger
			 * @type {boolean}
			 */
			large?: boolean,
			/**
			 * Label text for the file input
			 * @type {string}
			 */
			label?: string,
			/**
			 * Name of the file input
			 * @type {string}
			 */
            name?: string,
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IFileProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            label?: string | mdash.IScopePath,
            fill?: boolean | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
            large?: boolean | mdash.IScopePath,
            name?: string | mdash.IScopePath,       
		}
	}

	mdash.registerControl(BPFileInput,'dashboard.components.controls', BPFileInput.meta);
}