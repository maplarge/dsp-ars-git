namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;
	
	export class BPTooltip extends mdash.BaseControl<BPTooltip.IOptionsDefinition> {
		public static type: string = 'BPTooltip';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Tooltip',
			shortDescription: "A tooltip is a lightweight popover for showing additional information on hover",
			categories: ["Layout"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPTooltip.builder().options(BPTooltip.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPTooltip.IOptions> = {
			'BPTooltip': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		constructor(id: string, definition: mdash.IControlDefinition<BPTooltip.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		private tooltipContainerDiv: JQuery;
		private tooltipArrow: JQuery;
		private tooltipContent: JQuery;
		private popperInstance: any;

		/**
		 * @inheritdoc
		 */
		public build() {
			super.build();
			this.tooltipContainerDiv = ml.$(`<div class="ml-blueprint-tooltip-div"></div>`).appendTo(this.contentDiv);
			this.tooltipArrow = ml.$(`<div class="ml-blueprint-tooltip-arrow"></div>`).appendTo(this.tooltipContainerDiv);
			this.tooltipContent = ml.$(`<div class="ml-blueprint-tooltip-content">Tooltip</div>`).appendTo(this.tooltipContainerDiv);
			this.tooltipArrow.append(`<svg viewBox="0 0 30 30">
			<path class="ml-blueprint-tooltip-arrow-border" d="M8.11 6.302c1.015-.936 1.887-2.922 1.887-4.297v26c0-1.378-.868-3.357-1.888-4.297L.925 17.09c-1.237-1.14-1.233-3.034 0-4.17L8.11 6.302z"></path>
			<path class="ml-blueprint-tooltip-arrow-fill" d="M8.787 7.036c1.22-1.125 2.21-3.376 2.21-5.03V0v30-2.005c0-1.654-.983-3.9-2.21-5.03l-7.183-6.616c-.81-.746-.802-1.96 0-2.7l7.183-6.614z"></path>
		</svg>`);
			this.container.classList.add('namespace-blueprint-tooltip');
		}

		/**
		 * @inheritdoc
		 */
		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		/**
		 * @inheritdoc
		 */
		protected init() {
			super.init();
			if (this.isDOMLoaded) {
				this.createChildren();
				this.watchDefinitionOptions((options: BPTooltip.IOptions) => {
					this.tooltipContainerDiv[0].className = 'ml-blueprint-tooltip-div';
					if(options.intent)
					{
						this.tooltipContainerDiv.addClass(`ml-blueprint-tooltip-div-intent-${options.intent}`);
					}
					else
					{
						this.tooltipContainerDiv.addClass(`ml-blueprint-tooltip-div-intent-default`);
					}

					// instead of null, the target of the Popper should be a child of the Tooltip component instead
					
					// this.popperInstance = new (<any>window).Popper(null, this.tooltipContainerDiv, {
					// 	placement: options.position || 'auto',
					// 	modifiers: {
					// 		arrow: {
					// 			enabled: true
					// 		},
					// 		preventOverflow: {
					// 			enabled: true,
					// 		},
					// 		hide: {
					// 			enabled: false
					// 		}
					// 	}
					// });

					this.children.forEach(item => console.log(item));
				});
			}
		}	

		/**
		 * @inheritdoc
		 */
		protected cleanup() {
			super.cleanup();
		}

		/**
		 * @inheritdoc
		 */
		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPTooltip.IOptionsDefinition>(this.type);
        }
        
    }
    export namespace BPTooltip {

		/**
		 * Popover position enumeration
		 * @enum {string}
		 * @readonly
		 */
		export enum Position{
			/** Automatically place it relative to target */
			AUTO = 'auto',
			/** Automatically place it relative to the start of the target */
			AUTO_START = 'auto-start',
			/** Automatically place it relative to the end of the target */
			AUTO_END = 'auto-end',
			/** Right of the target */
			RIGHT = 'right',
			/** Right bottom of the target */
			RIGHT_BOTTOM = 'right-bottom',
			/** Right top of the target */
			RIGHT_TOP = 'right-top',
			/** Bottom of the target */
			BOTTOM = 'bottom',
			/** Bottom right of the target */
			BOTTOM_RIGHT = 'bottom-right',
			/** Bottom left of the target */
			BOTTOM_LEFT = 'bottom-left',
			/** Left of the target */
			LEFT = 'left',
			/** Left bottom of the target */
			LEFT_BOTTOM = 'left-bottom',
			/** Left top of the target */
			LEFT_TOP = 'left-top',
			/** Top of the target */
			TOP = 'top',
			/** Top left of the target */
			TOP_LEFT = 'top-left',
			/** Top right of the target */
			TOP_RIGHT = 'top-right'
		}


		/**
		 * Interface for Tooltip
		 * @typedef ITooltipProps
		 * @interface
		 * @readonly
		 */
		export interface ITooltipProps{
			/**
			 * Close Tooltip by pressing ESC
			 * @type {boolean}
			 */
			canEscapeKeyClose?: boolean,
			/**
			 * Disable the Tooltip
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Open Tooltip when the target is focused
			 * @type {boolean}
			 */
			openOnTargetFocus?: boolean,
			/**
			 * Set Tooltip position
			 * @type {Position}
			 */
			position?: Position,
			/**
			 * Intent background for the Tooltip
			 * @type {BPBase.Intent}
			 */
			intent?: BPBase.Intent
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ITooltipProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			canEscapeKeyClose?: boolean | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
			openOnTargetFocus?: boolean | mdash.IScopePath,
			position?: Position | mdash.IScopePath,
			intent?: BPBase.Intent | mdash.IScopePath

		}
	}
	mdash.registerControl(BPTooltip,'dashboard.components.controls', BPTooltip.meta);
}