namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPTextInput extends mdash.BaseInputControl<BPTextInput.IOptionsDefinition> {
		public static type: string = 'BPTextInput';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Text Input',
			shortDescription: "A foundational component for inputting text into the app via a keyboard.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.SINGLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPTextInput.builder().options(BPTextInput.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPTextInput.IOptions> = {
			'BPTextInput': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected textInputDiv: JQuery;
		protected textInput: JQuery;
		protected rightButton: JQuery;
		protected buttonUnLock: JQuery;
		protected spinner: JQuery;

		
		constructor(id: string, definition: mdash.IControlDefinition<BPTextInput.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
			this.container.classList.add('namespace-blueprint-textInput');			
			this.textInputDiv = ml.$('<div></div>');
			this.contentDiv.append(this.textInputDiv);
			this.textInputDiv.addClass('ml-blueprint-textInput');
			this.textInput = ml.$('<input>');
			this.textInputDiv.append(this.textInput);
			this.textInput.addClass('ml-blueprint-input');
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}		

		private initializeListeners(valueObs: mdash.RefCountedObservable<any>) {
			this.textInput.on('input', (event: JQueryEventObject) => { 
					if (this.textInput.val() != valueObs() || valueObs() == null) {
						valueObs(this.textInput.val()); 
					}
				});

			this.objectsToDispose.push(
				valueObs.subscribe(newValue => {
					if (valueObs() == null || newValue != this.textInput.val()) {
						this.textInput.val(newValue);
					}
				})
			)
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			this.initTransforms();
			this.initializeListeners(obs);
			this.textInput.focusin(() => {
				this.textInput.closest(this.textInputDiv).addClass('ml-blueprint-textInputBoxShadow');
			});
			this.textInput.focusout(() => {
				this.textInput.closest(this.textInputDiv).removeClass('ml-blueprint-textInputBoxShadow');                    
			});
			if (this.isDOMLoaded) {
				this.textInput.val(obs());			
				this.watchDefinitionOptions((options: BPTextInput.IOptions) => {
					ml.$(this.buttonUnLock).detach();
					ml.$(this.rightButton).detach();
					this.rightButton = ml.$(`<button class="ml-blueprint-button"><i class="ml-blueprint-textinput-right-icon ml-icon-${options.iconRight}"></i></button>`);
					this.buttonUnLock = ml.$(`<button class="ml-blueprint-button-unlock"><i class="ml-blueprint-textinput-right-icon ml-icon-un${options.iconRight} "></i></button>`);
					
				var changeType = () => {
					this.rightButton.on('click', ()=>{
						if(this.textInput.attr('type') == 'password') {
							this.textInput.attr('type', 'text');
						}						
					})
				}
				changeType()
				this.buttonUnLock.on('click', ()=>{
					if(this.textInput.attr('type') !== 'password'){
						this.textInput.attr('type', 'password');
					}
				})
				   
				if(options.fill)
				{
					this.textInputDiv.addClass('ml-Blueprint-TextInput-Fill');
				} else
				{
					this.textInputDiv.removeClass('ml-Blueprint-TextInput-Fill');
				}
				if(options.large) {
					this.textInputDiv.addClass('ml-blueprint-large-textInput');
				} else 
				{
					this.textInputDiv.removeClass('ml-blueprint-large-textInput');
				}
				if(options.intent)
				{
					this.textInputDiv.attr('class', (i, c) => c.replace(/(^|\s)ml-blueprint-textInput-\S+/g, ''));
					this.textInputDiv.addClass(`ml-blueprint-textInput-${options.intent}`);
				} else
				{
					this.textInputDiv.attr('class', (i,c) => c.replace(/(^|\s)ml-blueprint-textInput-\S+/g, ''));
				} 					
				if(options.iconLeft)
				{
					ml.$('i', this.textInputDiv).detach()
					this.textInputDiv.prepend(`<i class="ml-blueprint-textinput-left-icon ml-icon-${options.iconLeft}"></i>`);						
				} else
				{
					ml.$(`.ml-icon-${options.iconLeft}`).detach()
				}
				if(options.iconRight)
				{
					this.textInputDiv.append(this.rightButton);
				} else
				{
					this.rightButton.detach()
				}
				if(options.placeholder)
				{
					this.textInput.attr('placeholder', options.placeholder);
				} else
				{
					this.textInput.removeAttr('placeholder');
				}
				if(options.round) 
				{
					this.textInputDiv.addClass('ml-blueprint-textInput-round');

				} else 
				{
					this.textInputDiv.removeClass('ml-blueprint-textInput-round');
				}			
				if(options.type)
				{
					this.textInput.attr('type', options.type)
						
					if(options.type == 'password') {
					
						this.textInput.focusin(() => {
							this.textInput.closest(this.textInputDiv).addClass('ml-blueprint-textInput-password');
						});
						this.textInput.focusout(() => {
							this.textInput.closest(this.textInputDiv).removeClass('ml-blueprint-textInput-password');                    
						});
						this.rightButton.on('click',(()=>{
							this.rightButton.detach();
							this.textInputDiv.append(this.buttonUnLock);
						}));
	
						this.buttonUnLock.on('click',(()=>{
							this.buttonUnLock.detach();
							this.textInputDiv.append(this.rightButton);
						}))
						this.rightButton.attr('title', 'button' );					
					};
					
				} else
				{
					this.textInput.removeAttr('type');
				}
				if(options.value)
				{
					let checkOnce: boolean = false;	
					var spin =  ()=>{
						this.textInput.on('keyup', (event) => {
							this.textInput.attr('value', this.textInput.val());
							options.value = this.textInput.val();
							if(options.spin){
								if(this.textInput.val() !== '' && !checkOnce) {
									ml.$(this.spinner).detach();
									this.spinner = ml.$('<button class="ml-blueprint-button"><i class="ml-blueprint-textinput-right-icon ml-icon-spinner ml-icon-spin"></i></button>');
									this.textInputDiv.append(this.spinner);
									checkOnce = true;
								}
								else if(this.textInput.val() == '')
								{this.spinner.detach();
									checkOnce = false;
								}
							}
						});
					} 
					spin()
				} else 
				{
					this.textInput.off('keyup')
				}

				if(options.readonly)
				{
					this.textInput.attr('readonly', 'true');
					this.textInput.off('keyup')
				} else
				{
					this.textInput.removeAttr('readonly');
				}

				if(options.disabled)
				{
					this.textInput.attr('disabled', 'true');
					this.textInput.addClass('ml-blueprint-textInput-disabled');
					this.textInputDiv.addClass('ml-blueprint-textInput-disabled');
					this.textInputDiv.removeClass(`ml-blueprint-textInput-${options.intent}`);
					this.rightButton.off('click');
				}
				else
				{
					this.textInput.removeAttr('disabled');
					this.textInput.removeClass('ml-blueprint-textInput-disabled');
					this.textInputDiv.removeClass('ml-blueprint-textInput-disabled');
					this,this.textInputDiv.addClass(`ml-blueprint-textInput-${options.intent}`);
					changeType()
				}
			});
			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPTextInput.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPTextInput {

		/**
		 * Text Input properties
		 * @typedef ITextInput
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface ITextInput extends BPBase.IControlProps{
			/**
			 * Disable the input
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Intent color of the text input
			 * @type {BPBase.Intent}
			 */
			intent?: BPBase.Intent,
			/**
			 * Type of the input
			 * @type {string}
			 */
			type?: string,
			/**
			 * Assign value of the textbox to the value attribute
			 * @type {boolean}
			 */
			value?: boolean,
			/**
			 * Add a spinner
			 * @type {boolean}
			 */
			spin?: boolean,
			/**
			 * Make spinner readonly
			 * @type {boolean}
			 */
			readonly?: boolean
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ITextInput{         
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			disabled?: boolean | mdash.IScopePath,
			intent?: BPBase.Intent | mdash.IScopePath,
			large?: boolean | mdash.IScopePath,
			fill?: boolean | mdash.IScopePath,
			readonly?: boolean | mdash.IScopePath,
			iconLeft?: string | mdash.IScopePath,
			iconRight?: string | mdash.IScopePath,
			placeholder?: string | mdash.IScopePath,	
			type?: string | mdash.IScopePath,
			round?: boolean | mdash.IScopePath,
			value?: boolean | mdash.IScopePath,
			spin?: boolean | mdash.IScopePath
		}
	}

	mdash.registerControl(BPTextInput,'dashboard.components.controls', BPTextInput.meta);
}