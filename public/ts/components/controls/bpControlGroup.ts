namespace dashboard.components.controls {

	//import the ml dashboard namespace and give it the name 'mdash'
	import mdash = ml.ui.dashboard;
	
	export namespace BPControlGroup {

		export interface IControlGroup extends BPBase.IAppearance, BPBase.IActionProps{
			
		}

		export interface IOptionsDefinition {
			btnData: BPControl.IOptions[] | mdash.IScopePath,
			icon?: string | mdash.IScopePath;
            inputIcon?: string | mdash.IScopePath;
            vertical?: boolean | mdash.IScopePath;
			fill?: boolean | mdash.IScopePath;
			buttonLeft?: boolean | mdash.IScopePath;
			buttonRight?: boolean | mdash.IScopePath;
			htmlSelect?: boolean | mdash.IScopePath;
            placeholder?: string | mdash.IScopePath;
			selectOption?: string[] | mdash.IScopePath;
			buttonLeftText?: string | mdash.IScopePath;
			buttonRightText?: string | mdash.IScopePath;
		}

		/**
		 * @typedef IOptions
		 * @readonly
		 * @interface
		 * @augments IControlGroup
		 */
		export interface IOptions extends IControlGroup{
			/**
			 * Data object of the controls
			 * @type {BPControl.IOptions[]}
			 */
			btnData: BPControl.IOptions[]
		}
	}

	export class BPControlGroup extends mdash.controls.CompositeControl<BPControlGroup.IOptionsDefinition, BPControlGroup.IOptions> {
		public static type: string = 'BPControlGroup';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.INTERNAL,
			friendlyName: 'Control Group',
			shortDescription: "The control group, receiving no intervention, is used as a baseline to comparegroups and assess the effect of that intervention.",
			categories: ['Input'],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		constructor(id: string, definition: mdash.IControlDefinition<BPControlGroup.IOptionsDefinition>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		/* Init is still an important function here, but because of the limited 
		 * functionality of this control specifically, we're not doing more than calling the parent  */
		protected init() {
			super.init();
		}

		/* This function is what largely separates a composite control from others.
		 * This is the function that defines the structure of the control. If you want it to show up in the DOM, it needs to go here.*/
		protected getControlLayout(options: BPControlGroup.IOptions): mdash.IControlDefinition<any> { 

			//create a container for all the child controls
			return mdash.controls.Flexbox.builder()
				.options({
					justify: "Start",
				}).children([
					//a repeater is simply that: a control that repeats. Its 'data' field represents an array, and it repeats it's children for each element in the array
					mdash.controls.Repeater.builder().options({
						data: { $scopePath: "options.btnData" },
						layout: options.vertical ? 'Vertical' : 'Horizontal' //'options' is a valid scope path since it is what's passed into the getControlLayout function
					}).children([
							//"item" is the default name for each element in the repeater's data array
							BPControl.builder().options({
								icon: {$scopePath: "item.icon"},
								inputIcon: {$scopePath: "item.inputIcon"},
								vertical: {$scopePath: "options.vertical"},
								fill: {$scopePath: "options.fill"},
								left: {$scopePath: "item.left"},
								right: {$scopePath: "item.right"},
								placeholder: {$scopePath: "item.placeholder"},
								selectOption: {$scopePath: "item.selectOption"},
								buttonText: {$scopePath: "item.buttonText"},
								inputValue: {$scopePath: "item.inputValue"},
								intent: {$scopePath: "item.intent"},

							}),
						])
			]).toJSON(); //toJSON converts the controls into the form needed. dont forget it.
		}

		protected cleanup() {
			super.cleanup();
		}

		protected validateDefinition(): boolean {
			return super.validateDefinition();
		}	

		public static builder() {
			return new mdash.ControlDefinitionBuilder<BPControlGroup.IOptionsDefinition>(this.type);
		}
	}
	mdash.registerControl(BPControlGroup, 'dashboard.components.controls', BPControlGroup.meta);
}  