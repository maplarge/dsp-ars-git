namespace dashboard.components.controls {
	import mdash = ml.ui.dashboard;

	export class BPProgress extends mdash.BaseControl<BPProgress.IOptionsDefinition> {
		public static type: string = 'BPProgress';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Progress',
			shortDescription: "With a progress bar, users can follow the progress of a lengthy operation",
			categories: ["Visualization"],
			childSupport: mdash.ChildSupport.NONE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPProgress.builder().options(BPProgress.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPProgress.IOptions> = {
			'BPProgress': {
				options: {
					label: 'Progress',
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected Progress: HTMLElement;
		protected ProgressBarSection: JQuery;
		// private icon: string;
		// private text: string;

		constructor(id: string, definition: mdash.IControlDefinition<BPProgress.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
			this.container.classList.add('namespace-blueprint-progress')
			this.Progress = ml.create("div", "ml-BPProgress",  this.contentDiv[0]);
			this.Progress.classList.add('progressLine');
			this.ProgressBarSection = ml.$(`
				<div class="progressBackground"></div>
			`)
			this.ProgressBarSection.appendTo(this.Progress)
	
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		protected init() {
			super.init();

			var obs = this.registerScopeObservable(this.definition.name, null, false);

			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPProgress.IOptions) => {
					this.ProgressBarSection[0].className = "progressBackground";
					// set it to the initial Progress class before adding every other class
					if(options.intent){
						switch(options.intent){
							case BPBase.Intent.Danger:this.ProgressBarSection.addClass(`progress-${options.intent}`);
								break;
							case BPBase.Intent.Primary:this.ProgressBarSection.addClass(`progress-${options.intent}`);
								break;
							case BPBase.Intent.Success:this.ProgressBarSection.addClass(`progress-${options.intent}`);
								break;
							case BPBase.Intent.Warning:this.ProgressBarSection.addClass(`progress-${options.intent}`);
								break;
						}
					}
					
					if(options.rangeDisable){
						if(options.rangeWidth || options.rangeWidth == 0){
							this.ProgressBarSection.attr('style', `width:${options.rangeWidth}%`)
						}	
					}
					else{
						this.ProgressBarSection.attr('style', 'width:100%')
					}
					

				});
			}
			
		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPProgress.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPProgress {

		/**
		 * Interface for working with Progress component
		 * @typedef IProgressProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IActionProps
		 * @augments BPBase.IAppearance
		 */
		export interface IProgressProps extends BPBase.IActionProps, BPBase.IAppearance{
			/**
			 * Intent color of the progress bar
			 * @type {BPBase.Intent}
			 */
			intent?:BPBase.Intent,
			/**
			 * Make the progress bar's indefinite
			 * @type {boolean}
			 */
			rangeDisable?:boolean,
			/**
			 * Percentage of the progress on the bar
			 * @type {number}
			 */
			rangeWidth?: number

		}
		
		export interface IOptions extends mdash.IControlOptionsWithTransforms, IProgressProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            intent?: BPBase.Intent | mdash.IScopePath,
			rangeWidth?: number | mdash.IScopePath,
			rangeDisable?:boolean | mdash.IScopePath
		}
	}
	
	mdash.registerControl(BPProgress,'dashboard.components.controls', BPProgress.meta);
}
