namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPNavBar extends mdash.BaseControl<BPNavBar.IOptionsDefinition> {
		public static type: string = 'BPNavBar';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'NavBar',
			shortDescription: "Navbars are responsive meta components that serve as navigation headers for your application or site.",
			categories: ["Visualization", "Layout"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPNavBar.builder().options(BPNavBar.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPNavBar.IOptions> = {
			'BPNavBar': {
				options: {
					outputTransforms: [],
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}
		
		protected navBar: HTMLElement;
		protected navGroup: HTMLElement;
		protected header: HTMLElement;
		protected buttonGroup: HTMLElement;
		protected button: HTMLElement;
		protected input: HTMLElement;

		constructor(id: string, definition: mdash.IControlDefinition<BPNavBar.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
			this.container.classList.add('namespace-blueprint-Navbar');
			this.navBar = ml.create("div", "ml-BONavBar",  this.contentDiv[0]);
			this.navBar.classList.add('ml-blueprint-navbar');
			this.navGroup = ml.create("div", "ml-BONavBar", this.navBar);
			this.navGroup.classList.add('ml-blueprint-navbar-group');
			this.header = ml.create("div", "ml-BONavBar", this.navGroup);
			this.header.classList.add('ml-blueprint-navbar-header');
			this.header.innerHTML = 'Header';
			this.buttonGroup = ml.create("div", "ml-BONavBar", this.navGroup);
			this.buttonGroup.classList.add('ml-blueprint-button-group');
		}

		
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPNavBar.IOptions) => {
					
					if(document.querySelector('.ml-blueprint-navbar-input')){
						document.querySelector('.ml-blueprint-navbar-input').parentElement.removeChild(document.querySelector('.ml-blueprint-navbar-input'));
					} 
										
					if(options.btnData) {
						this.buttonGroup.innerHTML = ""
						Array.prototype.map.call(options.btnData, (element) => {
							this.button = ml.create('button', "ml-BONavBar", this.buttonGroup);
							this.button.classList.add('ml-blueprint-navbar-button');
							let icon: HTMLElement = ml.create('i', "ml-BONavBar", this.button);
							if(element.label) {
								let text: HTMLElement = ml.create('span', "ml-BONavBar", this.button);
								text.innerHTML = element.label;
							}
							else {
								icon.style.marginRight = '0'
							}
							icon.classList.add(`mlicon-${element.icon}`);
							if(element.divider) {
								this.button.classList.add('ml-blueprint-divider')
							}
						})
					}
										
					if(options.align){
						this.navGroup.setAttribute('class', "ml-blueprint-navbar-group");
						switch(options.align){
							case ctrl.BPBase.Alignment.Around:this.navGroup.classList.add('ml-blueprint-navbar-around');
							break;
							case ctrl.BPBase.Alignment.Center:this.navGroup.classList.add('ml-blueprint-navbar-center');
							break;
							case ctrl.BPBase.Alignment.Left:this.navGroup.classList.add('ml-blueprint-navbar-left');
							break;
							case ctrl.BPBase.Alignment.Right:this.navGroup.classList.add('ml-blueprint-navbar-right');
							break;							
						}
					}

					if(options.align == ctrl.BPBase.Alignment.Around) {
						document.querySelector('.ml-blueprint-button-group').children[0].classList.remove('ml-blueprint-divider')
					}

					if(options.fixedToTop) {
						this.navBar.style.position = 'fixed';
						this.navBar.style.top = '0';
						this.navBar.style.left = '0';
						this.navBar.style.right = '0';
					}
					else {
						this.navBar.style.position = 'static'
					}

					if(options.input) {						
						this.input = ml.create('input', "ml-BONavBar", this.header);
						this.input.classList.add('ml-blueprint-navbar-input')
					} 
					
					(options.dark)? this.navBar.classList.add('ml-blueprint-navbar-dark'): this.navBar.classList.remove('ml-blueprint-navbar-dark') 
					var  matchWindow: number = this.buttonGroup.getBoundingClientRect().width +  this.header.getBoundingClientRect().width +40;
					window.addEventListener('resize', ()=>{
						if(window.innerWidth <= matchWindow){
							this.navGroup.classList.add('ml-blueprint-wrap');
						}
						else {
							this.navGroup.classList.remove('ml-blueprint-wrap');
						}
					});
					if(window.innerWidth <= matchWindow){
						this.navGroup.classList.add('ml-blueprint-wrap');
					}
				});
			}
		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPNavBar.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPNavBar {

		/**
		 * Navbar props
		 * @typedef INavBar
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface INavBar extends BPBase.IControlProps{
			/**
			 * Object buttons for navbar
			 * @type {ctrl.BPBase.IControlProps[]}
			 */
			btnData?: ctrl.BPBase.IControlProps[],
			/**
			 * Make navbar stick to top
			 * @type {boolean}
			 */
			fixedToTop?: boolean,
			/**
			 * Dark color styling for the navbar
			 * @type {boolean}
			 */
			dark?: boolean,
			/**
			 * Input field for the navbar
			 * @type {boolean}
			 */
			input?: boolean
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, INavBar{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			btnData?: ctrl.BPBase.IControlProps[] | mdash.IScopePath,
			label?: string | mdash.IScopePath,
			icon?: string | mdash.IScopePath,	
			align?: ctrl.BPBase.Alignment | mdash.IScopePath,
			fixedToTop?: boolean | mdash.IScopePath,
			divider?: boolean | mdash.IScopePath,
			dark?: boolean | mdash.IScopePath,
			input?: boolean | mdash.IScopePath,
		}
	}

	mdash.registerControl(BPNavBar,'dashboard.components.controls', BPNavBar.meta);
}