namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPBase extends mdash.BaseControl<BPBase.IOptionsDefinition> {
		public static type: string = 'BPBase';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.HIDDEN_FROM_EDITOR,
			friendlyName: 'Blueprint Base',
			shortDescription: "Contains base control functions and fields that other components implement",
			categories: [],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPBase.builder().toJSON();
		}

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		constructor(id: string, definition: mdash.IControlDefinition<BPBase.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}


		/**
		 * @internal
		 * This is where you might create elements to be added to the dom and appened to this.contentDiv
		 * **Important Note:** Elements created here and appended to the control's content div are not appened to the DOM at this point.
		 * 
		 * */
		public build() {
			super.build();
		}


		/**
		 * Run any validation against the defintion (options passed into the control) that you might require.
		 * This function is called when control is created AND whenever the control's options change.
		 * */
		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}


		/**
		 * @internal
		 * This function is called twice in the begining of a control's lifecycle.
		 * 1. To initialze the control but before it is loaded in the DOM (when this.isDOMLoaded == false)
		 * 2. After the control has been created in the DOM (when this.isDOMLoaded == true)
		 * */
		protected init() {
			super.init();
		}


		/**
		 * This function is called just before the control gets destroyed and can be used to cleanup the control beforehand.
		 * i.e. to remove delegate listeners.
		 * (Any elements that exist in the DOM created by the control will be removed, and any listeners placed directly on any DOM elements are removed)
		 * */
		protected cleanup() {
			super.cleanup();
		}


		/**
		 * Creates an instance of the control and (depending on options passed to it) will add the controls' DOM elements to the page.
		 * */
		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPBase.IOptionsDefinition>(this.type);
		}
	}
	
	export namespace BPBase {

		export interface IOptions extends mdash.IControlOptionsWithTransforms {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
		}
		
		/**
		 * Enumeration for specific color codes used inside the components for styling purposes
		 * @readonly
		 * @enum {string}
		 */
		export enum Intent {
			/** Default color, usually gray.*/
			Default = "default",
			/** Primary color, blue. Often associated with displaying general information. */
			Primary = "primary",
			/** Success color, green. Associated with actions that are successfully completed. */
			Success = "success",
			/** Warning color, orange. Used to display warnings. */
			Warning = "warning",
			/** Danger color, red. Used for marking dangerous actions or displaying errors. */
			Danger = "danger"
		}

		/**
		 * Enumeration for a starting position of an element
		 * @readonly
		 * @enum {string}
		 */
		export enum Position {
			/** Start is often the beginning of something */
			Start = "start",
			/** End is the last possible place */
			End = "end"
		}
		
		/**
		 * Enumeration for a type of control used in control group
		 * @readonly
		 * @enum {string}
		 */
		export enum ControlGroup {
			/** Button component */
			Button = 'button',
			/** Select with options input */
			Select = 'select',
		}
		
		/**
		 * Enumeration for specifying the alignment of an element relative to their parent container
		 * @readonly
		 * @enum {string}
		 */
		export enum Alignment{
			/** At the left or other elements or the parent container */
			Left = 'left',
			/** At the center inside the container */
			Center = 'center',
			/** At the right of an element inside the container */
			Right = 'right',
			/** In between other elements */
			Around = 'around'
		}

		/**
		 * Elevation enumeration specifies the intensity of box shadow styles of a component
		 * @readonly
		 * @enum {number}
		 */
		export enum Elevation{
			/** No box shadows */
			Zero,
			/** Slight box shadow */
			One,
			/** More box shadow */
			Two,
			/** Stronger shadow */
			Three,
			/** Highest box shadow level, element looks like it pops out */
			Four,
		}

		/**
		 * Used inside TimePicker to indicate the precision of the timer
		 * @readonly
		 * @enum {string}
		 */
		export enum Precision {
			/** Timer won't display */
			None = 'none',
			/** Timer will show hours and minutes */
			Minute = 'minute',
			/** Timer will show hours,minutes and seconds */
			Second = 'second',
			/** Timer will show hours,minutes,seconds,miliseconds */
			Milisecond = 'milisecond'
		}

		/**
		 * List of common properties that other components may use
		 * @typedef IProps
		 * @deprecated
		 * @readonly
		 * @interface
		 */
		export interface IProps{
			/** HTML element's class parameter 
			 * @type {string} 
			 * */
			className?: string
		}

		/**
		 * List of common props used to describe the appearance of a component
		 * @typedef IAppearance
		 * @interface
		 * @readonly
		 */
		export interface IAppearance{
			/**
			 * Specifies if the component uses styling for being large or not
			 * @type {boolean}
			 */
			large?: boolean,
			/**
			 * Toggle for the components that may contain minimal styling
			 * @type {boolean}
			 */
			minimal?: boolean,
			/**
			 * Vertical orientation toggle
			 * @type {boolean}
			 */
			vertical?: boolean,
			/**
			 * A boolean value that describes if the component is small or not
			 * @type {boolean}
			 */
			small?: boolean,
			/**
			 * Toggle for only displaying the icons inside a component
			 * @type {boolean}
			 */
			iconsOnly?: boolean,
			/**
			 * Toggle for focused or active state of an element
			 * @type {boolean}
			 */
			active?: boolean
		}
		
		/**
		 * List of properties used for anchor links or elements that link to some place
		 * @typedef ILinkProps
		 * @readonly
		 * @interface
		 */
		export interface ILinkProps{
			/**
			 * href is the URL/URI location to a place, often used inside anchor links
			 * @type {string}
			 */
			href?: string,
			/**
			 * Target specificies how the anchor link will open (_blank for example opens the link in a new tab)
			 * @type {string}
			 */
			target?: string,
			/**
			 * Specifies if the anchor link is disabled
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * A Text label for the anchor link
			 * @type {string}
			 */
			label?: string
		}

		/**
		 * Intent enum as an interface
		 * @typedef IIntentProps
		 * @readonly
		 * @deprecated
		 * @interface
		 */
		export interface IIntentProps {
			/**
			 * The Intent of the element
			 * @type {Intent}
			 */
			intent?: Intent
		}

		/**
		 * Interface for buttons used to make a toast appear on the screen
		 * @typedef IToastButtonProps
		 * @readonly
		 * @interface
		 */
		export interface IToastButtonProps{
			/**
			 * The name of the button
			 * @type {string}
			 */
			name?: string,
			/**
			 * The Intent type color of the button
			 * @type {Intent}
			 */
			intent?: ctrl.BPBase.Intent,
			/**
			 * The text to display inside the toast modal
			 * @type {string}
			 */
			modalText?: string,
			/**
			 * Text to display after an action button inside the toast modal is pressed
			 * @type {string}
			 */
			replaceItemText?: string,
		}

		/**
		 * List of properties used to describe an action
		 * @typedef IActionProps
		 * @interface
		 * @readonly
		 * @augments IProps
		 * @augments IIntentProps
		 */
		export interface IActionProps extends IProps, IIntentProps{
			/**
			 * Used to specify if the element is disabled or not
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * The icon name that the element uses
			 * @type {string}
			 */
			icon?: string,
			/**
			 * The label displayed inside the element
			 * @type {string}
			 */
			label?: string,
			/**
			 * Event handler for onclick event on elements
			 * @function
			 * @param {IScope} scope - the current scope
			 * @param {MouseEvent} event - the event object of the mouse click event
			 * @event click
			 * @returns {void}
			 */
			onClick?: (scope: mdash.IScope, event: MouseEvent) => void  
		}

		/**
		 * List of props that describe state or part of a component
		 * @typedef IControlProps
		 * @interface
		 * @readonly
		 * @augments IProps
		 */
		export interface IControlProps extends IProps{
			/**
			 * Specifies if there's a divider after or before an element
			 * @type {boolean}
			 */
			divider?: boolean,
			/**
			 * Element alignment inside the parent
			 * @type {Alignment}
			 */
			align?: Alignment,
			/**
			 * Specifies if a checkbox or a radio button inside the component is checked or not
			 * @type {boolean}
			 */
			checked?: boolean,
			/**
			 * Determines if the element can be interacted with or not
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Inline components often display in 1 line, the prop controls if the styling is applied or not
			 * @type {boolean}
			 */
			inline?: boolean,
			/**
			 * Apply block element styling
			 * @type {boolean}
			 */
			block?: boolean,
			/**
			 * Placeholder is often used to place text inside an empty input field like textboxes
			 * @type {string}
			 */
			placeholder?: string,
			/**
			 * Label for an element. Input fields often have this
			 * @type {string}
			 */
			label?: string,
			/**
			 * Apply styling to make the element large
			 * @type {boolean}
			 */
			large?: boolean,
			/**
			 * Fill is an attribute that states if the component fills the whole width of the parent container or not
			 * @type {boolean}
			 */
			fill?: boolean,
			/**
			 * Apply minimal styling for the element
			 * @type {boolean}
			 */
			minimal?: boolean,
			/**
			 * Add interaction like hover or focus on elements
			 * @type {boolean}
			 */
			interactive?: boolean,
			/**
			 * Make the element look round
			 * @type {boolean}
			 */
			round?: boolean,
			/**
			 * An icon name for an icon displayed at the right of the element
			 * @type {string}
			 */
			iconRight?: string,
			/**
			 * An icon name for an icon displayed at the left of the element
			 * @type {string}
			 */
			iconLeft?: string,
			/**
			 * An icon name for an icon displayed at some place inside the element
			 * @type {string}
			 */
			icon?: string,
			/**
			 * Event handler for onchange event on elements
			 * @function
			 * @param {string} value - value received when the event happened
			 * @event change
			 * @returns {void}
			 */
			onChange?: (value: string, scope: mdash.IScope) => void,
			/**
			 * Event handler triggered when an element is removed
			 * @function
			 * @param {MouseEvent} Event - event object of a mouse event (click,mouseover,mousedown...)
			 * @event remove
			 * @returns {void}
			 */
			onRemove?: (Event: MouseEvent) => void,
			/**
			 * Event handler that executes on blur
			 * @function
			 * @param {MouseEvent} Event - event object of a mouse event (click,mouseover,mousedown...)
			 * @event blur
			 * @returns {void}
			 */
			addOnBlur?: (Event: MouseEvent) => void,
		}

		/**
		 * List of properties used for options inside dropdowns
		 * @typedef IOptionProps
		 * @interface
		 * @readonly
		 * @augments IProps
		 */
		export interface IOptionProps extends IProps{
			/**
			 * Makes the option non-interactive
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Label of the option
			 * @type {string}
			 */
			label?: string,
			/**
			 * Value of the option
			 * @type {string | number}
			 */
			value: string | number,
			/**
			 * Event handler triggered when an option is removed
			 * @function
			 * @param {MouseEvent} Event - event object of a mouse event (click,mouseover,mousedown...)
			 * @event remove
			 * @returns {void}
			 */
			onRemove?: (Event: MouseEvent) => void,
		}

		/**
		 * List of props used inside Slider-based components
		 * @interface
		 * @readonly
		 * @augments IProps
		 */
		export interface ISliderBaseProps extends IProps{
			/**
			 * Make the slider non-interactive
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Highest number displayed on the slider
			 * @type {number}
			 */
			max?: number,
			/**
			 * Lowest numer displayed on the slider
			 * @type {number}
			 */
			min?: number,
			/**
			 * Show or hide the colored bar on the slider
			 * @type {boolean}
			 */
			showTrackFill?: boolean,
			/**
			 * The number by which the slider value is incremented
			 * @type {number}
			 */
			step?: number,
			/**
			 * Make the slider orientation vertical or horizontal
			 * @type {number}
			 */
			vertical?: boolean
		}

		/**
		 * List of properties used inside Time picker
		 * @interface
		 * @readonly
		 * @augments IProps
		 */
		export interface ITimePickerProps extends IProps {
			/**
			 * Precision of the timer
			 * @type {Precision}
			 */
			Precision?: Precision,
			/**
			 * Show the arrows used to increment or decrement the numbers inside inputs
			 * @type {boolean}
			 */
			showArrowButtons?: boolean,
			/**
			 * Select the input field when focus event is fired
			 * @type {boolean}
			 */
			selectAllOnFocus?: boolean,
			/**
			 * The default date shown on the timepicker
			 * @type {Date}
			 */
			defaultValue?: Date,
			/**
			 * Use 12 or 24 hour system
			 * @type {boolean}
			 */
			useAmPm?: boolean,
			/**
			 * Minimum possible time on timepicker
			 * @type {number}
			 */
			minTime?: number,
			/**
			 * Maximum possible time on timepicker
			 * @type {number}
			 */
			maxTime?: number,
		}
	}
}