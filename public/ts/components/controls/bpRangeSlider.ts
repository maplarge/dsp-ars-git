// <reference path="./bpSlider.ts" />

namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;
	
	export class BPRangeSlider<T extends BPRangeSlider.IOptionsDefinition> extends BPSlider<BPRangeSlider.IOptionsDefinition> {
		public static type: string = 'BPRangeSlider';
		
		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Range Slider',
			shortDescription: "RangeSlider creates a numeric range slider UI component. It is used for granular filtering of numeric data.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPRangeSlider.builder().options(BPRangeSlider.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPRangeSlider.IOptions> = {
			'BPRangeSlider': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}
		
		constructor(id: string, definition: mdash.IControlDefinition<T>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
			
		}

		private isInitialized: boolean = false;
		protected observer: MutationObserver;

		public build() {
			super.build();
		}

		/**
		 * overrides parent method to make it work with 2 handles
		 * @param options the options object with properties
		 * @param obs observable variable
		 */
		protected initializeHandleTooltip(options: BPRangeSlider.IOptions, obs: mdash.RefCountedObservable<any>): void
		{
			this.slider.slider('option','values', obs());
			if(!this.handleAppended){
				this.contentDiv.find('.mlui-slider-handle').append(this.handleLabel);
				this.handleAppended = true;
			}
			this.contentDiv.find('.mlui-slider-handle .ml-blueprint-slider-handle-label').eq(0).text(obs()[0]);
			this.contentDiv.find('.mlui-slider-handle .ml-blueprint-slider-handle-label').eq(1).text(obs()[1]);
		}

		/**
		 * Update the slider on observable change
		 * @param {RefCountedObservable} valueObs valueObs - Observable
		 */
		protected initializeListeners(valueObs: mdash.RefCountedObservable<any>): void {

			//objectsToDispose is an array of listeners that get destroyed when the control is destroyed
			//add listeners to this.objectsToDispose in order to prevent memory leaks
			this.objectsToDispose.push(
				valueObs.subscribe(newValue => {

					if(Array.isArray(newValue))
					{
						if(newValue.length == 1)
						{
							this.contentDiv.find('.ml-blueprint-slider-handle-label')[0].innerText = newValue[0];
							this.slider.slider('values',0,Number(newValue[0]));
						}
						else if(newValue.length > 1)
						{
							newValue.sort((a,b) => a-b);
							this.contentDiv.find('.ml-blueprint-slider-handle-label')[0].innerText = newValue[0];
							this.slider.slider('values',0,Number(newValue[0]));	
							this.contentDiv.find('.ml-blueprint-slider-handle-label')[1].innerText = newValue[1];
							this.slider.slider('values',1,Number(newValue[1]));	
						}
					}
					else
					{
						this.contentDiv.find('.ml-blueprint-slider-handle-label')[0].innerText = newValue;
						this.slider.slider('values',0,Number(newValue));
					}

				})
			);
		}

		/**
		 * call the super function and then additionally fix the position of second handle
		 * @param options the options object with properties
		 * @param obs observable variable
		 */
		protected initializeLabels(options: BPRangeSlider.IOptions, obs: mdash.RefCountedObservable<any>){
			super.initializeLabels(options, obs);
			if(options.vertical)
			{
				this.contentDiv.find('.ml-blueprint-slider-handle-label').addClass('ml-blueprint-slider-handle-label-vertical');
				if (ml.util.isNotNullUndefinedOrEmpty(obs()))
				{
					// fixing the slider handles not being in the right positions in vertical mode
					const firstHandleBottom: number = parseFloat(this.contentDiv.find('.mlui-slider-handle')[0].style.bottom);
					const secondHandleBottom: number = parseFloat(this.contentDiv.find('.mlui-slider-handle')[1].style.bottom);
					this.contentDiv.find('.mlui-slider-handle')[0].style.top = (100 - firstHandleBottom)+'%';
					this.contentDiv.find('.mlui-slider-handle')[1].style.top = (100 - secondHandleBottom)+'%';
				}
			}
			else
			{
				this.contentDiv.find('.ml-blueprint-slider-handle-label').removeClass('ml-blueprint-slider-handle-label-vertical');
			}
		}

		/**
		 * The purpose of this function is to watch attribute changes on handles
		 * and then correct their top% using their bottom% everytime their attributes change
		 * it's also used to correct the slider's values so the obs,slider and label values are all the same
		 * @param obs the observable variable
		 */
		protected observeAttributeChanges(obs: mdash.RefCountedObservable<any>): void 
		{
			if(this.observer) this.observer.disconnect(); // disconnect the observer if it already exists
			const handles: JQuery = this.contentDiv.find('.mlui-slider-handle');
			// create a new MutationObserver instance with a callback and assign it to the class variable
			this.observer = new MutationObserver(() => {
				const handleVals: number[] = [];
				handles.each((index: number,handle: HTMLElement) => {
					handle.style.top = `${100 - parseFloat(handle.style.bottom)}%`;
					handleVals.push(Number(handle.textContent));
					this.slider.slider('values', handleVals);
					obs(handleVals);
				});
			});
			// attach the observer to every handle
			handles.each((index: number,handle: HTMLElement) => this.observer.observe(handle, {attributes: true}));
		}

		/**
		 * overrides the parent method with different values
		 * @param options the options object with properties
		 * @param obs observable variable
		 */
		protected initializeSliderOptions(options: BPRangeSlider.IOptions, obs: mdash.RefCountedObservable<any>){
			return {
				// using ternary and logic operators because if/else take longer to write
				disabled: Boolean(options.disabled),
				range: true,
				min: options.min || 0,
				max: options.max || ~~options.min + 100,
				orientation: options.vertical ? 'vertical' : 'horizontal',
				step: options.step || 1,
				values: obs() || [~~options.min, ~~options.max],
				slide: (event: JQueryEventObject, {value, values, handle}): void => {
					// update the observable value on change
					obs(values);
					handle.querySelector('.ml-blueprint-slider-handle-label').textContent = value;
					// if the slide event source was a generated label
					if(event.srcElement.classList.contains('ml-blueprint-slider-label'))
					{
						this.dragClosestHandleToLabel(event, handle, obs, options.vertical);
					}

					if(options.vertical)
					{
						// I've already written about this line in bpSlider.ts
						// check the comment there
						handle.style.top = 100 - parseFloat(handle.style.bottom) + '%';
					}
					this.observeAttributeChanges(obs);
				}
			}
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;
			
			return true;
		}
		
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, [0, 0], false);
			this.initializeListeners(obs);

			if(this.isDOMLoaded)
			{
				this.watchDefinitionOptions((options: BPRangeSlider.IOptions) => {

					if(!this.isInitialized)
					{
						if(!this.scope[this.definition.name]().length)
						{
							this.scope[this.definition.name]([~~options.min, ~~options.max]);
						}
						
						this.slider.slider(this.initializeSliderOptions(options, obs));
						this.initializeLabels(options, obs);
						this.generateLabels(Boolean(options.vertical), this.slider.slider('option', 'min'),this.slider.slider('option', 'max'), options.labelPercentages);
						this.giveIdentityToHandles();
						this.observeAttributeChanges(obs);
						this.isInitialized = true;
					}
				});
			}
		}
		
		protected cleanup() {
			super.cleanup();
			this.observer.disconnect();
		}
		
		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPRangeSlider.IOptionsDefinition>(BPRangeSlider.type);
		}
	}
	export namespace BPRangeSlider {

		/**
		 * Options interface for Range Slider properties
		 * @typedef IOptions
		 * @interface
		 * @readonly
		 * @augments BPSlider.IOptions
		 */
		export interface IOptions extends BPSlider.IOptions {

			/**
			 * since the range slider will always hold the value true for range parameter of the JQuery UI Slider
			 * and since it extends from BPSlider, I make sure that showTrackFill can not be used as a prop for Range Slider
			 * @type {never}
			 */
			showTrackFill?: never,

		}

		export interface IOptionsDefinition extends BPSlider.IOptionsDefinition {
			showTrackFill?: never,
		}
	}

	mdash.registerControl(BPRangeSlider,'dasboard.component.controls', BPRangeSlider.meta);
}