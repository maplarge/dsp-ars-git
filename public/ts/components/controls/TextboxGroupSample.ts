namespace dashboard.components.controls {

	//import the ml dashboard namespace and give it the name 'mdash'
	import mdash = ml.ui.dashboard;

	export namespace TextboxGroup {

		export interface tbData {
			label: string,
			placeHolder?: string
		}

		export interface IOptionsDefinition {
			tbData: tbData[] | mdash.IScopePath
		}

		export interface IOptions {
			tbData: tbData[]
		}
	}

	export class TextboxGroup extends mdash.controls.CompositeControl<TextboxGroup.IOptionsDefinition, TextboxGroup.IOptions> {
		public static type: string = 'TextboxGroup';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.INTERNAL,
			friendlyName: 'TextboxGroup',
			shortDescription: "TextboxGroup",
			categories: ['Input'],
			childSupport: mdash.ChildSupport.SINGLE
		};

		constructor(id: string, definition: mdash.IControlDefinition<TextboxGroup.IOptionsDefinition>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		/* Init is still an important function here, but because of the limited 
		 * functionality of this control specifically, we're not doing more than calling the parent  */
		protected init() {
			super.init();
		}

		/* This function is what largely separates a composite control from others.
		 * This is the function that defines the structure of the control. If you want it to show up in the DOM, it needs to go here.*/
		protected getControlLayout(options: TextboxGroup.IOptions): mdash.IControlDefinition<any> { 

			//create a container for all the child controls
			return mdash.controls.Flexbox.builder()
				.options({
					justify: "Start",
				}).children([
					//a repeater is simply that: a control that repeats. Its 'data' field represents an array, and it repeats it's children for each element in the array
					mdash.controls.Repeater.builder().options({
						data: { $scopePath: "options.tbData" }, //'options' is a valid scope path since it is what's passed into the getControlLayout function
					}).children([
							//"item" is the default name for each element in the repeater's data array
							TextboxSample.builder().options({
								label: "{{item.label}}",
								placeholder: "{{item.placeholder}}"
							})
						])
			]).toJSON(); //toJSON converts the controls into the form needed. dont forget it.
		}

		protected cleanup() {
			super.cleanup();
		}

		protected validateDefinition(): boolean {
			return super.validateDefinition();
		}	

		public static builder() {
			return new mdash.ControlDefinitionBuilder<TextboxGroup.IOptionsDefinition>(this.type);
		}
	}
	mdash.registerControl(TextboxGroup, 'db.examples.controls', TextboxGroup.meta);
}  