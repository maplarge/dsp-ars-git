namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPNumericInput extends mdash.BaseInputControl<BPNumericInput.IOptionsDefinition> {
		public static type: string = 'BPNumericInput';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Numeric Input',
			shortDescription: "Enter a number within certain range with the mouse or keyboard.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPNumericInput.builder().options(BPNumericInput.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPNumericInput.IOptions> = {
			'BPNumericInput': {
				options: {
					outputTransforms: []
				}
			}
		}

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		constructor(id: string, definition: mdash.IControlDefinition<BPNumericInput.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		private input: JQuery;
		private incrementBtn: JQuery;
		private decrementBtn: JQuery;
		private btnFlexContainer: JQuery;
		private numInputContainer: JQuery;
		private leftIcon: JQuery;
		private timeout: any;
		private defaultGiven: boolean = false;

		public build() {
			super.build();

			this.input = ml.$(`<input type='text' class='ml-blueprint-numeric-input'>`);

			this.btnFlexContainer = ml.$('<div class="ml-blueprint-numeric-input-flex-container"></div>');

			this.incrementBtn = ml.$(`<button class='ml-blueprint-increment-btn'><i class="mlicon-arrowup"></span></button>`);
			this.decrementBtn = ml.$(`<button class='ml-blueprint-decrement-btn'><i class="mlicon-arrowdown"></span></button>`);
			
			this.numInputContainer = ml.$(`<div class='ml-blueprint-numeric-input-container'></div>`);
			
			this.btnFlexContainer.append(this.incrementBtn, this.decrementBtn);
			
			this.container.classList.add('namespace-blueprint-numeric-input');
			
			this.contentDiv.append(this.numInputContainer);
		}
		
		/**
		 * Adds or subtracts the number received as an argument
		 * @param currentNum The current number from the input field
		 * @param step The step by which the number will be increment or decremented
		 * @param operation The math operation. Add(+) or Subract(-)
		 * @param precision Floating point precision. if it's 4, a number like 1.2547 can be used on input
		 */
		private addOrSubtract(currentNum: number | '', step: number, operation: '+' | '-', precision?: number): number{
			// Programming languages have rounding errors when it comes to working with floating point numbers
			// toFixed() fixes that error,but adds a decimal point even after integer numbers (4.0, 55.0)
			// the + sign before currentNum fixes the floating 0 error
			return operation === '+' ? Number((+currentNum + step).toFixed(precision || 3)) : 
									   Number((+currentNum - step).toFixed(precision || 3));
		}
		
		/**
		 * make sure the number isn't bigger or smaller than the max or min that might be defined
		 * if the number does exceed the maximum or is lower than the minimum
		 * the value is either lowered to maximum or increased to the minimum
		 * @param currentNum The current number or empty string if the input is empty
		 * @param options The current options object with properties
		 */
		private checkBoundary(currentNum: number | '', options: BPNumericInput.IOptions): number | ''{
			const min: number = options.min;
			const max: number = options.max;
			const num: number | '' = currentNum;
			// if we receive an actual number as our input, we check it against the maximum and minimum values
			// for adjustments
			if(ml.util.isNotNullUndefinedOrEmpty(num))
			{
				if(ml.util.isNotNullUndefinedOrEmpty(max))
				{
					if(num > max)
					{
						return max;
					}
				}
				
				if(ml.util.isNotNullUndefinedOrEmpty(min))
				{
					if(num < min)
					{
						return min;
					}
				}
			}

			// if the num argument is an empty string or an invalid number - an empty string is returned
			// if not - the number is assumed valid and is returned
			return num === '' || isNaN(num) ? '' : num;
		}
		
		/**
		 * compare and update the observable when the input is changed
		 * @param valueObs Observable variable
		 */
		private updateObservableValue(valueObs: mdash.RefCountedObservable<string | number>): void
		{
			if(this.input.val() !== valueObs() || valueObs() === null) {
				// Number('') is 0 in JavaScript which creates a bug where you can't remove a 0 and update the observable value to empty string
				this.input.val() == '' ? valueObs('') : valueObs(Number(this.input.val()));
			}
		}

		/**
		 * select the whole field on focus
		 */
		private attachFocusEvent(): void{
			this.input.off('focusin').on('focusin',() => {
				this.input.select();
			});
		}
		
		/**
		 * calls the checkBoundary function on input blur
		 * @param valueObs observable variable
		 * @param options current options with properties
		 */
		private checkBoundariesOnBlur(valueObs: mdash.RefCountedObservable<any>, options: BPNumericInput.IOptions): void{
			this.input.off('blur').on('blur',() => {
				const currentNum: number = Number(this.input.val());
				this.input.val(this.checkBoundary(currentNum, options).toString());
				this.updateObservableValue(valueObs);
			});
		} 
		
		/**
		 * Function that takes the mouse event and proceeds to call the proper action depending on arguments
		 * @param currentNum Current number on the input field
		 * @param e Event object of the mouse event
		 * @param operation The operation. Add(+) or Subtract(-)
		 * @param options THe current options object with the properties
		 */
		private processMouseEvent(currentNum: number, e: JQueryEventObject | JQueryMouseEventObject, operation: '+' | '-', 
			options: BPNumericInput.IOptions): string{
			/*  
			 *  increment or decrement by majorstepsize,minorstepsize or stepsize
			 *	depending on a key that was pressed the moment the button was clicked
			 *	shift + click = major step
			 *	alt + click = minor step
			 *	everything else = normal step
			 */
			let num: number | '' = currentNum;

			if(isNaN(num))
			{
				// don't try to increment or decrement a value that is not a number
				// just clear the whole input in case the number is not provided
				return '';
			}

			if(e.shiftKey && options.majorStepSize)
			{
				num = this.addOrSubtract(num, options.majorStepSize, operation, options.decimalPrecision);
				num = this.checkBoundary(num, options);
			}
			else if(e.altKey && options.minorStepSize)
			{
				num = this.addOrSubtract(num, options.minorStepSize, operation, options.decimalPrecision);
				num = this.checkBoundary(num, options);
			}
			else
			{
				num = this.addOrSubtract(num, options.stepSize || 1, operation, options.decimalPrecision);
				num = this.checkBoundary(num, options);
			}
			
			return num.toString();
		}

		/**
		 * checks some props for mouse events
		 * exists to prevent code duplication
		 * @param options The options object with parameters
		 */
		private checkPropsOnMouseEvent(options: BPNumericInput.IOptions): void
		{
			// if custom prop functions are defined, call them with current values
			if(options.onValueChange) 
			{
				options.onValueChange(Number(this.input.val()), this.input.val());
			}
			if(options.onButtonClick) 
			{
				options.onButtonClick(Number(this.input.val()), this.input.val());
			}
	
			// select the input field when the prop is enabled
			if(options.selectAllOnIncrement)
			{
				this.input.select();
			}
		}
		
		/**
		 * just to minimize code duplication in mouseIncrementEvent and mouseDecrementEvent functions
		 * prepares the values and then sends it to 2 different functions to process the input
		 * @param e The Event object for mouse event
		 * @param valueObs The observable variable
		 * @param options The options object with current props
		 * @param operation Addition or Subtraction operation
		 */
		private initiateMouseEventProcessing(e: JQueryEventObject, valueObs: mdash.RefCountedObservable<string | number>, options: BPNumericInput.IOptions, 
			operation: '+' | '-')
		{
			const currentNum: number = Number(this.input.val());
			this.input.val(this.processMouseEvent(currentNum, e, operation, options));
			this.updateObservableValue(valueObs);
			this.checkPropsOnMouseEvent(options);
		}
		
		/**
		 * Starts the mouse event listeners on the increment and decrement buttons
		 * @param valueObs The observable
		 * @param options Options object with the props
		 */
		private mouseListeners(valueObs: mdash.RefCountedObservable<string | number>, options: BPNumericInput.IOptions): void {
			// add click listeners for adding and subtracting the number on the increment and decrement buttons
			this.incrementBtn.off('click').on('click',(e): void => this.initiateMouseEventProcessing(e, valueObs, options, '+'));
			this.decrementBtn.off('click').on('click',(e): void => this.initiateMouseEventProcessing(e, valueObs, options, '-'));
			
			// mousedown listeners
			/*
				when users hold the mouse click on the buttons
				the number increases continuously without having to click it lots of times
			*/
	    	this.incrementBtn.off('mousedown').on('mousedown',(e): false => {
				if((<MouseEvent>e.originalEvent).which == 1) 
					this.timeout = setInterval(() => this.initiateMouseEventProcessing(e, valueObs, options, '+'), 100);
				return false;
			});
			
			this.decrementBtn.off('mousedown').on('mousedown',(e): false => {
				if((<MouseEvent>e.originalEvent).which == 1)
					this.timeout = setInterval(() => this.initiateMouseEventProcessing(e, valueObs, options, '-'), 100);
				return false;
			});

			// the reason I declared this function is because I'm going to add it to the document element,
			// and by just adding an external function to the event, I won't override existing events on the document,
			// unfortunately, attaching class methods as event listener functions doesn't work,so I had to go this way,
			// to be able to disable it outside of this function as well
			this.mouseListeners.prototype.clearMousedown = (): false => {
				clearInterval(this.timeout);
				return false;
			}

			// cleans up the intervals from mousedown event so it doesn't go on forever
			// if the user doesn't hold down the mouse click button on the increment/decrement buttons anymore
			ml.$(document).off('mouseup',this.mouseListeners.prototype.clearMousedown)
						  .on('mouseup',this.mouseListeners.prototype.clearMousedown);
		}
		
		/**
		 * Processes the keyboard input and then calls the appropriate functions to finalize it
		 * @param currentNum The current number on the input field
		 * @param e Keyboard Event Object
		 * @param options The options object with parameters
		 */
		private processKeyboardEvent(currentNum: number, e: JQueryKeyEventObject, options: BPNumericInput.IOptions): string{
			/*  
			 *	increment or decrement by majorstepsize,minorstepsize or stepsize
			 *	depending on the key combination that was pressed at the moment
			 *	shift + up arrow/down arrow = major step
			 *	alt + up arrow/down arrow = minor step
			 *	everything else = normal step 
			*/
			
			// 38 - up arrow
			// 40 - down arrow
			
			const key: number = e.which || e.keyCode;
			let num: number | '' = currentNum;

			if(isNaN(num))
			{
				// don't try to increment or decrement a value that is not a number
				// just clear the whole input in case the number is not provided
				return '';
			}
			
			// add
			if(key == 38 && e.shiftKey && options.majorStepSize)
			{
				num = this.addOrSubtract(num, options.majorStepSize, '+', options.decimalPrecision);
				num = this.checkBoundary(num, options);
			}
			else if(key == 38 && e.altKey && options.minorStepSize)
			{
				num = this.addOrSubtract(num, options.minorStepSize, '+', options.decimalPrecision);
				num = this.checkBoundary(num, options);
			}
			else if(key == 38)
			{
				num = this.addOrSubtract(num, options.stepSize || 1, '+', options.decimalPrecision);
				num = this.checkBoundary(num, options);
			}
			
			// subtract
			if(key == 40 && e.shiftKey && options.majorStepSize)
			{
				num = this.addOrSubtract(num, options.majorStepSize, '-', options.decimalPrecision);
				num = this.checkBoundary(num, options);
			}
			else if(key == 40 && e.altKey && options.minorStepSize)
			{
				num = this.addOrSubtract(num, options.minorStepSize, '-', options.decimalPrecision);
				num = this.checkBoundary(num, options);
			}
			else if(key == 40)
			{
				num = this.addOrSubtract(num, options.stepSize || 1, '-', options.decimalPrecision);
				num = this.checkBoundary(num, options);
			}
			
			return num.toString();
		}

		/**
		 * Adds keyboard events to the input
		 * @param valueObs observable variable
		 * @param options options object with props
		 */
		private keyboardListeners(valueObs: mdash.RefCountedObservable<string | number>, options: BPNumericInput.IOptions): void{
			// listen to keyboard input
			this.input.off('keydown').on('keydown', (e: JQueryKeyEventObject) => {
				const key: number =  e.which || e.keyCode;
				const allowedKeys: number[] = [8,38,40,46];
				const symbolCodes: number[] = [106,107,111,186,187,188,191,192,219,220,221,222];

				// block letter and symbol input when numericOnly is declared
				if(options.numericOnly)
				{
					// if letters are pressed, the numbers above the letters with shiftkey or capslock modifiers or a symbol key was pressed
					const isCapsOn: boolean = (<KeyboardEvent>e.originalEvent).getModifierState('CapsLock');
					const isLetter: boolean = key >= 65 && key <= 90;
					const isNumberAndModifierPressed: boolean = key >= 48 && key <= 57 && (e.shiftKey || isCapsOn);
					const isUnderscore: boolean = key == 189 && (e.shiftKey || isCapsOn);
					const isSymbol: boolean = symbolCodes.indexOf(key) > - 1;

					if(isLetter || isNumberAndModifierPressed || isSymbol || isUnderscore)
					{
						// prevent an insertion
						e.preventDefault();
					}
				}
				
				if(options.selectAllOnIncrement)
				{
					// only select the whole text if up,down arrows are pressed
					if(key == 38 || key == 40)
					{
						// this doesn't work during keydown event
						// but it works on click/mousedown events
						// also the Select All On Focus prop works with this same method
						this.input.select();
					}
				}
				
				// while setTimeout breaks the essence of keydown event being 1 keystroke behind the real DOM value,
				// every method is justified for fixing bugs.
				// it fixes the problem with not being able to insert a minus or a dot in numeric input.
				setTimeout(() => {
					// if a number was inserted or up/down arrow keys were pressed or backspace/delete were pressed
					if(key >= 96 && key <= 105 || key >= 48 && key <= 57 || allowedKeys.indexOf(key) > -1)
					{
						this.input.val(this.processKeyboardEvent(this.input.val(), e, options));
						this.updateObservableValue(valueObs);
						// call the custom prop function if it's defined with current values
						if(options.onValueChange) 
						{
							options.onValueChange(Number(this.input.val()), this.input.val());
						}
					}
				}, 1);
			})
		}
		
		/**
		 * initialize input listeners
		 * @param valueObs the observable
		 * @param options options object with current props
		 */
		protected initEventListeners(valueObs: mdash.RefCountedObservable<any>, options: BPNumericInput.IOptions): void {
			if(options.position != BPNumericInput.Position.None)
			{
				// only start mouse event listeners when the buttons are present
				this.mouseListeners(valueObs, options);
			}
			else
			{
				// turn off the mouse event listeners when the buttons aren't active
				this.incrementBtn.off('click mousedown');
				this.decrementBtn.off('click mousedown');
				ml.$(document).off('mouseup', this.mouseListeners.prototype.clearMousedown);
			}
			this.keyboardListeners(valueObs, options);
			this.objectsToDispose.push(
				valueObs.subscribe(newValue => {

					if(valueObs() == null || Number(newValue) != Number(this.input.val())){
						this.input.val(this.checkBoundary(newValue, options));
					}
				})
			);
		}
		
		protected validateDefinition(): boolean {
			
			if (!super.validateDefinition())
				return false;
			
			return true;
		}
		
		protected init() {
			super.init();
			this.initTransforms();
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPNumericInput.IOptions) => {
					var obs = this.registerScopeObservable(this.definition.name, this.checkBoundary(0, options), false);
					this.initEventListeners(obs, options);
					this.input.val(obs());
					
					// reset classes and the container before adding to them
					this.input[0].className = 'ml-blueprint-numeric-input';
					this.incrementBtn[0].className = 'ml-blueprint-increment-btn';
					this.decrementBtn[0].className = 'ml-blueprint-decrement-btn';
					this.numInputContainer[0].className = 'ml-blueprint-numeric-input-container';
					this.btnFlexContainer[0].className = 'ml-blueprint-numeric-input-flex-container';
					
					this.numInputContainer[0].innerHTML = '';

					if(options.disabled)
					{
						this.input.attr('disabled', 'true');
						this.incrementBtn.attr('disabled', 'true');
						this.decrementBtn.attr('disabled', 'true');
					}
					else
					{
						this.input.removeAttr('disabled');
						this.incrementBtn.removeAttr('disabled');
						this.decrementBtn.removeAttr('disabled');
					}
					
					// select the whole field on focus
					options.selectAllOnFocus ? this.attachFocusEvent() : this.input.off('focusin');
					
					// give a default value
					if(obs() && !this.defaultGiven)
					{
						this.input.val(obs());
						this.updateObservableValue(obs);
						this.defaultGiven = true;
					}
					// add a placeholder
					options.placeholder ? this.input.attr('placeholder', options.placeholder) : this.input.removeAttr('placeholder');
					
					// on blur - enforce the min-max boundary
				    options.clampValueOnBlur ? this.checkBoundariesOnBlur(obs, options) : this.input.off('blur');
					
					// attach a fill class
					if(options.fill) this.numInputContainer.addClass('ml-blueprint-numeric-input-fill');
					
					if(options.large)
					{
						// give a large class
						this.numInputContainer.addClass('ml-blueprint-numeric-input-container-large');
						this.input.addClass('ml-blueprint-numeric-input-large');
						this.incrementBtn.addClass('ml-blueprint-numeric-input-large');
						this.decrementBtn.addClass('ml-blueprint-numeric-input-large');
						this.btnFlexContainer.addClass('ml-blueprint-numeric-input-container-large');
					}
					
					if(options.leftIcon)
					{
						if(this.leftIcon){
							this.leftIcon.detach();
						}
						this.leftIcon = ml.$(`<i class="ml-blueprint-numeric-input-icon mlicon-${options.leftIcon}"></i>`);
						this.numInputContainer.prepend(this.leftIcon);
					}

					// the position prop positions the increment and decrement buttons at the right or the left side of the input
					options.position == 'left' ? this.numInputContainer.append(this.btnFlexContainer, this.input) : 
												 this.numInputContainer.append(this.input, this.btnFlexContainer);
					
					// if the position is none - they are hidden
					if(options.position == 'none' || !options.position) 
					{
						this.btnFlexContainer.css('display', 'none');
						// the none class doesn't hide the input,but makes sure it won't go out of parent's bounds when the fill prop is enabled
						this.input.addClass('ml-blueprint-numeric-input-none');
					}
					else
					{
						this.btnFlexContainer.css('display', 'inline-flex');
					}
					
					if(options.position == 'left')
					{
						// make adjustments for the icon and the input when the buttons are on the left
						if(this.leftIcon){
							this.leftIcon.addClass('ml-blueprint-numeric-input-icon-left');
						}
						this.input.addClass('ml-blueprint-numeric-input-left');
					}

					if(options.intent)
					{
						// give the buttons,the icon and the input field the intent color
						if(this.leftIcon){
							this.leftIcon.addClass(`ml-blueprint-numeric-input-icon-${options.intent}`);
						}
						this.input.addClass(`ml-blueprint-numeric-input-${options.intent}`);
						this.incrementBtn.addClass(`ml-blueprint-numeric-input-${options.intent}`);
						this.decrementBtn.addClass(`ml-blueprint-numeric-input-${options.intent}`);
					}
				});
			}

		}

		protected cleanup() {
			super.cleanup();
			if(this.input) this.input.off('keydown focusin blur');
			if(this.incrementBtn) this.incrementBtn.off('click mousedown');
			if(this.decrementBtn) this.decrementBtn.off('click mousedown');
			ml.$(document).off('mouseup', this.mouseListeners.prototype.clearMousedown);
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPNumericInput.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPNumericInput {

		/**
		 * Enumeration for increment/decrement button positions for the Numeric Input
		 * @enum {string}
		 * @readonly
		 */
		export enum Position {
			/** Place buttons at the left of the input field */
			Left = "left",
			/** Place buttons at the right of the input field */
			Right = "right",
			/** Don't place the buttons */
			None = "none"
		}

		/**
		 * Interface that provides props to Numeric Input
		 * @typedef INumericInputProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IIntentProps
		 * @augments BPBase.IProps
		 */
		export interface INumericInputProps extends BPBase.IIntentProps,BPBase.IProps{
			/**
			 * Only allows numeric characters to be entered inside the text field
			 * @type {boolean}
			 */
			numericOnly?: boolean,
			/**
			 * The position of the increment/decrement buttons of the Numeric Input
			 * @type {Position}
			 */
			position?: Position,
			/**
			 * Restrict the numbers to a min-max range when the blur event happens
			 * @type {boolean}
			 */
			clampValueOnBlur?: boolean,
			/**
			 * Disable the interactions on numeric input
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Fill the whole parent container
			 * @type {boolean}
			 */
			fill?: boolean,
			/**
			 * Apply styling to make numeric input large
			 * @type {boolean}
			 */
			large?: boolean,
			/**
			 * Add an icon on the left of the numeric input
			 * @type {string}
			 */
			leftIcon?: string,
			/**
			 * Specify the number by which the number inside numeric input is incremented when holding Shift modifier key
			 * @type {number}
			 */
			majorStepSize?: number | null,
			/**
			 * Highest number of the numeric input
			 * @type {number}
			 */
			max?: number,
			/**
			 * Lowest number of the numeric input
			 * @type {number}
			 */
			min?: number,
			/**
			 * Specify the number by which the number inside numeric input is incremented when holding Alt modifier key
			 * @type {number}
			 */
			minorStepSize?: number | null,
			/**
			 * Placeholder text for numeric input,when the input field is empty
			 * @type {string}
			 */
			placeholder?: string,
			/**
			 * Select the input field when focused
			 * @type {boolean}
			 */
			selectAllOnFocus?: boolean,
			/**
			 * Select the input field when the field is incremented
			 * @type {boolean}
			 */
			selectAllOnIncrement?: boolean,
			/**
			 * Step size of the numeric input increment when no modifier key is pressed
			 * @type {number}
			 */
			stepSize?: number | null,
			/**
			 * Number of decimals that should be saved after the decimal point
			 * @type {number}
			 */
			decimalPrecision?: number,
			/**
			 * Event handler called when the increment/decrement button is clicked
			 * @function
			 * @param {number} valueAsNumber - Numeric input field value as a number
			 * @param {string} valueAsString - Numeric input field value as a string
			 * @param {mdash.IScope} scope - The current scope
			 * @event click
			 * @returns {void}
			 */
			onButtonClick?: (valueAsNumber: number, valueAsString: string, scope?: mdash.IScope) => void,
			/**
			 * Event handler called when the input value is changed
			 * @function
			 * @param {number} valueAsNumber - Numeric input field value as a number
			 * @param {string} valueAsString - Numeric input field value as a string
			 * @param {mdash.IScope} scope - The current scope
			 * @event click
			 * @returns {void}
			 */
			onValueChange?: (valueAsNumber: number, valueAsString: string, scope?: mdash.IScope) => void
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, INumericInputProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			numericOnly?: boolean | mdash.IScopePath,
			position?: Position | mdash.IScopePath,
			clampValueOnBlur?: boolean | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
			fill?: boolean | mdash.IScopePath,
			large?: boolean | mdash.IScopePath,
			leftIcon?: string | mdash.IScopePath,
			majorStepSize?: number | null | mdash.IScopePath,
			max?: number | mdash.IScopePath,
			min?: number | mdash.IScopePath,
			minorStepSize?: number | null | mdash.IScopePath,
			placeholder?: string | mdash.IScopePath,
			selectAllOnFocus?: boolean | mdash.IScopePath,
			selectAllOnIncrement?: boolean | mdash.IScopePath,
			stepSize?: number | null | mdash.IScopePath,
			intent?: BPBase.Intent | mdash.IScopePath,
			decimalPrecision?: number | mdash.IScopePath,
			onButtonClick?: (valueAsNumber: number, valueAsString: string, scope?: mdash.IScope) => void,
			onValueChange?: (valueAsNumber: number, valueAsString: string, scope?: mdash.IScope) => void
		}
	}

	mdash.registerControl(BPNumericInput,'dashboard.components.controls', BPNumericInput.meta);
}