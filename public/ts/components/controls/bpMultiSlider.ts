/// <reference path="./bpSlider.ts" />

namespace dashboard.components.controls {
	
	import mdash = ml.ui.dashboard;
	
	
	export class BPMultiSlider<T extends BPMultiSlider.IOptionsDefinition> extends BPSlider<BPMultiSlider.IOptionsDefinition> {
		public static type: string = 'BPMultiSlider';
		
		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Multiple Slider',
			shortDescription: "MultiSlider is a flexible solution for picking arbitrary values on a number line.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPMultiSlider.builder().options(BPMultiSlider.initialOptions.GenericOptions.options).toJSON();
		}
		
		public static initialOptions: mdash.IDashboardInitialOptions<BPMultiSlider.IOptions> = {
			'BPMultiSlider': {
				options: {
					handles: [
						{value: 20, intentAfter: BPBase.Intent.Primary},
						{value: 50}
					],
					min: 0,
					max: 100,
					outputTransforms: []
				}
			}
		};
		
		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}
		
		constructor(id: string, definition: mdash.IControlDefinition<T>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
			
		}
		
		private isInitialized: boolean = false;
		private initObsOnce: boolean = false;
		protected observer: MutationObserver;
		
		public build() {
			super.build();
		}
		
		/**
		 * overrides the Slider method to work with n number of handles
		 * @param options The options object with current properties
		 * @param obs The observable
		 */
		protected initializeHandleTooltip(options: BPMultiSlider.IOptions, obs: mdash.RefCountedObservable<any>): void
		{
			let values: number[] = [];
			options.handles.forEach(({ value }: any) => {
				const val = mdash.isIScopePath(value) ? this.getScopeValue(value.$scopePath) : value;
				values.push(val);
			});
			this.slider.slider('option','values', values);
			this.contentDiv.find('.mlui-slider-handle').append(this.handleLabel);
			
			for(let i = 0; i < values.length; i++)
			{
				this.contentDiv.find('.mlui-slider-handle .ml-blueprint-slider-handle-label').eq(i).text(values[i]);
			}
		}

		/**
		 * Update the slider on observable change
		 * @param {RefCountedObservable} valueObs valueObs - Observable
		 */
		protected initializeListeners(valueObs: mdash.RefCountedObservable<any>): void {

			//objectsToDispose is an array of listeners that get destroyed when the control is destroyed
			//add listeners to this.objectsToDispose in order to prevent memory leaks
			this.objectsToDispose.push(
				valueObs.subscribe(newValue => {

					if(Array.isArray(newValue))
					{
						if(newValue.length == 1)
						{
							this.contentDiv.find('.ml-blueprint-slider-handle-label')[0].innerText = newValue[0];
							this.slider.slider('values',0,Number(newValue[0]));
						}
						else if(newValue.length > 1 && newValue.length <= this.contentDiv.find('.mlui-slider-handle').length)
						{
							newValue.sort((a,b) => a-b);
							newValue.forEach((item,index) => {
								this.contentDiv.find('.ml-blueprint-slider-handle-label')[index].innerText = item;
								this.slider.slider('values',index,Number(item));
							});

						}
					}
					else
					{
						this.contentDiv.find('.ml-blueprint-slider-handle-label')[0].innerText = newValue;
						this.slider.slider('values',0,Number(newValue));
					}

				})
			);
		}
		
		/**
		 * override the super function to fix the position of all handles of Multi Slider
		 * @param options The options object with current properties
		 * @param obs The observable
		 */
		protected initializeLabels(options: BPMultiSlider.IOptions, obs: mdash.RefCountedObservable<any>){
			let values: number[] = [];
			
			if(options.handles)
			{
				options.handles.forEach(({ value }: any) => {
					const val = mdash.isIScopePath(value) ? this.getScopeValue(value.$scopePath) : value;
					values.push(val);
				} );
			}
			
			if(options.vertical)
			{
				this.contentDiv.find('.ml-blueprint-slider-handle-label').addClass('ml-blueprint-slider-handle-label-vertical');
				if(options.handles)
				{
					// fixing the slider handles not being in the right positions in vertical mode
					for(let i = 0; i < values.length; i++)
					{
						const firstHandleBottom: number = parseFloat(this.contentDiv.find('.mlui-slider-handle')[i].style.bottom);
						this.contentDiv.find('.mlui-slider-handle')[i].style.top = (100 - firstHandleBottom) + '%';
					}
				}
			}
			else
			{
				this.contentDiv.find('.ml-blueprint-slider-handle-label').removeClass('ml-blueprint-slider-handle-label-vertical');
				
				if(this.contentDiv.find('.ml-blueprint-slider-handle-label').length !== values.length)
				{
					this.initializeHandleTooltip(options, obs);
				}
				
				this.contentDiv.find('.mlui-slider-handle').css('top', '-5px');
				// overriding Slider.ts range class with a custom one
				this.contentDiv.find('.ml-blueprint-slider-custom-range').css('height', '100%');
			}
			
		}
		
		/**
		 * Take the amount of handles that are on the track
		 * generate just as many blank divs
		 * other functions should handle the placement of the bars
		 * @param options Options object with current properties
		 */
		protected generateRangeDivsForHandles(options: BPMultiSlider.IOptions): void {
			
			// detach previously generated divs if they exist
			// then sort the handles in the right order
			ml.$('.ml-blueprint-slider-custom-range').detach();
			options.handles.sort((a:any,b:any) => {
				const valA = mdash.isIScopePath(a) ? this.getScopeValue(a.$scopePath) : a;
				const valB = mdash.isIScopePath(b) ? this.getScopeValue(b.$scopePath) : b;
				return valA.value - valB.value
			} );
			
			for(let i = 0; i < options.handles.length; i++)
			{
				if(options.handles[i].intentBefore)
				{
					const isScope: boolean = mdash.isIScopePath(options.handles[i].intentBefore);
					const intentBefore = isScope ? this.getScopeValue((<any>options.handles[i].intentBefore).$scopePath) : options.handles[i].intentBefore;
					this.slider.append(`<div data-id=${i+'-before'} class="ml-blueprint-slider-custom-range ml-blueprint-slider-custom-range-${intentBefore}"></div>`);
				}
				
				if(options.handles[i].intentAfter)
				{
					const isScope: boolean = mdash.isIScopePath(options.handles[i].intentAfter);
					const intentAfter = isScope ? this.getScopeValue((<any>options.handles[i].intentAfter).$scopePath) : options.handles[i].intentAfter;
					this.slider.append(`<div data-id=${i+'-after'} class="ml-blueprint-slider-custom-range ml-blueprint-slider-custom-range-${intentAfter}"></div>`);
				}
			}
		}
		
		/**
		 * Get the left % and top % of absolutely positioned handles
		 */
		protected fetchHandlePlacementPercentages(): BPMultiSlider.HandlePercentage {
			const percentages: BPMultiSlider.HandlePercentage = { left: [], top: [] };
            const elements: NodeListOf<Element> = this.contentDiv[0].querySelectorAll('.mlui-slider-handle');
			
			elements.forEach((element: HTMLElement) => {
				percentages.left.push(element.style.left);
				percentages.top.push(element.style.top);
			});
			
			return percentages;
		}
		
		/** 
		 *	In Blueprint slider, a bar is a trail that's behind or after the handle
		 *	The bar extends either from it's behind to the obstacle or from the handle until an obstacle
		 *	An obstacle is either a handle, or the start/end of the track
		 *	Bar gets Intent colors
		 *	
		 *   @param {string[]} values - Percentages,left or top % of the handles
		 *   @param {BPMultiSlider.IOptions} options - Component options object
		 *   @param {boolean} vertical - Argument specificying if the orientation of the slider is vertical or not
		 *   @param {BPMultiSlider.MultiSliderHandle[]} handles - The handles themselves supplied as a prop to the component
		 */
		protected drawBarsForAllHandles(values: string[], options: BPMultiSlider.IOptions, vertical?: boolean, handles?: BPMultiSlider.MultiSliderHandle[]): void
		{
			// generate new range divs every time before placing them correctly
			this.generateRangeDivsForHandles(options);
			const numbers: number[] = [...values.map(item => parseFloat(item))];
			// sort numbers in ascending order
			numbers.sort((a, b) => a - b);
			
			// go over handle values in a loop
			for(let i = 0; i < handles.length; i++)
			{
				// select the appropriate bars
				const beforeBar: HTMLElement = this.contentDiv.find(`.ml-blueprint-slider-custom-range[data-id=${i}-before]`)[0];
				const afterBar: HTMLElement = this.contentDiv.find(`.ml-blueprint-slider-custom-range[data-id=${i}-after]`)[0];
				
				// if there's a previous element, apply the correct styling
				if(numbers[i-1] && handles[i].intentBefore)
				{
					// we set 2 types of styles - vertical and horizontal
					// inside the template strings, we check if it's vertical with a ternary operator - we set the vertical style,
					// if it's not, we put a : and then tell the horizontal style
					beforeBar.style.top = `${vertical ? `${numbers[i-1]}%` : '0'}`;
					beforeBar.style.height = `${vertical ? `${numbers[i] - numbers[i-1]}%` : '100%'}`;
					beforeBar.style.width = `${vertical ? '100%': `${numbers[i] - numbers[i-1]}%`}`;
					beforeBar.style.left = `${vertical ? '0' : `${numbers[i-1]}%`}`;
				}
				else
				{
					// first element with intent before
					if(handles[i].intentBefore)
					{
						beforeBar.style.top = '0';
						beforeBar.style.height = `${vertical ? `${numbers[i]}%` : '100%'}`;
						beforeBar.style.width = `${vertical ? '100%': `${numbers[i]}%`}`;
						beforeBar.style.left = '0';
					}
				}
				
				if(numbers[i+1] && handles[i].intentAfter)
				{
					// we set 2 types of styles - vertical and horizontal
					// inside the template strings, we check if it's vertical with a ternary operator - we set the vertical style,
					// if it's not, we put a : and then tell the horizontal style
					afterBar.style.top = `${vertical ? `${numbers[i]}%` : '0'}`;
					afterBar.style.height = `${vertical ? `${numbers[i+1] - numbers[i]}%` : '100%'}`;
					afterBar.style.width = `${vertical ? '100%': `${numbers[i+1] - numbers[i]}%`}`;
					afterBar.style.left = `${vertical ? '0': `${numbers[i]}%`}`;
				}
				else
				{
					// if it's the last handle and it has intent after
					if(handles[i].intentAfter)
					{
						afterBar.style.top = `${vertical ? `${numbers[i]}%` : '0'}`;
						afterBar.style.height = `${vertical ? `${100 - numbers[i] == 100 ? 0 : 100 - numbers[i]}%` : '100%'}`;
						afterBar.style.width = `${vertical ? '100%': `${100 - numbers[i] == 100 ? 0 : 100 - numbers[i]}%`}`;
						afterBar.style.left = `${vertical ? '0': `${numbers[i]}%`}`;
					}
				}
			}
		}

		/**
		 * implements handle push interaction
		 * During Push interaction,dragging 1 handle over another pushes that other handle with it as well
		 * @param values The current values on the slider track
		 * @param options The options object with current properties
		 */
		protected handlePushInteraction(values: number[],options: BPMultiSlider.IOptions): void{
			setTimeout(() => {
				const valuesCopy: number[] = [...values];
				
				valuesCopy.forEach((val, index) => {
					// overwrite values when a handle is moved forwards, over other handles
					const nextHandle: JQuery = this.contentDiv.find(`.mlui-slider-handle[data-id=${index+1}]`);
					const previousHandle: JQuery = this.contentDiv.find(`.mlui-slider-handle[data-id=${index}]`);
					if(valuesCopy[index+1] && valuesCopy[index+1] - val <= options.step)
					{
						this.slider.slider('values', index+1, val);
						nextHandle.find('.ml-blueprint-slider-handle-label').text(val);
						if(options.vertical)
						{
							nextHandle[0].style.top = `${100 - parseFloat(nextHandle[0].style.bottom)}%`;
						}
					}
					
					// overwrite values when a handle is moved backwards, over other handles
					if(valuesCopy[index+1] && valuesCopy[index+1] - val <= -options.step)
					{
						this.slider.slider('values', index, valuesCopy[index+1]);
						previousHandle.find('.ml-blueprint-slider-handle-label').text(valuesCopy[index+1]);
						if(options.vertical)
						{
							previousHandle[0].style.top = `${100 - parseFloat(previousHandle[0].style.bottom)}%`;
						}
					}
				}) 
			}, 1);
		}
		
	    /**
		 * fetchHandlePlacementPercentages() was getting older values, it was 1 step behind the actual DOM values
		 * That's why I have put this function call and value assignment inside a promise that invokes a setTimeout
		 * that pushes the code execution to the end of the event loop of JavaScript, where the actual DOM values are available.
		 */
		protected fetchPercentages(): Promise<{}>{
			return new Promise((resolve, reject) => {
				setTimeout(() => {
					try{
						resolve(this.fetchHandlePlacementPercentages());
					}
					catch(err)
					{
						reject(err);
					}
				}, 1);
			})
		}
		
		/**		
		 * The purpose of this function is to watch attribute changes on handles
		 * and then correct their top% using their bottom% everytime their attributes change
		 * it's also used to correct the slider's values so the obs,slider and label values are all the same
		 * @param obs Observable variable
		 */
		protected observeAttributeChanges(obs: mdash.RefCountedObservable<any>): void 
		{
			if(this.observer) this.observer.disconnect(); // disconnect the observer if it already exists
			const handles: JQuery = this.contentDiv.find('.mlui-slider-handle');
			
			// create a new MutationObserver instance with a callback and assign it to the class variable
			this.observer = new MutationObserver(() => {
				const handleVals: number[] = [];
				handles.each((index, handle: HTMLElement) => {
					handle.style.top = `${100 - parseFloat(handle.style.bottom)}%`;
					handleVals.push(Number(handle.textContent));
					this.slider.slider('values', handleVals);
					obs(handleVals);
				});
			});
			// attach the observer to every handle
			handles.each((index,handle: HTMLElement) => this.observer.observe(handle, { attributes: true }));
		}
		
		/**
		 * overrides the parent method with different values
		 * @param options The options object with current properties
		 * @param obs The observable variable
		 * @param handles The handles, sorted in an ascending order
		 */
		protected initializeSliderOptions(options: BPMultiSlider.IOptions, obs: mdash.RefCountedObservable<any>, 
			handles?: BPMultiSlider.MultiSliderHandle[]){
				return {
					// using ternary and logic operators because if/else take longer to write
					disabled: Boolean(options.disabled),
					range: false,
					min: options.min || 0,
					max: options.max || ~~options.min + 100,
					orientation: options.vertical ? 'vertical' : 'horizontal',
					step: options.step || 1,
					values: obs() || [~~options.min, ~~options.max],
					slide: (event: JQueryEventObject, { handle, values, value }): boolean => {
						// if the handle interaction type is lock and the handles collided
						// prevent it from moving
						if(options.handleInteraction == BPMultiSlider.HandleInteraction.Lock)
						{
							const handleIndex: number = Number(ml.$(handle).attr('data-id'));
							let rightHandleDifference: number, leftHandleDifference: number;
							
							//Check with right handles
							if(handleIndex > 0)
							{
								rightHandleDifference = values[handleIndex] - values[handleIndex-1];
							}
							
							//Check with left handles
							if(handleIndex < values.length-1)
							{
								leftHandleDifference = values[handleIndex+1] - values[handleIndex];
							}
							
							//checks for the values and prevent the slide event from happening if the differences are smaller or equal to 0
							if (rightHandleDifference <= 0 || leftHandleDifference <= 0)
							{
								return false;
							}
						}
						else
						{
							this.handlePushInteraction(values, options);
						}
					
						// do a one-time only adjustment to observables
						if(!this.initObsOnce)
						{
							let obsValues: number[] = [];
							options.handles.forEach((item:any) => {
								const itemVal = mdash.isIScopePath(item) ? this.getScopeValue(item.$scopePath) : item;
								return obsValues.push(itemVal.value);
							} );
							
							obs(obsValues);
							this.initObsOnce = true;
						}
						else
						{
							obs(values);
						}
						
						if(options.vertical)
						{
							// extract the top percentages off the percentages received from Promise
							this.fetchPercentages()
							.then(({ top }: BPMultiSlider.HandlePercentage) => {
								this.drawBarsForAllHandles(top, options, true, handles || options.handles);
							});
						}
						else
						{
							// extract the left percentages off the percentages received from Promise
							this.fetchPercentages()
							.then(({ left }: BPMultiSlider.HandlePercentage) => {
								this.drawBarsForAllHandles(left,options, false, handles || options.handles);
							});
						}
						// if the slide event source was a generated label
						if(event.srcElement.classList.contains('ml-blueprint-slider-label'))
						{
							this.dragClosestHandleToLabel(event, handle, obs, options.vertical);
							this.fetchPercentages()
							.then(({ top,left }: BPMultiSlider.HandlePercentage) => {
								this.drawBarsForAllHandles(options.vertical ? top : left, options, Boolean(options.vertical), handles || options.handles);
							});
						}
						
						handle.querySelector('.ml-blueprint-slider-handle-label').textContent = value;
						this.observeAttributeChanges(obs);
				}
			}
		}
		
		protected validateDefinition(): boolean {
			
			if (!super.validateDefinition())
				return false;
			
			return true;
		}
		
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, [0, 0, 0], false);
			this.initializeListeners(obs);
			
			if(this.isDOMLoaded)
			{
				this.watchDefinitionOptions((options: BPMultiSlider.IOptions) => {
					// one time initialization
					if(!this.isInitialized)
					{
						let obsValues: number[] = [];
						// we take the handles, then we sort them from lowest to highest handle values
						if(options.handles)
						{
							options.handles.sort((a:any,b:any) => {
								const valA = mdash.isIScopePath(a) ? this.getScopeValue(a.$scopePath) : a;
								const valB = mdash.isIScopePath(b) ? this.getScopeValue(b.$scopePath) : b;
								return valA.value - valB.value
							});
							options.handles.forEach((item:any) => {
								const itemVal = mdash.isIScopePath(item) ? this.getScopeValue(item.$scopePath) : item;
								return obsValues.push(itemVal.value);
							} );
						}
						
						obs(obsValues);
						this.slider.slider(this.initializeSliderOptions(options, obs, options.handles));
						this.initializeLabels(options, obs);
						this.generateLabels(Boolean(options.vertical), this.slider.slider('option', 'min'),this.slider.slider('option', 'max'), options.labelPercentages);
						this.giveIdentityToHandles();
						this.observeAttributeChanges(obs);
						this.isInitialized = true;
					}
					this.watchScopePath(this.definition.name,() => {
						this.fetchPercentages()
							.then(({ top, left }: BPMultiSlider.HandlePercentage) => {
								this.drawBarsForAllHandles(options.vertical ? top : left,options,Boolean(options.vertical),options.handles);
							});
					})
					// fetch new percentages and draw bars everytime we update the props
					this.fetchPercentages()
						.then(({ top, left }: BPMultiSlider.HandlePercentage) => {
							this.drawBarsForAllHandles(options.vertical ? top : left,options,Boolean(options.vertical),options.handles);
						});
				});
			}
			
		}
		
		protected cleanup() {
			super.cleanup();
			if(this.observer) this.observer.disconnect();
		}
		
		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPMultiSlider.IOptionsDefinition>(BPMultiSlider.type);
		}
	}
	export namespace BPMultiSlider {
		
		/**
		 * An object for holding absolute percentages of top and left CSS parameters
		 * @typedef {Object} HandlePercentage
		 * @readonly
		 */
		export type HandlePercentage = {
			/**
			 * Absolute percentages of the left CSS parameter
			 * @type {string[]}
			 */
			left: string[],
			/**
			 * Absolute percentages of the top CSS parameter
			 * @type {string[]} 
			 */
			top: string[],
		}

		/**
		 * Interface for an individual handle on Multi Slider
		 * @typedef MultiSliderHandle
		 * @interface
		 * @readonly
		 */
		export interface MultiSliderHandle{
			/**
			 * Value of the handle
			 * @type {number | mdash.IScopePath}
			 */
			value: number | mdash.IScopePath,
			/**
			 * Intent color of the bar that comes before it
			 * @type {BPBase.Intent | mdash.IScopePath}
			 */
			intentBefore?: BPBase.Intent | mdash.IScopePath,
			/**
			 * Intent color of the bar that comes after it
			 * @type {BPBase.Intent | mdash.IScopePath}
			 */
			intentAfter?: BPBase.Intent | mdash.IScopePath,
		}
		
		/**
		 * Enumeration for interaction type of the handle
		 * @readonly
		 * @enum {string}
		 */
		export enum HandleInteraction{
			/**
			 * Lock interaction blocks handles from moving over each other
			 */
			Lock = 'lock',
			/**
			 * Push interaction allows the handle to push other handles on it's way
			 */
			Push = 'push'
		}

		/**
		 * @augments BPRangeSlider.IOptions
		 */
		export interface IOptions extends BPRangeSlider.IOptions {
			handleInteraction?: HandleInteraction,
			handles: MultiSliderHandle[],
			values?: never // gonna give you up
		}
		
		export interface IOptionsDefinition extends BPRangeSlider.IOptionsDefinition {
			handleInteraction?: HandleInteraction | mdash.IScopePath,
			handles: MultiSliderHandle[] | mdash.IScopePath,
			values?: never // gonna let you down
		}
	}
	
	mdash.registerControl(BPMultiSlider,'dasboard.component.controls', BPMultiSlider.meta);
}