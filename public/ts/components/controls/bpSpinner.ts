namespace dashboard.components.controls {
	import mdash = ml.ui.dashboard;

	export class BPSpinner extends mdash.BaseControl<BPSpinner.IOptionsDefinition> {
		public static type: string = 'BPSpinner';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Spinner',
			shortDescription: "The spinner component provides a variety of animated SVG spinners.",
			categories: ["Visualization"],
			childSupport: mdash.ChildSupport.NONE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPSpinner.builder().options(BPSpinner.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPSpinner.IOptions> = {
			'BPSpinner': {
				options: {
					label: 'Spinner',
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected CircleContent: HTMLElement;
		protected Circle: JQuery;

		constructor(id: string, definition: mdash.IControlDefinition<BPSpinner.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
			this.container.classList.add('namespace-blueprint-spinner')
			this.CircleContent = ml.create("div", "ml-BPSpinner",  this.contentDiv[0]);
			this.CircleContent.classList.add('circleContent');
			this.Circle = ml.$(`
			<svg width="75" height="75" stroke-width="5.33" viewBox="2.33 2.33 95.33 95.33">
				<path class="spinner-track" d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90">
				</path>
				<path class="spinner-head" d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" pathLength="280" stroke-dasharray="280 280" stroke-dashoffset="196">
				</path>
				</svg>
			`);
			this.Circle.appendTo(this.CircleContent);
			
		}
		
		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
			return false;

			return true;
		}

		protected init() {
			super.init();

			var obs = this.registerScopeObservable(this.definition.name, null, false);

			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPSpinner.IOptions) => {
				this.CircleContent.className = 'circleContent';
				if(options.intent){
					switch(options.intent){
						case BPBase.Intent.Danger:this.CircleContent.classList.add(`spiner-${options.intent}`);
							break;
						case BPBase.Intent.Primary:this.CircleContent.classList.add(`spiner-${options.intent}`);
						break;
						case BPBase.Intent.Success:this.CircleContent.classList.add(`spiner-${options.intent}`);
							break;
						case BPBase.Intent.Warning:this.CircleContent.classList.add(`spiner-${options.intent}`);
							break;
					}
					
				}
				
				if(options.circleSize){
					ml.$('.circleContent').find('svg').attr('height', options.circleSize).attr('width', options.circleSize)
					
				}
				if(options.animationStop){
					ml.$('.circleContent').find('.spinner-head').attr('stroke-dashoffset', 280-options.strokeSize)
				}
				else{
					ml.$('.circleContent').find('.spinner-head').attr('stroke-dashoffset', 190)
				}
				(options.animationStop)?ml.$('.circleContent').find('svg').attr('class', 'stopAnimation'):ml.$('.circleContent').find('svg').attr('class', '')
				
			
				});
			}
			
		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPSpinner.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPSpinner {

		/**
		 * Props for Spinner component
		 * @typedef ISpinnerProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IActionProps
		 * @augments BPBase.IAppearance
		 */
		export interface ISpinnerProps extends BPBase.IActionProps, BPBase.IAppearance{
			/**
			 * Intent color of the spinning circle
			 * @type {BPBase.Intent}
			 */
			intent?: BPBase.Intent,
			/**
			 * Size of the spinning circle
			 * @type {number}
			 */
			circleSize?:number,
			/**
			 * Stop or start the animation
			 * @type {boolean}
			 */
			animationStop?:boolean,
			/**
			 * The fill number of the circle
			 * @type {number}
			 */
			strokeSize?:number

		}
		
		export interface IOptions extends mdash.IControlOptionsWithTransforms, ISpinnerProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			intent?: BPBase.Intent | mdash.IScopePath,
			circleSize?: number | mdash.IScopePath,
			animationStop?:boolean | mdash.IScopePath,
			strokeSize?:number | mdash.IScopePath
		}
	}
	
	mdash.registerControl(BPSpinner,'dashboard.components.controls', BPSpinner.meta);
}
