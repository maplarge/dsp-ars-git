namespace dashboard.components.controls {
	import mdash = ml.ui.dashboard;
	
	export class BPTimeZonePicker extends mdash.BaseControl<BPTimeZonePicker.IOptionsDefinition> {
		public static type: string = 'BPTimeZonePicker';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Time Zone Picker',
			shortDescription: "TimeZonePicker is a component for to select and get timezone value of selected area(country)",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPTimeZonePicker.builder().options(BPTimeZonePicker.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPTimeZonePicker.IOptions> = {
			'BPTimeZonePicker': {
				options: {
					outputTransforms: []
				}
			}
		};
		protected SelectBlockAll:HTMLInputElement;
		protected valueSelectTimeZone:string;
		protected SelectButton:JQuery;
		protected position:Function;
		protected TimeZoneData:JQuery;
		protected elementsTimeZoneEach:JQuery;
		private searchTimeZone:JQuery; 
		protected showBooleanClone = false;
		protected standartTimeDisplay = "";
		protected currentItem = null;
		protected AbrriviationConvert:Function = (th:Element)=>{
			if(new RegExp(/\(.*\)/).test(ml.$(th).find("a").attr('data-location'))){
				return ml.$(th).find("a").attr('data-location').substring(
					ml.$(th).find("a").attr('data-location').indexOf('(')+1, ml.$(th).find("a").attr('data-location').length-1
				);
			}
			else{
				return ml.$(th).find("a").attr('data-zone')
			}
		}

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}


		constructor(id: string, definition: mdash.IControlDefinition<BPTimeZonePicker.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

			//this is where you might create/initialize class variables of observables
		}

		public build() {
			super.build();
			
			this.container.classList.add('namespace-blueprint-timeZonePicker')
			this.SelectBlockAll = ml.create("div", "ml-BPTimeZonePicker",  this.contentDiv[0]) as HTMLInputElement;
			this.SelectBlockAll.classList.add('ml-blueprint-dataZonePicker');
			this.valueSelectTimeZone = "Select timezone...";
			this.SelectButton = ml.$(`
				<button class="ml-blueprint-selectButton">${this.valueSelectTimeZone} <span class="mlicon-MapLarge-icons_layer-expanded"></span></button>
			`)
			this.searchTimeZone =  ml.$(`
				<input type="text" placeholder="Search for timezones...">
			`);
			this.TimeZoneData = ml.$(`
				<div class="ml-blueprint-AllSelects"></div>
			`).appendTo('body');
			
			ml.$(this.SelectBlockAll).append(this.SelectButton);
			this.elementsTimeZoneEach = ml.$("<ul type='none' class='ml-blueprint-listUL'></ul>");
			
			// var date  = ml.data.datetime.getTimezoneList();
			
			var offsetZone:number = Math.abs(new Date().getTimezoneOffset()/60),
				self = this, 
				countArrow:number = 0,
				// Arrow Keys Event
				scrollToAnchor = (aid, elementsTimeZoneEach)=>{
					ml.$(elementsTimeZoneEach).scrollTop(0);
					ml.$(elementsTimeZoneEach).scrollTop(ml.$(aid).offset().top - (ml.$(elementsTimeZoneEach).offset().top+ml.$(elementsTimeZoneEach).height()-2*(ml.$(aid).height())));
				},
				//______________
				date = null;


			ml.onload(function() {

				const ABBR_REGEX = /^[^-+]/;

				function getPopulousTimezoneItems(date) {
					// Filter out noisy timezones. See https://github.com/moment/moment-timezone/issues/227
					const timezones = ml.moment.tz.names(); //.filter(function(timezone) { /\//.test(timezone) && !/Etc\//.test(timezone)});

					const timezoneToMetadata = timezones.reduce(function(store, zone) {
						store[zone] = getTimezoneMetadata(zone, date);
						return store;
					}, {});

					// reduce timezones array to maximum population per offset, for each unique offset.
					const maxPopPerOffset = timezones.reduce(function(maxPop, zone) {
						const data = timezoneToMetadata[zone];
						const currentMax = maxPop[data.offsetAsString];
						if (currentMax == null || data.population > timezoneToMetadata[currentMax].population) {
							maxPop[data.offsetAsString] = zone;
						}
						return maxPop;
					}, {});


					return (
						Object.keys(maxPopPerOffset)
						// get metadata object
						.map(k => timezoneToMetadata[maxPopPerOffset[k]])
						// sort by offset
						// .sort(function(tz1, tz2) {tz1.offset - tz2.offset})
						// convert to renderable item
						.map(toTimezoneItem)
					);
				}
				function toTimezoneItem(mapItem) {
					var abbreviation = mapItem.abbreviation, offsetAsString = mapItem.offsetAsString, timezone = mapItem.timezone;
					return {
						key: timezone,
						label: offsetAsString,
						text: timezone + (abbreviation ? "(" + abbreviation  + ")" : ""),
						timezone,
					};
				}
				function getTimezoneMetadata(timezone, date) {
					const timestamp = date.getTime();
					const zone = ml.moment.tz.zone(timezone);
					const zonedDate = ml.moment.tz(timestamp, timezone);
					// const offset = zonedDate.utcOffset();
					const offsetAsString = zonedDate.format("Z");

					// Only include abbreviations that are not just a repeat of the offset:
					// moment-timezone's `abbr` falls back to the time offset if a zone doesn't have an abbr.
					const abbr = zone.abbr(timestamp);
					const abbreviation = ABBR_REGEX.test(abbr) ? abbr : undefined;

					return {
						abbreviation,
						// offset,
						offsetAsString,
						// population: zone.population,
						timezone,
					};
				}

				date = getPopulousTimezoneItems(new Date());

			});
			// hasn't my currect Zone, for that i added my zone, which i show what it return me
			date.push({key: "Asia/Yerevan", label: "+04:00", text: "Asia/Yerevan(GMT)", timezone: "Asia/Yerevan"});
			//________________________
			function TimeZone (){
				this.__init.call(this)
			}
			TimeZone.prototype = {
				__init:function():void{
					// this.__ButtonClick();
				},
				__UpDownFunc:(e:KeyboardEvent):void=>{
					let key = e.which || e.keyCode;
					if(key == 38 || key == 40){
						Array.prototype.map.call(ml.$(this.elementsTimeZoneEach).children(), (element:JQuery)=> {
							ml.$(element).removeClass("currentTime");
						})
					}
					if(key == 40){
						(countArrow == ml.$(this.elementsTimeZoneEach).children().length-1)?countArrow = 0:countArrow++;
						ml.$(ml.$(this.elementsTimeZoneEach).children()[countArrow]).addClass("currentTime");
						scrollToAnchor(ml.$(this.elementsTimeZoneEach).children()[countArrow], this.elementsTimeZoneEach);
						
					}
					if(key == 38){
						(countArrow > 0)?countArrow--:countArrow = ml.$(this.elementsTimeZoneEach).children().length-1;
						ml.$(ml.$(this.elementsTimeZoneEach).children()[countArrow]).addClass("currentTime");
						scrollToAnchor(ml.$(this.elementsTimeZoneEach).children()[countArrow], this.elementsTimeZoneEach);
					}
					if(key == 13){
						ml.$('.ml-blueprint-selectButton').removeClass('buttonToggle')
						this.currentItem = ml.$(this.elementsTimeZoneEach).children()[countArrow];
						timezone.__clickEachItem(ml.$(this.elementsTimeZoneEach).children()[countArrow]);
					}
					else if(key == 8 || key != 40 && key != 38 && key != 13){
						ml.$(this.elementsTimeZoneEach).scrollTop(0);
						countArrow = 0;
					}
				},
				__ButtonClick:()=>{
					ml.$(this.TimeZoneData).html(`
						<div class="searchdata">
							<span class="mlicon-x16Search"></span>
						</div>
					`);
					ml.$('.searchdata').prepend(this.searchTimeZone);
					ml.$(this.elementsTimeZoneEach).html('')
					Array.prototype.map.call(date, (elm:any, ind:number)=>{
						if(Intl.DateTimeFormat().resolvedOptions().timeZone == elm.key && elm.label.split("").find((elem:number)=>{
							return elem == offsetZone;
						})){
							if(this.showBooleanClone ){
								ml.$(this.elementsTimeZoneEach).prepend(`
								<li class="currentTime"><a tabindex="-1" href="javascript:void(0)" data-location=${elm.text} data-zone=${elm.label}><span><i class="mlicon-bullseye2"></i> Current timezone</span> <span>${elm.label}</span></a> </li>
								`);
							}
							else{
								ml.$(ml.$(this.elementsTimeZoneEach).children()[0]).addClass('currentTime')
							}
						}
						else{
							ml.$(this.elementsTimeZoneEach).append(`
								<li><a tabindex="-1" href="javascript:void(0)" data-location=${elm.text} data-zone=${elm.label}><span>${elm.text}</span>  <span>${elm.label}</span></a></li>
							`);
						}
					})
					ml.$(this.TimeZoneData).append(ml.$(this.elementsTimeZoneEach))
					ml.$(this.TimeZoneData).addClass("zoneListShow")
					ml.$(this.elementsTimeZoneEach).children().on('click', function():void{
						ml.$('.ml-blueprint-selectButton').removeClass('buttonToggle')
						timezone.__clickEachItem(this);
						self.currentItem = this;
					})
					timezone.__SearchTimeZon()
					this.position()
				},
				__SearchTimeZon:():void=>{
					ml.$(this.searchTimeZone).focus();
					ml.$(this.searchTimeZone).on('keyup', (e)=>{
						ml.$(this.elementsTimeZoneEach).children().detach();
						let valueInputSearch = (<any>e.target).value.replace(/\+/g, "\\+").replace(/\\/g, "\\\\").replace(/\(/g, "\\(").replace(/\)/g, "\\)").toLowerCase();
						
						if(!/[!$%^&*|~=`{}\[\]";'<>?,.]/g.test(valueInputSearch)){
							Array.prototype.map.call(date, (elm, ind:number)=>{
								var BoldTxt = "", replaceBold = "";
								if(valueInputSearch == '' && Intl.DateTimeFormat().resolvedOptions().timeZone == elm.key  && elm.label.split("").find((elem:number)=>{
									return elem == offsetZone;
								})){
									if(this.showBooleanClone ){
										ml.$(this.elementsTimeZoneEach).prepend(`
										<li class="currentTime"><a tabindex="-1" href="javascript:void(0)" data-location=${elm.text} data-zone=${elm.label}><span><i class="mlicon-bullseye2"></i> Current timezone</span> <span>${elm.label}</span></a> </li>
										`);
									}
									else{
										ml.$(ml.$(this.elementsTimeZoneEach).children()[0]).addClass('currentTime')
									}
								}
								let symbolArr:Array<string> = ['text', "label"]; 
								for(let i =0; i< symbolArr.length; i++){
									if(new RegExp(valueInputSearch).test(elm[symbolArr[i]].toLowerCase())){
										let lengthSymbols = (/[()+-]/g.test(valueInputSearch))?1:0;
										for(let j =elm[symbolArr[i]].toLowerCase().match(valueInputSearch).index; j< elm[symbolArr[i]].toLowerCase().match(valueInputSearch).index+valueInputSearch.length-lengthSymbols; j++){
											if(symbolArr[i] == "text"){
												BoldTxt += elm.text[j];
											}
										}
										replaceBold = elm.text.replace(BoldTxt, `<b>${BoldTxt}</b>`);
										if(valueInputSearch==""){
											ml.$(ml.$(this.elementsTimeZoneEach).children()[ind]).detach()
										}
										return ml.$(this.elementsTimeZoneEach).append(`
											<li><a tabindex="-1" href="javascript:void(0)" data-location=${elm.text} data-zone=${elm.label}><span>${replaceBold}</span>  <span>${elm.label}</span></a></li>
										`);
									}
								}
							})
							if(valueInputSearch!=""){
								ml.$(ml.$(this.elementsTimeZoneEach).children()[0]).addClass('currentTime');
							}
							ml.$(this.elementsTimeZoneEach).children().on('click', function(){
								timezone.__clickEachItem(this)
								self.currentItem = this;
							})	
							if(ml.$(this.elementsTimeZoneEach).children().length == 0){
								ml.$(this.elementsTimeZoneEach).append(`
									<li class="noResult">No matching timezones.</li>
								`)
							}
						}
						else{
							if(ml.$(this.elementsTimeZoneEach).children().length == 0){
								ml.$(this.elementsTimeZoneEach).append(`
									<li class="noResult">No matching timezones.</li>
								`)
							}
						}
						
						if(ml.$('.noResult').length == 0){
							let key:number = e.which || e.keyCode;
							if(key == 38 || key == 40){
								Array.prototype.map.call(ml.$(this.elementsTimeZoneEach).children(), (element:Element)=> {
									ml.$(element).removeClass("currentTime");
								})
							}
							if(key == 40){								
								ml.$(ml.$(this.elementsTimeZoneEach).children()[countArrow]).addClass("currentTime");
								scrollToAnchor(ml.$(this.elementsTimeZoneEach).children()[countArrow], this.elementsTimeZoneEach);								
							}
							if(key == 38){
								if(ml.$(this.elementsTimeZoneEach).children().length != 1){
									ml.$(ml.$(this.elementsTimeZoneEach).children()[countArrow]).addClass("currentTime");
									scrollToAnchor(ml.$(this.elementsTimeZoneEach).children()[countArrow], this.elementsTimeZoneEach);
								}
							}
							if(key == 13){
								ml.$('.ml-blueprint-selectButton').removeClass('buttonToggle');
								this.currentItem = ml.$(this.elementsTimeZoneEach).children()[countArrow];
								timezone.__clickEachItem(ml.$(this.elementsTimeZoneEach).children()[countArrow]);
							}
						}
					})
					let boolEnter = true;
					ml.$(this.searchTimeZone).on('keydown', (e)=>{
						let key = e.which || e.keyCode;
						if(key == 27 ){
							ml.$(self.TimeZoneData).removeClass("zoneListShow");
							ml.$('.ml-blueprint-selectButton').removeClass('buttonToggle')
							setTimeout(() => {
								ml.$(self.searchTimeZone).val("")
								ml.$(self.TimeZoneData).find('ul').detach();
								countArrow = 0;
							}, 100);
							boolEnter = false;
						}
						if(ml.$(this.elementsTimeZoneEach).children().length != 1 && boolEnter){
							timezone.__UpDownFunc(<any>e);
						}
					})
					
				},
				__clickEachItem:(th:HTMLInputElement)=>{
					switch(this.standartTimeDisplay){
						case BPTimeZonePicker.TimeZoneStandart.Abbreviation:(<string>self.valueSelectTimeZone) = this.AbrriviationConvert(th);
						break;
						case BPTimeZonePicker.TimeZoneStandart.Composite:(<string>self.valueSelectTimeZone) = ml.$(th).find("a").attr('data-location')+ ml.$(th).find("a").attr('data-zone');
						break;
						case BPTimeZonePicker.TimeZoneStandart.Name:(<string>self.valueSelectTimeZone) = ml.$(th).find("a").attr('data-location');
						break;
						case BPTimeZonePicker.TimeZoneStandart.Offset:(<string>self.valueSelectTimeZone) = ml.$(th).find("a").attr('data-zone');
						break;
						default:self.valueSelectTimeZone = ml.$(th).find("a").attr('data-location')+ ml.$(th).find("a").attr('data-zone');
						break;
						}
					ml.$(self.SelectButton).html(
						self.valueSelectTimeZone+'<span class="mlicon-MapLarge-icons_layer-expanded"></span>'
					);
					ml.$(self.TimeZoneData).removeClass("zoneListShow");
					ml.$('.ml-blueprint-selectButton').removeClass('buttonToggle')
					setTimeout(() => {
						ml.$(self.searchTimeZone).val("")
						ml.$(self.TimeZoneData).find('ul').detach();
						countArrow = 0;
					}, 100);
				}
			}
			var timezone = new TimeZone();
			
			ml.$('.ml-blueprint-selectButton').on('click', (e:Event)=>{
				e.srcElement.classList.toggle("buttonToggle");
				if(!e.srcElement.closest('.buttonToggle')){
					ml.$(this.TimeZoneData).removeClass("zoneListShow");
					ml.$(this.searchTimeZone).val("")
					setTimeout(() => {
						ml.$(this.TimeZoneData).find('ul').detach();
					}, 100);
				}
				else{
					timezone.__ButtonClick();
				}
			});

			document.addEventListener("click", (e:Event)=>{
				if(!e.srcElement.closest('.ml-blueprint-AllSelects') &&  !e.srcElement.closest('.ml-blueprint-selectButton')){
					ml.$('.ml-blueprint-selectButton').removeClass('buttonToggle');
					ml.$(this.TimeZoneData).removeClass("zoneListShow");
					ml.$(this.searchTimeZone).val("");
					setTimeout(() => {
						ml.$(this.TimeZoneData).find('ul').detach();
					}, 100);
				}
			})
			
			
			this.position = ()=>{
				// When DataList pass the window width limit
				// setTimeout(()=>{
					let dataList = ml.$('.ml-blueprint-AllSelects')[0],
				buttonPosition = ml.$('.ml-blueprint-selectButton')[0].getBoundingClientRect(),
				top=0,
				left=0,
				triangleLeft=0;
				if ((buttonPosition.left + ml.$(dataList).innerWidth()) > window.innerWidth) {
					left = buttonPosition.left - ((buttonPosition.left + ml.$(dataList).innerWidth()) - window.innerWidth) - 25;
					dataList.classList.add("triangleTransfer");
					triangleLeft = buttonPosition.left - left + buttonPosition.width / 2 - 20;
				}
				else {
					left = buttonPosition.left;
					dataList.classList.remove("triangleTransfer");
					triangleLeft = buttonPosition.width / 2 - 20;
				}

				// ________________________________
				// When DataList pass the window height limit
				if ((buttonPosition.top + buttonPosition.height + ml.$(dataList).innerHeight()) > window.innerHeight) {
					top = buttonPosition.top - ml.$(dataList).innerHeight() - 45;
					dataList.classList.add("triangleBottom");
				}
				else {
					top = buttonPosition.top + buttonPosition.height;
					dataList.classList.remove("triangleBottom");
				}
				// __________________________
				document.querySelector("body").style.setProperty('--triangelAfter', String(triangleLeft) + "px");
				dataList.style.left = left + "px";
				dataList.style.top = top + "px";
			// }, 1000)
			}
			this.position()

			//this is where you might create dom elements
			//elements are not yet added to the dom at this point though
		}
		
		protected validateDefinition(): boolean {

			//run any validation against the defintion (options passed into the control) that you might require

			if (!super.validateDefinition())
			return false;

			return true;
		}

		protected init() {
			super.init();
			
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPTimeZonePicker.IOptions) => {
					// console.log(this.UploadButton.text(options.name))
					// set it to the initial Progress class before adding every other class
					
					if(options.disabled){
						ml.$(this.SelectButton.attr("disabled", "disabled"))
					}
					else{
						ml.$(this.SelectButton.removeAttr("disabled"))
					}
					if(options.showLocalTimeZone){
						this.showBooleanClone = true;
					}
					else{
						this.showBooleanClone = false;
					}
					if(options.standartDisplay){
						this.standartTimeDisplay = options.standartDisplay;
					}
					else{
						this.standartTimeDisplay = "";
					}
					if(this.currentItem != null){

						switch(this.standartTimeDisplay){
							case BPTimeZonePicker.TimeZoneStandart.Abbreviation:this.valueSelectTimeZone = this.AbrriviationConvert(this.currentItem);
								break;
							case BPTimeZonePicker.TimeZoneStandart.Composite:this.valueSelectTimeZone = ml.$(this.currentItem).find("a").attr('data-location')+ ml.$(this.currentItem).find("a").attr('data-zone');;
								break;
							case BPTimeZonePicker.TimeZoneStandart.Name:this.valueSelectTimeZone = ml.$(this.currentItem).find("a").attr('data-location');
								break;
							case BPTimeZonePicker.TimeZoneStandart.Offset:this.valueSelectTimeZone = ml.$(this.currentItem).find("a").attr('data-zone');
								break;
							default:this.valueSelectTimeZone = ml.$(this.currentItem).find("a").attr('data-location')+ ml.$(this.currentItem).find("a").attr('data-zone');;
								break;
						}
						// obs(this.valueSelectTimeZone)
						ml.$(this.SelectButton).html(
								this.valueSelectTimeZone+'<span class="mlicon-MapLarge-icons_layer-expanded"></span>'
						);
						ml.$(this.TimeZoneData).removeClass("zoneListShow");
						setTimeout(() => {
							ml.$(this.searchTimeZone).val("");
							ml.$(this.TimeZoneData).find('ul').detach();
						}, 100);
					}

					this.position()
					
					obs(this.valueSelectTimeZone)
					ml.$(this.elementsTimeZoneEach).on('click', ():void => {
						obs(this.valueSelectTimeZone)
					})
				});
			}
			

			//called twice
			//1. To initialze the control but before it is loaded in the DOM (when this.isDOMLoaded == false)
			//2. After the control has been created in the DOM (when this.isDOMLoaded == true)
		}

		protected cleanup() {
			super.cleanup();
			//called just before the control gets destroyed
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPTimeZonePicker.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPTimeZonePicker {
		
		/**
		 * Enumeration for displaying the Timezone in the TimeZonePicker
		 * @readonly
		 * @enum {string}
		 */
		export enum TimeZoneStandart{
			/** Show the abbreviation only*/
			Abbreviation = 'Abbreviation',
			/** Show detailed timezone information */
			Composite = "Composite",
			/** Show timezone name only */
			Name = "Name",
			/** Show timezone offset only */
			Offset = "Offset"
		}

		/**
		 * Interface to work with Timezone Picker
		 * @typedef ITimeZonePickerProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IActionProps
		 * @augments BPBase.IAppearance
		 */
		export interface ITimeZonePickerProps extends BPBase.IActionProps, BPBase.IAppearance{
			/**
			 * Show or hide the local timezone
			 * @type {boolean}
			 */
			showLocalTimeZone?:boolean,
			/**
			 * Display mode of the timezone picker
			 * @type {TimeZoneStandart}
			 */
			standartDisplay?:TimeZoneStandart
        }
		
		export interface IOptions extends mdash.IControlOptionsWithTransforms, ITimeZonePickerProps {
		}
		
		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			showLocalTimeZone?:boolean | mdash.IScopePath,
			disabled?:boolean | mdash.IScopePath,
			standartDisplay?:string | BPTimeZonePicker.TimeZoneStandart | mdash.IScopePath,
		}
	}
	mdash.registerControl(BPTimeZonePicker,'dashboard.components.controls', BPTimeZonePicker.meta);
}
  