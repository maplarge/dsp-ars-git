//reference included to make sure that typescript knows to compile TextboxSample before TextboxPWSample since TextboxPWSample inherits from TextboxSample
/// <reference path="./TextBoxExample.ts" />
namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export namespace TextboxPWSample {
		export interface IOptionsDefinition extends TextboxSample.IOptionsDefinition { }
		export interface IOptions extends TextboxSample.IOptions { }
	}

	export class TextboxPWSample extends TextboxSample<TextboxPWSample.IOptionsDefinition> {
		public static type: string = 'TextboxPWSample';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.BETA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Password',
			shortDescription: "Input for a password",
			longDescription: "Provides an input for a user to enter a password.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};

		constructor(id: string, definition: mdash.IControlDefinition<TextboxPWSample.IOptionsDefinition>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		/* Notice how this is the only real significant overriding change since we want to use type=password instead of type=text  */
		protected getInputField() { return ml.$('<input type="password" />'); }

		public build() { super.build(); }

		protected validateDefinition(): boolean { return super.validateDefinition(); }

		protected init() { super.init(); }

		protected cleanup() { super.cleanup(); }

		public static builder() {
			return new mdash.ControlDefinitionBuilder<TextboxPWSample.IOptionsDefinition>(TextboxPWSample.type);
		}
	}
	mdash.registerControl(TextboxPWSample, 'db.examples.controls', TextboxPWSample.meta);
}   