namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPSelect extends mdash.BaseInputControl<BPSelect.IOptionsDefinition> {
		public static type: string = 'BPSelect';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Select',
			shortDescription: "Selects allow users to select from a single-option menu. It functions as a wrapper around the browser's native <select> element.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPSelect.builder().options(BPSelect.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPSelect.IOptions> = {
			'BPSelect': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		private select: JQuery;
		private button: JQuery;
		private portal: JQuery;
		private menu: JQuery;
		protected input: JQuery;
		protected overlay: JQuery;
		protected obsNewValue: Function;
		protected closeOnSelectBoolean = false;
		protected keyUpBoolean: boolean = false;
		private countArrow: number = 0;
		protected resetBoolean = false;
		protected items: { label: string, value: string }[];
		protected list: JQuery;
		protected text: string;
		protected listItems: Function;
		protected countMLID: string = String(ml.id());
		protected corectUL: string = `ml-overlay-${this.countMLID}`;
		private indexEsc: number = 0;

		constructor(id: string, definition: mdash.IControlDefinition<BPSelect.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {

			super.build();

			this.container.classList.add('namespace-blueprint-select');

			this.select = ml.$(`<span></span>`)
			this.portal = ml.$(`<div id="${this.corectUL}"></div>`)
			this.contentDiv.append(this.select);
			document.querySelector("body").appendChild(this.portal[0]);
			this.portal.append(`<div></div>`)
			this.select.addClass('ml-blueprint-select')
			this.select.append('<div></div>');
			this.button = ml.$(`<button data-ml-id="ml-overlay-${this.countMLID}"></button>`);
			this.list = ml.$('<ul class="ml-blueprint-select-menu"></ul>')
		}


		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}
		//  each button click get him Toggle data list 
		protected getCurrentToggleList(eventCurrent, untilParentClass, toggleListClass) {
			let parent, child, classNameUntilParent = document.querySelector(untilParentClass).className.split(" ")[0];
			parent = this.getParents(eventCurrent, document.querySelector(untilParentClass), classNameUntilParent);
			child = this.findJSFunction(toggleListClass, parent);
			return child;
		}
		// ___________________________

		// find any element by class, id and tagName into the Main elemnt
		protected findJSFunction(selector, parentElement) {
			var empChild = [], manyElement = [], result = null;
			function callElement(pseudoSelector, parentSelector) {
				Array.prototype.map.call((parentSelector.children as HTMLElement), (elem: Element) => empChild.push(elem))
				empChild.find((element) => {
					if (element.classList.contains(pseudoSelector)) {
						manyElement.push(element)
					}
					else if (element.tagName == pseudoSelector.toUpperCase()) {
						manyElement.push(element)
					}
					else if (element.id == pseudoSelector) {
						manyElement.push(element)
						return element
					}
				});
				if (manyElement[0] == undefined) {
					Array.prototype.map.call(parentSelector.children, element => {
						callElement(selector, element)
					})
				}
				return manyElement[0]
			}
			result = callElement(selector, parentElement);
			return result;
		}

		// _________________________

		// get Until Parents begin from  any elements

		protected getParents(el: HTMLElement, parentSelector /* optional */, untilClass) {
			let parent = null;
			if (parentSelector === undefined) {
				parentSelector = document;
			}
			var parents: {}[] = [];
			var p = el.parentNode;

			while (p !== parentSelector) {
				var o = p;
				parents.push(o);
				p = o.parentNode;
			}
			parents.push(parentSelector); // Push that parentSelector you wanted to stop at
			for (let elem = 0; elem < parents.length; elem++) {
				if ((parents[elem] as HTMLElement).classList.contains("" + untilClass)) {
					parent = parents[elem]
					break;
				}
			}
			return parent;
		}
		// ________________________________

		protected scrollToAnchor = (aid: HTMLLIElement, elementsList: JQuery) => {
			ml.$(elementsList).scrollTop(0);
			ml.$(elementsList).scrollTop(ml.$(aid).offset().top - (ml.$(elementsList).offset().top + ml.$(elementsList).height() - 2 * (ml.$(aid).height())));
		};
		protected upDownFunc(list: JQuery, e: JQueryEventObject): void {
			let key: number = e.which || e.keyCode;
			if (key == 38 || key == 40) {
				Array.prototype.map.call(list.children(), (element: HTMLElement) => {
					ml.$(element).removeClass("selected");
				})
			}
			if (key == 40) {
				(this.countArrow == list.children().length - 1) ? this.countArrow = 0 : this.countArrow++;
				ml.$(list.children()[this.countArrow]).addClass("selected");
				this.scrollToAnchor((list.children()[this.countArrow] as HTMLLIElement), list);

			}
			if (key == 38) {
				(this.countArrow > 0) ? this.countArrow-- : this.countArrow = list.children().length - 1;
				ml.$(list.children()[this.countArrow]).addClass("selected");
				this.scrollToAnchor((list.children()[this.countArrow] as HTMLLIElement), list);
			}
			if (key == 13) {
				// this.currentItem = list.children()[this.countArrow];
				// timezone.__clickEachItem(list.children()[this.countArrow]);
			}
			else if (key == 8 || key != 40 && key != 38 && key != 13) {
				// list.scrollTop(0);
				// this.countArrow = 0;
			}
		}
		protected callArrowIndex(list: JQuery, index: number) {
			Array.prototype.map.call(list.children(), (element) => {
				if (element.classList.contains("selected")) {
					element.classList.remove('selected')
				}
			});
			this.countArrow = index;
			list.children()[this.countArrow].classList.add("selected")
		}

		protected findElementReplaceInputValue(element: HTMLElement, connectToIndex) {
			let findArr: {}[] = [], childItem: Object = null;
			Array.prototype.map.call(element.children[0].children[0].children, (element: HTMLElement) => {
				findArr.push(element)
				childItem = findArr.find((el: HTMLElement) => {
					return el.classList.contains("ml-blueprint-item-name")
				})
			})
			ml.$('*[data-ml-id="' + connectToIndex + '"] .option').text((childItem as HTMLElement).innerText);
		}
		protected clickEachItem(list: JQuery) {
			if (list.children().length > 0) {
				Array.prototype.map.call(list.children(), (el: HTMLElement, index: number) => {
					el.addEventListener("click", (e: JQueryEventObject) => {
						let parentSelected = this.getParents(document.querySelector('.showList'), document.querySelector('body'), "ml-blueprint-select-portal"),
							findInput = this.findJSFunction("ml-blueprint-select-filter", parentSelected);
						if (findInput != null && (findInput as HTMLInputElement).value != "") {
							this.countArrow = Number(el.getAttribute("data-index"));
						}
						el.classList.add('selected');
						this.resetOnSelect(list, el, index);
						this.closeOnSelect(list, this.countArrow);
						if (this.resetBoolean && this.closeOnSelectBoolean) {
							ml.$(this.input).blur()
						}
						// Each click Item take currect position of DataList
						list = ml.$(`#${this.corectUL} .ml-blueprint-select-menu`);
						let DataListParentOverlay = this.getCurrentToggleList(list[0], `#${this.corectUL}`, "ml-blueprint-select-overlay");
						this.DataListPosition(DataListParentOverlay, ml.$('*[data-ml-id="' + this.corectUL + '"]')[0].parentNode)
						// ________________________________________
						this.scrollToAnchor((this.list.children()[this.countArrow] as HTMLLIElement), this.list);
					})
				})
			}
		}
		protected resetOnSelect(list: JQuery, el: HTMLElement, index: number) {
			let parent = this.getParents(el, document.querySelector('body'), "ml-blueprint-select-portal");

			this.findElementReplaceInputValue(el, parent.getAttribute("id"));
			if (!this.resetBoolean) {
				this.callArrowIndex(list, index)
			}
			else {
				if ((this.input[0] as HTMLInputElement).value != "") {
					(this.input[0] as HTMLInputElement).value = "";
					ml.$(list).scrollTop(0);
					setTimeout(() => {
						list.children().detach();
						this.listItems();
						this.clickEachItem(list);
						this.callArrowIndex(list, 0)
					}, 0);
				}
				else {
					(this.input[0] as HTMLInputElement).value = "";
					ml.$(list).scrollTop(0);
					this.callArrowIndex(list, index);
				}
			}
		}
		protected closeOnSelect(list: JQuery, index: number) {
			if (this.closeOnSelectBoolean) {
				if (this.keyUpBoolean) {
					ml.$(list).scrollTop(0);
					setTimeout(() => {
						list.children().detach();
						this.listItems();
						this.clickEachItem(list);
						this.callArrowIndex(list, 0)
					}, 0)
				}
				else {
					this.callArrowIndex(list, index);
				}
				(this.input[0] as HTMLInputElement).value = this.input[0].getAttribute("placeholder");
				ml.$(this.input).blur();
				this.keyUpBoolean = false;
			}
		}
		private DataListPosition(DataListParentOverlay, element) {
			setTimeout(() => {
				let parentTransferDatList = this.getParents(DataListParentOverlay, document.querySelector("body"), "ml-blueprint-select-portal"),
					buttonPosition = element.getBoundingClientRect(),
					parentTransferDatListPosition = parentTransferDatList.getBoundingClientRect(),
					top = 0,
					left = 0,
					triangleLeft = 0;

				// When DataList pass the window width limit
				if ((buttonPosition.left + parentTransferDatListPosition.width) > window.innerWidth) {
					left = buttonPosition.left - ((buttonPosition.left + parentTransferDatListPosition.width) - window.innerWidth) - 25;
					parentTransferDatList.classList.add("triangleTransfer");
					triangleLeft = buttonPosition.left - left + buttonPosition.width / 2 - 20;
				}
				else {
					parentTransferDatList.classList.remove("triangleTransfer");
					left = buttonPosition.left;
					triangleLeft = buttonPosition.width / 2 - 20;
				}
				// ________________________________

				// When DataList pass the window height limit
				if (buttonPosition.top + buttonPosition.height + (parentTransferDatListPosition.height) > window.innerHeight) {
					top = buttonPosition.top - parentTransferDatListPosition.height - 15;
					parentTransferDatList.classList.add("triangleBottom");
				}
				else {
					top = buttonPosition.top + buttonPosition.height;
					parentTransferDatList.classList.remove("triangleBottom");
				}
				// __________________________

				document.querySelector("body").style.setProperty('--triangelAfter', String(triangleLeft) + "px");
				parentTransferDatList.style.left = left + "px";
				parentTransferDatList.style.top = top + "px";
				ml.$(`#${this.corectUL} .ml-blueprint-select-filter`).focus();
			}, 0)
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			this.initTransforms()

			if (this.isDOMLoaded) {

				this.watchDefinitionOptions((options: BPSelect.IOptions) => {

					var Datalist = this.list;
					ml.$(this.select).children('div').html("");
					ml.$(this.button).text("");
					ml.$(this.overlay).html('');
					ml.$(this.list).html("")

					ml.$(this.button).append(`<i></i>`)

					this.items = []
					let optionText: string;
					if (options.items) {
						this.items = options.items.map(m => {
							return {
								value: m[options.valueField || "value"],
								label: m[options.labelField || "label"]
							}
						});
						var fil = this.items.filter((el, ind) => {
							return (el.label != "") && (el.label != undefined)
						})
						this.items = fil
						if (this.items.length > 0) {
							this.items.unshift({ value: "", label: "" });
						}

						if (this.items.length > 2) {
							if (options.sortFunction) {
								this.items = this.items.sort(options.sortFunction);
							}
							else {
								this.items = this.items.sort((a: any, b: any) => {
									const labelA = a.label.toLowerCase();
									const labelB = b.label.toLowerCase();
									if (labelA < labelB)
										return -1;
									if (labelA > labelB)
										return 1;
									return 0;
								});
							}
						}
					}

					if (this.items[0] != undefined) {
						optionText = this.items[0].label,
							ml.$(this.button).append(`<span class='option'>${optionText}</span>`);
						// obs(this.button.text())
					}
					else {
						ml.$(this.button).append(`<span class='option'></span>`);
					}

					ml.$(this.button).append(`<i class="mlicon-MapLarge-icons_layer-expanded"></i>`)
					ml.$(this.portal).addClass('ml-blueprint-select-portal')
					this.overlay = ml.$(this.portal).children('div').addClass('ml-blueprint-select-overlay')
					ml.$(this.select).children('div').append(this.button)
					ml.$(this.select).children('div').addClass('button')
					ml.$(this.button).addClass('ml-blueprint-select-button');

					this.menu = ml.$(`<div></div>`).appendTo(this.overlay)
					this.menu.addClass('ml-blueprint-popover-content')

					let inputGroup: JQuery = ml.$('<div></div>')
					this.input = ml.$(`<input class='ml-blueprint-select-filter'/>`)
					inputGroup.hide()
					inputGroup.addClass('ml-blueprint-select-input-group')
					this.menu.append(inputGroup)
					inputGroup.append('<i class="mlicon-x16Search"></i>')
					inputGroup.append(this.input)


					this.list.addClass('ml-blueprint-select-menu')
					this.menu.append(this.list)

					this.listItems = (): void => {
						if (options.items && this.items.length > 1) {
							ml.$.each(this.items, function (key: number, value: any) {
								Datalist.append(`<li data-index=${key}><a class='ml-blueprint-select-menu-item'><span class='ml-blueprint-item-left'><span class='ml-blueprint-item-name'>${value.label}</span></span> </a></li>`);
							}.bind(this));
							this.callArrowIndex(Datalist, this.countArrow);
						}
						else {
							Datalist.append(`
								<li class="ml-blueprint-select-disabled">No result.</li>
							`)
						}
					}

					ml.$(this.input).on("keyup", (e) => {
						let lengthInputValue;
						ml.$(Datalist).children().detach();
						let key: number = e.which || e.keyCode;
						let valueInputFilter: string = (<any>e.target).value.replace(/\\/g, "\\\\").replace(/\./g, '\\.').toLowerCase(),
							indexNumeric: number = 0;
						// Input value length whit dot 
						lengthInputValue = (/\./g.exec(valueInputFilter)) ? valueInputFilter.length - 1 : valueInputFilter.length;
						// _________________________
						if (!/[!$%^&*|~=`{}\[\]";<>?]/g.test(valueInputFilter)) {
							if (ml.$(Datalist).scrollTop() != 0 && (<any>e.target).value != "") {
								ml.$(Datalist).scrollTop(0);
							}
							let writeMatchString: string = "";

							Array.prototype.map.call(this.items, (elm: { label: string, value: string }, indexObj: string) => {
								let BoldTxt: string = "", replaceBold: string = "";
								let multiplyValue: string[] = ['label'];
								indexNumeric++;
								for (let i = 0; i < multiplyValue.length; i++) {
									if (i <= 0) {
										writeMatchString = elm[multiplyValue[i]]
									}
									if (writeMatchString.toString().toLowerCase().match(valueInputFilter) != null) {
										// console.log(valueInputFilter)
										for (let j = writeMatchString.toString().toLowerCase().match(valueInputFilter).index; j < writeMatchString.toString().toLowerCase().match(valueInputFilter).index + lengthInputValue; j++) {
											// console.log(writeMatchString.toString().toLowerCase().match(valueInputFilter).index, valueInputFilter.length)
											if (multiplyValue[i] == "label") {
												BoldTxt += elm.label[j];
											}
										}
										replaceBold = elm.label.replace(BoldTxt, `<b>${BoldTxt}</b>`);
									}
									if (new RegExp(valueInputFilter).test(writeMatchString.toString().toLowerCase())) {
										if (valueInputFilter == "") {
											ml.$(Datalist.children()[indexObj]).detach();
											replaceBold = elm.label;
										}
										return Datalist.append(`<li  data-index=${indexObj}><a class='ml-blueprint-select-menu-item '><span class='ml-blueprint-item-left'> <span class='ml-blueprint-item-name '>${replaceBold}</span></span></a></li>`)
									}
								}
							})

							if (valueInputFilter != "" || (valueInputFilter == "" && ml.$(Datalist).children().length) > 0) {
								// ml.$(ml.$(list).children()[this.countArrow]).addClass('selected');
							}
							if (Datalist.children().length == 0) {
								Datalist.append(`
									<li class="ml-blueprint-select-disabled">No result.</li>
								`);
							}
						}
						else {
							if (Datalist.children().length == 0) {
								Datalist.append(`
									<li class="ml-blueprint-select-disabled">No result.</li>
								`);
							}
						}
						if (!Datalist.children()[0].classList.contains("ml-blueprint-select-disabled")) {
							if (Datalist.children().length == 1 || key != 40 && key != 38 && key !== 13 && key != 37 && key != 39 && key != 27) {
								this.callArrowIndex(Datalist, 0);
							}
							if (Datalist.children().length > 1) {
								this.callArrowIndex(Datalist, this.countArrow);
								if (key == 38 || key == 40) {
									Array.prototype.map.call(Datalist.children(), (element: HTMLElement): void => {
										ml.$(element).removeClass("selected");
									})
								}
								if (key == 40) {
									ml.$(Datalist.children()[this.countArrow]).addClass("selected");
									this.scrollToAnchor((Datalist.children()[this.countArrow] as HTMLLIElement), Datalist);
								}

								if (key == 38) {
									ml.$(Datalist.children()[this.countArrow]).addClass("selected");
									this.scrollToAnchor((Datalist.children()[this.countArrow] as HTMLLIElement), Datalist);
								}
							}
							if (key == 13) {
								document.querySelectorAll(".ml-blueprint-select-overlay").forEach(element => {
									(element as HTMLElement).classList.remove("showList");
								});
								document.querySelectorAll('.openButton').forEach((elem: HTMLElement) => {
									elem.classList.remove("openButton");
								})
								this.resetOnSelect(Datalist, Datalist.children()[this.countArrow], this.countArrow);
								this.closeOnSelect(Datalist, this.countArrow);
								obs(this.button.text())
								// this.scrollToAnchor((Datalist.children()[this.countArrow] as HTMLLIElement), Datalist);
							}
						}
						// Each click Item take currect position of DataList
						Datalist = ml.$(`#${this.corectUL} .ml-blueprint-select-menu`);
						let DataListParentOverlay = this.getCurrentToggleList(Datalist[0], `#${this.corectUL}`, "ml-blueprint-select-overlay");
						this.DataListPosition(DataListParentOverlay, ml.$('*[data-ml-id="' + this.corectUL + '"]')[0].parentNode)
						// ________________________________________
						if ((<any>e.target).value != "") {
							this.keyUpBoolean = true;
						}
						this.clickEachItem(Datalist)
					})
					ml.$(this.input).on("keydown", (e: JQueryEventObject) => {
						let key: number = e.which || e.keyCode;
						if (key == 27) {
							document.querySelectorAll(".ml-blueprint-select-overlay").forEach(element => {
								(element as HTMLElement).classList.remove("showList");
							});
							document.querySelectorAll('.ml-blueprint-select-button').forEach((elem: HTMLElement) => {
								elem.classList.remove("openButton")
							})

							Array.prototype.map.call(Datalist.children(), (elem, index) => {
								if (elem.classList.contains("selected")) {
									this.indexEsc = index;
								}
							})
							this.countArrow = this.indexEsc;
							this.input.blur()
						}
						if (Datalist.children().length != 1) {
							this.upDownFunc(Datalist, <any>e);
						}
					})
					ml.$(this.button).on("click", () => {
						if (ml.$('*[data-ml-id="' + this.corectUL + '"] .option').text() == "") {
							this.countArrow = this.indexEsc;
						}
						if (this.input.val() == '' || this.closeOnSelectBoolean) {
							ml.$(Datalist).children().detach();
							this.listItems();
							this.input.val('');
						}
						setTimeout(() => {
							this.scrollToAnchor((Datalist.children()[this.countArrow] as HTMLLIElement), Datalist);
						}, 0)
						this.clickEachItem(Datalist);
						this.callArrowIndex(Datalist, this.countArrow);
					})
					document.querySelectorAll('.ml-blueprint-select-button').forEach((element: HTMLElement, index) => {
						element.onclick = () => {
							// Connect to Clickable button of this Attribute
							this.corectUL = ml.$(element).attr('data-ml-id');
							Datalist = ml.$(`#${this.corectUL} .ml-blueprint-select-menu`);
							// _________________________________

							let DataListParentOverlay = this.getCurrentToggleList(Datalist[0], `#${this.corectUL}`, "ml-blueprint-select-overlay");
							setTimeout(() => {

								if (element.classList.contains("openButton")) {
									DataListParentOverlay.classList.remove("showList");
									element.classList.remove("openButton")
								}
								else {
									document.querySelectorAll('.openButton').forEach((elem: HTMLElement) => {
										elem.classList.remove("openButton")
									})
									element.classList.add("openButton")
									document.querySelectorAll(".ml-blueprint-select-overlay").forEach(element => {
										(element as HTMLElement).classList.remove("showList");
									});
									DataListParentOverlay.classList.add("showList");
								}
							}, 0)
							this.DataListPosition(DataListParentOverlay, element);
						}
					})

					const intents: string[] = ['primary', 'success', 'warning', 'danger', 'default'];
					intents.forEach(intent => {
						this.input.removeClass(`ml-blueprint-select-focus-${intent}`);
						ml.$(Datalist).removeClass(`ml-blueprint-select-${intent}`);
					})

					this.resetBoolean = (options.resetOnSelect) ? true : false;
					this.closeOnSelectBoolean = (options.resetOnClose) ? true : false;

					if (options.disabled) {
						this.button.addClass('ml-blueprint-select-button-disabled');
						ml.$(this.button).attr('disabled');
					}
					else {
						this.button.removeClass('ml-blueprint-select-button-disabled');
						ml.$(this.button).removeAttr('disabled');
					}

					if (options.placeholder) {
						this.input.attr('placeholder', options.placeholder);
					}
					else {
						this.input.removeAttr('placeholder');
					}

					if (options.minimal) {
						ml.$(this.menu).addClass('ml-blueprint-select-minimal');
					}
					else {
						ml.$(this.menu).removeClass('ml-blueprint-select-minimal');
					}

					if (options.filterable) {
						inputGroup.show();
					}
					else {
						inputGroup.hide();
					}

					if (options.intent) {
						this.input.addClass(`ml-blueprint-select-focus-${options.intent} `);
						ml.$(Datalist).addClass(`ml-blueprint-select-${options.intent}`);
						ml.$(this.button).children('i:first').removeClass();
						ml.$(this.button).children('i:first').addClass(`mlicon-${options.icon}`);
					}

					if (!options.icon) {
						ml.$("i", this.button).first().detach();
					}

					ml.$(document).on('click', (event: JQueryEventObject): void => {
						obs(this.button.text());
						if (!event.srcElement.closest('.ml-blueprint-select-input-group') && !event.srcElement.closest('.openButton')) {
							if (ml.$(`#${this.corectUL} .ml-blueprint-select-menu`).children().length > 0) {
								document.querySelectorAll(".ml-blueprint-select-overlay").forEach(element => {
									if (element.classList.contains("showList")) {
										(element as HTMLElement).classList.remove("showList");
									}
								});
								let parentSelected = this.getParents(Datalist[0], document.querySelector('body'), "ml-blueprint-select-portal"),
									findInput = this.findJSFunction("ml-blueprint-select-filter", parentSelected);
								document.querySelectorAll('.ml-blueprint-select .openButton').forEach((elem: HTMLElement) => {
									elem.classList.remove("openButton");
								})
								if (findInput != null && (findInput as HTMLInputElement).value != "") {
									this.countArrow = 0;
								}
								this.callArrowIndex(ml.$(`#${this.corectUL} .ml-blueprint-select-menu`), this.countArrow);
							}
						}
					})

					this.obsNewValue = (newValue) => {
						setTimeout(() => {
							Datalist.children().detach()
							this.listItems()
							ml.$(`#${this.corectUL} .ml-blueprint-select-menu li`).map((id, value) => {
								ml.$(value).removeClass('selected');
								if ((value as HTMLElement).innerText.trim().toLowerCase() == newValue.toLowerCase()) {
									this.scrollToAnchor((ml.$(`#${this.corectUL} .ml-blueprint-select-menu li`)[this.countArrow] as HTMLLIElement), ml.$(`#${this.corectUL} .ml-blueprint-select-menu`));
									ml.$(value).addClass('selected')
									ml.$(this.contentDiv).on('click', () => {
										this.scrollToAnchor((ml.$(`#${this.corectUL} .ml-blueprint-select-menu li`)[this.countArrow] as HTMLLIElement), ml.$(`#${this.corectUL} .ml-blueprint-select-menu`));
									})
									this.text = ml.$(".option", this.button)[0].innerText = (value as HTMLElement).innerText.trim()
								}
							})
							this.input.trigger('keyup')
						}, 0);
					}
					this.obsNewValue(obs())
					this.objectsToDispose.push(
						obs.subscribe((newValue) => {
							if (obs() == null || (ml.$('*[data-ml-id="' + this.corectUL + '"]').text().toLowerCase() != newValue.toLowerCase())) {
								this.obsNewValue(newValue)
								setTimeout(() => {
									obs(this.text)
								}, 0);
							}
						})
					);


				});



			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPSelect.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPSelect {

		/**
		 * Interface with props for Select
		 * @typedef ISelectProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface ISelectProps extends BPBase.IControlProps {
			/**
			 * Array of items in a select component
			 * @typedef items[]
			 * @param {string} label - Text of the select item
			 * @param {any} value - Value of the select item
			 */
			items?: any[];
			/**
			 * Intent color of the selected options
			 * @type {ctrl.BPBase.Intent}
			 */
			intent?: ctrl.BPBase.Intent;
			/**
			 * Enable the search bar for Select
			 * @type {boolean}
			 */
			filterable?: boolean;
			/**
			 * Reset the select state when it's closed
			 * @type {boolean}
			 */
			resetOnClose?: boolean;
			/**
			 * Reset the select state when an item is selected
			 * @type {boolean}
			 */
			resetOnSelect?: boolean;
			/**
			 * Icon class for the dropdown
			 * @type {string}
			 */
			icon?: string;
			/**
			 * The name of the value field in the array of items passed to the Select
			 * @type {string}
			 */
			valueField?: string,
			/**
			 * The name of the label field in the array of items passed to the Select
			 * @type {string}
			 */
			labelField?: string,
			/**
			 * Custom sorting function for the data inside the Select
			 * @function
			 * @param {any} a - First item for comparison
			 * @param {any} b - Second item for comparison 
			 * @return {number}
			 */
			sortFunction?: (a: any, b: any) => number
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ISelectProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			items?: any[] | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
			icon?: string | mdash.IScopePath,
			minimal?: boolean | mdash.IScopePath,
			intent?: ctrl.BPBase.Intent | mdash.IScopePath,
			filterable?: boolean | mdash.IScopePath,
			resetOnClose?: boolean | mdash.IScopePath,
			resetOnSelect?: boolean | mdash.IScopePath,
			placeholder?: string | mdash.IScopePath,
			valueField?: string | mdash.IScopePath,
			labelField?: string | mdash.IScopePath,
			sortFunction?: (a: any, b: any) => number
		}
	}

	mdash.registerControl(BPSelect, 'dashboard.components.controls', BPSelect.meta);
}