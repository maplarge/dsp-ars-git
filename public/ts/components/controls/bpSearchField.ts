namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPSearchField extends mdash.BaseControl<BPSearchField.IOptionsDefinition> {
		public static type: string = 'BPSearchField';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.DEPRECATED,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Search Field',
			shortDescription: "The search box component is where your users type their search queries.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPSearchField.builder().options(BPSearchField.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPSearchField.IOptions> = {
			'BPSearchField': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
        }

		protected textInputDiv: HTMLElement;
        protected textInput: HTMLElement;
        protected search: JQuery;
        


		constructor(id: string, definition: mdash.IControlDefinition<BPSearchField.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
            super.build();
			this.container.classList.add('namespace-blueprint-textInput');
			this.textInputDiv = ml.create("div", "ml-BPSearchField", this.contentDiv[0]);
			this.textInputDiv.classList.add('ml-blueprint-textInput');
			this.textInput = ml.create("input", "ml-BPSearchField", this.contentDiv[0]);
			this.textInput.classList.add('ml-blueprint-input');
			this.textInput.setAttribute('type', `${this.definition.options.type}`);		
            this.textInputDiv.appendChild(this.textInput);            
            }

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, null, false);

				this.textInput.onfocus = () => {
					this.textInputDiv.classList.add('ml-blueprint-textInputBoxShadow');
				};
				this.textInput.onblur = () => {
					this.textInputDiv.classList.remove('ml-blueprint-textInputBoxShadow');					
				}
			
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPSearchField.IOptions) => {
                    if(options.disabled)
                    {
                        this.textInput.setAttribute('disabled', 'true');
						this.textInput.classList.add('ml-blueprint-textInput-disabled');
						this.textInputDiv.classList.add('ml-blueprint-textInput-disabled')						
                    }
                    if(options.fill)
                    {
						this.textInputDiv.classList.add('ml-blueprint-textInput-fill')						                        
                    }
                    if(options.large) {
						this.textInputDiv.classList.add('ml-blueprint-textInput-large')
                    }
                    if(options.intent)
                    {
						this.textInputDiv.classList.add(`ml-blueprint-textInput-${options.intent}`);
					}
					if(options.readonly)
                    {
						this.textInput.setAttribute('readonly', 'true');						
					}				
					if(options.iconLeft)
					{
						this.textInput.insertAdjacentHTML('beforebegin',`<i class="ml-blueprint-textinput-left-icon ml-icon-${options.iconLeft}"></i>`);						
					}
					if(options.iconRight)
					{
						this.textInput.insertAdjacentHTML('afterend',`<button class="ml-blueprint-button"><i class="ml-blueprint-textinput-right-icon ml-icon-${options.iconRight}"></i></button>`);
					}
					if(options.placeholder)
					{
                        this.textInput.setAttribute('placeholder', options.placeholder);
					}
					if(options.round) 
					{
						this.textInputDiv.classList.add('ml-blueprint-textInput-round')
					}
				});
			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPSearchField.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPSearchField {

		/**
		 * Interface with props for a search input
		 * @typedef ISearchField
		 * @interface
		 * @readonly
		 * @augments ctrl.BPBase.IControlProps
		 */
		export interface ISearchField extends ctrl.BPBase.IControlProps{
			/**
			 * Disable the input
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Intent color of the search input border
			 * @type {ctrl.BPBase.Intent}
			 */
			intent?: ctrl.BPBase.Intent,
			/**
			 * Input type
			 * @type {string}
			 */
			type?: string,
			/**
			 * Make the input read-only
			 * @type {boolean}
			 */
			readonly?: boolean
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ISearchField{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            disabled?: boolean | mdash.IScopePath,
            intent?: ctrl.BPBase.Intent | mdash.IScopePath,
            large?: boolean | mdash.IScopePath,
			fill?: boolean | mdash.IScopePath,
			readonly?: boolean | mdash.IScopePath,
			iconLeft?: string | mdash.IScopePath,
			iconRight?: string | mdash.IScopePath,
			placeholder?: string | mdash.IScopePath,	
			type?: string | mdash.IScopePath,
			round?: boolean | mdash.IScopePath,
		}
	}

	mdash.registerControl(BPSearchField,'dashboard.components.controls', BPSearchField.meta);
}