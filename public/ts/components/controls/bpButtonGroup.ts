namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;
	
	export namespace BPButtonGroup {

		/**
		 * @typedef IButtonGroup
		 * @interface
		 * @readonly
		 * @augments BPBase.IAppearance
		 * @augments BPBase.IActionProps
		 */
		export interface IButtonGroup extends BPBase.IAppearance, BPBase.IActionProps{
		}

		export interface IOptionsDefinition{
			btnData: BPButton.IOptionsDefinition[] | mdash.IScopePath,
			large?: boolean | mdash.IScopePath,
			minimal?: boolean | mdash.IScopePath,
			small?: boolean | mdash.IScopePath,
            vertical?: boolean | mdash.IScopePath,
            iconsOnly?: boolean | mdash.IScopePath,
            alignText?: BPBase.Alignment | mdash.IScopePath
		}

		/**
		 * Interface for button group's 1 property
		 * @interface 
		 * @typedef IOptions
		 * @readonly
		 * @augments IButtonGroup
		 */
		export interface IOptions extends IButtonGroup{
			/**
			 * Object with button properties for rendering them as a group
			 * @type {BPButton.IOptions[]}
			 */
			btnData: BPButton.IOptions[]
		}
	}

	export class BPButtonGroup extends mdash.controls.CompositeControl<BPButtonGroup.IOptionsDefinition, BPButtonGroup.IOptions> {
		public static type: string = 'BPButtonGroup';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.INTERNAL,
			friendlyName: 'Button Group',
			shortDescription: "Creates a list of buttons either inline or vertically.",
			categories: ['Input'],
			childSupport: mdash.ChildSupport.SINGLE
		};

		constructor(id: string, definition: mdash.IControlDefinition<BPButtonGroup.IOptionsDefinition>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		/* Init is still an important function here, but because of the limited 
		 * functionality of this control specifically, we're not doing more than calling the parent  */
		protected init() {
			super.init();
		}

		/* This function is what largely separates a composite control from others.
		 * This is the function that defines the structure of the control. If you want it to show up in the DOM, it needs to go here.*/
		protected getControlLayout(options: BPButtonGroup.IOptions): mdash.IControlDefinition<any> { 
			return mdash.controls.Flexbox.builder()
				.options({
					justify: "Start",
				}).children([
					mdash.controls.Repeater.builder().options({
						data: { $scopePath: "options.btnData" },
						layout: options.vertical ? 'Vertical' : 'Horizontal'
					}).children([
							BPButton.builder().options({
								label: { $scopePath: 'item.label' },
								intent: { $scopePath: 'item.intent' },
								icon: { $scopePath: 'item.icon' },
								large: { $scopePath: "options.large" },
								minimal: { $scopePath: "options.minimal" },
								small: { $scopePath: 'options.small' },
								iconsOnly: { $scopePath: 'options.iconsOnly' },
								onClick: <any>{ $scopePath: 'item.onClick' },
								active: { $scopePath: 'item.active' },
								disabled: { $scopePath: 'item.disabled' },
								loading: { $scopePath: 'item.loading' }
							}),
						])
			]).toJSON(); 
		}

		protected cleanup() {
			super.cleanup();
		}

		protected validateDefinition(): boolean {
			return super.validateDefinition();
		}	

		public static builder() {
			return new mdash.ControlDefinitionBuilder<BPButtonGroup.IOptionsDefinition>(this.type);
		}
	}
	mdash.registerControl(BPButtonGroup, 'dashboard.components.controls', BPButtonGroup.meta);
}  