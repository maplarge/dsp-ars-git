/// <reference path="./bpSwitch.ts" />

namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export namespace BPFormGroup {
		export interface IOptionsDefinition extends BPSwitch.IOptionsDefinition { }
		export interface IOptions extends BPSwitch.IOptions { }
	}

	export class BPFormGroup <T extends BPFormGroup.IOptionsDefinition> extends BPSwitch<BPFormGroup.IOptionsDefinition> {
		public static type: string = 'BPFormGroup';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Form Group',
			shortDescription: "The FormGroup component wraps a form control with proper spacing, along with support for a label, help text, and validation state.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPFormGroup.builder().options(BPFormGroup.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPFormGroup.IOptions> = {
			'BPFormGroup': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected label: HTMLElement;
		protected input: HTMLElement;
		protected formGroup: HTMLElement;
		protected switchGroup: HTMLElement;
		protected helperText: HTMLElement;
		protected labelInfo: HTMLElement;
		protected form: HTMLElement;
		protected switch: HTMLElement;

		constructor(id: string, definition: mdash.IControlDefinition<BPFormGroup.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
			this.container.classList.remove('namespace-blueprint-checkbox')
			let classes  = ['namespace-blueprint-label-input', 'namespace-blueprint-FormGroup'];
			this.container.classList.add(...classes);
			this.formGroup = ml.create("div", "ml-BPLabel",  this.contentDiv[0]);
			this.formGroup.classList.add('ml-blueprint-formGroup');
			this.switchGroup = ml.create("div", "ml-BPLabel",  this.contentDiv[0]);
			this.switchGroup.classList.add('ml-blueprint-switchGroup');
			this.form = ml.create("div", "ml-BPLabel",  this.formGroup);
			this.switch = ml.create("div", "ml-BPLabel",  this.switchGroup);
			this.form.classList.add('ml-blueprint-form-content');
			this.switch.classList.add('ml-blueprint-switch-content');
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);

			if (this.isDOMLoaded) {
				this.text.parentElement.removeChild(this.text);
				this.contentDiv[0].querySelector('.ml-blueprint-checkbox-label').parentElement.removeChild(this.contentDiv[0].querySelector('.ml-blueprint-checkbox-label'));
				this.input = ml.create("input", "ml-BPLabel",  this.form);

				
				this.watchDefinitionOptions((options: BPFormGroup.IOptions) => {
					
					const intents: string[] = ['primary','success','warning','danger', 'default'];
						  intents.forEach(intent => {
							this.input.classList.remove(`ml-blueprint-input-intent-${intent}`);						
						  }) 
						ml.$('.ml-blueprint-label',this.switchGroup).detach()
						ml.$('.ml-blueprint-label',this.formGroup).detach()
						ml.$('.ml-blueprint-helper-text',this.switchGroup).detach()
						ml.$('.ml-blueprint-helper-text',this.formGroup).detach()
						ml.$('.ml-blueprint-switch-parent').detach()
					
				        let label: Array<HTMLElement> = [],
						helperText: Array<HTMLElement> = [],
						labelInfo: Array<HTMLElement> = [],
						switchParent: HTMLElement;
					for(var i = 0; i < 2; i++) {
						this.label = document.createElement('label');
						this.label.classList.add('ml-blueprint-label');
						this.label.innerHTML = `${options.showLabelText}`;	
						label.push(this.label);
						this.helperText = document.createElement('div');
						this.helperText.classList.add('ml-blueprint-helper-text')
						this.helperText.classList.add(`ml-blueprint-text-${options.intent}`)
						this.helperText.innerHTML = `${options.helperText}`;
						helperText.push(this.helperText);	
						this.labelInfo = document.createElement('span');
						this.labelInfo.classList.add('ml-blueprint-label-info');
						this.labelInfo.innerHTML = `${options.showLabelInfoText}`;						
						labelInfo.push(this.labelInfo);
					}
					 
					if(options.showLabel == true) {
						this.switchGroup.insertAdjacentElement('afterbegin', label[1]);
						this.formGroup.insertAdjacentElement('afterbegin', label[0]);

						if(options.showLabelInfo == true) {
							label[0].appendChild(labelInfo[0])
							label[1].appendChild(labelInfo[1])
						}
					}	
					
					this.input.classList.add('ml-blueprint-label-input');
					this.input.classList.add(`ml-blueprint-input-intent-${options.intent}`);
					this.input.setAttribute('placeholder','Placeholder text')
					switchParent = ml.create("div", "ml-BPLabel",  this.switch);
					switchParent.classList.add('ml-blueprint-switch-parent')
					switchParent.appendChild(this.node);

					if(options.switchTextShow == true) {
						switchParent.innerHTML += options.switchText;
					}

					if(options.helperShow == true) {
						this.form.appendChild(helperText[0]);
						this.switch.appendChild(helperText[1])
					}

					if (options.disabled) {
						this.input.setAttribute('disabled', 'disabled');
						this.formGroup.classList.add('ml-blueprint-disabled');
						this.switchGroup.classList.add('ml-blueprint-disabled');
					} else {
						this.input.removeAttribute('disabled');
						this.formGroup.classList.remove('ml-blueprint-disabled')
						this.switchGroup.classList.remove('ml-blueprint-disabled');
					}
					
					if(options.inline) {
						this.formGroup.classList.add('ml-blueprint-inline');
						this.switchGroup.classList.add('ml-blueprint-inline');						
					} else {
						this.formGroup.classList.remove('ml-blueprint-inline');
						this.switchGroup.classList.remove('ml-blueprint-inline');						
					}
				});
			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPFormGroup.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPFormGroup {

		/**
		 * Interface for displaying the form with helper text
		 * @typedef IFormShow
		 * @interface
		 * @readonly
		 */
		export interface IFormShow {
			/**
			 * Show or hide the helper text
			 * @type {boolean}
			 */
			show: boolean;
			/**
			 * The helper text
			 * @type {string}
			 */
			text?: string;
		}

		/**
		 * Interface to work with IFormGroup
		 * @typedef IFormGroup
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface IFormGroup extends BPBase.IControlProps{
			/**
			 * Show or hide the helper text
			 * @type {boolean}
			 */
			helperShow?: boolean,
			/**
			 * The helper text
			 * @type {string}
			 */
			helperText?: string,
			/**
			 * Show or hide the label
			 * @type {boolean}
			 */
			showLabel?: boolean,
			/**
			 * Label text
			 * @type {string}
			 */
			showLabelText?: string,
			/**
			 * Show or hide label info
			 * @type {boolean}
			 */
			showLabelInfo?: boolean,
			/**
			 * Label info text
			 * @type {string}
			 */
			showLabelInfoText?: string,
			/**
			 * Show or hide the switch text
			 * @type {boolean}
			 */
			switchTextShow?: boolean,
			/**
			 * Switch text
			 * @type {string}
			 */
			switchText?: string,
			/**
			 * Intent of the form group
			 * @type {ctrl.BPBase.Intent}
			 */
			intent?: ctrl.BPBase.Intent,
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IFormGroup{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			label?: string | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
			inline?: boolean | mdash.IScopePath,
			intent?: ctrl.BPBase.Intent | mdash.IScopePath,
			showLabel?: boolean | mdash.IScopePath,
			showLabelText?: string | mdash.IScopePath,
			helperShow?: boolean | mdash.IScopePath,
			helperText?: string | mdash.IScopePath,
			showLabelInfo?: boolean | mdash.IScopePath,
			showLabelInfoText?: string | mdash.IScopePath,
			switchTextShow?: boolean | mdash.IScopePath,
			switchText?: string | mdash.IScopePath,

		}
	}

	mdash.registerControl(BPFormGroup,'dashboard.components.controls', BPFormGroup.meta);
}