namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPSuggest extends mdash.BaseInputControl<BPSuggest.IOptionsDefinition> {
		public static type: string = 'BPSuggest';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Suggest',
			shortDescription: "The SuggestComponent in Solr provides users with automatic suggestions for query terms",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.SINGLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPSuggest.builder().options(BPSuggest.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPSuggest.IOptions> = {
			'BPSuggest': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label:'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label:'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		private suggest: JQuery;
		private portal: JQuery;
		private input: JQuery; 
		private menu: JQuery;
		protected obsNewValue: Function;
		private countArrow:number = 0;
		protected resetBoolean = false;
		protected overlay:JQuery;
		protected closeOnSelectBoolean = false;
		protected clickInputOpen = true;
		protected listItems:Function;
		protected red:Readonly<any>;
		protected keyUpBoolean:boolean  = false;
		protected items:{label:string,value:string}[];
		protected list:JQuery;
		protected indexEsc:number = 0;
		protected corectUL: string = null;
		protected countMLID: string = String(ml.id());
		private placeHolderSuggest:string="";
		private opwnListOnlyKeyDown:boolean = false;

		constructor(id: string, definition: mdash.IControlDefinition<BPSuggest.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}
		public build() {
			super.build();
			
			this.container.classList.add('namespace-blueprint-suggest');	
			this.suggest =  ml.$(`<div></div>`)
			this.portal = ml.$(`<div id="ml-suggest-overlay-${this.countMLID}"></div>`)
			ml.$(this.portal).addClass(`ml-blueprint-suggest-portal`)
			this.contentDiv.append(this.suggest);
			document.querySelector("body").appendChild(this.portal[0]);
			this.input = ml.$(`<input data-mlSuggest-id="ml-suggest-overlay-${this.countMLID}" class="suggest-input" type='text'/>`);
			this.list = ml.$('<ul class="ml-blueprint-suggest-menu"></ul>')
		}

		protected validateDefinition(): boolean {
			
			if (!super.validateDefinition())
			return false;
			
			return true; 

		}

		//  each button click get him Toggle data list 
		protected getCurrentToggleList(eventCurrent/* current element */, untilParentClass/* current element's until parent */, toggleListClass /* find this classlistElement  */){
			let parent, child, classNameUntilParent = document.querySelector(untilParentClass).className.split(" ")[0];
			parent = this.getParents(eventCurrent, document.querySelector(untilParentClass), classNameUntilParent);
			child = this.findJSFunction(toggleListClass, parent);
			return child;
		}
		// ___________________________

		// find any element by class, id and tagName into the Main elemnt
		protected findJSFunction(selector, parentElement){
			var empChild = [], manyElement = [], result = null;
			function callElement(pseudoSelector, parentSelector){
				Array.prototype.map.call((parentSelector.children as HTMLElement), (elem:Element)=>empChild.push(elem))
				empChild.find((element)=>{
					if(element.classList.contains(pseudoSelector)){
						manyElement.push(element)
					}
					else if(element.tagName == pseudoSelector.toUpperCase()){
						manyElement.push(element)
					}
					else if(element.id == pseudoSelector){
						manyElement.push(element)
						return element
					}
				});
				if(manyElement[0] == undefined){
					Array.prototype.map.call(parentSelector.children, element=>{
						callElement(selector, element)
					})
				}
				return manyElement[0]
			}
			result = callElement(selector, parentElement);
			return result;
		}
		
		// _________________________

		// get Until Parents begin from  any elements

		protected getParents(el:HTMLElement, parentSelector /* optional */, untilClass) {
			let parent = null;
			if (parentSelector === undefined) {
				parentSelector = document;
			}
			var parents:{}[] = [];
			var p = el.parentNode;
		
			while (p !== parentSelector) {
				var o = p;
				parents.push(o);
				p = o.parentNode;
			}
			parents.push(parentSelector); // Push that parentSelector you wanted to stop at
			for(let elem = 0; elem < parents.length; elem ++){
				if((parents[elem] as HTMLElement).classList.contains("" + untilClass)){
					parent = parents[elem]
					break;
				}
			}
			return parent;
		}
		// ________________________________
	
		protected scrollToAnchor = (aid:Element, elementsList:JQuery):void=>{
			if(ml.$(aid)[0] != undefined){
				ml.$(elementsList).scrollTop(0);
				ml.$(elementsList).scrollTop(ml.$(aid).offset().top - (ml.$(elementsList).offset().top+ml.$(elementsList).height()-2*(ml.$(aid).height())));
			}
		};
		protected upDownFunc(list:JQuery, e: KeyboardEvent):void{
			let key:number = e.which || e.keyCode;
			if(key == 38 || key == 40){
				Array.prototype.map.call(list.children(), (element:Element)=> {
					ml.$(element).removeClass("selected");
				})
			}
			if(key == 40){
				(this.countArrow == list.children().length-1)? this.countArrow = 0:this.countArrow++;
				ml.$(list.children()[this.countArrow]).addClass("selected");
				this.scrollToAnchor(list.children()[this.countArrow], list);
				
			}
			if(key == 38){
				(this.countArrow > 0)?this.countArrow--:this.countArrow = list.children().length-1;
				ml.$(list.children()[this.countArrow]).addClass("selected");
				this.scrollToAnchor(list.children()[this.countArrow], list);
			}
			if(key == 13){
				// this.currentItem = list.children()[this.countArrow];
				// timezone.__clickEachItem(list.children()[this.countArrow]);
			}
			else if(key == 8 || key != 40 && key != 38 && key != 13){
				// list.scrollTop(0);
				// this.countArrow = 0;
			}
		}
		protected callArrowIndex(list:JQuery, index:number):void{
			Array.prototype.map.call(list.children(), (element:Element)=>{
				if(element.classList.contains("selected")){
					element.classList.remove('selected')
				}
			});
			this.countArrow = index;
			list.children()[this.countArrow].classList.add("selected")
		}
		protected findElementReplaceInputValue(element: HTMLElement, connectToIndex){
			let findArr: {}[] = [], childItem: Object = null;
			Array.prototype.map.call(element.children[0].children[0].children, (element: HTMLElement) => {
				findArr.push(element)
				childItem = findArr.find((el: HTMLElement) => {
					return el.classList.contains("ml-blueprint-item-name")
				})
			})
			this.input[0].setAttribute("placeholder", (childItem as HTMLElement).innerText);
			this.input[0].focus()
		}
		protected clickEachItem (list:JQuery){
			if(list.children().length > 0){
				Array.prototype.map.call(list.children(), (el, index)=>{
					el.addEventListener("click",  (e)=>{
						if((this.input[0] as HTMLInputElement).value !=""){
							this.countArrow = el.getAttribute("data-index");
							this.input.select()
						}
						el.classList.add('selected');
						this.resetOnSelect(list, el, index);			
						this.closeOnSelect(list, this.countArrow);
						if(this.resetBoolean && this.closeOnSelectBoolean){
							ml.$(this.input).blur()
						}
					})
				})
			}
		}
		protected resetOnSelect(list:JQuery, el:HTMLElement, index:number){
			let parent = this.getParents(el, document.querySelector('body'), "ml-blueprint-suggest-portal");
			this.findElementReplaceInputValue(el, parent.getAttribute("id"));
			if(!this.resetBoolean){
				this.callArrowIndex(list, index)
			}
			else{
				if((this.input[0] as HTMLInputElement).value != ""){
					ml.$(this.input).attr("no-click-item", "");
					(this.input[0] as HTMLInputElement).value = ""
					setTimeout(()=>{
						list.children().detach();
						this.listItems();
						this.clickEachItem(list);
						this.callArrowIndex(list, 0)
					}, 0)
				}
				else{
					(this.input[0] as HTMLInputElement).value = ""
					this.callArrowIndex(list, index);
				}
				

			}
		}
		protected closeOnSelect(list:JQuery, index:number){
			if(this.closeOnSelectBoolean){
				if(ml.$(this.input).val() == ""){
					setTimeout(()=>{
						list.children().detach();
						this.listItems();
						this.clickEachItem(list);
						index = (this.resetBoolean)?0:index;
						this.callArrowIndex(list, index);
					}, 0)
				}
				(this.input[0] as HTMLInputElement).value =  this.input[0].getAttribute("placeholder");
				ml.$(this.input).blur();
				ml.$('*[data-mlsuggest-id="' + this.corectUL + '"]')[0].classList.remove("openButton");
				let DataList = this.getParents(list.children()[index], document.querySelector('body'), "ml-blueprint-suggest-overlay");
				DataList.classList.remove("showList");

			}
		}
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			this.initTransforms()
			
			if (this.isDOMLoaded) {
				
				this.watchDefinitionOptions((options: BPSuggest.IOptions) => {
					
					options.closeOnSelect = options.closeOnSelect || true
					var DatListSuggest = this.list;
					this.countArrow = 0;
					
					ml.$(this.input).html("")
					ml.$(this.portal).html("")
					ml.$(this.suggest).html("")
					ml.$(this.overlay).html('');
					ml.$(this.list).html("");
					this.items = [];

					if(options.items){
						this.items = options.items.map(m => {
							return {
								value: m[options.valueField || "value"], 
								label: m[options.labelField || "label"] 
							}
						});
						var fil = this.items.filter((el, ind)=>{
							return (el.label != "") && (el.label != undefined)
						})
						this.items = fil
						if(this.items.length>0){
							this.items.unshift({value: "", label: ""});
						}
						if(this.items.length > 2){
							if(options.sortFunction)
							{
								this.items = this.items.sort(options.sortFunction);
							}
							else
							{
								this.items = this.items.sort((a:any, b:any) => {
									const labelA = a.label.toLowerCase();
									const labelB = b.label.toLowerCase();
									if (labelA < labelB)
										return -1;
									if (labelA > labelB)
										return 1;
									return 0;
								} );
							}
						}
					}

					if(this.suggest){
						this.suggest.addClass('ml-blueprint-suggest-inputGroup');
						this.suggest.append(this.input);
						this.portal.append(`<div></div>`);
					}
		
					this.overlay = ml.$(this.portal).children('div').addClass('ml-blueprint-suggest-overlay')
					this.menu = ml.$(`<div></div>`)
					this.menu.addClass('ml-blueprint-popover-content')
					this.overlay.append(this.menu)
					
					this.menu.append(DatListSuggest)
					let caret: JQuery  = ml.$('<div></div>')
		
					this.listItems = ():void=>{
						if(options.items && this.items.length > 1){
							ml.$.each( this.items, function( key:number, value:any ):void {
								DatListSuggest.append(`<li data-index=${key}><a class='ml-blueprint-suggest-menu-item'><span class='ml-blueprint-item-left'> <span class='ml-blueprint-item-name'>${value.label}</span></span></a></li>`);
							}.bind(this));
							this.callArrowIndex(this.list, this.countArrow);
							// ml.$(list).children()[this.countArrow].classL}ist.add('selected')
						}
						else{
							DatListSuggest.append(`
								<li class="ml-blueprint-suggest-disabled">No result.</li>
							`)
						}
					}

					this.obsNewValue = (newValue)=>{
						
						setTimeout(() => {
							DatListSuggest.children().detach();
							this.listItems();
							ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu li`).map((id,value) => {
								ml.$(value).removeClass('selected');
								if((value as HTMLElement).innerText.trim().toLowerCase() == newValue.toLowerCase()){
									this.countArrow = Number(ml.$(value).attr('data-index'))
									this.scrollToAnchor((ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu li`)[this.countArrow] as HTMLLIElement), ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu`));
									ml.$(value).addClass('selected')
									ml.$(this.contentDiv).on('click',()=>{
										this.scrollToAnchor((ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu li`)[this.countArrow] as HTMLLIElement),ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu`));
									})
									ml.$(this.input).attr('placeholder',(value as HTMLElement).innerText.trim())
									if(!ml.$('.ml-blueprint-suggest-overlay').hasClass('showList')){
										ml.$(this.input).val((value as HTMLElement).innerText.trim())
									}
									obs((value as HTMLElement).innerText.trim())
								}
							})
							this.input.trigger('keyup')
						}, 0);

					}
					this.objectsToDispose.push(
						obs.subscribe((newValue) => {
							if (obs() == null || (ml.$(this.input).attr('placeholder').toLowerCase() != newValue.toLowerCase()) || (ml.$(this.input).val().toLowerCase() != newValue.toLowerCase())) {
								this.obsNewValue(newValue)
							}
						})
					);
				
						
					ml.$(this.input).on("keyup", (e:JQueryEventObject)=>{
						DatListSuggest.children().detach();
						let key:number = e.which || e.keyCode;
						let valueInputFilter:string = (<any>e.target).value.replace(/\\/g, "\\\\").toLowerCase(),
						indexNumeric:number = 0;
						
						if(!/[!$%^&*|~=`{}\[\]";<>?,]/g.test(valueInputFilter)){
							if(DatListSuggest.scrollTop() != 0 && (<any>e.target).value != ""){
								DatListSuggest.scrollTop(0);
							}
							let writeMatchString:string = "";
							Array.prototype.map.call(this.items, (elm:{label:string,value:string}, indexObj:string)=>{
								let BoldTxt:string = "", replaceBold:string = "";
								let multiplyValue:string[] = ['label'];
								indexNumeric++;
								for(let i:number = 0; i < multiplyValue.length; i++){
									if(i<= 0){
										writeMatchString = elm[multiplyValue[i]]
									}
									if(String(writeMatchString).toLowerCase().match(valueInputFilter) != null){
										for(let j:number = String(writeMatchString).toLowerCase().match(valueInputFilter).index ; j< String(writeMatchString).toLowerCase().match(valueInputFilter).index+valueInputFilter.length; j++){
											if(multiplyValue[i] == "label"){
												BoldTxt += elm.label[j];
											}
										}
										replaceBold = elm.label.replace(BoldTxt, `<b>${BoldTxt}</b>`);
									}
									if(new RegExp(valueInputFilter).test(String(writeMatchString).toLowerCase())){
										if(valueInputFilter==""){
											ml.$(DatListSuggest.children()[indexObj+1]).detach();
											replaceBold = elm.label;
										}
										return DatListSuggest.append(`<li data-index=${indexObj}><a class='ml-blueprint-suggest-menu-item '><span class='ml-blueprint-item-left'><span class='ml-blueprint-item-name '>${replaceBold}</span></span> </a></li>`)
									}
								}
							})
							if(valueInputFilter!="" || (valueInputFilter=="" && ml.$(this.list).children().length)> 0){
								// ml.$(ml.$(this.list).children()[this.countArrow]).addClass('selected');
							}
							if(DatListSuggest.children().length == 0){
								DatListSuggest.append(`
									<li class="ml-blueprint-suggest-disabled">No result.</li>
								`)
							}
						}
						else{
							if(DatListSuggest.children().length == 0){
								DatListSuggest.append(`
									<li class="ml-blueprint-suggest-disabled">No result.</li>
								`)
							}
						}
						if(!DatListSuggest.children()[0].classList.contains("ml-blueprint-suggest-disabled")){
							if(DatListSuggest.children().length == 1 || key != 40 && key != 38 && key !==13 && key != 37 && key != 39){
								this.callArrowIndex(DatListSuggest, 0);
							}
							if(DatListSuggest.children().length > 1){
								this.callArrowIndex(DatListSuggest, this.countArrow);
								if(key == 38 || key == 40){
									Array.prototype.map.call((DatListSuggest.children() as JQuery), (element:HTMLElement)=> {
										ml.$(element).removeClass("selected");
									})
								}
								if(key == 40){		
									ml.$(DatListSuggest.children()[this.countArrow]).addClass("selected");
									this.scrollToAnchor((DatListSuggest.children()[this.countArrow] as HTMLLIElement), (DatListSuggest as JQuery));								
								}
								if(key == 38){
									ml.$(DatListSuggest.children()[this.countArrow]).addClass("selected");
									this.scrollToAnchor(DatListSuggest.children()[this.countArrow], DatListSuggest);
								}
							}
							if(key == 13){
								
								this.scrollToAnchor(DatListSuggest.children()[this.countArrow], DatListSuggest);
								this.resetOnSelect(DatListSuggest, DatListSuggest.children()[this.countArrow], this.countArrow);
								this.closeOnSelect(DatListSuggest, this.countArrow);
								if(this.placeHolderSuggest != (this.input[0] as HTMLInputElement).getAttribute('placeholder')){
									obs((this.input[0] as HTMLInputElement).getAttribute('placeholder'))
								}
							}
						}
						if((<any>e.target).value !=""){
							if(this.opwnListOnlyKeyDown){
								let DataList = this.getCurrentToggleList((ml.$(this.input)[0] as HTMLElement), "ml-dash-content", "ml-blueprint-suggest-overlay");
								DataList.classList.add("showList");
							}
						}
						this.clickEachItem(DatListSuggest)
					})
					ml.$(this.input).on("keydown", (e:JQueryEventObject)=>{
						let key:number = e.which || e.keyCode;
						if (key == 27) {
							document.querySelectorAll(".ml-blueprint-suggest-overlay").forEach(element => {
								(element as HTMLElement).classList.remove("showList");
							});
							document.querySelectorAll('.ml-blueprint-suggest-inputGroup .openButton').forEach((elem:HTMLElement)=>{
								elem.classList.remove("openButton")
							})
							if(this.input[0].getAttribute("placeholder") != this.placeHolderSuggest){
								(this.input[0] as HTMLInputElement).value = this.input[0].getAttribute("placeholder");
							}
							if(this.input[0].getAttribute("placeholder") == this.placeHolderSuggest && ((ml.$(this.input)[0] as HTMLInputElement).hasAttribute("no-click-item") && (ml.$(this.input)[0] as HTMLInputElement).getAttribute("no-click-item") != "")){
								(this.input[0] as HTMLInputElement).value = "";
							}
							Array.prototype.map.call(DatListSuggest.children(), (elem, index)=>{
								if(elem.classList.contains("selected")){
									this.indexEsc = index;
								}
							})
							this.countArrow = this.indexEsc;
							this.input.blur()
						}
						if(DatListSuggest.children().length != 1){
							this.upDownFunc(DatListSuggest, <any>e);
						}
					})
					ml.$(this.input).on('input',(e:JQueryEventObject)=>{
						(ml.$(this.input)[0] as HTMLInputElement).setAttribute("no-click-item",(ml.$(this.input)[0] as HTMLInputElement).value);
					})
					
					document.querySelectorAll('.suggest-input').forEach((element:HTMLElement)=>{
						element.onclick = ()=>{
							// Connect to Clickable button of this Attribute
							this.corectUL = ml.$(element).attr('data-mlsuggest-id');
							DatListSuggest = ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu`);
							// _________________________________

							// Remove all customElements, which classList has this class
							document.querySelectorAll(".ml-blueprint-suggest-overlay").forEach(element => {
								(element as HTMLElement).classList.remove("showList");
							});
							// _________________________


							if((ml.$(this.input)[0] as HTMLInputElement).hasAttribute("no-click-item") && (ml.$(this.input)[0] as HTMLInputElement).getAttribute("no-click-item") != ""){
								(ml.$(this.input)[0] as HTMLInputElement).value = (ml.$(this.input)[0] as HTMLInputElement).getAttribute("no-click-item")
							}
							let DataListParentOverlay = this.getCurrentToggleList(DatListSuggest[0], `#${this.corectUL}`, "ml-blueprint-suggest-overlay");
							document.querySelectorAll('.openButton').forEach((elem: HTMLElement) => {
								elem.classList.remove("openButton");
							})
							element.classList.add("openButton");
							if(!this.opwnListOnlyKeyDown){
								document.querySelectorAll(".showList").forEach(element => {
									(element as HTMLElement).classList.remove("showList");
								});	
								DataListParentOverlay.classList.add("showList");
							}
							let parentTransferDatList = this.getParents(DataListParentOverlay, document.querySelector("body"), "ml-blueprint-suggest-portal"),
								buttonPosition = element.getBoundingClientRect(),
								parentTransferDatListPosition = parentTransferDatList.getBoundingClientRect(),
								top = 0,
								left = 0,
								triangleLeft = 0;
							// When DataList pass the window width limit
							if ((buttonPosition.left + parentTransferDatListPosition.width) > window.innerWidth) {
								left = buttonPosition.left - ((buttonPosition.left + parentTransferDatListPosition.width) - window.innerWidth) - 25;
								parentTransferDatList.classList.add("triangleTransfer");
								triangleLeft = buttonPosition.left - left + buttonPosition.width / 2 - 20;
							}
							else {
								parentTransferDatList.classList.remove("triangleTransfer");
								left = buttonPosition.left;
								triangleLeft = buttonPosition.width / 2 - 20;
							}
							// ________________________________

							// When DataList pass the window height limit
							if (buttonPosition.top + buttonPosition.height + (parentTransferDatListPosition.height) > window.innerHeight) {
								top = buttonPosition.top - parentTransferDatListPosition.height - 15;
								parentTransferDatList.classList.add("triangleBottom");
							}
							else {
								top = buttonPosition.top + buttonPosition.height;
								parentTransferDatList.classList.remove("triangleBottom");
							}
							// __________________________

							document.querySelector("body").style.setProperty('--triangelAfter', String(triangleLeft) + "px");
							parentTransferDatList.style.left = left + "px";
							parentTransferDatList.style.top = top + "px";
						} 
					})	
					
					ml.$(this.input).on('click',(e:JQueryEventObject)=>{
						let selectedIndex = this.countArrow;
						if((this.input[0] as HTMLInputElement).value == ""){
							this.input.attr('value', '');
							// ml.$(this.list).children().detach();
							// this.listItems();
							this.clickEachItem(this.list)
						}
						else{
							if((ml.$(this.input)[0] as HTMLInputElement).hasAttribute("no-click-item") && (ml.$(this.input)[0] as HTMLInputElement).getAttribute("no-click-item") != ""){
								(ml.$(this.input)[0] as HTMLInputElement).value = (ml.$(this.input)[0] as HTMLInputElement).getAttribute("no-click-item")
							}
							else{
								this.input.attr('placeholder', this.input.attr('value'))
								this.input.attr('value', "");
							}
							Array.prototype.map.call(this.list.children(), (elem, ind)=>{
								if(elem.classList.contains("selected")){
									// selectedIndex = elem.getAttribute("data-index");
									selectedIndex = ind;
								}
							})
							ml.$(this.input).trigger("keyup")
							this.countArrow = selectedIndex;
						}
						this.callArrowIndex(this.list, selectedIndex);
						this.scrollToAnchor(this.list.children()[selectedIndex], this.list)
					})			
				

					ml.$(this.list).children().detach();
					const intents:string[] = ['primary','success','warning','danger', 'default'];
					intents.forEach(intent => {
						this.input.removeClass(`ml-blueprint-suggest-input-${intent} `);
						ml.$(this.list).removeClass(`ml-blueprint-select-${intent}`)
						caret.removeClass()
					}) 
					
					if(options.intent){
						this.input.addClass(`ml-blueprint-suggest-input-${options.intent}`)
						ml.$(this.list).addClass(`ml-blueprint-select-${options.intent}`)
					}

					this.resetBoolean = (options.resetOnSelect)?true:false;
					this.closeOnSelectBoolean = (options.closeOnSelect)? true:false;

					if(options.minimal){
						ml.$(this.menu).addClass('ml-blueprint-suggest-minimal')
					}
					else{
						ml.$(this.menu).removeClass('ml-blueprint-suggest-minimal')
					}

					this.placeHolderSuggest = (options.placeholder != "" && options.placeholder)?options.placeholder:"Search...";

					setTimeout(()=>{
						ml.$(this.input).attr("placeholder", this.placeHolderSuggest)
					}, 0)
					
					this.opwnListOnlyKeyDown = (options.openOnKeyDown)?true:false; 
				
					ml.$(document).on('click',(event: JQueryEventObject):void=>{
						if(this.placeHolderSuggest != (this.input[0] as HTMLInputElement).getAttribute('placeholder')){
							obs((this.input[0] as HTMLInputElement).getAttribute('placeholder'))
						}
						
						if(!event.srcElement.closest('.ml-blueprint-suggest-inputGroup .openButton') && !event.srcElement.closest('.ml-blueprint-suggest-overlay')){
							if(ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu`).children().length > 0 ){
								document.querySelectorAll(".ml-blueprint-suggest-overlay").forEach(element => {
									(element as HTMLElement).classList.remove("showList");
								}); 
								document.querySelectorAll('.ml-blueprint-suggest-inputGroup .openButton').forEach((elem:HTMLElement)=>{
									elem.classList.remove("openButton")
								})
								let parentSelected = this.getParents(DatListSuggest[0], document.querySelector('body'), "ml-blueprint-suggest-portal"),
									findInput = ml.$('*[data-mlsuggest-id="' + parentSelected.getAttribute('id') + '"]');
								if(findInput[0].getAttribute("placeholder") != this.placeHolderSuggest){
									(findInput[0] as HTMLInputElement).value = findInput[0].getAttribute("placeholder");
								}
								if(findInput[0].getAttribute("placeholder") == this.placeHolderSuggest && ((ml.$(this.input)[0] as HTMLInputElement).hasAttribute("no-click-item") && (ml.$(this.input)[0] as HTMLInputElement).getAttribute("no-click-item") != "")){
									(findInput[0] as HTMLInputElement).value = "";
								}
								this.callArrowIndex(ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu`), this.countArrow);
							}
						}
					})
					if(ml.$(this.input).val() == ""){
						this.obsNewValue(obs())
					}		
				});

			}
		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPSuggest.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPSuggest {

		/**
		 * Suggest component props
		 * @typedef ISuggestProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface ISuggestProps extends BPBase.IControlProps{
			/**
			 * Items object for the suggest dropdown
			 * @typedef items[]
			 * @param {string} label - Text of the item
			 * @param {any} value - Value of the dropdown item
			 */
			items?: any[];
			/**
			 * Intent color for the Suggest items
			 * @type {ctrl.BPBase.Intent}
			 */
			intent?: ctrl.BPBase.Intent;
			/**
			 * Reset suggest on select
			 * @type {boolean}
			 */
			resetOnSelect?: boolean;
			/**
			 * Close suggest on select
			 * @type {boolean}
			 */
			closeOnSelect?: boolean;
			/**
			 * Open suggest when pressing a key
			 * @type {boolean}
			 */
			openOnKeyDown?: boolean;
			/**
			 * Placeholder text for the input on the Suggest
			 * @type {string}
			 */
			placeholder?: string;
			/**
			 * The name of the value field in the array of items passed to the Suggest
			 * @type {string}
			 */
			valueField?: string,
			/**
			 * The name of the label field in the array of items passed to the Suggest
			 * @type {string}
			 */
			labelField?: string,
			/**
			 * Custom sorting function for the data inside the Suggest
			 * @function
			 * @param {any} a - First item for comparison
			 * @param {any} b - Second item for comparison 
			 * @return {number}
			 */
			sortFunction?: (a: any, b: any) => number
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ISuggestProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			items?: any[] | mdash.IScopePath,
			minimal?: boolean | mdash.IScopePath,
			intent?: ctrl.BPBase.Intent | mdash.IScopePath,
			closeOnSelect?: boolean | mdash.IScopePath,
			resetOnSelect?: boolean | mdash.IScopePath,
			openOnKeyDown?: boolean | mdash.IScopePath,
			placeholder?: string | mdash.IScopePath,
			valueField?: string | mdash.IScopePath,
			labelField?: string | mdash.IScopePath,
			sortFunction?: (a: any, b: any) => number
		}
	}

	mdash.registerControl(BPSuggest,'dashboard.components.controls', BPSuggest.meta);
}