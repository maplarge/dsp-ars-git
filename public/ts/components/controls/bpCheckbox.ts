namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPCheckbox<T extends BPCheckbox.IOptionsDefinition> extends mdash.BaseInputControl<BPCheckbox.IOptionsDefinition> {
		public static type: string = 'BPCheckbox';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Checkbox',
			shortDescription: "A dashboard control that displays a checkbox. The output value is either true or false.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPCheckbox.builder().toJSON();
		}

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected checkbox: HTMLInputElement;
		protected fancyCheckbox: HTMLElement;
		protected labelContainer: HTMLElement;

		constructor(id: string, definition: mdash.IControlDefinition<T>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		/**
		 * @inheritDoc
		 */
		public build() {
			super.build();

			this.labelContainer = document.createElement('label');
			this.checkbox = document.createElement('input');
			this.fancyCheckbox = document.createElement('span');
			this.labelContainer.appendChild(this.checkbox);
			this.labelContainer.appendChild(this.fancyCheckbox);
			this.checkbox.classList.add('ml-blueprint-checkbox');
			this.checkbox.setAttribute('type', 'checkbox');
			this.labelContainer.classList.add('ml-blueprint-checkbox-label');
			this.fancyCheckbox.classList.add('ml-blueprint-checkbox-check');

			this.contentDiv[0].appendChild(this.labelContainer);

			this.container.classList.add('namespace-blueprint-checkbox');

		}
		/**
		 * @inheritDoc
		 */
		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		/**
		 * listens to checkbox changes and then updates the observable value
		 */
		private initializeListeners(valueObs: mdash.RefCountedObservable<any>) {
			var checkbox = this.contentDiv.find('input:checkbox');
			checkbox
				.off('change')//remove any previous change event listeners
				.on('change', (event: JQueryEventObject) => { 
					if (checkbox.is(':checked') != valueObs() || valueObs() == null) {
						valueObs(checkbox.is(":checked")); //if new value of checkbox is different than current value of the control, update the value of the control
					}
				});

			this.objectsToDispose.push(
				valueObs.subscribe(newValue => {
					if (valueObs() != checkbox.prop('checked')) {
						checkbox.prop('checked', Boolean(newValue));
					}
				})
			);

			checkbox.prop('checked', valueObs());
		}

		/**
		 * @inheritDoc
		 */
		protected init() {
			super.init();
			//valueObs, in this case, represents the observable attached to the control's value.
			var valueObs = this.registerScopeObservable(this.definition.name, false/*represnts value and type of control */ , true);
			this.initTransforms();
			if (this.isDOMLoaded) {
				var checkbox = this.contentDiv.find('input:checkbox');

				checkbox.prop('checked', valueObs());//set value of checkbox to current value of control
				this.initializeListeners(valueObs);

				this.watchDefinitionOptions((options: BPCheckbox.IOptions) => {

					options.name ? this.checkbox.setAttribute('name', options.name) :
							 	   this.checkbox.removeAttribute('name');

					options.indeterminate ? this.checkbox.indeterminate = true :
											this.checkbox.indeterminate = false;

					if(options.label)
					{
						this.checkbox.setAttribute('value', options.label);
						this.contentDiv.find('.ml-blueprint-checkbox-left-align').detach();
						this.contentDiv.find('.ml-blueprint-checkbox-span').detach();
						
						if(options.align && options.align == BPBase.Alignment.Right)
						{
							this.labelContainer.insertAdjacentHTML('afterbegin', `<span class='ml-blueprint-checkbox-span'>${options.label}</span>`);
							this.labelContainer.style.display = 'block';							
						}
						else
						{
							this.contentDiv[0].insertAdjacentHTML('afterbegin', `<span class='ml-blueprint-checkbox-left-align'>${options.label}</span>`);
							this.labelContainer.style.display = 'inline';
						}

					}

					options.disabled ? this.checkbox.setAttribute('disabled', 'disabled') : 
									   this.checkbox.removeAttribute('disabled');
					
					options.inline ? this.labelContainer.classList.add('ml-blueprint-checkbox-inline') :
									 this.labelContainer.classList.remove('ml-blueprint-checkbox-inline');
					  
					if(options.large) 
					{
						this.labelContainer.classList.add('ml-blueprint-checkbox-large');
						this.contentDiv.find('.ml-blueprint-checkbox-left-align').addClass('ml-blueprint-checkbox-label-large');
					}
					else
					{
						this.labelContainer.classList.remove('ml-blueprint-checkbox-large'); 
						this.contentDiv.find('.ml-blueprint-checkbox-left-align').removeClass('ml-blueprint-checkbox-label-large');
					}

					if(options.onChange){
						this.checkbox.onchange = () => {
							options.onChange(`${this.checkbox.checked}`, this.scope); 
						}
					}
					else
					{
						this.checkbox.onchange = null;
					}
				});
			}
		}

		/**
		 * @inheritDoc
		 */
		protected cleanup() {
			if(this.checkbox) this.checkbox.onchange = null;
			if(this.contentDiv) this.contentDiv.find('input:checkbox').off('change');
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPCheckbox.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPCheckbox {

		/**
		 * Props for managing checkbox component's state
		 * @typedef ICheckboxButtonProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface ICheckboxButtonProps extends BPBase.IControlProps{
			/**
			 * Name of the checkbox
			 * @type {string}
			 */
			name?: string,
			/**
			 * Adds an indeterminate state on checkbox, where the checkbox is neither checked nor unchecked
			 * @type {boolean}
			 */
			indeterminate?: boolean
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ICheckboxButtonProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			name?: string | mdash.IScopePath,
			align?: BPBase.Alignment | mdash.IScopePath,
			checked?: boolean | mdash.IScopePath,
			indeterminate?: boolean | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
			inline?: boolean | mdash.IScopePath,
			label?: string | mdash.IScopePath,
			large?: boolean | mdash.IScopePath,
			onChange?: (value: string, scope: mdash.IScope) => void          
		}
	}

	mdash.registerControl(BPCheckbox,'dashboard.components.controls', BPCheckbox.meta);
}