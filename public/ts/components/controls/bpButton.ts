namespace dashboard.components.controls {
	import mdash = ml.ui.dashboard;

	export class BPButton<T extends BPButton.IOptionsDefinition> extends mdash.BaseControl<BPButton.IOptionsDefinition> {
		public static type: string = 'BPButton';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'A Button Control',
			shortDescription: "A control that generates a button with some additional options available.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPButton.builder().options({label: "Button"}).toJSON();
		}

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected button: HTMLElement;
		protected icon: string;
		protected text: string;

		constructor(id: string, definition: mdash.IControlDefinition<BPButton.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		/** 
		 * @inheritdoc
		 * */
		public build() {
			super.build();

			this.button = ml.create("button", "ml-bpbutton", this.contentDiv[0]);
			this.button.classList.add('ml-blueprint-button');

			this.container.classList.add('namespace-blueprint-button');
		}

		/** 
		 * @inheritdoc
		 * */
		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		/** 
		 * @inheritDoc
		 * */
		protected init() {
			super.init();

			var obs = this.registerScopeObservable(this.definition.name, null, false);

			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPButton.IOptions) => {
					// set it to the initial button class before adding every other class
					this.button.className = 'ml-blueprint-button';

					if(options.label != "" && options.label != null) {
						this.text = options.label;
						this.button.textContent = options.label;
					}

					if(options.active){
						this.button.classList.add('ml-blueprint-button-active');
					}
								
					if(options.large)
					{
						this.button.classList.add('ml-blueprint-button-large');
					} 
								
					if(options.loading)
					{
						this.button.setAttribute('disabled', 'disabled');
						this.button.style.display = 'none';
						this.contentDiv.find('.ml-blueprint-button-loading-background').detach();
					    this.contentDiv.append(
							`<div class="ml-blueprint-button-loading-background 
							${options.intent ? `ml-blueprint-button-intent-${options.intent}` : ''}">
								<svg width="20" height="20" stroke-width="16.00" viewBox="-3.00 -3.00 106.00 106.00">
									<path class="ml-blueprint-spinner-track" d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90"></path>
									<path class="ml-blueprint-spinner-head" d="M 50,50 m 0,-45 a 45,45 0 1 1 0,90 a 45,45 0 1 1 0,-90" pathLength="280" stroke-dasharray="280 280" stroke-dashoffset="210"></path>
								</svg>
							</div>`);
					}
					else
					{
						ml.$('.ml-blueprint-button-loading-background').detach();
						this.button.style.display = 'inline';
						this.button.removeAttribute('disabled');
					}
					
					options.disabled ? this.button.setAttribute('disabled','disabled') : this.button.removeAttribute('disabled');

					if(options.minimal)
					{
						this.button.classList.add('ml-blueprint-button-minimal');
					}

					if(options.intent)
					{
						this.button.classList.add(`ml-blueprint-button-intent-${options.intent}`);
					}
					else 
					{
						this.button.classList.add('ml-blueprint-button-default');
					}
					
					if(options.iconsOnly)
					{
						this.text = this.button.textContent;
						this.button.textContent = "";
					}
					else
					{
						this.button.textContent = this.text;
					}

					options.onClick ? this.button.onclick = (e) => options.onClick(this.scope, e) :
										this.button.onclick = null;

					if(options.className)
					{
						this.button.className += ` ${options.className}`;
					}

					if(options.small)
					{
						this.button.classList.add('ml-blueprint-button-small')
					}
					
					if(options.icon)
					{
						this.icon = options.icon;
						this.button.innerHTML = `<i class="ml-blueprint-button-icon mlicon-${this.icon}"></i> ${this.button.innerHTML}`;
					}
					else
					{
						this.button.innerHTML = this.button.innerHTML.replace(`<i class="ml-blueprint-button-icon ml-icon-${this.icon}"></i>`,'');
					}

				});
			}
		}

		/** 
		 * @inheritDoc
		 * */
		protected cleanup() {
			super.cleanup();
			if(this.button) this.button.onclick = null;
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPButton.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPButton {

		/**
		 * Props used for rendering a button on the screen
		 * @typedef IButtonProps
		 * @interface
		 * @augments IActionProps
		 * @augments IAppearance
		 * @readonly
		 */
		export interface IButtonProps extends BPBase.IActionProps, BPBase.IAppearance{
			/**
			 * Sets the state of the button to loading
			 * @type {boolean}
			 */
			loading?: boolean,
			/**
			 * Event handler for click event on buttons
			 * @function
			 * @param {IScope} scope - The current scope
			 * @param {MouseEvent} event - Click event object
			 * @event click
			 * @returns {void}
			 */
			onClick?: (scope: mdash.IScope, event: MouseEvent) => void  
		}
		
		export interface IOptions extends mdash.IControlOptionsWithTransforms, IButtonProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			label?: string | mdash.IScopePath,
			active?: boolean | mdash.IScopePath,
			small?: boolean | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
			large?: boolean | mdash.IScopePath,
			loading?: boolean | mdash.IScopePath,
			minimal?: boolean | mdash.IScopePath,
			intent?: BPBase.Intent | mdash.IScopePath,
			iconsOnly?: boolean | mdash.IScopePath,
			icon?: string | mdash.IScopePath,
			onClick?: (scope: mdash.IScope, event: MouseEvent) => void | mdash.IScopePath
		}
	}
	
	mdash.registerControl(BPButton,'dashboard.components.controls', BPButton.meta);
}
