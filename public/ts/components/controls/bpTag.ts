namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPTag extends mdash.BaseControl<BPTag.IOptionsDefinition> {
		public static type: string = 'BPTag';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Tag',
			shortDescription: "By adding components this way, you automate the components that are included in resources.",
			categories: ["Layout"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPTag.builder().options(BPTag.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPTag.IOptions> = {
			'BPTag': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}
		
		protected tagComp: JQuery;
		protected removeIcon: JQuery;


		constructor(id: string, definition: mdash.IControlDefinition<BPTag.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();

			this.container.classList.add('namespace-blueprint-tagComp');
			this.tagComp = ml.$('<span></span>');
			this.contentDiv.append(this.tagComp);
			this.tagComp.addClass('ml-blueprint-tagComp');
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);

			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPTag.IOptions) => {

					const intents: string[] = ['primary','success','warning','danger'];
					intents.forEach(intent => {
						this.tagComp.removeClass(`ml-blueprint-tagComp-${intent} `);						
					}) 

					if (options.label) {
						this.tagComp.text(options.label);
						obs(this.tagComp.text())
					}
					else{
						this.tagComp.text('')
					}
					if(options.large)
					{
						this.tagComp.addClass('ml-blueprint-tagComp-large');
					} 
					else
					{
						this.tagComp.removeClass('ml-blueprint-tagComp-large');
					}
					if(options.interactive)
					{
						this.tagComp.addClass('ml-blueprint-tagComp-interactive');                        
					}
					else
					{
						this.tagComp.removeClass('ml-blueprint-tagComp-interactive');
					}
					if(options.minimal)
					{
						this.tagComp.addClass('ml-blueprint-tagComp-minimal');

						if(options.interactive)
						{
							this.tagComp.addClass('ml-blueprint-tagComp-interactive-minimal');						
						}
						else
						{
							this.tagComp.removeClass('ml-blueprint-tagComp-interactive-minimal');
						}
					}
					else
					{
						this.tagComp.removeClass('ml-blueprint-tagComp-minimal');
					}
					if(options.round)
					{
						this.tagComp.addClass('ml-blueprint-tagComp-round');						
					}
					else
					{
						this.tagComp.removeClass('ml-blueprint-tagComp-round');
					}
					if(options.intent)
					{
						this.tagComp.addClass(`ml-blueprint-tagComp-${options.intent}`);
					}
				
					if(options.iconRight)
					{
						this.tagComp.append(`<i class="ml-blueprint-right-icon mlicon-${options.iconRight}"></i>`);
					}
					else
					{
						this.tagComp.find('.ml-blueprint-right-icon').detach();
					}
					if(options.iconLeft)
					{
						this.tagComp.prepend(`<i class="ml-blueprint-left-icon mlicon-${options.iconLeft}"></i>`);						
					}
					else
					{
						this.tagComp.find('.ml-blueprint-left-icon').detach();
					}
					if(options.onRemove) {
						this.removeIcon = ml.$('<button class="mlicon-cancel"> </button>');
						this.removeIcon.addClass('ml-blueprint-remove');
						this.tagComp.append(this.removeIcon);                        
						this.removeIcon.click(()=>{
							this.tagComp.detach();
						});                        
					}
					else
					{
						if(this.removeIcon)
						{
							this.removeIcon.detach();
						}
					}
				});
			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPTag.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPTag {

		/**
		 * Properties for Tag
		 * @typedef ITagProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface ITagProps extends BPBase.IControlProps{
			/**
			 * Intent color of the tag
			 * @type {BPBase.Intent}
			 */
			intent?: BPBase.Intent
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ITagProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			intent?: BPBase.Intent | mdash.IScopePath,
			label?: string | mdash.IScopePath,
			round?: boolean | mdash.IScopePath,
			minimal?: boolean | mdash.IScopePath,
			large?: boolean | mdash.IScopePath,
			interactive?: boolean | mdash.IScopePath,
			iconRight?: string | mdash.IScopePath,
			iconLeft?: string | mdash.IScopePath,
			onRemove?: (Event: MouseEvent) => void | mdash.IScopePath,
		}
	}

	mdash.registerControl(BPTag, 'dashboard.components.controls', BPTag.meta);
}