namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPDatePicker <T extends BPDatePicker.IOptionsDefinition> extends mdash.BaseControl<BPDatePicker.IOptionsDefinition> {
		public static type: string = 'BPDatePicker';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Date Picker',
			shortDescription: "The datepicker allows users to enter a date either through text input, or by choosing a date from the calendar.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPDatePicker.builder().options(BPDatePicker.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPDatePicker.IOptions> = {
			'BPDatePicker': {
				options: {
					outputTransforms: [],
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
        }
        protected parentDiv: JQuery
		protected newDate: JQuery;
		protected selectDate: JQuery
		protected dateText: JQuery;
		protected buttonBar: JQuery;
		protected buttonToday: JQuery;
		protected buttonClear: JQuery;
		protected timePicker: HTMLElement;
		protected hours: HTMLElement;
		protected minute: HTMLElement;
		protected second: HTMLElement;
		protected milisecond: HTMLElement;
		protected datePicker: Function;
		protected select: Function;
		protected icon: JQuery;
		protected chekonce: boolean = false;
		protected chekonce2: boolean = false;

		constructor(id: string, definition: mdash.IControlDefinition<BPDatePicker.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

			//this is where you might create/initialize class variables of observables
		}


		public build() {
			super.build();			
            this.container.classList.add('namespace-blueprint-datePicker');
			this.parentDiv = ml.$('<div class="ml-blueprint-parentDiv"></div>');
			this.newDate = ml.$ ('<div type="text" class="ml-blueprint-datePicker"></div>');
			this.selectDate = ml.$('<span class="ml-blueprint-select-date"></span>');		
			this.dateText = ml.$('<span class="ml-blueprint-date-text">no date</span>');
			this.selectDate.append(this.dateText);
			this.buttonToday = ml.$('<button class="ml-blueprint-button-today">Today</button>');
			this.parentDiv.append(this.newDate,  this.selectDate);		
			this.contentDiv.append(this.parentDiv);
			this.timePicker = ml.create("div", "ml-BPDatePicker",);
			this.timePicker.classList.add('ml-blueprint-timePicker');
		}

		protected validateDefinition(): boolean {

			//run any validation against the defintion (options passed into the control) that you might require

			if (!super.validateDefinition())
				return false;

			return true;
		}

		private keys: Array<number>  = [49,50,51,52,53,54,55,56,57,97,98,99,100,101,102,103,104,105];

		protected updown(input: HTMLElement,max: number, min: number, currentMax: number, currentMin: number, e: JQueryEventObject) {
			if(ml.$(input).val() == currentMax) {
				if(e.which === 38) {								
					ml.$(input).val(min)
				}							
			}
			
			if(ml.$(input).val() == currentMin) {
				if(e.which === 40) {								
					ml.$(input).val(max)
				}
			}
		}
		
		protected leadingZerosHours(input: any, e: JQueryEventObject): void {
			if(!isNaN(input.value) && input.value.length === 0 && e.which != 8 && e.which !=46) {
				input.value = '0' + input.value;
			}
		}
		protected leadingZerosSecond(input: any, e: JQueryEventObject): void {
			if(!isNaN(input.value) && input.value.length === 1 && e.which != 8 && e.which !=46 && this.keys.indexOf(e.which) == -1) {
				input.value = '0' + input.value;
			}
			if(!isNaN(input.value) && input.value.length === 0 && e.which != 8 && e.which !=46) {
				input.value = '00' + input.value;
			}
		}
		protected leadingZeros(input:any, e: JQueryEventObject): void {
			if(!isNaN(input.value) && input.value.length <= 1 && e.which != 8 && e.which !=46 && this.keys.indexOf(e.which) == -1) {
				input.value = '00' + input.value;
			}
			if(!isNaN(input.value) && input.value.length === 2 && e.which != 8 && e.which !=46 && this.keys.indexOf(e.which) == -1) {
				input.value = '0' + input.value;															
			}						
		}
		
		protected maxmin(max :number, min :number, _self: JQuery): void {						
			var value: any = _self.val();
			
			if ((value !== '') && (value.indexOf('.') === -1)) {
				_self.val(Math.max(Math.min(value, max), min));
			}
		};
		protected inputBlur (input: HTMLElement, e: JQueryEventObject ): void {
			if(e.which === 13) {
				input.blur();
			}
		}
		
		protected validate(e: JQueryEventObject) {
			var charCode: number = e.keyCode? e.keyCode : e.charCode
			if ((charCode === 190) || (charCode === 110) || (charCode === 189) || (charCode === 109) || (charCode === 107) || (charCode === 187)) {
					return false;
				}
			};

			protected generateMinutes(): void {
				this.hours = ml.create("input", "ml-hours", this.timePicker);
				ml.$(this.hours).after('<span class="colon">:</span>');
				ml.$(this.hours).attr('type', 'number');
				ml.$(this.hours).val('0');
				
				this.minute = ml.create("input", "ml-minute", this.timePicker);
				ml.$(this.minute).attr('type', 'number');
				ml.$(this.minute).val('00');
			}
			protected generateSeconds(): void{
				this.generateMinutes();
				this.second = ml.create("input", "ml-second", this.timePicker);
				ml.$(this.second).before('<span class="colon">:</span>');
				ml.$(this.second).attr('type', 'number');
				ml.$(this.second).val('00');
			}
	
			protected generateMilliseconds(): void{
				this.generateSeconds();
				this.milisecond = ml.create("input", "ml-milisecond", this.timePicker);
				ml.$(this.milisecond).before('<span class="colon">.</span>');
				ml.$(this.milisecond).attr('type', 'number');
				ml.$(this.milisecond).val('000');
			}			
		
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPDatePicker.IOptions) => {

					ml.$(this.hours).detach();
					ml.$(this.second).detach();
					ml.$(this.minute).detach();
					ml.$(this.milisecond).detach();
					ml.$('.colon').detach();

			if(options.Precision == BPBase.Precision.Minute)
			{				
				this.generateMinutes();
			}

			if(options.Precision == BPBase.Precision.Second)
			{
				this.generateSeconds();
			}

			if(options.Precision == BPBase.Precision.Milisecond)
			{
				this.generateMilliseconds();
			}


			if(options.Precision == BPBase.Precision.None) {
				ml.$(this.timePicker).detach();
			}

			var dateText: JQuery = this.dateText;
			var selectDate: JQuery = this.selectDate;
			var timePicker: HTMLElement = this.timePicker;
			var newDate: JQuery = this.newDate;
			var thisHours: HTMLElement = this.hours;
			var thisSecond: HTMLElement = this.second;
			var thisMinute: HTMLElement = this.minute;
			var thisMilisecond: HTMLElement = this.milisecond;
					
					this.datePicker = function() {
						ml.$( ".ml-blueprint-datePicker" ).datepicker({
							changeMonth: true,
							changeYear: true,
							monthNamesShort:[
							  'January',
							  'February',
							  'March',
							  'April',
							  'May',
							  'June',
							  'July',
							  'August',
							  'September',
							  'October',
							  'November',
							  'December'
							],
							yearRange: options.year_range,
							showOtherMonths: true,
							selectOtherMonths: true,
							minDate: options.min_date,
							maxDate: options.max_date,
							dateFormat: 'DD, MM d, yy',				
						});
						
						ml.$( ".ml-blueprint-datePicker" ).datepicker( "option", "showMonthAfterYear", options.reverseMonthAndYearMenus );
					};	
					
					
					ml.$( ".ml-blueprint-datePicker" ).on("change",function(){
						var selected: string = ml.$(this).val();
						if(options.Precision){
							var time: Date = new Date();
	
							if(options.Precision == BPBase.Precision.Minute){
								time.setHours(parseInt(ml.$(thisHours).val()));
								time.setMinutes(parseInt(ml.$(thisMinute).val()));
							}
							if(options.Precision == BPBase.Precision.Second){
								time.setHours(parseInt(ml.$(thisHours).val()));
								time.setMinutes(parseInt(ml.$(thisMinute).val()));
								time.setSeconds(parseInt(ml.$(thisSecond).val()));
							}
							if(options.Precision == BPBase.Precision.Milisecond){
								time.setHours(parseInt(ml.$(thisHours).val()));
								time.setMinutes(parseInt(ml.$(thisMinute).val()));
								time.setSeconds(parseInt(ml.$(thisSecond).val()));
								time.setMilliseconds(parseInt(ml.$(thisMilisecond).val()));									
							}
	
							time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
	
							ml.$(dateText).text(`${selected} ${time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })}`);							
							obs(dateText.text())
						}
						else
						{						
							ml.$(dateText).text(selected);
							obs(dateText.text())
						}

						ml.$(selectDate).css('background', '#137cbd');
						ml.$(dateText).css('color', 'white');
						ml.$( ".ml-blueprint-datePicker" ).datepicker( "option", "gotoCurrent", true );						
					} )
					
					this.datePicker();					
					
					ml.$('.mlui-datepicker-current-day').css('background', 'none');
					ml.$('.mlui-datepicker-current-day a').css('color', 'black');

						newDate.append(timePicker);
						ml.$(this.hours).on('input', () => this.maxmin(23, 0,ml.$(this.hours)));
						ml.$(this.minute).on('input', () => this.maxmin(59, 0,ml.$(this.minute)));
						ml.$(this.second).on('input', () => this.maxmin(59, 0,ml.$(this.second)));
						ml.$(this.milisecond).on('input', () => this.maxmin(999, 0,ml.$(this.milisecond)));
						
						ml.$(this.hours).on('keydown', this.validate);
						ml.$(this.minute).on('keydown', this.validate);
						ml.$(this.second).on('keydown', this.validate);
						ml.$(this.milisecond).on('keydown', this.validate);
						
						ml.$(this.hours).on('change', (e) => this.leadingZerosHours(this.hours, e));						
						ml.$(this.minute).on('change', (e) => this.leadingZerosSecond(this.minute, e));						
						ml.$(this.second).on('change', (e) => this.leadingZerosSecond(this.second, e));						
						ml.$(this.milisecond).on('change', (e) => this.leadingZeros(this.milisecond, e));
						
						ml.$(this.hours).on('keydown', (e) => this.updown(this.hours, 24, -1, 23, 0, e));
						ml.$(this.minute).on('keydown', (e) => this.updown(this.minute, 60, -1, 59, 0, e));
						ml.$(this.second).on('keydown', (e) => this.updown(this.second, 60, -1, 59, 0, e));
						ml.$(this.milisecond).on('keydown', (e) => this.updown(this.milisecond, 1000, -1, 999, 0, e));	
						
						ml.$(this.hours).on('keydown', (e) => this.inputBlur(this.hours, e));
						ml.$(this.minute).on('keydown', (e) => this.inputBlur(this.minute, e));
						ml.$(this.second).on('keydown', (e) => this.inputBlur(this.second, e));
						ml.$(this.milisecond).on('keydown', (e) => this.inputBlur(this.milisecond, e));					
					
						if(options.showActionsBar) 
						{
						if(this.chekonce2 == false){
							this.buttonBar = ml.$('<div class="ml-blueprint-button-bar"></div>');
							this.chekonce2 = true;							
						}
						
						this.newDate.append(this.buttonBar);
						
						this.buttonToday.on('click', (e) => {
							var dateForm: string = ml.$.datepicker.formatDate('DD, MM d, yy', new Date());
							ml.$( ".ml-blueprint-datePicker" ).datepicker( "setDate", dateForm );
							ml.$(selectDate).css('background', '#137cbd');
							ml.$(dateText).css('color', 'white');
							
							var time: Date = new Date();
							ml.$(this.hours).val(time.getHours());
							ml.$(this.minute).val(time.getMinutes());
							ml.$(this.second).val(time.getSeconds());
							ml.$(this.milisecond).val(time.getMilliseconds());
							
							if(options.Precision) {
								this.dateText.text(`${dateForm} ${time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })}`);
								obs(dateText.text())
								if(options.Precision == BPBase.Precision.Minute) {this.leadingZerosSecond(this.minute, e)}
								if(options.Precision == BPBase.Precision.Second) {
									this.leadingZerosSecond(this.second, e)}
									this.leadingZerosSecond(this.minute, e);

								if(options.Precision == BPBase.Precision.Milisecond) {
									this.leadingZeros(this.milisecond, e);
									this.leadingZerosSecond(this.second, e);
									this.leadingZerosSecond(this.minute, e);
								}
							}
							else
							{
								this.dateText.text(dateForm);
								obs(dateText.text())
								this.chekonce2 = false;
							}
						});
						if(options.canClearSelection)
						{	
							if(!this.chekonce) {
								this.buttonClear = ml.$('<button class="ml-blueprint-button-clear">Clear</button>');
								this.chekonce = true;
							}						

							this.buttonClear.on('click', ()=> {
								ml.$('.mlui-datepicker-current-day').removeClass('mlui-datepicker-current-day');
			
								this.dateText.text('no date');					
				
								if(this.dateText.text('no date')) {
									ml.$(selectDate).css('background', 'rgba(167,182,194,.3)');
									ml.$(dateText).css('color', 'black');												
								};
								obs(dateText.text())
								ml.$(this.hours).val('0');
								ml.$(this.minute).val('00');
								ml.$(this.second).val('00');
								ml.$(this.milisecond).val('000');
							})
							this.buttonBar.append(this.buttonClear);
						} else {
							ml.$(this.buttonClear).detach();
							this.chekonce = false;
						}
						
						this.buttonBar.append(this.buttonToday)
					}
					else {
						ml.$(this.buttonBar).detach();
					}
					if(options.defaultValue) {
						ml.$('.ml-blueprint-datePicker').datepicker('setDate', options.defaultValue);
					}
				});
			}
			//called twice
			//1. To initialze the control but before it is loaded in the DOM (when this.isDOMLoaded == false)
			//2. After the control has been created in the DOM (when this.isDOMLoaded == true)
		}

		protected cleanup() {
			super.cleanup();
            //called just before the control gets destroyed
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPDatePicker.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPDatePicker {

		/**
		 * Props to work with the date picker
		 * @typedef IDatePicker
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 * @augments BPBase.ITimePickerProps
		 */
		export interface IDatePicker extends BPBase.IControlProps, BPBase.ITimePickerProps{
			/**
			 * Minimum date on the calendar
			 * @type {string | Date}
			 */
			min_date?: string | Date,
			/**
			 * Maximum date on the calendar
			 * @type {string}
			 */
			max_date?: string,
			/**
			 * Year range to display in the dropdown for years on the calendar i.e. '1990:2020'
			 * @type {string}
			 */
			year_range?: string,
			/**
			 * Show a bar with action on the calendar
			 * @type {boolean}
			 */
			showActionsBar?: boolean,
			/**
			 * Reverse the places of the month and year dropdowns
			 * @type {boolean}
			 */
			reverseMonthAndYearMenus?: boolean,
			/**
			 * Show a clear button,which clears the date
			 * @type {boolean}
			 */
			canClearSelection?: boolean,
			/**
			 * Default date to display on the calendar
			 * @type {Date}
			 */
			defaultValue?: Date
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IDatePicker{
            //mirrors IOptionsDefinition, but contains real values, whereas IOptionsDefinition can contain path's to values (scopePath)            
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			//options that can be passed to the control when created
			Precision?: BPBase.Precision | mdash.IScopePath,
			min_date?: string | Date | mdash.IScopePath,
			max_date?:  string | mdash.IScopePath,
			year_range?: string | mdash.IScopePath,
			showActionsBar?: boolean | mdash.IScopePath,
			reverseMonthAndYearMenus?: boolean | mdash.IScopePath,
			canClearSelection?: boolean | mdash.IScopePath,
			defaultValue?: Date | mdash.IScopePath
		}
	}

	mdash.registerControl(BPDatePicker,'dashboard.components.controls', BPDatePicker.meta);
}