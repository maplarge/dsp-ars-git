// It probably should be a composite control since it's a container. 
// The blueprint versions of form controls like textbox don't exist yet.

namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPContextMenu extends mdash.BaseControl<BPContextMenu.IOptionsDefinition> {
		public static type: string = 'BPContextMenu';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Context Menu',
			shortDescription: "The ContextMenu enables you to customize its content, items, and the content of its items by using templates.",
			categories: ["Visualization"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPContextMenu.builder().children(children).toJSON();
		}

		public static configSchema: mdash.IComponentConfigSchema = { 
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected contextMenu: JQuery;
		protected contextTab1: JQuery;
		protected contextTab2: JQuery;


		constructor(id: string, definition: mdash.IControlDefinition<BPContextMenu.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		/**
		 * @inheritDoc
		 * */
		public build() {
			super.build();
			this.container.classList.add('namespace-blueprint-contextMenu');
			this.contextMenu = ml.$('<div></div>')
			this.contextMenu.addClass('ml-blueprint-context-menu-node')
			this.contextTab1 = ml.$(`<div class="ml-blueprint-popover-content"></div>`)
			this.contextTab2 = ml.$(`<div class="ml-blueprint-popover-content"></div>`)
			this.contentDiv.append(this.contextMenu, this.contextTab1, this.contextTab2);  
			this.contentDiv.append('<span class="ml-blueprint-text-muted">Right-click on node or background.</span>')

		}


		/**
		 * @inheritDoc
		 * */
		protected validateDefinition(): boolean {
			if (!super.validateDefinition())
				return false;
			return true;
		}
		
		/**
		 * @inheritDoc
		 * */
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, null, false);
			
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPContextMenu.IOptions) => {
					this.contextMenu.children('i').removeClass()
					this.contextMenu.append(`<i class='mlicon-${options.icon}'></i>`)
					var namespaceClass:string = '.namespace-blueprint-contextMenu'
					var overlayClass:string = '.ml-blueprint-submenu-overlay'

					ml.$(this.contextMenu).contextmenu(()=>{
						ml.$(this.contextMenu).addClass('ml-blueprint-context-menu-open')
					})
					ml.$(namespaceClass).click(()=>{
						ml.$(this.contextMenu).removeClass('ml-blueprint-context-menu-open')
					})

					ml.$('.ml-blueprint-node-menu').detach()
					ml.$('.ml-blueprint-other-menu').detach()

					var nodeClickList:{icon:string,name:string}[] = [
						{
							icon: 'MapLarge-icons_zoom-extents',
							name: 'Search around...'
						},
						{
							icon: 'x16Search', 
							name: 'Object viewer'
						},
						{
							icon: 'delete', 
							name: 'Remove'
						},
						{
							icon: 'user-group', 
							name: 'Group'
						},
					];

					var otherClickList:{icon:string, name:string, subMenu?:{icon:string, name:string}[] }[] = [
						{
							icon: 'selectshape', 
							name: 'Select all'
						},
						{
							icon: 'add210', 
							name: 'Insert...', 
							subMenu:[
								{icon: 'MapLarge-icons_add', name: 'Object'},
								{icon: 'MapLarge-icons_italic', name: 'Text box'},
								{icon: 'MapLarge-icons_save-layer', name: 'Astral body'},
							]
						},
						{
							icon: 'analytics5', 
							name: 'Leyout...',
							subMenu: [
								{icon: 'marketing8', name: 'Auto'},
								{icon: 'drawingtools-circle', name: 'Circle'},
								{icon: 'MapLarge-icons_table', name: 'Grid'},
							]
						},

					]
					
					var nodeClick:JQuery = ml.$('<ul class="ml-blueprint-node-menu"></ul>');
					var otherClick:JQuery = ml.$('<ul class="ml-blueprint-other-menu"></ul>');

					ml.$(this.contextTab1).append(nodeClick)
					ml.$(this.contextTab2).append(otherClick)

					ml.$.each(nodeClickList, (key:number, item:any) => {
						ml.$(nodeClick).append(`
							<li><a class='ml-blueprint-menu-item'><i class='mlicon-${item.icon}'></i><span class='ml-blueprint-text'>${item.name}</span></a></li>
						`)
					})
					ml.$(nodeClick).append(`
						<li class='ml-blueprint-menu-divider'></li>
						<li><a class='ml-blueprint-menu-item ml-blueprint-menu-item-disabled'>Clicked on node</a></li>
					`)

					
					ml.$.each(otherClickList,function (key:number, item:any) {
						if(!item.subMenu){
							ml.$(otherClick).append(`
								<li><a class='ml-blueprint-menu-item'><i class='mlicon-${item.icon}'></i><span class='ml-blueprint-text'>${item.name}</span></a></li>
							`)
						}
						else{
							ml.$(otherClick).append(`
								<li class='ml-blueprint-submenu'>
									<div class='ml-blueprint-popover-content ml-blueprint-submenu-overlay'><ul class='ml-blueprint-submenu-list-${key}'></ul></div>
									<a class='ml-blueprint-menu-item'><i class='mlicon-${item.icon}'></i><span class='ml-blueprint-text'>${item.name}</span><span><i class="mlicon-MapLarge-icons_layer-collapsed"></i></span></a>
								</li>`)
									
							item.subMenu.forEach(element => {
								ml.$(`.ml-blueprint-submenu-list-${key}`).append(
									`<li><a class='ml-blueprint-menu-item'><i class='mlicon-${element['icon']}'></i><span class='ml-blueprint-text'>${element['name']}</span></a></li>`
								)
							});
												
							
							}			
						})

						ml.$(overlayClass).hide()

						ml.$('.ml-blueprint-submenu').on('mouseover',function(){
							ml.$(overlayClass, this).show()
						})
						ml.$('.ml-blueprint-submenu').on('mouseout',function(){
							ml.$(overlayClass, this).hide()
						})

				

					ml.$(otherClick).append(`
						<li class='ml-blueprint-menu-divider'></li>
						<li><a class='ml-blueprint-menu-item ml-blueprint-menu-item-disabled'>Clicked at (0,0)</a></li>
					`)



					var $contextMenu1:JQuery = ml.$(this.contextTab1);
					$contextMenu1.hide();
					var $contextMenu2:JQuery = ml.$(this.contextTab2);
					$contextMenu2.hide();

					ml.$(this.contextMenu).on("contextmenu", (e:JQueryEventObject) => {
						$contextMenu2.hide();
						$contextMenu1.css({
							  display: "block",
							  left: e.offsetX,
							  top: e.offsetY
						 });
						 return false;
					});
					

					ml.$(namespaceClass).on("contextmenu", (e:JQueryEventObject) => {
						if(!ml.$(event.target).parentsUntil().hasClass('ml-blueprint-popover-content')){

							ml.$(this.contextMenu).removeClass('ml-blueprint-context-menu-open')

							var maxWidth:number = ml.$(namespaceClass).outerWidth()
							var maxHeight:number = ml.$(namespaceClass).outerHeight()

							var coordinate = ml.$('.ml-blueprint-other-menu .ml-blueprint-menu-item-disabled').text(`Clicked at (${e.pageX}, ${e.pageY})`)

							$contextMenu1.hide();
							$contextMenu2.css({
								 display: "block",
							});

						if(e.offsetX + 200 < maxWidth){
							$contextMenu2.css({
								left: e.offsetX,
								right: 'unset',
						   });
						  
						}
						else{
							$contextMenu2.css({
								right: maxWidth-e.offsetX,
								left: 'unset',
						   });
						 
						}

						if(e.offsetX + 420 < maxWidth){
							
						   ml.$(overlayClass).css({
							   'left':'175px',
							   'right': 'unset'
						   })
						}
						else{
							
						   ml.$(overlayClass).css({
							'left':'unset',
							'right': '175px'
							})
						}
						if(e.offsetX < maxWidth - e.offsetX - 175){
							
							ml.$(overlayClass).css({
								'left':'175px',
								'right': 'unset'
							})
						 }


						if(e.offsetY + 200 < maxHeight){
							$contextMenu2.css({
								top: e.offsetY,
								bottom: 'unset',
						   	});
						   	ml.$(overlayClass).css({
								'bottom':'unset',
							})
						}
						else{
							$contextMenu2.css({
								bottom: maxHeight-e.offsetY,
								top: 'unset',
						   	});
						   	ml.$(overlayClass).css({
								'bottom':'0px',
							})
						}

						return false;
						}
						else{
							return false
						}


					});
				
					ml.$(document).on('click',(event:JQueryEventObject):void =>{
						if(!ml.$(event.target).parentsUntil().hasClass('ml-blueprint-popover-content')){
							ml.$(this.contextMenu).removeClass('ml-blueprint-context-menu-open')
							$contextMenu1.hide();
							$contextMenu2.hide();
						}
					});

					ml.$('body').not(namespaceClass).on('contextmenu',()=>{
						if(!ml.$(event.target).parentsUntil().hasClass('ml-blueprint-popover-content')){
							ml.$(this.contextMenu).removeClass('ml-blueprint-context-menu-open')
							$contextMenu1.hide();
							$contextMenu2.hide();
						}
					})


			
				})
			}
		}

		/**
		 * @inheritDoc
		 * */
		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPContextMenu.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPContextMenu {

		export interface IContextMenuProps extends BPBase.IControlProps{
            
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IContextMenuProps{
            //mirrors IOptionsDefinition, but contains real values, whereas IOptionsDefinition can contain path's to values (scopePath)            
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            //options that can be passed to the control when created
			icon?: string | mdash.IScopePath,
            
		}
	}

	mdash.registerControl(BPContextMenu,'dashboard.components.controls', BPContextMenu.meta);
}