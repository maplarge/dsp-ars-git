namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;
	
	export namespace BPRadioGroup {

		/**
		 * @typedef IRadioGroup
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface IRadioGroup extends BPBase.IControlProps{
		}

		export interface IOptionsDefinition {
			radioData: BPRadioButton.IOptionsDefinition[] | mdash.IScopePath,
			large?: boolean | mdash.IScopePath,
			align?: BPBase.Alignment | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath
		}

		/**
		 * @typedef IOptions
		 * @interface
		 * @readonly
		 * @augments IRadioGroup
		 */
		export interface IOptions extends IRadioGroup{
			/**
			 * Options for an individual radio button inside a radio group
			 * @type {BPBase.IOptionProps[]}
			 */
			radioData: BPBase.IOptionProps[],
		}
	}

	export class BPRadioGroup extends mdash.controls.CompositeControl<BPRadioGroup.IOptionsDefinition, BPRadioGroup.IOptions> {
		public static type: string = 'BPRadioGroup';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.INTERNAL,
			friendlyName: 'Radio Group',
			shortDescription: "Radio buttons allow the user to select one option from a set",
			categories: ['Input'],
			childSupport: mdash.ChildSupport.SINGLE
		};

		constructor(id: string, definition: mdash.IControlDefinition<BPRadioGroup.IOptionsDefinition>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		private initializeListeners(valueObs: mdash.RefCountedObservable<any>) {
			if(this.isDOMLoaded)
			{
				var radio = this.contentDiv.find('input[type=radio]');
				radio
				.off('change')//remove any previous change event listeners
				.on('change', (event: JQueryEventObject) => { 
						const radioValue = ml.$(event.target).attr('value');
						if (radioValue != valueObs() || valueObs() == null) {
							valueObs(radioValue); //if new value of radio is different than current value of the control, update the value of the control
						}
					});

				//objectsToDispose is an array of listeners that get destroyed when the control is destroyed
				//add listeners to this.objectsToDispose in order to prevent memory leaks
				this.objectsToDispose.push(
				//listen and respond to changes of the value of the control and update the radio accordingly
					valueObs.subscribe(newValue => {
						if (valueObs() == null || this.contentDiv.find(`input:radio[value=${newValue}]`)) {
							radio.prop('checked', false);
							this.contentDiv.find(`input:radio[value=${newValue}]`).prop('checked', true);
						}
					})
				);
			}
		}

		/* Init is still an important function here, but because of the limited 
		 * functionality of this control specifically, we're not doing more than calling the parent  */
		protected init() {
			super.init();

			var obs = this.registerScopeObservable(this.definition.name, null, false);
			this.initializeListeners(obs);
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPRadioGroup.IOptions) => {
					let scopeVal: string = this.scope[this.definition.name]();
					if(ml.util.isNotNullUndefinedOrEmpty(scopeVal))
					{
						this.contentDiv.find(`input[value=${scopeVal}]`).prop('checked',true);
					}
                });
			}
		}

		/* This function is what largely separates a composite control from others.
		 * This is the function that defines the structure of the control. If you want it to show up in the DOM, it needs to go here.*/
		protected getControlLayout(options: BPRadioGroup.IOptions): mdash.IControlDefinition<any> { 

			//create a container for all the child controls
			return mdash.controls.Flexbox.builder()
				.options({
					justify: "Start",
				}).children([
					//a repeater is simply that: a control that repeats. Its 'data' field represents an array, and it repeats it's children for each element in the array
					mdash.controls.Repeater.builder().options({
						data: { $scopePath: "options.radioData" },
						layout: options.inline ? 'Horizontal' : 'Vertical' //'options' is a valid scope path since it is what's passed into the getControlLayout function
					}).children([
							//"item" is the default name for each element in the repeater's data array
							BPRadioButton.builder().options({
								label: {$scopePath: 'item.label'},
								value: {$scopePath: 'item.value'},
								large: {$scopePath: "options.large"},
                                align: {$scopePath: 'options.align'},
                                name: {$scopePath: (scope) => this.definition.name || this.definition.id },
                                checked: {$scopePath: 'item.checked'},
                                disabled: {$scopePath: 'options.disabled'},
							}),
						])
			]).toJSON(); //toJSON converts the controls into the form needed. dont forget it.
		}

		protected cleanup() {
			super.cleanup();
		}

		protected validateDefinition(): boolean {
			return super.validateDefinition();
		}	

		public static builder() {
			return new mdash.ControlDefinitionBuilder<BPRadioGroup.IOptionsDefinition>(this.type);
		}
	}
	mdash.registerControl(BPRadioGroup, 'dashboard.components.controls', BPRadioGroup.meta);
}  