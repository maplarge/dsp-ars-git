namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPHTMLSelect extends mdash.BaseInputControl<BPHTMLSelect.IOptionsDefinition> {
		public static type: string = 'BPHTMLSelect';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'HTML Select',
			shortDescription: "The select element is used to create a drop-down list.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPHTMLSelect.builder().options(BPHTMLSelect.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPHTMLSelect.IOptions> = {
			'BPHTMLSelect': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

        protected selectOption: HTMLElement;
        protected select: HTMLElement;
		protected option: JQuery;

		constructor(id: string, definition: mdash.IControlDefinition<BPHTMLSelect.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
            super.build();
            this.container.classList.add('namespace-blueprint-htmlSelect');
			this.selectOption = ml.create("div", "ml-BPHTMLSelect",  this.contentDiv[0]);
			this.selectOption.classList.add('ml-blueprint-select-div');
            this.select = ml.create("select", "ml-BPHTMLSelect",  this.selectOption);
            this.select.classList.add('ml-blueprint-select');            
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		private initializeListeners(valueObs: mdash.RefCountedObservable<any>) {
			ml.$(this.select).on('change', (event: JQueryEventObject) => { 
				if(ml.$(this.select).find(':selected').text() != valueObs() || valueObs() == null) {
					valueObs(ml.$(this.select).find(':selected').text());
				}
			});

			//objectsToDispose is an array of listeners that get destroyed when the control is destroyed
			this.objectsToDispose.push(
				valueObs.subscribe(newValue => {
					if (newValue.toLowerCase() != ml.$(this.select).find(':selected').text().toLowerCase()) {
						ml.$(this.select).find(':selected').prop('selected', false);
						ml.$(this.select).children().each((index, item) => {
							if(item.textContent.toLowerCase() == newValue.toLowerCase())
							{
								ml.$(item).prop('selected', true);
								valueObs(newValue);
							}
						})
					}
				})
			)
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			this.initTransforms();
			this.initializeListeners(obs);
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPHTMLSelect.IOptions) => {
					
                    if(options.options)
                    {
						ml.$(this.select).children().detach();
						
						let items = options.options.map(m => {
							return {
								value: m[options.valueField || "value"],
								label: m[options.labelField || "label"] 
							}
						});
						
						if(options.sortFunction)
						{
							items = items.sort(options.sortFunction);
						}
						else
						{
							items = items.sort((a:any, b:any) => {
								const labelA = a.label.toLowerCase();
								const labelB = b.label.toLowerCase();
								if (labelA < labelB)
									return -1;
								if (labelA > labelB)
									return 1;
								return 0;
							} );
						}

                        items.forEach((opt) => {
                        	ml.$(this.select).append(`<option class="ml-blueprint-option" value="${opt.value}">${opt.label}</option>`);
                        })
						
						if(obs()){
							ml.$(this.select).children().each((index, item) => {
								if(item.textContent.toLowerCase() == obs().toLowerCase())
								{
									ml.$(item).prop('selected', true);
								}
							})
						}
					}


                    if(options.large)
                    {
                        this.select.classList.add('ml-blueprint-select-large');
					}
					else
					{
						this.select.classList.remove('ml-blueprint-select-large');
					}

                    if(options.fill)
                    {
                        this.select.classList.add('ml-blueprint-select-fill');
					}
					else
					{
						this.select.classList.remove('ml-blueprint-select-fill');
					}

					if(options.minimal)
                    {
                        this.select.classList.add('ml-blueprint-select-minimal');
					}
					else
                    {
                        this.select.classList.remove('ml-blueprint-select-minimal');
					}

					if(options.disabled)
                    {
						this.select.setAttribute('disabled', 'true');
						this.select.classList.add('ml-blueprint-select-disabled');
					}
					else
					{
						this.select.removeAttribute('disabled');
						this.select.classList.remove('ml-blueprint-select-disabled');
					}
					
					if(options.onChange)
					{
						options.onChange(ml.$(this.select).find(':selected').text(), this.scope);
						ml.$(this.select).on('change', () => options.onChange(ml.$(this.select).find(':selected').text(), this.scope));
						obs(ml.$(this.select).find(':selected').text());
					}
				});
			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPHTMLSelect.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPHTMLSelect {

		/**
		 * Props to work with HTML Select
		 * @typedef IHTMLSelectProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface IHTMLSelectProps extends BPBase.IControlProps{
			/**
			 * Enable or disable interaction with the dropdown
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Object with options provided for the dropdown
			 * @type {BPBase.IOptionProps[]}
			 */
			options?: BPBase.IOptionProps[],
			/**
			 * The name of the value field in the array of items passed to the HTMLSelect
			 * @type {string}
			 */
			valueField?: string,
			/**
			 * The name of the label field in the array of items passed to the HTMLSelect
			 * @type {string}
			 */
			labelField?: string,
			/**
			 * Custom sorting function for the data inside the HTMLSelect
			 * @function
			 * @param {any} a - First item for comparison
			 * @param {any} b - Second item for comparison 
			 * @return {number}
			 */
			sortFunction?: (a: any, b: any) => number
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IHTMLSelectProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            disabled?: boolean | mdash.IScopePath,
			options?: BPBase.IOptionProps[] | mdash.IScopePath,
			fill?: boolean | mdash.IScopePath,
			minimal?: boolean | mdash.IScopePath,
			large?: boolean | mdash.IScopePath,
			onChange?: (value: string, scope: mdash.IScope) => void | mdash.IScopePath,
			valueField?: string | mdash.IScopePath,
			labelField?: string | mdash.IScopePath,
			sortFunction?: (a: any, b: any) => number
		}
	}

	mdash.registerControl(BPHTMLSelect,'dashboard.components.controls', BPHTMLSelect.meta);
}