namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPSkeleton extends mdash.BaseControl<BPSkeleton.IOptionsDefinition> {
		public static type: string = 'BPSkeleton';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Skeleton',
			shortDescription: "The skeleton is composed of fibrous and mineralized connective tissues that give it firmness and flexibility",
			categories: ["Visualization", 'Input'],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPSkeleton.builder().options(BPSkeleton.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPSkeleton.IOptions> = {
			'BPSkeleton': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label:'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label:'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		private card: JQuery;	
		private once: boolean;

		constructor(id: string, definition: mdash.IControlDefinition<BPSkeleton.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();

            this.container.classList.add('namespace-blueprint-skeleton');		
			this.card = ml.$('<div></div>');
			this.card.addClass('ml-blueprint-card');
			this.contentDiv.append(this.card);	
		
		}

		
		protected validateDefinition(): boolean {
			
			if (!super.validateDefinition())
			return false;
			
			return true;
		}
		
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, false, false);			
			
			if (this.isDOMLoaded) {
				var heading:JQuery = ml.$('<h5 class="ml-blueprint-heading"></h5>')
				var link:JQuery = ml.$('<a href="#"></a>')
				var text:JQuery = ml.$('<p></p>')
				var button: JQuery = ml.$('<button> Submit</button>')
				var skeletonClass:string = 'ml-blueprint-skeleton'
				heading.append(link)
				this.card.append(heading, text, button)

				this.watchDefinitionOptions((options: BPSkeleton.IOptions) => {
					link.html(options.title)
					text.html(options.text)
					this.once = false;
					button.removeClass()
					button.addClass(`ml-blueprint-button mlicon-${options.icon}`)
					

					if(options.skeleton){
						ml.$(link).addClass(skeletonClass)
						ml.$(text).addClass(skeletonClass)
						ml.$(button).addClass(skeletonClass)
					}
					else{
						ml.$(link).removeClass(skeletonClass)
						ml.$(text).removeClass(skeletonClass)
						ml.$(button).removeClass(skeletonClass)
					}

				});

			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPSkeleton.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPSkeleton {

		/**
		 * Interface for props on Skeleton component
		 * @typedef ISkeletonProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface ISkeletonProps extends BPBase.IControlProps{
			/**
			 * Title text
			 * @type {string}
			 */
			title?: string;
			/**
			 * Content text
			 * @type {string}
			 */
			text?: string;
			/**
			 * Skeleton style(hide the element content)
			 */
			skeleton?: boolean;
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ISkeletonProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			title?: string | mdash.IScopePath,
			text?: string | mdash.IScopePath,
			icon?: string | mdash.IScopePath,
			skeleton?: boolean | mdash.IScopePath,
		}
	}

	mdash.registerControl(BPSkeleton,'dashboard.components.controls', BPSkeleton.meta);
}