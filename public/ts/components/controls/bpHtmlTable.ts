namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPHtmlTable extends mdash.BaseControl<BPHtmlTable.IOptionsDefinition> {
		public static type: string = 'BPHtmlTable';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'HTML Table',
			shortDescription: "The HTML tables allow web authors to arrange data like text, images, links, other tables, etc. into rows and columns of cells.",
			categories: ["Visualization"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPHtmlTable.builder().options(BPHtmlTable.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPHtmlTable.IOptions> = {
			'BPHtmlTable': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label:'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label:'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected table: JQuery;
		protected thead: JQuery;
		protected tbody: JQuery;

		constructor(id: string, definition: mdash.IControlDefinition<BPHtmlTable.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();

			this.container.classList.add('namespace-blueprint-table');		
			this.table = ml.$('<table class="ml-blueprint-html-table"></table>');
			this.thead = ml.$('<thead><tr></tr></thead>');
			this.tbody = ml.$('<tbody></tbody>');
			this.table.append(this.thead, this.tbody)
			this.contentDiv.append(this.table);
		
		}
		
		protected validateDefinition(): boolean {
			
			if (!super.validateDefinition())
			return false;
			
			return true;
		}
		
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, null, false);			
			
			if (this.isDOMLoaded) {
				
				this.watchDefinitionOptions((options: BPHtmlTable.IOptions) => {

					ml.$('tr',this.thead).html('')
					this.tbody.html('')
					for(let i = 0; i < options.dataTable.thead.length; i++){
						ml.$('tr',this.thead).append(`<th>${options.dataTable.thead[i]}</th>`)
					}
					for(let i = 0; i < options.dataTable.tbody.length; i++){
						var tr:JQuery = ml.$('<tr></tr>');
						ml.$(this.tbody).append(tr)
						for(let j = 0; j < options.dataTable.tbody[i].length; j++){
							ml.$(tr).append(`<td>${options.dataTable.tbody[i][j]}</td>`)
						}
					}

					(options.condensed) ? this.table.addClass('ml-blueprint-condensed') : this.table.removeClass('ml-blueprint-condensed');
					(options.striped) ? this.table.addClass('ml-blueprint-striped') : this.table.removeClass('ml-blueprint-striped');
					(options.bordered) ? this.table.addClass('ml-blueprint-bordered') : this.table.removeClass('ml-blueprint-bordered');
					(options.interactive) ? this.table.addClass('ml-blueprint-interactive') : this.table.removeClass('ml-blueprint-interactive');

				});

			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPHtmlTable.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPHtmlTable {

		/**
		 * Props for HTML Table
		 * @typedef IHtmlTableProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface IHtmlTableProps extends BPBase.IControlProps{
			/**
			 * Structure for the table
			 * @typedef dataTable
			 * @property {string[]} thead - Table heads array
			 * @property {string[][]} tbody - Table body array
			 */
			dataTable?: {thead:string[],tbody: string[][]};
			/**
			 * Condensed CSS styling for table
			 * @type {boolean}
			 */
			condensed?: boolean;
			/**
			 * Adds stripes on the table
			 * @type {boolean}
			 */		
			striped?: boolean;
			/**
			 * Adds borders for table columns
			 * @type {boolean}
			 */
			bordered?: boolean;		
			/**
			 * Adds mouse hover interactivity with table data
			 * @type {boolean}
			 */
			interactive?: boolean;		

		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IHtmlTableProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			dataTable?: {thead:string[],tbody: string[][]} | mdash.IScopePath ,	
			condensed?: boolean | mdash.IScopePath;		
			striped?: boolean | mdash.IScopePath;		
			bordered?: boolean | mdash.IScopePath;		
			interactive?: boolean | mdash.IScopePath;		
		}
	}

	mdash.registerControl(BPHtmlTable,'dashboard.components.controls', BPHtmlTable.meta);
}