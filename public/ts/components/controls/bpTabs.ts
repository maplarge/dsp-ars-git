namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPTabs extends mdash.BaseControl<BPTabs.IOptionsDefinition> {
		public static type: string = 'BPTabs';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Tabs',
			shortDescription: "Tabs organize content across different screens, data sets, and other interactions.",
			categories: ["Visualization"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPTabs.builder().options(BPTabs.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPTabs.IOptions> = {
			'BPTabs': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}
		
		protected navBar: JQuery;
		protected navBarHeading: JQuery;
		protected navBarTabs: JQuery;
		protected navBarTabsList: JQuery;
		protected content: JQuery;
		protected tabs: JQuery;
		protected tabList: JQuery;
		protected tabInput: JQuery;
		protected tabContent: JQuery;
		protected indicator: JQuery;


		constructor(id: string, definition: mdash.IControlDefinition<BPTabs.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();

			this.container.classList.add('namespace-blueprint-tabs');
			this.navBar = ml.$('<div class="ml-blueprint-navbar"></div>');
			this.tabs = ml.$('<div class="ml-blueprint-tabs"></div>');
			this.tabContent = ml.$('<div class="ml-blueprint-tab-main-content"></div>');
			this.content = ml.$('<div class="ml-bluerint-content"></div>');
			this.navBarTabsList = ml.$('<div class="ml-blueprint-navbar-tabs-list"></div>');
			this.navBarHeading = ml.$('<div class="ml-blueprint-navbar-heading">Current Page: <strong></strong></div>');
			this.navBarTabs = ml.$(`<div class="ml-blueprint-navbar-tabs"></div>`);
			this.tabInput = ml.$('<div class="ml-blueprint-tab-input"><input class="ml-blueprint-input" placeholder="Search..."/></div>');
			this.tabList = ml.$('<div class="ml-blueprint-tab-list"></div>');
			this.tabs.append(this.tabList);
			this.navBar.append(this.navBarHeading, this.navBarTabs);
			this.content.append( this.tabs, this.tabContent);
			this.contentDiv.append(this.content);

		}

		protected validateDefinition(): boolean {
			
			if (!super.validateDefinition())
			return false;
			
			return true;
		}
		
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, null, false);
			
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPTabs.IOptions) => {
					this.navBarTabsList.html('')
					this.tabList.html('')
					this.tabContent.html('')

					if(options.searchInput){
						this.tabs.append(this.tabInput);
					}
					else
					{
						if(this.tabInput) this.tabInput.detach();
					}
	
					if(options.navbarTabsList)
					{
						this.contentDiv.prepend(this.navBar);
						ml.$.each(options.navbarTabsList, (id,it)=>{
							if(it.disabled){
								this.navBarTabsList.append(`<div data-tab="${it.name}" aria-disabled='${it.disabled}' class="ml-blueprint-navbar-tab">${it.name}</div>`)
							}
							else{
								this.navBarTabsList.append(`<div data-tab="${it.name}" class="ml-blueprint-navbar-tab">${it.name}</div>`)
							}
						})
						this.navBarTabs.append(this.navBarTabsList)
						ml.$('.ml-blueprint-navbar-tab').first().attr('aria-selected', 'true')
						ml.$('strong', this.navBarHeading).text(ml.$('.ml-blueprint-navbar-tab').first().text())
						ml.$('.ml-blueprint-navbar-tab').on('click',function():void{
							ml.$('.ml-blueprint-navbar-tab').removeAttr('aria-selected');
							ml.$(this).attr('aria-selected', 'true')
							ml.$('strong', this.navBarHeading).text(ml.$(this).text())
						})
					}

					ml.$.each(options.tabList, (id,it)=>{
						let { disabled, name } = it;
						disabled = mdash.isIScopePath(disabled) ? this.getScopeValue(disabled.$scopePath) : disabled;
						name = mdash.isIScopePath(name) ? this.getScopeValue(name.$scopePath) : name;
						if(disabled){
							this.tabList.append(`<div data-tab="#content-${id}" aria-disabled='${disabled}' class="ml-blueprint-tab">${name}</div>`)
						}
						else{
							this.tabList.append(`<div data-tab="#content-${id}" class="ml-blueprint-tab">${name}</div>`)
						}
					
					})
					this.tabContent.append(`
						<div class="ml-blueprint-tab-content" id="content-0">
							<h3>${mdash.isIScopePath(options.tabList[0].body.title) ? this.getScopeValue(options.tabList[0].body.title) : options.tabList[0].body.title}</h3>
							<p>${mdash.isIScopePath(options.tabList[0].body.text) ? this.getScopeValue(options.tabList[0].body.text) : options.tabList[0].body.text}</p>
						</div>`
					);
					this.tabContent.children()[0].classList.add("activeTab")
					ml.$('.ml-blueprint-tab').first().attr('aria-selected', 'true')
				
					if(options.vertical){
						this.content.addClass('ml-blueprint-content-vertical')
					}
					else{
						this.content.removeClass('ml-blueprint-content-vertical')
					}

					
					if(options.activePanelOnly){
						const that: this = this;
						this.content.addClass('ml-blueprint-content-active-panel')
						var tabContent = this.tabContent
						ml.$('.ml-blueprint-tab').on('click',function():void{
							ml.$(tabContent).html('')
							let bodyTitle: string = '';
							let bodyText: string = '';
							const isBodyTitleScoped: boolean = mdash.isIScopePath(options.tabList[ml.$(this).attr('data-tab').split('-')[1]].body.title);
							const isBodyTextScoped: boolean = mdash.isIScopePath(options.tabList[ml.$(this).attr('data-tab').split('-')[1]].body.text);
							
							if(isBodyTitleScoped)
							{
								bodyTitle = that.getScopeValue(options.tabList[ml.$(this).attr('data-tab').split('-')[1]].body.title.$scopePath);
							}
							else
							{
								bodyTitle = options.tabList[ml.$(this).attr('data-tab').split('-')[1]].body.title;
							}

							if(isBodyTextScoped)
							{
								bodyText = that.getScopeValue(options.tabList[ml.$(this).attr('data-tab').split('-')[1]].body.text.$scopePath);
							}
							else
							{
								bodyText = options.tabList[ml.$(this).attr('data-tab').split('-')[1]].body.text;
							}

							tabContent.append(`
							<div class="ml-blueprint-tab-content" id="content">
								<h3>${bodyTitle}</h3>
								<p>${bodyText}</p>
							</div>`
							);
							ml.$('.ml-blueprint-tab').removeAttr('aria-selected');
							ml.$(this).attr('aria-selected', 'true');
						})
					}
					else{
						ml.$('.ml-blueprint-tab').on('click',function():void{
							Array.prototype.map.call(document.querySelectorAll('.ml-blueprint-tab-main-content > div'), elm=>{
								elm.classList.remove("activeTab")
								document.querySelector(this.getAttribute("data-tab")).classList.add("activeTab")
							}) ;
							ml.$('.ml-blueprint-tab').removeAttr('aria-selected');
							ml.$(this).attr('aria-selected', 'true')
						})

						this.content.removeClass('ml-blueprint-content-active-panel')
						ml.$.each(options.tabList, (id,it)=>{
							if(it.body && id != 0){
								this.tabContent.append(`
								<div class="ml-blueprint-tab-content" id="content-${id}">
									<h3>${mdash.isIScopePath(it.body.title) ? this.getScopeValue(it.body.title.$scopePath) : it.body.title }</h3>
									<p>${mdash.isIScopePath(it.body.text) ? this.getScopeValue(it.body.text.$scopePath) : it.body.text }</p>
								</div>`)
							}
						})
					}

					if(options.animate){
						var indicator: JQuery[] = [];
						for(var i = 0; i < 2; i++) {
							this.indicator = ml.$('<span class="ml-blueprint-indicator-wrapper"></span>')
							indicator.push(this.indicator)
						}
						 
						this.navBarTabsList.addClass('ml-blueprint-animate').prepend(indicator[0])
						this.tabList.addClass('ml-blueprint-animate').prepend(indicator[1])

						var self:this = this;
						var tab:HTMLElement = document.querySelector('.ml-blueprint-tab') 
						var navTab:HTMLElement = document.querySelector('.ml-blueprint-navbar-tab') 

						if(options.vertical){
							ml.$('.ml-blueprint-indicator-wrapper', self.tabList).css({
								top:tab.getBoundingClientRect().top - self.tabList[0].getBoundingClientRect().top +"px",
								height:tab.getBoundingClientRect().height+"px",
							})

							ml.$('.ml-blueprint-tab').on('click',function(){
								ml.$('.ml-blueprint-indicator-wrapper', self.tabList).css({
									top:this.getBoundingClientRect().top - self.tabList[0].getBoundingClientRect().top +"px",
									height:this.getBoundingClientRect().height+"px"
								})
							})

						}
						ml.$('.ml-blueprint-indicator-wrapper', self.tabList).css({
							left:tab.getBoundingClientRect().left - self.tabList[0].getBoundingClientRect().left+'px',
							width:tab.getBoundingClientRect().width+"px",
						})

						if(options.navbarTabsList)
						{
							ml.$('.ml-blueprint-indicator-wrapper', self.navBarTabsList).css({
								left:navTab.getBoundingClientRect().left - self.navBarTabsList[0].getBoundingClientRect().left +'px',
								width:navTab.getBoundingClientRect().width+"px",
							})
						}

						ml.$('.ml-blueprint-tab').on('click',function(){
							ml.$('.ml-blueprint-indicator-wrapper', self.tabList).css({
								left:this.getBoundingClientRect().left - self.tabList[0].getBoundingClientRect().left + 'px',
								width:this.getBoundingClientRect().width+"px",
							})
						})

						if(options.navbarTabsList)
						{
							ml.$('.ml-blueprint-navbar-tab').on('click',function(){
								ml.$('.ml-blueprint-indicator-wrapper', self.navBarTabsList).css({
									left:this.getBoundingClientRect().left - self.navBarTabsList[0].getBoundingClientRect().left +'px',
									width:this.getBoundingClientRect().width+"px",
								})
							})
						}

					}
					else{
						if(options.navbarTabsList) this.navBarTabsList.removeClass('ml-blueprint-animate')
						this.tabList.removeClass('ml-blueprint-animate')
						indicator = [];
					}
				});
			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPTabs.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPTabs {

		/**
		 * Tablist interface for an object that contains Tab information
		 * @typedef ITabList
		 * @interface
		 * @readonly
		 */
		export interface ITabList{
			/**
			 * Tab name
			 * @type {string}
			 */
			name: string,
			/**
			 * Content for the tab body
			 * @typedef body
			 * @param {string} title - Tab body title
			 * @param {string} text - Tab body text
			 */
			body?: {
				/**
				 * Body title
				 * @type {string}
				 */
				title?: string,
				/**
				 * Body text
				 * @type {string}
				 */
				text?: string
			},
			/**
			 * Tab disabled/enabled
			 * @type {boolean}
			 */
			disabled?: boolean | mdash.IScopePath,
		}

		/**
		 * Tabs component interface with properties
		 * @typedef ITabsProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface ITabsProps extends BPBase.IControlProps{
			/**
			 * List of tabs with their names
			 * @typedef navBarTabsList[]
			 * @param {string} name - Name of the tab
			 * @param {boolean} disabled - Disable the tab
			 */
			navbarTabsList?: {name: string, disabled?: boolean}[];
			/**
			 * Detailed information for tabs in an array of objects
			 * @type {ITabList[]}
			 */
			tabList?: ITabList[],
			/**
			 * Orientation of the tabs (vertical/horizontal)
			 * @type {boolean}
			 */
			vertical?: boolean;
			/**
			 * Enable animation for the tabs
			 * @type {boolean}
			 */
			animate?: boolean;
			/**
			 * Only create the DOM structures for the active panel
			 * @type {boolean}
			 */
			activePanelOnly?: boolean;
			searchInput?: boolean;
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ITabsProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			navbarTabsList?: {name: string, disabled?: boolean}[] | mdash.IScopePath ;
			tabList?: ITabList[] | mdash.IScopePath ;
			animate?: boolean | mdash.IScopePath ;
			vertical?: boolean | mdash.IScopePath ;
			activePanelOnly?: boolean | mdash.IScopePath;
			searchInput?: boolean | mdash.IScopePath
		}
	}

	mdash.registerControl(BPTabs, 'dashboard.components.controls', BPTabs.meta);
}