namespace dashboard.components.controls {

    import mdash = ml.ui.dashboard;

    export class BPDivider extends mdash.BaseControl<BPDivider.IOptionsDefinition> {
        public static type: string = 'BPDivider';

        public static meta: mdash.IControlMeta = {
            status: mdash.ControlReleaseStatus.ALPHA,
            level: mdash.ControlLevel.BASIC,
            friendlyName: 'Divider',
            shortDescription: "Divider visually separate contents with a thin line and margin on all sides.",
            categories: ["Visualization"],
            childSupport: mdash.ChildSupport.NONE
        };
        public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
            return BPDivider.builder().options(BPDivider.initialOptions.GenericOptions.options).children(children).toJSON();
        }

        public static initialOptions: mdash.IDashboardInitialOptions<BPDivider.IOptions> = {
            'BPDivider': {
                options: {
                    outputTransforms: []
                }
            }
        };

        public static configSchema: mdash.IComponentConfigSchema = {
            settings: [
                { path: 'validators', label:'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
                { path: 'outputTransforms', label:'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
            ]
        }

        private    div:JQuery;
        protected  buttonsLine:JQuery;
        private    button:JQuery;
        private    model:Object;
        constructor(id: string, definition: mdash.IControlDefinition<BPDivider.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
            super(id, definition, container, scope, parent);

        }

        public build() {
            super.build();

            this.container.classList.add('namespace-blueprint-divider');
            this.div =ml.$(`<div class="ml-blueprint-default"></div>`);
            this.buttonsLine = ml.$(`
                <div class="ml-blueprint-buttonLine"> </div>
            `)

            ml.$(this.div).append(this.buttonsLine);


            this.contentDiv.append(this.div);

        }

        protected validateDefinition(): boolean {


            if (!super.validateDefinition())
                return false;

            return true;
        }

        protected init() {
            super.init();
            var obs = this.registerScopeObservable(this.definition.name, false, false);

            if (this.isDOMLoaded) {

                 this.watchDefinitionOptions((options: BPDivider.IOptions) => {



                     if(options.button_data) {
                         ml.$('.ml-blueprint-btn-divider').detach()
                         Array.prototype.map.call(options.button_data, (element, index)=>{
                             // this.button = ml.create('button', "ml-BONavBar", this.div);
                                 this.button = ml.$(`
                                 <button class="ml-blueprint-btn-divider"><span class="ml-blueprint-span-divider">${options.button_data[index].label}</span></button>`)

                             if(options.vertical) {
                                 this.buttonsLine.addClass('ml-blueprint-vertical');
                             }else{
                                 this.buttonsLine.removeClass('ml-blueprint-vertical');
                             }
                             if(element.divider){
                                 this.button.addClass('ml-blueprint-divider-after');
                             }

                             ml.$(this.buttonsLine).prepend(this.button)

                         })
                     }


                     options.button_data.forEach((element)=> {
                         document.querySelectorAll('.ml-blueprint-btn-divider').forEach((elm:HTMLElement) => {
                             if(element.onClick) {
                                 elm.onclick = (e) => element.onClick(this.scope, e);
                                 console.log(elm);
                             }

                         })
                     })




                });

            }

        }

        protected cleanup() {
            super.cleanup();
        }

        public static builder() {
            return new mdash.ControlDefinitionBuilderWithChildren<BPDivider.IOptionsDefinition>(this.type);
        }
    }

    export namespace BPDivider {

        export interface IDividerData {
            label?: string;
            divider?:boolean;
            onClick?: (scope: mdash.IScope, event: MouseEvent) => void;
        }

        /**
         * Props for divider component
         * @typedef IDividerProps
         * @interfacef,
         * @readonly
         * @augments BPBase.IControlProps
         */
        export interface IDividerProps extends BPBase.IControlProps{
            /**
             * Show a divider
             * @type {boolean}
             */
            divider?:boolean
            /**
             * Object with buttons
             * @type {ctrl.BPBase.IDividerData[]}
             */
            button_data?: ctrl.BPDivider.IDividerData[],
            /**
             * Orientation of the component (vertical/horizontal)
             * @type {boolean}
             */
            vertical?: boolean
        }

        export interface IOptions extends mdash.IControlOptionsWithTransforms, IDividerProps{

        }

        export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            button_data?: ctrl.BPDivider.IDividerData[] | mdash.IScopePath;
            vertical ?: boolean | mdash.IScopePath;
            divider  ?: boolean | mdash.IScopePath;
        }
    }

    mdash.registerControl(BPDivider,'dashboard.components.controls', BPDivider.meta);
}