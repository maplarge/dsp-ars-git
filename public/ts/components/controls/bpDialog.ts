namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPDialog extends mdash.BaseControl<BPDialog.IOptionsDefinition> {
		public static type: string = 'BPDialog';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Dialog',
			shortDescription: "Dialogs inform users about a task and can contain critical information, require decisions, or involve multiple tasks.",
			categories: ["Visualization"],
			childSupport: mdash.ChildSupport.SINGLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPDialog.builder().options(BPDialog.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPDialog.IOptions> = {
			'BPDialog': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label:'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label:'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		private button: JQuery;
		private portal: JQuery;
		private conteiner: JQuery;
		private dialogHeader: JQuery;
		private dialogBody: JQuery;
		private dialogFooter: JQuery;
		private footerActions: JQuery;
		private icon:JQuery;
		private heading: JQuery;
		private closeButton: JQuery;
		private closeModal: Function;
		private dialog: JQuery;

		constructor(id: string, definition: mdash.IControlDefinition<BPDialog.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();

			this.container.classList.add('namespace-blueprint-dialog');		
			this.container.classList.add('namespace-blueprint-button');		
			this.button = ml.$('<button data-modal="dialog" class="ml-blueprint-button"></button>')
			this.portal = ml.$('<div class="ml-blueprint-dialog-portal"></div>')
			this.contentDiv.append(this.button);
			
			this.conteiner = ml.$('<div class="ml-blueprint-dialog-conteiner"></div>')
			this.dialog = ml.$('<div class="ml-blueprint-dialog" id="dialog"></div>')
			this.dialogHeader = ml.$('<div class="ml-blueprint-dialog-header"></div>')
			this.dialogBody = ml.$('<div class="ml-blueprint-dialog-body"></div>')
			this.dialogFooter = ml.$('<div class="ml-blueprint-dialog-footer"></div>')
			this.footerActions = ml.$('<div class="ml-blueprint-dialog-footer-actions"></div>')
			this.icon = ml.$('<i></i>')
			this.heading = ml.$('<h4 class="ml-blueprint-heading"></h4>')
			this.closeButton = ml.$('<button class="ml-blueprint-button mlicon-delete30"></button>')
			this.footerActions.append('<button class="ml-blueprint-button"></button>')
			this.footerActions.append('<button class="ml-blueprint-button ml-blueprint-button-intent-primary"></button>')
			this.dialogFooter.append(this.footerActions)
			this.dialogHeader.append(this.icon, this.heading, this.closeButton)
	
			this.portal.append( this.conteiner)
			this.conteiner.append(this.dialog)
			this.dialog.append(this.dialogHeader, this.dialogBody, this.dialogFooter)
			ml.$('body').append(this.portal)

			this.closeModal = () => {
				ml.$(this.conteiner).css({
					opacity: "0",
					transform: "scale(0.5)"
				})
				setTimeout(()=>{
					this.portal.removeClass("show")
				}, 200)
			}
			
			ml.$(this.button).on('click', ()=>{
				ml.$(this.conteiner).attr('style', "")
				this.portal.addClass("show")
			})
		
			ml.$(this.closeButton).on('click', () => this.closeModal())
			ml.$('button:first', this.footerActions).on('click', () => this.closeModal())
			
		
		}
		
		protected validateDefinition(): boolean {
			
			if (!super.validateDefinition())
			return false;
			
			return true;
		}
		
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, false, false);			
			
			if (this.isDOMLoaded) {
				
				this.watchDefinitionOptions((options: BPDialog.IOptions) => {
					this.button.html(options.buttonText || "Show Dialog")
					this.icon.removeClass()
					this.icon.addClass(`mlicon-${options.icon}`)
					this.heading.text(options.heading)
					this.dialogBody.html(`<p>${options.text}</p>`)

					if(options.closeButton){
						ml.$('button:first', this.footerActions).text(options.closeButton)
					}
					else{
						ml.$('button:first', this.footerActions).detach()
					}
					if(options.submitButton){
						ml.$('button:last', this.footerActions).text(options.submitButton)

						if(options.onClick){
							ml.$('button:last', this.footerActions).on('click',(e) => options.onClick(this.scope, e));
						}
						else{
							ml.$('button:last', this.footerActions).onclick = null;
						}
					}
					else{
						ml.$('button:last', this.footerActions).detach()
					}

					if(options.canOutsideClickClose){
						ml.$('body').on('click',(e:JQueryEventObject)=>{
							if(!ml.$(event.target).hasClass('ml-blueprint-button') && !(e.srcElement as HTMLElement).closest('.ml-blueprint-dialog')){
									this.closeModal()
							}
						})
					}
					else{
						ml.$('body').off('click')
					}

					if(options.canEscapeKeyClose){
						ml.$('body').on('keydown', (e:JQueryEventObject):void=>{
							if(e.which == 27){
								this.closeModal()
							}
						})
					}
					else{
						ml.$('body').off('keydown')
					}
	

				});

			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPDialog.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPDialog {

		/**
		 * Interface for properties on Dialog component
		 * @typedef IDialogProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface IDialogProps extends BPBase.IControlProps{
			/**
			 * Text for heading of the dialog
			 * @type {string}
			 */
			heading?: string;
			/**
			 * CSS class for the close icon
			 * @type {string}
			 */
			closeIcon?: string;
			/**
			 * Text inside the dialogue
			 * @type {string}
			 */
			text?: string;
			/**
			 * Text for the first button on modal footer
			 * @type {string}
			 */
			closeButton?: string;
			/**
			 * Text for the second button on modal footer
			 * @type {string}
			 */
			submitButton?: string;
			/**
			 * Ability to click on the modal backdrop and close it
			 * @type {boolean}
			 */
			canOutsideClickClose?: boolean;
			/**
			 * Ability to close the modal by pressing the ESC button on keyboard
			 * @type {boolean}
			 */
			canEscapeKeyClose?: boolean;
			/**
			 * Event handler for click event on buttons
			 * @function
			 * @param {IScope} scope - The current scope
			 * @param {MouseEvent} event - Click event object
			 * @event click
			 * @returns {void}
			 */
			onClick?: (scope: mdash.IScope, event: JQueryEventObject) => void ;
			buttonText?: string;
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IDialogProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			icon?: string | mdash.IScopePath,
			buttonText?: string | mdash.IScopePath,
			closeIcon?: string | mdash.IScopePath,
			heading?: string | mdash.IScopePath,
			text?: string | mdash.IScopePath,
			closeButton?: string | mdash.IScopePath,
			submitButton?: string | mdash.IScopePath,
			canOutsideClickClose?: boolean | mdash.IScopePath;
			canEscapeKeyClose?: boolean | mdash.IScopePath;
			onClick?: (scope: mdash.IScope, event: JQueryEventObject) => void | mdash.IScopePath

		}
	}

	mdash.registerControl(BPDialog,'dashboard.components.controls', BPDialog.meta);
}