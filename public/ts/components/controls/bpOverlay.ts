namespace dashboard.components.controls {

    import mdash = ml.ui.dashboard;

    export class BPOverlay extends mdash.BaseControl<BPOverlay.IOptionsDefinition> {
        public static type: string = 'BPOverlay';

        public static meta: mdash.IControlMeta = {
            status: mdash.ControlReleaseStatus.ALPHA,
            level: mdash.ControlLevel.BASIC,
            friendlyName: 'Overlay',
            shortDescription: "Overlay is a generic low-level component for rendering content on top of its siblings, or above the entire application.",
            categories: ["Layout"],
            childSupport: mdash.ChildSupport.MULTIPLE
        };
        public static initialOptions: mdash.IDashboardInitialOptions<BPOverlay.IOptions> = {
            'BPOverlay': {
                options: {
                    outputTransforms: []
                }
            }
        };
        public static configSchema: mdash.IComponentConfigSchema = {
            settings: [
                {
                    path: 'validators',
                    label: 'Validators',
                    type: mdash.IComponentConfigSettingTypes.VALIDATOR,
                    isArray: true
                },
                {
                    path: 'outputTransforms',
                    label: 'Transforms',
                    type: mdash.IComponentConfigSettingTypes.TRANSFORM,
                    isArray: true,
                    transformSources: ['Text']
                },
            ]
        }
        private modal: JQuery;
        private button: JQuery;

        constructor(id: string, definition: mdash.IControlDefinition<BPOverlay.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
            super(id, definition, container, scope, parent);

        }

        public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
            return BPOverlay.builder().options(BPOverlay.initialOptions.GenericOptions.options).toJSON();
        }

        public static builder() {
            return new mdash.ControlDefinitionBuilderWithChildren<BPOverlay.IOptionsDefinition>(this.type);
        }

        public build() {

            super.build();
            this.container.classList.add('namespace-blueprint-overley');
            this.button = ml.$(`<button id="ml-blueprint-myBtn">Open Modal</button>`)
            this.modal = ml.$(`
            <!--<div id="myModal-l" class="modal-l">-->
            <div id="ml-blueprint-myModal" class="modal mod">
            <!-- Modal content -->
            <div class="ml-blueprint-modal-content">
                <div class="ml-blueprint-modal-header">
            
                    <h2 id="ml-blueprint-title"></h2>
                </div>
                <div class="ml-blueprint-modal-body">
                    <p class="ml-blueprint-modal-body-paragraf"> </p>
                    <p class="ml-blueprint-modal-body-paragraf"></p>
                </div>
                <div class="ml-blueprint-modal-footer">
                    <button id="close" class="ml-blueprint-close-btn"></button>
                    <button id="focus" class="ml-blueprint-modal-btn"></button>
                </div>
            </div>
            <!--</div>-->
            </div>
            `);
            this.contentDiv.append(this.button);
            ml.$('body').append(this.modal);


        }

        protected validateDefinition(): boolean {


            if (!super.validateDefinition())
                return false;

            return true;

        }

        protected init() {

            super.init();
            var obs = this.registerScopeObservable(this.definition.name, false, false);

            if (this.isDOMLoaded) {
                this.watchDefinitionOptions((options: BPOverlay.IOptions) => {

                    // HTMLElement's variables
                    let modal: HTMLElement = document.getElementById('ml-blueprint-myModal');
                    let btn: HTMLElement = document.getElementById("ml-blueprint-myBtn");
                    let h2: HTMLElement = document.getElementById('ml-blueprint-title');
                    //let body: HTMLElement = document.querySelector('body');
                    let p: HTMLCollectionOf<Element> = document.getElementsByClassName('ml-blueprint-modal-body-paragraf');
                    let close_button_text = document.getElementById('close');
                    let focus_button_text = document.getElementById('focus');
                    // props modal_body_text_one
                    if (options.modal_body_text_one) {
                        p[0].innerHTML = options.modal_body_text_one;
                    } else {
                        p[0].innerHTML = '';
                    }

                    // props modal_body_text_two
                    if (options.modal_body_text_two) {
                        p[1].innerHTML = options.modal_body_text_two;
                    } else {
                        p[1].innerHTML = "";
                    }
                  //  props modal_body_text_two
                    if (options.CloseButton) {
                        close_button_text['innerHTML'] = options.CloseButton;

                   } else {

                        close_button_text.style.display ="none"

                    }


                    if (options.SubmitButton) {
                        focus_button_text['innerHTML'] = options.SubmitButton;
                    } else {
                        focus_button_text.style.display ="none"
                    }




                    //props transitionDuratione
                    if (options.transitionDuration) {
                        modal.style.cssText = ` -webkit-animation-duration: ${options.transitionDuration / 1000}s;
                        animation-name: animatetop;animation-duration:  ${options.transitionDuration / 1000}s`;
                    } else {
                        modal.style.cssText = ` -webkit-animation-duration: 2s;animation-name: 
                        animatetop;animation-duration:  2s; background : rgba(0,0,0,0.0)`;
                    }

                    //props  esc_key
                    document.querySelector('.namespace-blueprint-overley').addEventListener('keydown', (event: KeyboardEvent) => {
                        if (event['which'] == 27) {
                            if (options.esc_key) {
                                modal.style.display = "none";
                            } else {
                                modal.style.display = "block";

                            }
                        }
                    })

                    //props backdrop
                    if (options.backdrop) {
                        modal.style.backgroundColor = "rgba(0,0,0,0.6)";
                    }else{
                        modal.style.backgroundColor = "rgba(0,0,0,0)";
                    }

                    //props headerText
                    if (options.headerText) {
                        h2.innerHTML = options.headerText;
                    } else {
                        h2.innerHTML = "Hello";
                    }


                    //modal's logic code
                    btn.onclick = function (): void {
                        modal.style.display = "block";

                    }


                    document.querySelector('.ml-blueprint-close-btn')
                        .addEventListener('click', function (): void {
                            modal.style.display = "none";
                        });

                    if (options.click_outsitte) {
                        modal.onclick = function (event): void {
                            if (event.target == modal) {

                                modal.style.display = "none";
                            }
                        }
                    } else {
                        modal.onclick = function (event): void {
                            if (event.target == modal) {
                                return;

                            }
                        }
                    }

                });

            }

        }

        protected cleanup() {
            super.cleanup();

        }

        public static getComponent(): string {
            return document.getElementsByClassName("namespace-blueprint-overley")[0].innerHTML
        }

    }

    export namespace BPOverlay {

        /**
         * Interface for the overlay modal component
         * @typedef IOverlay
         * @interface
         * @readonly
         */
        export interface IOverlay {
            /**
             * Click outside of the modal to close it
             * @type {boolean}
             */
            click_outsitte?: boolean,
            /**
             * Text to be displayed on the heading of the overlay
             * @type {string}
             */
            headerText?: string,
            /**
             * First part of the text in modal
             * @type {string}
             */
            modal_body_text_one?: string,
            /**
             * Second part of the text in modal
             * @type {string}
             */
            modal_body_text_two?: string,
            /**
             * Press ESC key to close the modal
             * @type {boolean}
             */
            esc_key?: boolean,
            /**
             * Give the modal a backdrop
             * @type {boolean}
             */
            backdrop?: boolean,
            /**
             * Duration of the transition animation
             * @type {number}
             */
            transitionDuration?: number,
            CloseButton?:string ,
            SubmitButton?:string ,

        }

        export interface IOptions extends IOverlay, mdash.IControlOptionsWithTransforms {
        }

        export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            CloseButton?:string | mdash.IScopePath,
            SubmitButton?:string | mdash.IScopePath,
            click_outsitte?: boolean | mdash.IScopePath,
            headerText?: string | mdash.IScopePath,
            esc_key?: boolean | mdash.IScopePath,
            backdrop?: boolean | mdash.IScopePath,
            modal_body_text_one?: string | mdash.IScopePath,
            modal_body_text_two?: string | mdash.IScopePath,
            transitionDuration?: number | mdash.IScopePath
        }
    }

    mdash.registerControl(BPOverlay, 'dashboard.components.controls', BPOverlay.meta);
}