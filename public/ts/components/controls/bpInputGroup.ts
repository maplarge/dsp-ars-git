namespace dashboard.components.controls {

	//import the ml dashboard namespace and give it the name 'mdash'
	import mdash = ml.ui.dashboard;
	
	export namespace BPInputGroup {

		export interface IInputGroup extends BPBase.IAppearance, BPBase.IActionProps{
			
		}

		export interface IOptionsDefinition{
			btnData: BPTextInput.IOptions[] | mdash.IScopePath,
			large?: boolean | mdash.IScopePath,
            vertical?: boolean | mdash.IScopePath,		
			disabled?: boolean | mdash.IScopePath,
            intent?: BPBase.Intent | mdash.IScopePath,
			fill?: boolean | mdash.IScopePath,
			readonly?: boolean | mdash.IScopePath,
			iconLeft?: string | mdash.IScopePath,
			iconRight?: string | mdash.IScopePath,
			placeholder?: string | mdash.IScopePath,	
			type?: string | mdash.IScopePath,
			round?: boolean | mdash.IScopePath
			value?: string | number | mdash.IScopePath
		}

		/**
		 * Props for Input Group
		 * @typedef IOptions
		 * @interface
		 * @readonly
		 * @augments IInputGroup
		 */
		export interface IOptions extends IInputGroup{
			/**
			 * Object with textboxes to render
			 * @type {BPTextInput.IOptions[]}
			 */
			btnData: BPTextInput.IOptions[]
		}
	}

	export class BPInputGroup extends mdash.controls.CompositeControl<BPInputGroup.IOptionsDefinition, BPInputGroup.IOptions> {
		public static type: string = 'BPInputGroup';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.INTERNAL,
			friendlyName: 'Input Group',
			shortDescription: "An input group allows you to add icons and buttons within a text input to expand its functionality.",
			categories: ['Input'],
			childSupport: mdash.ChildSupport.SINGLE
		};

		constructor(id: string, definition: mdash.IControlDefinition<BPInputGroup.IOptionsDefinition>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		/* Init is still an important function here, but because of the limited 
		 * functionality of this control specifically, we're not doing more than calling the parent  */
		protected init() {
			super.init();
		}

		/* This function is what largely separates a composite control from others.
		 * This is the function that defines the structure of the control. If you want it to show up in the DOM, it needs to go here.*/
		protected getControlLayout(options: BPInputGroup.IOptions): mdash.IControlDefinition<any> { 

			//create a container for all the child controls
			return mdash.controls.Flexbox.builder()
				.options({
					justify: "Start",
				}).children([
					//a repeater is simply that: a control that repeats. Its 'data' field represents an array, and it repeats it's children for each element in the array
					mdash.controls.Repeater.builder().options({
						data: { $scopePath: "options.btnData" },
						layout: options.vertical ? 'Vertical' : 'Horizontal' //'options' is a valid scope path since it is what's passed into the getControlLayout function
					}).children([
							//"item" is the default name for each element in the repeater's data array
							BPTextInput.builder().options({
                                placeholder: {$scopePath: "item.placeholder"},
                                type: {$scopePath: "item.type"},
                                large: {$scopePath: "options.large"},
                                fill: {$scopePath: "options.fill"},
                                intent: {$scopePath: "item.intent"},
                                disabled: {$scopePath: "options.disabled"},
                                iconLeft: {$scopePath: "item.iconLeft"},
								iconRight: {$scopePath: "item.iconRight"},
								spin: {$scopePath: "item.spin"},
								value: {$scopePath: "item.value"},
								round: {$scopePath: "item.round"},
								readonly: {$scopePath: "options.readonly"},

							}),
						])
			]).toJSON(); //toJSON converts the controls into the form needed. dont forget it.
		}

		protected cleanup() {
			super.cleanup();
		}

		protected validateDefinition(): boolean {
			return super.validateDefinition();
		}	

		public static builder() {
			return new mdash.ControlDefinitionBuilder<BPInputGroup.IOptionsDefinition>(this.type);
		}
	}
	mdash.registerControl(BPInputGroup, 'dashboard.components.controls', BPInputGroup.meta);
}  