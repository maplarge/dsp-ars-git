namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;
	
	export class BPBreadcrumbs extends mdash.BaseControl<BPBreadcrumbs.IOptionsDefinition> {
		public static type: string = 'BPBreadcrumbs';
		
		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.ADVANCED,
			friendlyName: 'A Breadcrumbs Control',
			shortDescription: "Generates an interactive list of links. Usually used to indicate the path a user has taken to get the a page's or controls current state.",
			categories: ["Visualization"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPBreadcrumbs.builder().toJSON();
		}
		
		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}
		
		private breadcrumbList: HTMLElement;
		
		constructor(id: string, definition: mdash.IControlDefinition<BPBreadcrumbs.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}


		/** 
		 * @inheritDoc
		 * */
		public build() {
			super.build();
			
			this.breadcrumbList = ml.create('ul','ml-blueprint-breadcrumbs', this.contentDiv[0]);
			this.breadcrumbList.classList.add('ml-blueprint-breadcrumbs-list');
			
			this.container.classList.add('namespace-blueprint-breadcrumbs');
		}

		/** 
		 * @inheritDoc
		 * */
		protected validateDefinition(): boolean {
						
			if (!super.validateDefinition())
				return false;
			
			return true;
		}

		private initializeListeners(valueObs: mdash.RefCountedObservable<any>): void {
			this.objectsToDispose.push(
				valueObs.subscribe(newValue => {
					if (valueObs() == null || newValue != valueObs()) {
						// basically, the last child is always the li with the span (if current is not defined - will pick the last link's text)
						// the first child of the li is always the span(or an anchor link if there is no current)
						// and then we get to it's textContent
						this.breadcrumbList.lastChild.firstChild.textContent = newValue;
					}
				})
			);
		}

		/** 
		 * @inheritDoc
		 * */
		protected init() {
			super.init();
			
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			this.initializeListeners(obs);

			if(this.isDOMLoaded)
			{
				this.watchDefinitionOptions((options: BPBreadcrumbs.IOptions) => {
	
					// remove every child in the breadcrumb list before (re)creating the list
					this.breadcrumbList.innerHTML = '';
					
					// add the icon
					if(options.icon){
						const listItem: HTMLElement = ml.create('li','',this.breadcrumbList);
						const iconAnchor: HTMLElement = ml.create('a','',listItem);
						iconAnchor.setAttribute('href', '#');
						iconAnchor.innerHTML = `<i class='mlicon-${options.icon}'></i> <i class='mlicon-arrows-1 ml-blueprint-breadcrumb-arrow'></i>`;
					}
	
					// loop through the links and append it to the ul
					if(options.links){
						options.links.forEach(link => {
							const listItem: HTMLElement = ml.create('li','',this.breadcrumbList);
							const newLink: HTMLElement = ml.create('a','',listItem);
							newLink.textContent = link.label;
							link.disabled ? newLink.classList.add('ml-blueprint-breadcrumbs-link-disabled') :
											newLink.setAttribute('href', link.href || '');
							newLink.setAttribute('target', link.target || '');
							listItem.innerHTML += "<i class='mlicon-arrows-1 ml-blueprint-breadcrumb-arrow'></i>";
						})
					}
					
					// in the end, append the current as the last child
					if(options.current){
						const listItem: HTMLElement = ml.create('li','',this.breadcrumbList);
						const currentLink: HTMLElement = ml.create('span','',listItem);
						currentLink.textContent = options.current;
						currentLink.className = 'ml-blueprint-breadcrumb-current';
						obs(options.current);
					}
					
				});
			}
		}

		/** 
		 * @inheritDoc
		 * */
		protected cleanup() {
			super.cleanup();
		}
		
		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPBreadcrumbs.IOptionsDefinition>(this.type);
		}
	}
	export namespace BPBreadcrumbs {

		/**
		 * Interface for props used inside the Breadcrumbs component
		 * @typedef IBreadcrumbProps
		 * @interface
		 * @readonly
		 */
		export interface IBreadcrumbProps{
			/**
			 * Array of links that should be rendered in the component
			 * @type {BPBase.ILinkProps[]}
			 */
			links?: BPBase.ILinkProps[],
			/**
			 * The icon used for the very first element
			 * @type {string}
			 */
			icon?: string,
			/**
			 * Label for the current element in the breadcrumbs
			 * @type {string}
			 */
			current?: string
		}

		export interface IOptions extends IBreadcrumbProps, mdash.IControlOptionsWithTransforms {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			links?: BPBase.ILinkProps[] | mdash.IScopePath,
			icon?: string | mdash.IScopePath,
			current?: string | mdash.IScopePath
		}
	}

	mdash.registerControl(BPBreadcrumbs,'dashboard.components.controls', BPBreadcrumbs.meta);
}