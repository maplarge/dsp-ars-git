namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPCallout extends mdash.BaseControl<BPCallout.IOptionsDefinition> {
		public static type: string = 'BPCallout';
 
		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Callout',
			shortDescription: "A Callout component that can wrap a header and content text in a colored background based on need.",
			categories: ["Layout"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPCallout.builder().options({ headerText: "Callout" }).toJSON();
		}

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		private callout: HTMLElement;

		constructor(id: string, definition: mdash.IControlDefinition<BPCallout.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		/**
		 * @inheritDoc
		 * */
		public build() {
			super.build();

			this.callout = ml.create("div", "ml-bpcallout", this.contentDiv[0]);
			this.callout.classList.add('ml-blueprint-callout-default');

			this.container.classList.add('namespace-blueprint-callout');
		}

		/**
		 * @inheritDoc
		 * */
		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		/**
		 * @inheritDoc
		 * */
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, null, false);

			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPCallout.IOptions) => {
					let icon: string | null = options.icon ? `<i class="mlicon-${options.icon}"></i>` : null;
					let header: string = options.headerText ? options.headerText : '';
					this.callout.className = 'ml-blueprint-callout-default';

					if(options.intent)
					{
						this.callout.classList.add('ml-blueprint-callout-default');
                        this.callout.classList.add(`ml-blueprint-callout-${options.intent}`);
                        if(!icon)
                        {
							// if the user didn't provide an icon but have an intent passed
							switch(options.intent)
							{
								case BPBase.Intent.Primary:
									icon = 'info28';
									break;
								case BPBase.Intent.Success:
									icon = 'checked21';
									break;
								case BPBase.Intent.Warning:
									icon = 'warning17';
									break;
								case BPBase.Intent.Danger:
									icon = 'information';
									break;
							}
                        }
					}

					this.callout.innerHTML = `<i class="ml-blueprint-button-icon ${icon ? `mlicon-${icon}` : '' }"></i> 
												<h1 ${options.intent ? `class="ml-blueprint-callout-header-${options.intent}"` : '' }>
												${header}
												</h1>`;					
                    
					if(options.contentText)
					{
						this.callout.innerHTML += `<p class='ml-blueprint-callout-content-text'>${options.contentText}</p>`;
					}

                    if(options.hideHeader)
                    {
                        // Attach a css class, that has a css rule that hides the heading(h1 or whatever) tag inside the callout
						this.callout.classList.add('ml-blueprint-callout-hide-header');
						ml.$(this.callout).find('.ml-blueprint-callout-content-text').addClass('ml-blueprint-callout-content-centered');
					}
					else
					{
						ml.$(this.callout).find('.ml-blueprint-callout-content-text').removeClass('ml-blueprint-callout-content-centered');						
					}

				});
			}
		}

		/**
		 * @inheritDoc
		 * */
		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPCallout.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPCallout {

		/**
		 * Property list for the callout component
		 * @typedef ICalloutProps
		 * @interface
		 * @readonly
		 */
		export interface ICalloutProps extends BPBase.IActionProps{
			/**
			 * Text to be displayed as a header for Callout
			 * @type {string}
			 */
			headerText?: string,
			/**
			 * Text for the content inside Callout
			 * @type {string}
			 */
			contentText?: string,
			/**
			 * Icon name for the icon used beside the header
			 * @type {string}
			 */
			icon?: string,
			/**
			 * Show or hide the header
			 * @type {boolean}
			 */
			hideHeader?: boolean,
			/**
			 * Intent color for the callout
			 * @type {BPBase.Intent}
			 */
			intent?: BPBase.Intent
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ICalloutProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            headerText?: string | mdash.IScopePath,
            contentText?: string | mdash.IScopePath,
            intent?: BPBase.Intent | mdash.IScopePath,
			icon?: string | mdash.IScopePath,
            hideHeader?: boolean | mdash.IScopePath            
		}
	}

	mdash.registerControl(BPCallout,'dashboard.components.controls', BPCallout.meta);
}