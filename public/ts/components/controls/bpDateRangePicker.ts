namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPDateRangePicker extends mdash.BaseControl<BPDateRangePicker.IOptionsDefinition> {
		public static type: string = 'BPDateRangePicker';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Date Range Picker',
			shortDescription: "DateRangePicker component allows an end user to select start and end date values as a range from a calendar pop-up ",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPDateRangePicker.builder().options(BPDateRangePicker.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPDateRangePicker.IOptions> = {
			'BPDateRangePicker': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected dateRangePicker: JQuery;

		constructor(id: string, definition: mdash.IControlDefinition<BPDateRangePicker.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
            this.container.classList.add('namespace-blueprint-dateRangePicker');

			this.dateRangePicker = ml.$('<div style="min-width:100px;min-height:100px;background-color:red" id="daterange"></div>') 
			this.contentDiv.append(this.dateRangePicker)

		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, null, false);

			if (this.isDOMLoaded) {
                this.createChildren();
				this.watchDefinitionOptions((options: BPDateRangePicker.IOptions) => {
					
					ml.$('#daterange').daterangepicker({
						"showDropdowns": true,
						"minDate": "1/1/1999",
						"locale": {
							"monthNames": [
								"January",
								"February",
								"March",
								"April",
								"May",
								"June",
								"July",
								"August",
								"September",
								"October",
								"November",
								"December"
							],
						},

					})


					ml.$('#daterange').trigger('click')
					ml.$('.ml-daterangepicker .input-wrapper').detach()
					ml.$('.ml-daterangepicker .range').detach()
					ml.$('.ml-daterangepicker .ml-daterangepicker_input').detach()
					ml.$('.ml-daterangepicker .ranges').detach()
			

				});
			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPDateRangePicker.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPDateRangePicker {

		export interface IDateRangePickerProps extends BPBase.IProps{
            
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IDateRangePickerProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
 
		}
	}

	mdash.registerControl(BPDateRangePicker,'dashboard.components.controls', BPDateRangePicker.meta);
}