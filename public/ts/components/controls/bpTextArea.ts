namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPTextArea extends mdash.BaseInputControl<BPTextArea.IOptionsDefinition> {
		public static type: string = 'BPTextArea';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Text Area',
			shortDescription: "TextArea allows your app to accept a potentially lengthy text value from the user.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPTextArea.builder().options(BPTextArea.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPTextArea.IOptions> = {
			'BPTextArea': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
        }

		protected textarea: HTMLElement;

		constructor(id: string, definition: mdash.IControlDefinition<BPTextArea.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
            super.build();
			this.container.classList.add('namespace-blueprint-TextArea');
            this.textarea = ml.create("textarea", "ml-BPTextArea",  this.contentDiv[0])
			this.textarea.classList.add('ml-blueprint-textArea')
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		private initializeListeners(valueObs: mdash.RefCountedObservable<any>) {
			ml.$(this.textarea).on('input', (event: JQueryEventObject) => { 
					if (ml.$(this.textarea).val() != valueObs() || valueObs() == null) {
						valueObs(ml.$(this.textarea).val()); 
					}
				});

			//objectsToDispose is an array of listeners that get destroyed when the control is destroyed
			this.objectsToDispose.push(
				valueObs.subscribe(newValue => {
					if (newValue != ml.$(this.textarea).val()) {
						ml.$(this.textarea).val(newValue);
					}
				})
			)
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			this.initTransforms();
			this.initializeListeners(obs);
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPTextArea.IOptions) => {
					this.textarea.className = this.textarea.className.replace(/(^|\s)ml-blueprint-textArea-\S+/g, '');

                    if(options.disabled)
                    {
                        this.textarea.setAttribute('disabled', 'true');
						this.textarea.classList.add('ml-blueprint-disabled');
					}
					else
					{
						this.textarea.removeAttribute('disabled');
						this.textarea.classList.remove('ml-blueprint-disabled')
					}
                    if(options.fill)
                    {
						this.textarea.classList.add('ml-blueprint-textArea-fill')
					}
					else 
					{
						this.textarea.classList.remove('ml-blueprint-textArea-fill')
					}
                    if(options.large) {
                        this.textarea.classList.add('ml-blueprint-textArea-large')
					}
					else
					{
						this.textarea.classList.remove('ml-blueprint-textArea-large')
					}
                    if(options.intent)
                    {
						this.textarea.classList.add(`ml-blueprint-textArea-${options.intent}`);
					}
					if(options.readonly)
                    {
						this.textarea.setAttribute('readonly', 'true');
					}
					else
					{
						this.textarea.removeAttribute('readonly');	
					}
				});
			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPTextArea.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPTextArea {

		export interface ITextArea extends BPBase.IControlProps{
            disabled?: boolean,
			intent?: BPBase.Intent,
			readonly?: boolean,
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ITextArea{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            disabled?: boolean | mdash.IScopePath,
            intent?: BPBase.Intent | mdash.IScopePath,
            large?: boolean | mdash.IScopePath,
			fill?: boolean | mdash.IScopePath,
			readonly?: boolean | mdash.IScopePath,
		}
	}

	mdash.registerControl(BPTextArea,'dashboard.components.controls', BPTextArea.meta);
}