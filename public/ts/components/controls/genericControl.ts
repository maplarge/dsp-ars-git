namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export namespace GenericControl {

		export interface IOptions extends mdash.IControlOptionsWithTransforms {
			//mirrors IOptionsDefinition, but contains real values, whereas IOptionsDefinition can contain path's to values (scopePath)
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			//options that can be passed to the control when created
		}
	}

	export class GenericControl extends mdash.BaseControl<GenericControl.IOptionsDefinition> {
		public static type: string = 'GenericControl';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Generic Control',
			shortDescription: "Provides a template for developers to see a very basic control",
			categories: ["Layout"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return GenericControl.builder().options(GenericControl.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<GenericControl.IOptions> = {
			'GenericControl': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		constructor(id: string, definition: mdash.IControlDefinition<GenericControl.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

			//this is where you might create/initialize class variables of observables
		}

		public build() {
			super.build();

			//this is where you might create dom elements
			//elements are not yet added to the dom at this point though
		}

		protected validateDefinition(): boolean {

			//run any validation against the defintion (options passed into the control) that you might require

			if (!super.validateDefinition())
				return false;

			return true;
		}

		protected init() {
			super.init();


			//called twice
			//1. To initialze the control but before it is loaded in the DOM (when this.isDOMLoaded == false)
			//2. After the control has been created in the DOM (when this.isDOMLoaded == true)
		}

		protected cleanup() {
			super.cleanup();

			//called just before the control gets destroyed
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<GenericControl.IOptionsDefinition>(this.type);
		}
	}
}