namespace dashboard.components.controls {
	import mdash = ml.ui.dashboard;
	
	export class BPTree extends mdash.BaseControl<BPTree.IOptionsDefinition> {
		public static type: string = 'BPTree';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Tree',
			shortDescription: "A tree view or an outline view is a graphical control element that represents a hierarchical view of information.",
			categories: ["Layout", "Visualization"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPTree.builder().options(BPTree.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPTree.IOptions> = {
			'BPTree': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected TreeSelect: HTMLElement;
		protected ParentUl: JQuery;
		protected getParents(el:HTMLElement, parentSelector /* optional */) {

			if (parentSelector === undefined) {
				parentSelector = document;
			}
			var parents:{}[] = [];
			var p = el.parentNode;
		
			while (p !== parentSelector) {
				var o = p;
				parents.push(o);
				p = o.parentNode;
			}
			parents.push(parentSelector); // Push that parentSelector you wanted to stop at
			return parents;
		}
		

		constructor(id: string, definition: mdash.IControlDefinition<BPTree.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
			
			this.container.classList.add('namespace-blueprint-tree')
			this.TreeSelect = ml.create("div", "ml-BPTree",  this.contentDiv[0]) as HTMLElement;
			this.TreeSelect.classList.add('ml-bluprint-Tree');
			this.ParentUl = ml.$(`
				<ul type="none" class="parent-ul"></ul>
			`)
			var self = this;
			
			function GeneratedTree(){
				this.__init.call(this)
			}
			var paddingLeft:number = 50;
			GeneratedTree.prototype = {
				__init:function(){
					var clearEach = setInterval(()=>{
						if(_generate != undefined){
							this.__firstForEach();
							this.__EventBackgroundLI();
							clearInterval(clearEach)
						}
					}, 10);
				},
				__firstForEach:()=>{
					Array.prototype.map.call(self.definition.options.data as {}, (elm, ind:number)=>{
						var li = (document.createElement("li") as HTMLLIElement);
						li.innerHTML = `<span class="ml-blueprint-clickableItem" ><p class="genText">${elm.name}</p></span`;
						ml.$(self.ParentUl).append(li);
						_generate.__addIconWithType(elm, li);
						if(elm.hasOwnProperty('subMenu')){
							_generate.__AllForEach(elm.subMenu, li, paddingLeft, 0);
						}
						if(elm.type == "folder" && elm.isOpen){
							_generate.__IsShowAndSlide(li.children, {animated:false})
							if(li.children[0].children[0].hasAttributes() && li.children[0].children[0].getAttribute('data-caret') == "down"){
								li.children[0].children[0].classList.add('transformCaret');
							}
						}
					})
				},
				__AllForEach:(data, li, padd, paddPlus)=>{
					padd+=paddPlus;
					var ul = (document.createElement("ul") as HTMLUListElement)
					ul.className = `ml-blueprint-firstLevel`;
					li.appendChild(ul);
					for(let key in data){
						let liSub = (document.createElement('li') as HTMLLIElement)
						liSub.innerHTML = `<span class="ml-blueprint-clickableItem" style="padding-left:${padd}px"><p class="genText">${data[key].name}</p></span>`;
						ul.appendChild(liSub);
						if(data[key].hasOwnProperty('subMenu')){
							_generate.__AllForEach(data[key].subMenu, ul.children[key], padd, 20)
						}
						_generate.__addIconWithType(data[key], ul.children[key]);
						if(data[key].type == "folder" && data[key].isOpen){
							_generate.__IsShowAndSlide(ul.children[key].children, {animated:false})
							if(ul.children[key].children[0].children[0].hasAttributes() && ul.children[key].children[0].children[0].getAttribute('data-caret') == "down"){
								ul.children[key].children[0].children[0].classList.add('transformCaret');
							}
						}
					}
				},
				__addIconWithType:(data, li)=>{
					let iconItem = (document.createElement("i") as HTMLElement);
					iconItem.className = `${data.iconClass}`;
					let iconCaret = (document.createElement("i") as HTMLElement);
					iconCaret.className = `${data.iconCaretClass}`;
					iconCaret.setAttribute("data-caret", "down");
					data.hasIcon = (data.iconClass == "")? false:true;
					if(data.hasIcon){
						li.children[0].insertBefore(iconItem, li.children[0].childNodes[0]);
					}
					if(data.type == "folder"){
						li.children[0].insertBefore(iconCaret, li.children[0].childNodes[0]);
					}
					if(data.eye != ""){
						let iconEye = (document.createElement("i") as HTMLElement);
						iconEye.className = `${data.eye} eyeIcon`;
						let EyeTextToolTip = (document.createElement("span") as HTMLSpanElement);
						EyeTextToolTip.innerText = `${data.eyeTextToolTip}`;
						EyeTextToolTip.className = `eyeToolTip`;
						iconEye.appendChild(EyeTextToolTip)
						EyeTextToolTip.addEventListener("click", (e:Event)=>e.stopPropagation())
						li.children[0].insertBefore(iconEye, li.children[0].childNodes[0].nextSibling);
						iconEye.addEventListener('mouseover', function(){
							Array.prototype.map.call(self.getParents(this, document.querySelector('.parent-ul')), elem=>{
								if(elem.classList.contains('ml-blueprint-firstLevel')){
									elem.style.overflow= "visible"
								}
							})
						})
						iconEye.addEventListener('mouseleave', function(){
							Array.prototype.map.call(self.getParents(this, document.querySelector('.parent-ul')), elem=>{
								if(elem.classList.contains('ml-blueprint-firstLevel')){
									elem.style.overflow= "hidden"
								}
							})
						})
					}
				},
				__EventBackgroundLI:(e:Event)=>{
					Array.prototype.map.call((document.querySelectorAll('.ml-blueprint-clickableItem') as ElementCreationOptions), elm=>{
						elm.addEventListener('click', function(event:Event){
							if(this.classList.contains('activeItem')){
								this.classList.remove('activeItem')
							}
							else{
								for(let i = 0;i < document.querySelectorAll('.ml-blueprint-clickableItem').length; i++){
									document.querySelectorAll('.ml-blueprint-clickableItem')[i].classList.remove("activeItem")
								}
								this.classList.add("activeItem");
							}
						})
						if(elm.children[0] != undefined && elm.children[0].hasAttributes()){
							if(elm.children[0].getAttribute("data-caret") == "down"){
								elm.children[0].addEventListener('click', (e)=>{
									(e as Event).stopPropagation();
									(elm.children[0].classList.contains('transformCaret'))?
										elm.children[0].classList.remove('transformCaret'):elm.children[0].classList.add('transformCaret');									
									_generate.__IsShowAndSlide(elm.children[0].parentElement.parentElement.children, {
										animated:true, slideOpen:elm.children[0].classList.contains('transformCaret')
									})	
								})
							}
						}

					})
				},
				__IsShowAndSlide:(elementUL, animation)=>{
					Array.prototype.forEach.call(elementUL, (element, index)=>{
						if(!animation.animated){
							if(element.classList.contains('ml-blueprint-firstLevel')){
								let height = 0;
								for(let i =0; i< element.children.length; i++){
									height+=element.children[i].getBoundingClientRect().height;
								}
								element.style.height = height+"px"
							}
						}
						else{
							
							if(element.classList.contains('ml-blueprint-firstLevel')){
								let height:number = 0, parentHeightMinus:number = 0;
								element.style.transition = ".4s"
								let ParentsArr = this.getParents(element, document.querySelector('.parent-ul'));
								
								for(let i =0; i< element.children.length; i++){
									height+=element.children[i].getBoundingClientRect().height;
								}
								parentHeightMinus = height;
								if(animation.slideOpen){
									element.style.height = height+"px"
									Array.prototype.map.call(ParentsArr, par =>{
										if(par.classList.contains('ml-blueprint-firstLevel')){
											par.style.transition = ".4s";
											par.style.height = (par.getBoundingClientRect().height + parentHeightMinus) +"px";
										}
									});
								}
								else{
									height = 0;
									element.style.height = height+"px";
									Array.prototype.map.call(ParentsArr, par =>{
										if(par.classList.contains('ml-blueprint-firstLevel')){
											par.style.transition = ".4s";
											par.style.height = (par.getBoundingClientRect().height -
											 parentHeightMinus) +"px";
										}
									});
								}
								
							}
						}
					})
				},
				__apendAll:(()=>{
					ml.$(this.ParentUl).appendTo(this.TreeSelect);
				})()
			}
			var _generate = new GeneratedTree();
			
		}
		
		protected validateDefinition(): boolean {


			if (!super.validateDefinition())
			return false;

			return true;
		}

		protected init() {
			super.init();

			
			var obs = this.registerScopeObservable(this.definition.name, null, false);
			
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPTree.IOptions) => {
			
				});
			}
			
		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPTree.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPTree {
		
		/**
		 * Interface for creating Trees with nodes
		 * @typedef DataTree
		 * @interface
		 * @readonly
		 */
		export interface DataTree{
			/**
			 * Type of the tree node
			 * @type {string}
			 */
			type:string,
			/**
			 * Name of the tree
			 * @type {string}
			 */ 
			name: string,
			/**
			 * Display or hide the eye
			 * @type {string}
			 */ 
			eye?: string, 
			/**
			 * Text to display as a tooltip for the eye
			 * @type {string}
			 */
			eyeTextToolTip?: string, 
			/**
			 * CSS class for the icon caret
			 * @type {string}
			 */
			iconCaretClass?: string, 
			/**
			 * CSS class of an icon
			 * @type {string}
			 */
			iconClass?: string, 
			/**
			 * Display or hide the icon
			 * @type {Function}
			 */
			hasIcon?: Function,
			/**
			 * Tree is open or closed
			 * @type {boolean}
			 */
			isOpen?:boolean,
			/**
			 * Subtree(s) inside the tree
			 * @type {DataTree[]}
			 */
			subMenu?: DataTree[]
		}

		/**
		 * Data property for the tree
		 * @typedef ITreeProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IActionProps
		 * @augments BPBase.IAppearance
		 */
		export interface ITreeProps extends BPBase.IActionProps, BPBase.IAppearance{
			/**
			 * Tree objects to be rendered inside the component
			 * @type {ctrl.BPTree.DataTree[]}
			 */
			data?: ctrl.BPTree.DataTree[];
		}
		
		export interface IOptions extends mdash.IControlOptionsWithTransforms, ITreeProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			data?: ctrl.BPTree.DataTree[] | mdash.IScopePath,
		}
	}
	mdash.registerControl(BPTree,'dashboard.components.controls', BPTree.meta);
}





  

