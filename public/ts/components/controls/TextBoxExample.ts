namespace dashboard.components.controls {

	//import the ml dashboard namespace and give it the name 'mdash'
	import mdash = ml.ui.dashboard;

	export class TextboxSample<T extends TextboxSample.IOptionsDefinition> extends mdash.BaseInputControl<T> {

		//used to identify the control
		public static type: string = 'TextboxSample';

		//used to provide extra details about the control.
		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.PRODUCTION,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'TextboxSample',
			shortDescription: "Provides an input for text",
			longDescription: "Provides an input for text. Can have validators applied to defined allowed values.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};

		//the two jquery elements that represent the control in the DOM
		protected textBox: JQuery;
		protected label: JQuery;

		/* Called when the control is built/created */
		constructor(id: string, definition: mdash.IControlDefinition<T>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		/*Created so that other controls can extend this class and use it to create other textbox-ish inputs, such as number, password, email, etc...*/
		protected getInputField() {
			return ml.$('<input type="text" />');
		}

		/* called after the constructor. If the control is not visible, then it will be created, but nothing will be added to the dom 
		(i.e., build will not be called) until the control's visibilty is set to 'true'
		*/
		public build() {
			super.build();
			this.textBox = this.getInputField();

			//create an instance of the label element
			this.label = ml.$('<label></label>');
			

			this.contentDiv.append(this.label, this.textBox);
		}
		/* called after build and each time the value in the options object changes.
		This function can be used to validate options fields, such as making sure that (if this were a number input)
		that the value of "placeholder" had to be a number, you could validate that such was the case. 
		*/
		protected validateDefinition(): boolean {
			if (!super.validateDefinition())
				return false;
			return true;
		}

		/* This is the main setup method for the control, and the place to create any option-specific DOM, 
		 * wire up events, listen to observables on the scope, etc.
		 */
		protected init() {
			super.init();

				//represents current value inside the textbox element
			var currentValue = this.textBox && this.textBox.val ? this.textBox.val() : "",	

				//represents the observable of the value of the this control (the textbox CONTROL, not to be confused with the textbox ELEMENT)
				valueObs = this.registerScopeObservable(this.definition.name, currentValue, true);

			//Since init can be called multiple times in the control's life-cycle, it is important to make sure that references to the DOM happen
			//inside of an check to make sure that the control's DOM has been loaded.
			if (this.isDOMLoaded) {

				//focus on textbox if clicked anywhere on control
				this.contentDiv.on('click', (e) => this.textBox.trigger('focus')); 

				//update the 'value' of the control (stored in the observable 'this.scope[this.definition.name]' )
				this.textBox.on('keyup change', (a, b) => {
					var val = this.textBox.val();
					if (val != this.scope[this.definition.name]()) {
						this.scope[this.definition.name](val);
					}
				});

				//create listener for when value of options fields change 
				//(in this control's case, 'placeholder' and 'disabled') and respond to those changes
				//i.e. set the placeholder attr with the value in options.placeholder
				this.watchDefinitionOptions((options: TextboxSample.IOptions) => {
					if (options.label) {
						this.label.text(options.label);
					}
					if (ml.util.isNotEmptyString(options.placeholder)) {
						this.textBox.attr("placeholder", options.placeholder);
					}
					if (options.disabled) {
						this.textBox.attr('disabled', 'disabled');
						this.contentDiv[0].classList.add("ml-dash-input-disabled");
					}
					else {
						this.textBox.removeAttr('disabled');
						this.contentDiv[0].classList.remove("ml-dash-input-disabled");
					}
				});
			}

			//call the function that ensures the value being set in this control that is coming in from an outside source, is valid
			//i.e. must be a number or a properly formatted email address, etc...
			this.applyValidators(valueObs);

			if (this.isDOMLoaded) {
				//if the value of the control is changed from elsewhere (i.e., the scopepath) then set the textbox element's value to the new value
				if (valueObs() != this.textBox.val())
					this.textBox.val(valueObs());

				//add the subscription to the array of things to be destroyed at if/when this control is destroyed.
				this.objectsToDispose.push(
					valueObs.subscribe((newValue) => {
						if (newValue != this.textBox.val())
							this.textBox.val(newValue);
					})
				);
			}
		}

		/* This method is called when the control's options have changed, and before the control is destroyed. 
		 * This is the point to clean up anything created in the init method.*/
		protected cleanup() { super.cleanup(); }

		/* Called just before the control is destroyed. Remove any delegates, listeners, or whatever else in order to prevent memory leaks.
		 You dont need to worry about removing items from the DOM. Dashboards will deal with that automatically. */
		public destroy() {
			super.destroy();
			this.textBox.off('keyup change');
		}

		/* Called by the user. This is the funciton that creates an instance of this control. Since it returns an instance of this controls,
		 functions can be chained to it. (sunce as Textbox.builder().options({...}).visible(...).layoutOptions({...})... */
		public static builder() {
			return new mdash.ControlDefinitionBuilder<TextboxSample.IOptionsDefinition>(this.type);
		}
	}

	export namespace TextboxSample {
		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			label?: string | mdash.IScopePath,
			placeholder?: string | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
		}
		export interface IOptions extends mdash.IControlOptionsWithTransforms  {
			label?: string,
			placeholder?: string,
			disabled?: boolean,
		}
	}
	mdash.registerControl(TextboxSample, 'db.examples.controls', TextboxSample.meta);
}