namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;
	
	export class BPPopover<T extends BPPopover.IOptionsDefinition> extends BPButton<BPPopover.IOptionsDefinition> {
		public static type: string = 'BPPopover';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Popover',
			shortDescription: "A Popover can be used to display some content on top of another.",
			categories: ["Layout"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPPopover.builder().options(BPPopover.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPPopover.IOptions> = {
			'BPPopover': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		constructor(id: string, definition: mdash.IControlDefinition<BPPopover.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		private popover: JQuery;
		private popoverContentDiv: JQuery;
		private popoverArrow: JQuery;
		private popperInstance: any;
		private popoverContainer: JQuery;
		private clickCounter: number = 0;

		/**
		 * @inheritdoc
		 */
		public build() {
			super.build();
			this.popoverContainer = ml.$('<div class="ml-blueprint-popover-container"></div>').appendTo(this.contentDiv);
			this.popover = ml.$('<div class="ml-blueprint-popover-div" tabindex="0"></div>').appendTo(this.popoverContainer);
			this.popoverArrow = ml.$('<div class="ml-blueprint-popover-div-arrow" x-arrow></div>').appendTo(this.popover);
			this.popoverArrow.append(`<svg viewBox="0 0 30 30">
				<path class="ml-blueprint-popover-div-arrow-border" d="M8.11 6.302c1.015-.936 1.887-2.922 1.887-4.297v26c0-1.378-.868-3.357-1.888-4.297L.925 17.09c-1.237-1.14-1.233-3.034 0-4.17L8.11 6.302z"></path>
				<path class="ml-blueprint-popover-div-arrow-fill" d="M8.787 7.036c1.22-1.125 2.21-3.376 2.21-5.03V0v30-2.005c0-1.654-.983-3.9-2.21-5.03l-7.183-6.616c-.81-.746-.802-1.96 0-2.7l7.183-6.614z"></path>
			</svg>`);
			this.popoverContentDiv = ml.$('<div class="ml-blueprint-popover-div-content"></div>').appendTo(this.popover);
			ml.$(this.button).detach().appendTo(this.popoverContainer);
			this.container.classList.add('namespace-blueprint-popover');
		}

		/**
		 * @inheritdoc
		 */
		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		/**
		 * @internal
		 * 
		 * Explicitly sets the container than any control children will be created in.
		 */
		protected getChildContainer(index:number, definition: mdash.IControlDefinition<any>): HTMLElement { 
			return this.container.querySelector('.ml-blueprint-popover-div-content');
		}

		protected clearButtonEvents(): void{
			this.button.onclick = null;
			this.button.onmouseover = null;
			this.button.onmouseout = null;
			this.button.onfocus = null;
			this.contentDiv.find(`.${this.button.classList[0]}, .${this.popover[0].classList[0]}`).off('hover');
			ml.$(window).off('click', this.init.prototype.outsideClick);
		}

		protected clickEvent(e: MouseEvent, options: BPPopover.IOptions): void{
			this.clickCounter++;
			if(this.popover.css('display') == 'block')
			{
				if(this.clickCounter % 2 == 0){
					this.popover.hide();
				}
			}
			else
			{
				this.popover.toggle();
				this.popover.focus();
			}

			if(options.onClick)
			{
				options.onClick(this.scope,e);
			}
		}
		
		/**
		 * @inheritdoc
		 */
		protected init() {
			super.init();
			if (this.isDOMLoaded) {
				this.createChildren();
				this.watchDefinitionOptions((options: BPPopover.IOptions) => {
					this.clearButtonEvents();
					this.clickCounter = 0;

					if(!this.button.classList.contains('ml-blueprint-popper-ref')){
						this.button.classList.add('ml-blueprint-popper-ref');
					}
					
					this.popperInstance = new (<any>window).Popper(this.button, this.popover, {
						placement: options.position || 'auto',
						modifiers: {
							arrow: {
								enabled: Boolean(options.arrow)
							},
							preventOverflow: {
								enabled: Boolean(options.preventOverflow),
								boundariesElement: options.preventOverflowType
							},
							hide: {
								enabled: false
							},
							flip: {
								enabled: Boolean(options.flip)
							}
						}
					});

					const that: this = this;
					/**
					 * With a separate function,I can add and remove the event from the global window object when neccessary
					 * @param e Mouse Event object
					 */
					this.init.prototype.outsideClick = function(e: any){
						if(e.srcElement.classList.contains('ml-blueprint-popper-ref'))
						{
							return false;
						}

						if(!ml.$(e.srcElement).parentsUntil().hasClass('ml-blueprint-popover-div'))
						{
							that.popover.hide();
							if(that.clickCounter % 2 != 0){
								that.clickCounter++;
							}
						}
					}

					if(options.arrow)
					{
						ml.$('.ml-blueprint-popover-div-arrow').show();
					}
					else
					{
						ml.$('.ml-blueprint-popover-div-arrow').hide();
					}

					if(options.interactionKind)
					{
						switch(options.interactionKind){
							case BPPopover.PopoverInteractionKind.CLICK:
								this.button.onclick = (e) => this.clickEvent(e, options);
								ml.$(window).off('click', this.init.prototype.outsideClick);
								ml.$(window).on('click', this.init.prototype.outsideClick);
								break;
							case BPPopover.PopoverInteractionKind.CLICK_TARGET_ONLY:
								this.button.onclick = (e) => this.clickEvent(e, options);
								ml.$(window).off('click', this.init.prototype.outsideClick);
								break;
							case BPPopover.PopoverInteractionKind.HOVER:
								this.contentDiv.find(`.${this.button.classList[0]}, .${this.popover[0].classList[0]}`).off('hover').on('hover',(e: any) => {
									if(e.type == 'mouseleave' && !ml.$(e.toElement).parentsUntil().hasClass('ml-blueprint-popover-div'))
									{
										this.popover.hide();
									}
									else{
										this.popover.show();
										this.popover.focus();
									}
								});
								break;
							case BPPopover.PopoverInteractionKind.HOVER_TARGET_ONLY:
								this.button.onmouseover = () => {
									this.popover.show();
									this.popover.focus();
								}
								this.button.onmouseout = () => this.popover.hide();
								break;
						}

						if(options.openOnTargetFocus)
						{
							this.button.onfocus = () => {
								this.popover.focus();
								this.popover.show();
							}
						}

						if(options.canEscapeKeyClose)
						{
							this.popover.off('keyup').on('keyup', (e) => {
								if(e.which == 27)
								{
									this.popover.hide();
								}
							})
						}
					}
				});
			}
		}	

		/**
		 * @inheritdoc
		 */
		protected cleanup() {
			super.cleanup();
			this.clearButtonEvents();
			this.popover.off('keyup');
		}

		/**
		 * @inheritdoc
		 */
		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPPopover.IOptionsDefinition>(this.type);
        }
        
    }
    export namespace BPPopover {

		/**
		 * Popover position enumeration
		 * @enum {string}
		 * @readonly
		 */
		export enum Position{
			/** Automatically place it relative to target */
			AUTO = 'auto',
			/** Automatically place it relative to the start of the target */
			AUTO_START = 'auto-start',
			/** Automatically place it relative to the end of the target */
			AUTO_END = 'auto-end',
			/** Right of the target */
			RIGHT = 'right',
			/** Right bottom of the target */
			RIGHT_BOTTOM = 'right-bottom',
			/** Right top of the target */
			RIGHT_TOP = 'right-top',
			/** Bottom of the target */
			BOTTOM = 'bottom',
			/** Bottom right of the target */
			BOTTOM_RIGHT = 'bottom-right',
			/** Bottom left of the target */
			BOTTOM_LEFT = 'bottom-left',
			/** Left of the target */
			LEFT = 'left',
			/** Left bottom of the target */
			LEFT_BOTTOM = 'left-bottom',
			/** Left top of the target */
			LEFT_TOP = 'left-top',
			/** Top of the target */
			TOP = 'top',
			/** Top left of the target */
			TOP_LEFT = 'top-left',
			/** Top right of the target */
			TOP_RIGHT = 'top-right'
		}

		/**
		 * Options for preventOverflow property of Popper.js
		 * @readonly
		 * @enum {string}
		 */
		export enum PreventOverflowTypes{
			/** Prevent the popover from overflowing the scroll parent */
			SCROLL_PARENT = 'scrollParent',
			/** Prevent the popover from overflowing the window */
			WINDOW = 'window',
			/** Prevent the popover from overflowing the viewport */
			VIEWPORT = 'viewport'
		}

		/**
		 * Enumeration for popover interactions
		 * @enum {string}
		 * @readonly
		 */
		export enum PopoverInteractionKind{
			/** Show popover on click and hide it when clicked again outside of it*/
			CLICK = 'click',
			/** Show popover on click and hide it only when the popover target is clicked */
			CLICK_TARGET_ONLY = 'click-target',
			/** Show popover on hover and hide it when the mouse is away from the target and the popover */
			HOVER = 'hover',
			/** Show popover on hover and hide it when the mouse is away from the target only */
			HOVER_TARGET_ONLY = 'hover-target'
		}

		/**
		 * Interface for popover
		 * @typedef IPopoverProps
		 * @interface
		 * @readonly
		 * @augments BPButton.IOptions
		 */
		export interface IPopoverProps extends BPButton.IOptions{
			/**
			 * Close popover by pressing ESC
			 * @type {boolean}
			 */
			canEscapeKeyClose?: boolean,
			/**
			 * Disable the popover
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Set the popover interaction kind
			 * @type {PopoverInteractionKind}
			 */
			interactionKind?: PopoverInteractionKind,
			/**
			 * Open popover when the target is focused
			 * @type {boolean}
			 */
			openOnTargetFocus?: boolean,
			/**
			 * Set popover position
			 * @type {Position}
			 */
			position?: Position,
			/**
			 * Show or hide the arrow of popover
			 * @type {boolean}
			 */
			arrow?: boolean,
			/**
			 * Flip the popover position
			 * @type {boolean}
			 */
			flip?: boolean,
			/**
			 * Prevent the popover from going out of bounds
			 * @type {boolean}
			 */
			preventOverflow?: boolean,
			/**
			 * Specific bounds to prevent the overflow for
			 * @type {PreventOverflowTypes}
			 */
			preventOverflowType?: PreventOverflowTypes,
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IPopoverProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms, BPButton.IOptionsDefinition {
			canEscapeKeyClose?: boolean | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
			interactionKind?: PopoverInteractionKind | mdash.IScopePath,
			openOnTargetFocus?: boolean | mdash.IScopePath,
			position?: Position | mdash.IScopePath,
			arrow?: boolean | mdash.IScopePath,
			flip?: boolean | mdash.IScopePath,
			preventOverflow?: boolean | mdash.IScopePath,
			preventOverflowType?: PreventOverflowTypes | mdash.IScopePath,
		}
	}
	mdash.registerControl(BPPopover,'dashboard.components.controls', BPPopover.meta);
}