namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPMultiSelect extends mdash.BaseInputControl<BPMultiSelect.IOptionsDefinition> {
		public static type: string = 'BPMultiSelect';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Multi Select',
			shortDescription: "The MultiSelect is a form component that displays a list of options and allows for multiple selections from this list.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPMultiSelect.builder().options(BPMultiSelect.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPMultiSelect.IOptions> = {
			'BPMultiSelect': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
        }

        protected tagInput: JQuery;
        protected enterTags: JQuery;
		protected enterTagInput: JQuery;
		protected text: JQuery;
		protected removeIcon: JQuery;
		protected removeButoon: JQuery;
		private portal: JQuery;
		private overlay: JQuery;
		private menu: JQuery;
		private list: JQuery;
		private once: boolean = false;
		private arr: any;
		private obj: {value:string};
		protected IconOnce: boolean  = false;
		protected removeLength: any;
		protected removeTag: any;
		private namespaceClass: string;
		protected closeOnSelectBoolean = false;
		protected clickInputOpen = true;
		protected listItems:Function;
		protected keyUpBoolean:boolean  = false;
		private countArrow:number = 0;
		protected resetBoolean = false;
		protected textPush:Function;
		protected obsItems:Function;
		private intentOptions:string;
		private placeholderOptions:string;
		private minimalOptions:boolean;
		protected openOnKeyDownOptions:boolean;
		protected obsNewValue:Function;
		protected corectUL: string = null;
		protected countMLID: string = String(ml.id());
		private indexEsc:number = 0;

		constructor(id: string, definition: mdash.IControlDefinition<BPMultiSelect.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}


		public build() {
			super.build();
			this.namespaceClass = 'namespace-blueprint-multiSelect';
			let classList = [this.namespaceClass, 'namespace-blueprint-tagComp', 'namespace-blueprint-tagInput']
			this.container.classList.add(...classList);
            this.tagInput = ml.$('<div></div>');
			this.portal = ml.$(`<div id="ml-multi-overlay-${this.countMLID}" class="ml-blueprint-multi-select-portal"></div>`)		
			this.contentDiv.append(this.tagInput);  
			document.querySelector('body').appendChild(this.portal[0]); 
            this.tagInput.addClass('ml-blueprint-tagInput');
            this.enterTags = ml.$('<div></div>');
			this.enterTags.addClass('ml-blueprint-enterTags');
			this.tagInput.append(this.enterTags);
			this.removeButoon = ml.$(`<button class="ml-blueprint-multiSelect-removeTag"><i></i></button>`)
            this.enterTagInput = ml.$(`<input data-mlMulti-id="ml-multi-overlay-${this.countMLID}" class="ml-blueprint-enterTagInput multi-select-input">`);
			this.enterTags.append(this.enterTagInput);
			this.portal.append(`<div></div>`)
			this.menu = ml.$(`<div></div>`)
			this.menu.addClass('ml-blueprint-popover-content')	
		}

		protected addPlaceholder = (placeholder:string) => {
			if(this.arr && this.arr.length == 0) {
				this.enterTagInput.attr('placeholder', placeholder )
				this.removeButoon.detach()
			}			
		}
		protected removePlaceholder = () => {
			if(this.arr && this.arr.length > 0) {
				this.enterTagInput.attr('placeholder','')
			}
		}

			//  each button click get him Toggle data list 
			protected getCurrentToggleList(eventCurrent, untilParentClass, toggleListClass){
				let parent, child, classNameUntilParent = document.querySelector(untilParentClass).className.split(" ")[0];
				parent = this.getParents(eventCurrent, document.querySelector(untilParentClass), classNameUntilParent);
				child = this.findJSFunction(toggleListClass, parent);
				return child;
			}
			// ___________________________
	
			// find any element by class, id and tagName into the Main elemnt
			protected findJSFunction(selector, parentElement){
				var empChild = [], manyElement = [], result = null;
				function callElement(pseudoSelector, parentSelector){
					Array.prototype.map.call((parentSelector.children as HTMLElement), (elem:Element)=>empChild.push(elem))
					empChild.find((element)=>{
						if(element.classList.contains(pseudoSelector)){
							manyElement.push(element)
						}
						else if(element.tagName == pseudoSelector.toUpperCase()){
							manyElement.push(element)
						}
						else if(element.id == pseudoSelector){
							manyElement.push(element)
							return element
						}
					});
					if(manyElement[0] == undefined){
						Array.prototype.map.call(parentSelector.children, element=>{
							callElement(selector, element)
						})
					}
					return manyElement[0]
				}
				result = callElement(selector, parentElement);
				return result;
			}
			
			// _________________________
	
			// get Until Parents begin from  any elements
	
			protected getParents(el:HTMLElement, parentSelector /* optional */, untilClass) {
				let parent =null;
				if (parentSelector === undefined) {
					parentSelector = document;
				}
				var parents:{}[] = [];
				var p = el.parentNode;
			
				while (p !== parentSelector) {
					var o = p;
					parents.push(o);
					p = o.parentNode;
				}
				parents.push(parentSelector); // Push that parentSelector you wanted to stop at
				for(let elem = 0; elem < parents.length; elem ++){
					if((parents[elem] as HTMLElement).classList.contains(""+untilClass)){
						parent = parents[elem]
						break;
					}
				}
				return parent;
			}
			// ________________________________
	
		protected scrollToAnchor = (aid, elementsList)=>{
			ml.$(elementsList).scrollTop(0);
			ml.$(elementsList).scrollTop(ml.$(aid).offset().top - (ml.$(elementsList).offset().top+ml.$(elementsList).height()-2*(ml.$(aid).height())));
		};
		protected upDownFunc(list, e){
			let key = e.which || e.keyCode;
			if(key == 38 || key == 40){
				Array.prototype.map.call(list.children(), (element:JQuery)=> {
					ml.$(element).removeClass("selected");
				})
			}
			if(key == 40){
				(this.countArrow == list.children().length-1)?this.countArrow = 0:this.countArrow++;
				ml.$(list.children()[this.countArrow]).addClass("selected");
				this.scrollToAnchor(list.children()[this.countArrow], list);
				
			}
			if(key == 38){
				(this.countArrow > 0)?this.countArrow--:this.countArrow = list.children().length-1;
				ml.$(list.children()[this.countArrow]).addClass("selected");
				this.scrollToAnchor(list.children()[this.countArrow], list);
			}
			if(key == 13){
				// this.currentItem = list.children()[this.countArrow];
				// timezone.__clickEachItem(list.children()[this.countArrow]);
			}
			else if(key == 8 || key != 40 && key != 38 && key != 13){
				// list.scrollTop(0);
				// this.countArrow = 0;
			}
		}
		protected callArrowIndex(list, index){
			Array.prototype.map.call(list.children(), (element)=>{
				if(element.classList.contains("selected")){
					element.classList.remove('selected')
				}
			});
			this.countArrow = index;
			list.children()[this.countArrow].classList.add("selected")
		}
		protected findElementReplaceInputValue(element){
			var findArr = [], childItem = null;
			Array.prototype.map.call(element.children[0].children[0].children, (element) => {
				findArr.push(element)
				childItem = findArr.find((el)=>{
					return el.classList.contains("ml-blueprint-item-name")
				})
				parent = this.getParents(element, document.querySelector('body'), "ml-dash-content");
			})
		}
		protected clickEachItem (list){
			if(list.children().length> 0){
				Array.prototype.map.call(list.children(), (el, index)=>{
					el.addEventListener("click",  (e)=>{
						if((this.enterTagInput[0] as HTMLInputElement).value !=""){
							this.countArrow = el.getAttribute("data-index");
						}
						this.resetOnSelect(list, el, index);			
						if(this.resetBoolean && this.closeOnSelectBoolean){
							ml.$(this.enterTagInput).blur()
						}
						this.icon(el,list);

						// Each click Item take currect position of DataList
						list = ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu`);
						let DataListParentOverlay = this.getCurrentToggleList(list[0], `#${this.corectUL}`, "ml-blueprint-multi-select-overlay");
						this.DataListPosition(DataListParentOverlay, ml.$('*[data-mlmulti-id="' + this.corectUL + '"]')[0].parentNode)
						// ________________________________________
						setTimeout(()=>{
							this.enterTagInput.focus()
						}, 0)
					})
				})
			}


		}
		protected resetOnSelect(list, el, index){
			// this.findElementReplaceInputValue(el);

			if(!this.resetBoolean){
				this.callArrowIndex(list, index)
			}
			else{
				if((this.enterTagInput[0] as HTMLInputElement).value != ""){
					(this.enterTagInput[0] as HTMLInputElement).value = ""
					// ml.$(list).scrollTop(0);
					setTimeout(()=>{
						list.children().detach();
						this.listItems();
						// this.input.focus();
						this.clickEachItem(list);
						this.callArrowIndex(list, 0)
						document.querySelectorAll('.ml-blueprint-enterTags > span').forEach(function(el,id){
							if(document.querySelector(`.ml-blueprint-suggest-menu > li[data-index="${el.getAttribute("data-icon")}"]`) != null){
								document.querySelector(`.ml-blueprint-suggest-menu > li[data-index="${el.getAttribute("data-icon")}"] i`).classList.add('mlicon-MapLarge-icons_check')
							}
						})
					}, 0)
					/* if(this.overlay[0].style.display == "block"){
						// this.overlay.show()
					} */
				}
				else{
					(this.enterTagInput[0] as HTMLInputElement).value = ""
					// ml.$(list).scrollTop(0);
					this.callArrowIndex(list, index);
				}
				
			}
		}
		private inputWidth(tags){
			if(tags.length > 0){
				(this.enterTagInput[0] as HTMLInputElement).style.width = "60px";
			}
			else{
				(this.enterTagInput[0] as HTMLInputElement).style.width = "120px";
			}
		}
		protected activeItems(tags, list){
			let ulclass = list[0].className.split(" ")[0];
			Array.prototype.map.call(tags[0].children, (elem, index)=>{
				if(elem.hasAttribute('data-icon')){
					list.find(`li[data-index=${elem.getAttribute('data-icon')}]`).find("i").addClass('mlicon-MapLarge-icons_check');
				}
			})
		}
		protected icon(el, list){
			ml.$('i', el).toggleClass('mlicon-MapLarge-icons_check')
			ml.$('.selected').removeClass('selected')
			ml.$(el).addClass('selected')
			
			var text:string = ml.$('span.ml-blueprint-item-name',el).text()
			var icon:boolean = ml.$('i', el).hasClass('mlicon-MapLarge-icons_check')
			this.text = ml.$(`<span data-icon=${ml.$(el).attr('data-index')} class="ml-blueprint-tagComp-multi">${text}</span>`);

			if(icon){
				this.tagInput.append(this.removeButoon);
				this.obj.value = text;
				this.textPush();
				
				this.obj = { value: '' };
				if(this.definition.options.onremove) {
					let text:JQuery = this.text
					this.removeIcon = ml.$(`<button><i class="mlicon-MapLarge-icons_close-delete"></i></button>`);
					this.removeIcon.addClass('ml-blueprint-multiSelect-remove');
					this.text.append(this.removeIcon);
						
					ml.$(this.removeIcon).click((e):void => {
						document.querySelector(`.ml-blueprint-suggest-menu > li[data-index="${(e.currentTarget as HTMLElement).parentElement.getAttribute('data-icon')}"] i`).classList.remove('mlicon-MapLarge-icons_check')
						ml.$(text).detach();
						this.arr.splice(text.index(),1);
						if(this.placeholderOptions) {
							this.addPlaceholder(this.placeholderOptions);
						}
						ml.$('.mlicon-MapLarge-icons_check', el).removeClass();
					});
				}
			}
			else{
				var txt: string = ml.$('span.ml-blueprint-item-name',el).text()
				let text: JQuery = ml.$('.ml-blueprint-tagComp-multi')
				ml.$.each(text, (key:number, item:any)=>{
					let text:string = ml.$(item).text()
					if(txt == text){
						ml.$(ml.$(item)).detach();
						this.arr.splice( ml.$(item).index(),1);
					}
				})
				if(this.placeholderOptions) {
					this.addPlaceholder(this.placeholderOptions);
				}	
			}
			this.inputWidth(this.arr);
			if(this.intentOptions){
				this.text.addClass(`ml-blueprint-tagComp-multi-${this.intentOptions}`);
			}
			if(this.placeholderOptions)  {
				this.removePlaceholder()	
			}
			(this.minimalOptions)  ?   this.text.addClass('ml-blueprint-tagComp-multi-minimal') : 	this.text.removeClass('ml-blueprint-tagComp-multi-minimal');							
		}

		private DataListPosition(DataListParentOverlay, element){
			setTimeout(()=>{
				// if(!this.opwnListOnlyKeyDown){
					document.querySelectorAll(".ml-blueprint-multi-select-overlay").forEach(element => {
						(element as HTMLElement).classList.remove("showList");
					});	
					DataListParentOverlay.classList.add("showList");
				// }
				let parentTransferDatList = this.getParents(DataListParentOverlay, document.querySelector("body"), "ml-blueprint-multi-select-portal"),
				buttonPosition = element.getBoundingClientRect(),
				parentTransferDatListPosition = parentTransferDatList.getBoundingClientRect(),
				top = 0,
				left = 0,
				triangleLeft = 0;
				
				// When DataList pass the window width limit
				if ((buttonPosition.left + parentTransferDatListPosition.width) > window.innerWidth) {
					left = buttonPosition.left - ((buttonPosition.left + parentTransferDatListPosition.width) - window.innerWidth) - 25;
					parentTransferDatList.classList.add("triangleTransfer");
					triangleLeft = buttonPosition.left - left + buttonPosition.width / 2 - 20;
				}
				else {
					parentTransferDatList.classList.remove("triangleTransfer");
					left = buttonPosition.left;
					triangleLeft = buttonPosition.width / 2 - 20;
				}
				// ________________________________

				// When DataList pass the window height limit
				if (buttonPosition.top + buttonPosition.height + (parentTransferDatListPosition.height) > window.innerHeight) {
					top = buttonPosition.top - parentTransferDatListPosition.height - 15;
					parentTransferDatList.classList.add("triangleBottom");
				}
				else {
					top = buttonPosition.top + buttonPosition.height;
					parentTransferDatList.classList.remove("triangleBottom");
				}
				// __________________________
				
				document.querySelector("body").style.setProperty('--triangelAfter', String(triangleLeft) + "px");
				parentTransferDatList.style.left = left + "px";
				parentTransferDatList.style.top = top + "px";
				
			}, 0)
		}
		protected validateDefinition(): boolean {
			if (!super.validateDefinition())
				return false;

			return true;
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, [], false);
			this.initTransforms()
			if (this.isDOMLoaded) {


				this.arr = this.definition.options.onAdd || [];
				this.obj = {
					value: '' ,						
				};
				
				this.textPush = ():void => {
					if(this.arr){
						this.arr.push(this.obj);
						this.enterTagInput.before(this.text)		
					}
					this.obsItems()
				}
				
				var itemNames: string[] = []

				this.obsItems = ():void => {
					itemNames = []
					ml.$.each(this.arr, function(id, item){
						itemNames[id] = item.value
					})
					obs(itemNames)
				}
				
				ml.$(this.enterTagInput).on("keydown", (e)=>{
					if(this.list.children().length != 1){
						this.upDownFunc(this.list, <any>e);
					}
				})

				this.watchDefinitionOptions((options: BPMultiSelect.IOptions) => {
						
					let items = [];
					if(options.items){
						items = options.items.map(m => {
							return {
								value: m[options.valueField || "value"], 
								label: m[options.labelField || "label"] 
							}
						});
						var fil = items.filter((el, ind)=>{
							return (el.label != "") && (el.label != undefined)
						})
						items = fil
						if(options.sortFunction)
						{
							items = items.sort(options.sortFunction);
						}
						else
						{
							items = items.sort((a:any, b:any) => {
								const labelA = a.label.toLowerCase(); 
								const labelB = b.label.toLowerCase();
								if (labelA < labelB)
									return -1;
								if (labelA > labelB)
									return 1;
								return 0;
							} );
						}
					}

					this.overlay = ml.$(this.portal).children('div').addClass('ml-blueprint-multi-select-overlay')
					this.overlay.append(this.menu)
					// this.overlay.hide()
					
					// this.enterTagInput.on('focus', function(){
					// 	that.overlay.show()
					// })
					ml.$('.namespace-blueprint-multiSelect .ml-blueprint-suggest-menu').detach()
					this.list = ml.$('<ul class="ml-blueprint-suggest-menu"></ul>')
					var DatListMultiSelect = this.list;
					this.menu.append(DatListMultiSelect)
					this.listItems = ():void=>{
						if(options.items && items.length > 0){
							ml.$.each( items, function( key:number, value:any ) {
								DatListMultiSelect.append(`<li data-index=${key}><a class='ml-blueprint-select-menu-item'><span class='ml-blueprint-item-left'><i class='ml-blueprint-item-check'></i><span class='ml-blueprint-item-name '>${value.label}</span></span> </a></li>`)				
							}.bind(this));
							this.callArrowIndex(DatListMultiSelect, this.countArrow);
							// ml.$(this.list).children()[this.countArrow].classList.add('selected')
						}
						else {
							DatListMultiSelect.append(`
								<li class="ml-blueprint-select-disabled">No result.</li>
							`)
						}
					}
					
					ml.$(this.enterTagInput).on("keyup", (e)=>{
						let lengthInputValue;
						ml.$(DatListMultiSelect).children().detach();
						let key:number = e.which || e.keyCode;
						this.clickInputOpen = false;
						let valueInputFilter: string = (<any>e.target).value.replace(/\\/g, "\\\\").replace(/\./g, '\\.').toLowerCase(),
							indexNumeric: number = 0;
						// Input value length whit dot 
						lengthInputValue = (/\./g.exec(valueInputFilter))? valueInputFilter.length - 1: valueInputFilter.length;
						// _________________________
						if(!/[!$%^&*|~=`{}\[\]";<>?,.]/g.test(valueInputFilter)){
							// this.overlay.show()
							if(ml.$(DatListMultiSelect).scrollTop() != 0 && (<any>e.target).value != ""){
								ml.$(DatListMultiSelect).scrollTop(0);
							}
							let writeMatchString = "";
							
							Array.prototype.map.call(items, (elm, indexObj)=>{
								var BoldTxt = "", replaceBold = "";
								let multiplyValue:string[] = ['label'];
								indexNumeric++;
								for(let i =0; i< multiplyValue.length; i++){
									if(i<= 0){
										writeMatchString = elm[multiplyValue[i]] 
									}
			
									if(writeMatchString.toString().toLowerCase().match(valueInputFilter) != null){
										for(let j = writeMatchString.toString().toLowerCase().match(valueInputFilter).index; j< writeMatchString.toString().toLowerCase().match(valueInputFilter).index+lengthInputValue; j++){
											if(multiplyValue[i] == "label"){
												BoldTxt += elm.label[j];
											}
										}
										replaceBold = elm.label.replace(BoldTxt, `<b>${BoldTxt}</b>`);
									}
									if(new RegExp(valueInputFilter).test(writeMatchString.toString().toLowerCase())){
										if(valueInputFilter==""){
											ml.$(DatListMultiSelect.children()[indexObj+1]).detach();
											replaceBold = elm.label;
										}
										return DatListMultiSelect.append(`<li data-index=${indexObj}><a class='ml-blueprint-suggest-menu-item '><span class='ml-blueprint-item-left'><i class='ml-blueprint-item-check'></i><span class='ml-blueprint-item-name '>${replaceBold}</span></span></a></li>`)				
									}
								}
							})
		
							if(valueInputFilter!="" || (valueInputFilter=="" && ml.$(DatListMultiSelect).children().length)> 0){
								// ml.$(ml.$(this.list).children()[this.countArrow]).addClass('selected');
							}
							if(DatListMultiSelect.children().length == 0){
								DatListMultiSelect.append(`
									<li class="ml-blueprint-suggest-disabled">No result.</li>
								`)
							}
							document.querySelectorAll('.ml-blueprint-enterTags > span').forEach(function(el,id){
								if(DatListMultiSelect.children(`li[data-index="${el.getAttribute("data-icon")}"]`)[0]){
									DatListMultiSelect.children(`li[data-index="${el.getAttribute("data-icon")}"]`).find('i')[0].classList.add('mlicon-MapLarge-icons_check')
								}
							})
						}
						else{
							if(DatListMultiSelect.children().length == 0){
								DatListMultiSelect.append(`
									<li class="ml-blueprint-suggest-disabled">No result.</li>
								`)
							}
						}
						if(!DatListMultiSelect.children()[0].classList.contains("ml-blueprint-suggest-disabled")){
							if(DatListMultiSelect.children().length == 1 || key != 40 && key != 38 && key !==13 && key != 37 && key != 39 && key != 8 && key != 27){
								this.callArrowIndex(DatListMultiSelect, 0);
							}
							if(DatListMultiSelect.children().length > 1){
								this.callArrowIndex(DatListMultiSelect, this.countArrow);
								if(key == 38 || key == 40){
									Array.prototype.map.call(DatListMultiSelect.children(), (element:Element)=> {
										ml.$(element).removeClass("selected");
									})
								}
								if(key == 40){		
									ml.$(DatListMultiSelect.children()[this.countArrow]).addClass("selected");
									this.scrollToAnchor(DatListMultiSelect.children()[this.countArrow], DatListMultiSelect);								
								}
								if(key == 38){
									ml.$(DatListMultiSelect.children()[this.countArrow]).addClass("selected");
									this.scrollToAnchor(DatListMultiSelect.children()[this.countArrow], DatListMultiSelect);
								}
							}
							if(key == 13){
								document.querySelectorAll('.openButton').forEach((elem: HTMLElement) => {
									elem.classList.remove("openButton");
								})
								this.icon(DatListMultiSelect.children()[this.countArrow],DatListMultiSelect)
								this.scrollToAnchor(DatListMultiSelect.children()[this.countArrow], DatListMultiSelect);
								this.resetOnSelect(DatListMultiSelect, DatListMultiSelect.children()[this.countArrow], this.countArrow);
								
							}
	
							
						}
						// Each enter Item take currect position of DataList
						DatListMultiSelect = ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu`);
						let DataListParentOverlay = this.getCurrentToggleList(DatListMultiSelect[0], `#${this.corectUL}`, "ml-blueprint-multi-select-overlay");
						this.DataListPosition(DataListParentOverlay, ml.$('*[data-mlmulti-id="' + this.corectUL + '"]')[0].parentNode)
						// ________________________________________
						if((<any>e.target).value !=""){
							this.keyUpBoolean = true;
						}
						this.clickEachItem(DatListMultiSelect);
					})
	
					ml.$(this.tagInput).on('click',()=>{
						if(this.openOnKeyDownOptions){
							return;
						}
						else{
							if(this.enterTagInput.val() == '' || this.closeOnSelectBoolean){
								ml.$(DatListMultiSelect).children().detach();
								this.listItems();
								this.clickEachItem(DatListMultiSelect)
								// this.enterTagInput.val('')
							}
							/* Array.prototype.map.call(document.querySelectorAll('.ml-blueprint-enterTags > span'), function(el,id){
								document.querySelectorAll('.ml-blueprint-enterTags > span').forEach(function(el,id){
									if(document.querySelector(`.ml-blueprint-suggest-menu > li[data-index="${el.getAttribute("data-icon")}"]`) != null){
										document.querySelector(`.ml-blueprint-suggest-menu > li[data-index="${el.getAttribute("data-icon")}"] i`).classList.add('mlicon-MapLarge-icons_check')
									}
								})						
							}) */
							
							// this.enterTagInput.focus()
							this.callArrowIndex(DatListMultiSelect, this.countArrow);
							this.scrollToAnchor(DatListMultiSelect.children()[this.countArrow], DatListMultiSelect)
						}
						
						ml.$(this.removeButoon).on('click',(e)=> {
							if(this.arr.length !== 0) {
								this.removeTag 	  =	ml.$('.ml-blueprint-enterTags .ml-blueprint-tagComp-multi').detach();	 
								this.removeLength = this.arr.splice(0);
								ml.$('.mlicon-MapLarge-icons_check').removeClass('mlicon-MapLarge-icons_check');
								this.removeButoon.detach();
							}
							this.inputWidth(this.arr);

							if(options.placeholder) {
								this.addPlaceholder(options.placeholder);
								this.removePlaceholder();
							};
							ml.$('.ml-blueprint-enterTagInput').blur();
						});	

					})	
					document.querySelectorAll('.ml-blueprint-tagInput').forEach((element:HTMLElement)=>{
						element.onclick = ()=>{
							this.corectUL = ml.$(element).children().find("input").attr('data-mlmulti-id');
							
							DatListMultiSelect = ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu`);
							let DataListParentOverlay = this.getCurrentToggleList(DatListMultiSelect[0], `#${this.corectUL}`, "ml-blueprint-multi-select-overlay");
							// if(!element.classList.contains("openButton")){
								this.DataListPosition(DataListParentOverlay, element);
							// };
							document.querySelectorAll('.openButton').forEach((elem:HTMLElement)=>{
								elem.classList.remove("openButton")
							})
							element.classList.add("openButton");
							
							this.activeItems(ml.$(element).find('.ml-blueprint-enterTags'), DatListMultiSelect)
							
						}
					})

					function removeSingleTag(tag:JQuery) {
						ml.$(tag).detach();
						this.arr.splice(tag.index(),1);
						if(options.placeholder){
							this.addPlaceholder(options.placeholder);
						}
						
					};

					const intents: string[] = ['primary','success','warning','danger','default'];
					intents.forEach(intent => {
						ml.$('.ml-blueprint-tagComp-multi').removeClass(`ml-blueprint-tagComp-multi-${intent}`)
						ml.$('.ml-blueprint-tagInput').removeClass(`ml-blueprint-tagInputBoxShadow-${intent}`);                    
						ml.$(this.list).removeClass(`ml-blueprint-select-${intent}`)
					}) 
	
					if (options.onAdd){	
						if(!this.once){
							options.onAdd.forEach((opt) => {
								this.text = ml.$(`<span class="ml-blueprint-tagComp-multi-default ml-blueprint-tagComp-multi">${opt.value}</span>`);
								this.enterTags.prepend(this.text);
								if(options.onremove) {
									let text:JQuery = this.text;
									this.removeIcon = ml.$('<button class="mlicon-MapLarge-icons_close-delete"></button>');
									this.removeIcon.addClass('ml-blueprint-multiSelect-remove');
									this.text.append(this.removeIcon);								
									this.removeIcon.on('click', ()=> removeSingleTag(text));
								}								
							})
							this.once = true
						}
						
						if(options.addOnBlur) {						
							this.tagInput.click(e => {
								if(ml.$(e.currentTarget).hasClass('ml-blueprint-tagInput')){
									ml.$('.ml-blueprint-enterTagInput').off('blur');
									e.preventDefault();
									e.stopPropagation();
								}							
							})
						}							
					}
					
					if(options.intent){
						this.intentOptions = options.intent; 
						ml.$('.ml-blueprint-tagComp-multi').addClass(`ml-blueprint-tagComp-multi-${options.intent}`)
						ml.$(DatListMultiSelect).addClass(`ml-blueprint-select-${options.intent}`)
					}

					if(options.minimal){
						ml.$('.ml-blueprint-tagComp-multi').addClass(`ml-blueprint-tagComp-multi-minimal`)
					}
					else{
						ml.$('.ml-blueprint-tagComp-multi').removeClass(`ml-blueprint-tagComp-multi-minimal`)
					}
					this.minimalOptions = options.minimal; 

					if(options.popoverMinimal) {
						ml.$(this.menu).addClass('ml-blueprint-suggest-minimal')
					}
					else{
						ml.$(this.menu).removeClass('ml-blueprint-suggest-minimal')
					}

					this.resetBoolean = (options.resetOnSelect)? true:false;

					this.removeButoon.children('i').addClass('mlicon-delete30');		
					
					ml.$(document).on('click',(event: JQueryEventObject)=>{
						if(!event.srcElement.closest('.ml-blueprint-tagInput') && !event.srcElement.closest('.ml-blueprint-multi-select-overlay')){
							if(DatListMultiSelect.children().length > 0 ){
								this.clickInputOpen = true;
								document.querySelectorAll('.ml-blueprint-enterTags .openButton').forEach((elem: HTMLElement) => {
									elem.classList.remove("openButton");
								})
								document.querySelectorAll(".ml-blueprint-multi-select-overlay").forEach(element => {
									(element as HTMLElement).classList.remove("showList");
								});
								if (ml.$('*[data-mlmulti-id="' + this.corectUL + '"]') != null && (ml.$('*[data-mlmulti-id="' + this.corectUL + '"]')[0] as HTMLInputElement).value != "") {
									this.countArrow = 0;
								}
								this.callArrowIndex(ml.$(`#${this.corectUL} .ml-blueprint-suggest-menu`), this.countArrow);
							}
						}
					});
							
					this.openOnKeyDownOptions = options.openOnKeyDown;

					(options.placeholder) ?  this.addPlaceholder(options.placeholder) : false;
					this.placeholderOptions = options.placeholder;
					
					this.enterTagInput[0].addEventListener("focus", ()=>{
						this.enterTagInput.closest('.ml-blueprint-tagInput').addClass(`ml-blueprint-tagInputBoxShadow-${options.intent}`);
					})
					this.enterTagInput[0].addEventListener("blur", ()=>{
						this.enterTagInput.closest('.ml-blueprint-tagInput').removeClass(`ml-blueprint-tagInputBoxShadow-${options.intent}`);                    
					})
					this.enterTagInput[0].addEventListener("keydown", (e)=>{
						let key = e.keyCode || e.which;
						if(key == 27){
							document.querySelectorAll(".ml-blueprint-multi-select-overlay").forEach(element => {
								(element as HTMLElement).classList.remove("showList");
							});
							document.querySelectorAll('.ml-blueprint-enterTags .openButton').forEach((elem:HTMLElement)=>{
								elem.classList.remove("openButton")
							})
							
							Array.prototype.map.call(DatListMultiSelect.children(), (elem, index)=>{
								if(elem.classList.contains("selected")){
									this.indexEsc = index;
								}
							})
							this.countArrow = this.indexEsc;
							this.enterTagInput.blur()
						}
						if(key == 8){
							var text:JQuery = ml.$('.ml-blueprint-tagComp-multi').last()
							// var active:string = 'ml-blueprint-tagComp-multi-active'
							if(this.enterTagInput.val() == ''){
								var txt:JQuery = ml.$('.ml-blueprint-item-name')
								// if(ml.$(text).hasClass(active)){
									ml.$.each(txt, function(key:number, item:any){
										if(text.text() == ml.$(item).text()){
											ml.$('i', ml.$(item).parent()).removeClass('mlicon-MapLarge-icons_check')
										}
									})
									ml.$(text).detach()
									// ml.$('.ml-blueprint-tagComp-multi').last().addClass(active)
									this.arr.splice(text.index(),1);
									this.inputWidth(this.arr);
									if(this.placeholderOptions) {
										this.addPlaceholder(this.placeholderOptions)
									}
									
								// }
								// else{
								// 	ml.$(text).addClass(active)
								// }
							}
							

							if(ml.$(this.tagInput).find('.ml-blueprint-enterTags').children().length == 1){
								this.removeButoon.detach()
							}
						}
	
					})
					
					this.tagInput.click(() => {
						this.enterTagInput.focus()
					}) 

					ml.$(document).on('click keyup', ()=>{
						this.obsItems()
					})

			
			
				});


			}
		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPMultiSelect.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPMultiSelect {

		/**
		 * Properties for Multi Select component
		 * @typedef IMultiSelectProps
		 * @interface
		 * @readonly
		 * @augments ctrl.BPBase.IControlProps
		 */
		export interface IMultiSelectProps extends ctrl.BPBase.IControlProps{
			/**
			 * Items for multi select
			 * @typedef items[]
			 * @property {string} text - Item text
			 * @property {any} value - Item value
			 */
			items?: any[],
			/**
			 * Intent color of the multi select
			 * @type {ctrl.BPBase.Intent}
			 */
			intent?: ctrl.BPBase.Intent,
			/**
			 * Items to add on multiselect
			 * @type {ctrl.BPBase.IOptionProps}
			 */
			onAdd?: ctrl.BPBase.IOptionProps[],
			/**
			 * Toggle minimal popover styling for multiselect
			 * @type {boolean}
			 */
			popoverMinimal?: boolean,
			/**
			 * Reset selection when selecting an option
			 * @type {boolean}
			 */
			resetOnSelect?: boolean,
			/**
			 * Open multi select by pressing a key
			 * @type {boolean}
			 */
			openOnKeyDown?: boolean,
			/**
			 * Make selected items removeable
			 * @type {boolean}
			 */
			onremove?: boolean,
			/**
			 * The name of the value field in the array of items passed to the MultiSelect
			 * @type {string}
			 */
			valueField?: string,
			/**
			 * The name of the label field in the array of items passed to the MultiSelect
			 * @type {string}
			 */
			labelField?: string,
			/**
			 * Custom sorting function for the data inside the MultiSelect
			 * @function
			 * @param {any} a - First item for comparison
			 * @param {any} b - Second item for comparison 
			 * @return {number}
			 */
			sortFunction?: (a: any, b: any) => number
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IMultiSelectProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			items?: any[] | mdash.IScopePath,
			intent?: ctrl.BPBase.Intent | mdash.IScopePath,
			onAdd?: ctrl.BPBase.IOptionProps[] | mdash.IScopePath,
			minimal?: boolean | mdash.IScopePath,
			popoverMinimal?: boolean | mdash.IScopePath,
			openOnKeyDown?: boolean | mdash.IScopePath,
			placeholder?:  string | mdash.IScopePath,
			resetOnSelect?:  boolean | mdash.IScopePath,
			onremove?: boolean | mdash.IScopePath,
			addOnBlur?: (Event: MouseEvent) => void | mdash.IScopePath,
			valueField?: string | mdash.IScopePath,
			labelField?: string | mdash.IScopePath
			sortFunction?: (a: any, b: any) => number
		}
	}

	mdash.registerControl(BPMultiSelect,'dashboard.components.controls', BPMultiSelect.meta);
}