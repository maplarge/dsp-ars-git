namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPOmnibar extends mdash.BaseInputControl<BPOmnibar.IOptionsDefinition> {
		public static type: string = 'BPOmnibar';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Omnibar',
			shortDescription: "Omnibar is a component that renders a search/action bar.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.SINGLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPOmnibar.builder().options(BPOmnibar.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPOmnibar.IOptions> = {
			'BPOmnibar': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label:'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label:'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		private	mainOmnibar: JQuery;
		private	overlayOmnibar: JQuery;
		private	button: JQuery;
		private portal: JQuery;
		private modal: JQuery;
		private input: JQuery;
		private list: JQuery;
		private text: string;
		private backdrop: JQuery;
		private toastMessage: JQuery;
		protected obsNewValue: Function;
		protected closeOnSelectBoolean = false;
		protected clickInputOpen = true;
		protected listItems:Function;
		protected keyUpBoolean:boolean  = false;
		private countArrow:number = 0;
		protected resetBoolean = false;
		protected clickCountLi:number = 0;

		constructor(id: string, definition: mdash.IControlDefinition<BPOmnibar.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();

			this.container.classList.add('namespace-blueprint-select');
			this.container.classList.add('namespace-blueprint-omnibar');
			this.mainOmnibar = ml.$('<span></span>')
			this.mainOmnibar.addClass('ml-blueprint-omnibar-control')
			this.portal = ml.$('<div></div>')
			this.portal.addClass('ml-blueprint-omnibar-portal')
			this.overlayOmnibar = ml.$('<div></div>')
			this.portal.append(this.overlayOmnibar)
			this.button = ml.$('<button>Click to show Omnibar</button>')
			this.button.addClass('ml-blueprint-omnibar-button')
			this.modal = ml.$('<div></div>')
			this.modal.addClass('ml-blueprint-select-modal')
			this.contentDiv.append( this.mainOmnibar);	
			ml.$('body').append(this.modal,this.portal)	
			
		}

		protected validateDefinition(): boolean {
			
			if (!super.validateDefinition())
			return false;
			
			return true;
		}
		
		//  each button click get him Toggle data list 
		protected getCurrentToggleList(eventCurrent, untilParentClass, toggleListClass){
			let parent, child;
			parent = this.getParents(eventCurrent, document.querySelector('.'+untilParentClass), untilParentClass);
			child = this.findJSFunction(toggleListClass, parent);
			return child;
		}
		// ___________________________

		// find any element by class, id and tagName into the Main elemnt
		protected findJSFunction(selector, parentElement){
			var empChild = [], manyElement = [], result = null;
			function callElement(pseudoSelector, parentSelector){
				Array.prototype.map.call((parentSelector.children as HTMLElement), (elem:Element)=>empChild.push(elem))
				empChild.find((element)=>{
					if(element.classList.contains(pseudoSelector)){
						manyElement.push(element)
					}
					else if(element.tagName == pseudoSelector.toUpperCase()){
						manyElement.push(element)
					}
					else if(element.id == pseudoSelector){
						manyElement.push(element)
						return element
					}
				});
				if(manyElement[0] == undefined){
					Array.prototype.map.call(parentSelector.children, element=>{
						callElement(selector, element)
					})
				}
				return manyElement[0]
			}
			result = callElement(selector, parentElement);
			return result;
		}
		
		// _________________________

		// get Until Parents begin from  any elements

		protected getParents(el:HTMLElement, parentSelector /* optional */, untilClass) {
			let parent =null;
			if (parentSelector === undefined) {
				parentSelector = document;
			}
			var parents:{}[] = [];
			var p = el.parentNode;
		
			while (p !== parentSelector) {
				var o = p;
				parents.push(o);
				p = o.parentNode;
			}
			parents.push(parentSelector); // Push that parentSelector you wanted to stop at
			for(let elem = 0; elem < parents.length; elem ++){
				if((parents[elem] as HTMLElement).classList.contains(""+untilClass)){
					parent = parents[elem]
					break;
				}
			}
			return parent;
		}
		// ________________________________

		protected scrollToAnchor = (aid:HTMLLIElement, elementsList:JQuery)=>{
			ml.$(elementsList).scrollTop(0);
			ml.$(elementsList).scrollTop(ml.$(aid).offset().top - (ml.$(elementsList).offset().top+ml.$(elementsList).height()-2*(ml.$(aid).height())));
		};
		protected upDownFunc(list:JQuery, e:JQueryEventObject){
			let key:number = e.which || e.keyCode;
			if(key == 38 || key == 40){
				Array.prototype.map.call(list.children(), (element:HTMLElement)=> {
					ml.$(element).removeClass("selected");
				})
			}
			if(key == 40){
				(this.countArrow == list.children().length-1)?this.countArrow = 0:this.countArrow++;
				ml.$(list.children()[this.countArrow]).addClass("selected");
				this.scrollToAnchor((list.children()[this.countArrow] as HTMLLIElement), list);
			}
			if(key == 38){
				(this.countArrow > 0)?this.countArrow--:this.countArrow = list.children().length-1;
				ml.$(list.children()[this.countArrow]).addClass("selected");
				this.scrollToAnchor((list.children()[this.countArrow] as HTMLLIElement), list);
			}
			if(key == 13){
				// this.currentItem = list.children()[this.countArrow];
				// timezone.__clickEachItem(list.children()[this.countArrow]);
			}
			else if(key == 8 || key != 40 && key != 38 && key != 13){
				// list.scrollTop(0);
				// this.countArrow = 0;
			}
		}
		protected callArrowIndex(list:JQuery, index:number){
			Array.prototype.map.call(list.children(), (element:HTMLElement)=>{
				if(element.classList.contains("selected")){
					element.classList.remove('selected')
				}
			});
			this.countArrow = index;
			list.children()[this.countArrow].classList.add("selected")
		}
		protected findElementReplaceInputValue(element:HTMLElement){
			let findArr:{}[] = [], childItem: Object = null;
			Array.prototype.map.call(element.children[0].children[0].children, (element:HTMLElement):void => {
				findArr.push(element)
				childItem = findArr.find((el:HTMLElement)=>{
					return el.classList.contains("ml-blueprint-item-name")
				})
				// parent = this.getParents(element, document.querySelector('body'), "ml-dash-content");

			})
		}
		protected clickEachItem (list:JQuery, countClick){
			if(list.children().length> 0){
				countClick++;
				Array.prototype.map.call(list.children(), (el:HTMLElement, index:number)=>{
					el.addEventListener("click", (e)=>{
						if(countClick == 1){
							
							if((this.input[0] as HTMLInputElement).value !=""){
								this.countArrow = Number(el.getAttribute("data-index"));
							}
							el.classList.add('selected');
							this.resetOnSelect(list, el, index);			
							if(this.resetBoolean && this.closeOnSelectBoolean){
								ml.$(this.input).blur()
							}
							this.backdrop.hide()
							this.toast(el)
						}
					})
				})
			}
		}
		protected toast(el:HTMLElement){
			this.text = ml.$('span.ml-blueprint-item-name',el).text()
			let remove:JQuery = ml.$('<div><i class="mlicon-MapLarge-icons_close-delete"></i></div>')
			this.toastMessage = ml.$(`<div></div>`)
			remove.addClass('ml-blueprint-item-remove')
			this.toastMessage.addClass('ml-blueprint-item-toast')
			this.toastMessage.append(`<span>${ this.definition.options.selectedText || `You selected  <b>${this.text}.</b></span>`}`)
			this.toastMessage.append(remove)

			if(this.text!=''){
				ml.$(this.modal).append(this.toastMessage)
			}
		
			ml.$(remove).on('click',function():void{
				ml.$(this).parent().detach()
			})
			setTimeout(():void => {
				ml.$(remove).parent().detach()
			}, 5000);
		}
		protected resetOnSelect(list:JQuery, el:HTMLElement, index:number){
			this.findElementReplaceInputValue(el);

			if(!this.resetBoolean){
				this.callArrowIndex(list, index)
			}
			else{
				if((this.input[0] as HTMLInputElement).value != ""){
					(this.input[0] as HTMLInputElement).value = ""
					// ml.$(list).scrollTop(0);
					setTimeout(()=>{
						list.children().detach();
						this.listItems();
						// this.input.focus();
						this.clickEachItem(list, this.clickCountLi);
						this.callArrowIndex(list, 0)
					}, 0);
				}
				else{
					(this.input[0] as HTMLInputElement).value = ""
					// ml.$(list).scrollTop(0);
					this.callArrowIndex(list, index);
				}
				this.list.hide()
			}


		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			this.initTransforms()
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPOmnibar.IOptions) => {
					ml.$('.ml-blueprint-omnibar-control').html('')
					ml.$('.ml-blueprint-overlay-omnibar').html('')
					let items = [];
					if(options.items){
						items = options.items.map(m => {
							return {
								value: m[options.valueField || "value"], 
								label: m[options.labelField || "label"] 
							}
						});
						var fil = items.filter((el, ind)=>{
							return (el.label != "") && (el.label != undefined)
						})
						items = fil
						if(items.length>0){
							items.unshift({value: "", label: ""});
						}

						if(items.length > 2){
							if(options.sortFunction)
							{
								items = items.sort(options.sortFunction);
							}
							else
							{
								items = items.sort((a:any, b:any) => {
									const labelA = a.label.toLowerCase();
									const labelB = b.label.toLowerCase();
									if (labelA < labelB)
										return -1;
									if (labelA > labelB)
										return 1;
									return 0;
								} );
							}
						}
					}
		
					let key:JQuery = ml.$('<span></span>')
					this.mainOmnibar.append(this.button)
					this.mainOmnibar.append(' or press ')
					this.mainOmnibar.append(key)
					
					key.addClass('ml-blueprint-key')
					key.append(`<span><i class="mlicon-arrow-up"></i> shift</span>`)
					key.append(`<span>O</span>`)
					
					this.overlayOmnibar.addClass('ml-blueprint-overlay-omnibar')
					this.backdrop = this.overlayOmnibar.append('<div class="ml-blueprint-overlay-backdrop"></div>')
					this.backdrop.hide()
					this.input = ml.$(`<input class='ml-blueprint-select-filter' placeholder='Search...' type='text' />`)
					
					let overlayContent: JQuery =  ml.$('<div class="ml-blueprint-overlay-content"></div>')
					let inputGroup: JQuery  = ml.$('<div></div>')
					inputGroup.addClass('ml-blueprint-select-input-group')
					inputGroup.append('<i class="mlicon-x16Search"></i>')
					inputGroup.append(this.input)
					overlayContent.append(inputGroup)
					this.overlayOmnibar.append(overlayContent)
					
					this.list = ml.$('<ul></ul>')
					this.list.addClass('ml-blueprint-select-menu')
					overlayContent.append(this.list)
					
					this.listItems = ():void=>{
						if(options.items && items.length > 1){
							ml.$.each( items, function( key:number, value:any ):void {
								this.list.append(`<li data-index=${key}><a class='ml-blueprint-select-menu-item'><span class='ml-blueprint-item-left'> <span class='ml-blueprint-item-name'>${value.label}</span></span> </a></li>`);
							}.bind(this));
							this.callArrowIndex(this.list, this.countArrow);
							// ml.$(list).children()[this.countArrow].classList.add('selected')
						}
						else{
							this.list.append(`
								<li class="ml-blueprint-select-disabled">No result.</li>
							`)
						}
					}
					
					this.list.hide()

					ml.$(this.input).on("keyup", (e:JQueryEventObject):void=>{
						
						let inputVal:string = this.input.val().toLowerCase();
						(inputVal.length == 0) ? this.list.hide() : this.list.show()
						
						ml.$(this.list).children().detach();
						let key:number = e.which || e.keyCode;
						this.clickInputOpen = false;
						let valueInputFilter = (<any>e.target).value.replace(/\\/g, "\\\\").toLowerCase(),
						indexNumeric:number = 0;
						if(!/[!$%^&*|~=`{}\[\]";<>?,]/g.test(valueInputFilter)){
							if(ml.$(this.list).scrollTop() != 0 && (<any>e.target).value != ""){
								// ml.$(this.list).scrollTop(0);
							}
						let writeMatchString:string = "";
		
						Array.prototype.map.call(items, (elm:{label:string, value:string}, indexObj:string)=>{
							let BoldTxt:string = "", replaceBold:string = "";
							let multiplyValue:string[] = ['label'];
							indexNumeric++;
							for(let i =0; i< multiplyValue.length; i++){
								if(i<= 0){
									writeMatchString = elm[multiplyValue[i]]
								}
								if(writeMatchString.toString().toLowerCase().match(valueInputFilter) != null){
									for(let j = writeMatchString.toString().toLowerCase().match(valueInputFilter).index ; j< writeMatchString.toString().toLowerCase().match(valueInputFilter).index+valueInputFilter.length; j++){
										if(multiplyValue[i] == "label"){
											BoldTxt += elm.label[j];
										}
									}
									replaceBold = elm.label.replace(BoldTxt, `<b>${BoldTxt}</b>`);
								}
								if(new RegExp(valueInputFilter).test(writeMatchString.toString().toLowerCase())){
									if(valueInputFilter==""){
										ml.$(this.list.children()[indexObj+1]).detach();
										replaceBold = elm.label;
									}
									return this.list.append(`<li  data-index=${indexObj}><a class='ml-blueprint-select-menu-item '><span class='ml-blueprint-item-left'> <span class='ml-blueprint-item-name '>${replaceBold}</span></span></a></li>`)
								}
							}
						})
		
						if(valueInputFilter!="" || (valueInputFilter=="" && ml.$(this.list).children().length)> 0){
							// ml.$(ml.$(this.list).children()[this.countArrow]).addClass('selected');
						}
						if(this.list.children().length == 0){
							this.list.append(`
								<li class="ml-blueprint-select-disabled">No result.</li>
							`)
						}
						}
						else{
							if(this.list.children().length == 0){
								this.list.append(`
									<li class="ml-blueprint-select-disabled">No result.</li>
								`)
							}
						}
						if(!this.list.children()[0].classList.contains("ml-blueprint-select-disabled") && this.list.css('display') != 'none'){
		
							if(this.list.children().length == 1 || key != 40 && key != 38 && key !==13 && key != 37 && key != 39){
								this.callArrowIndex(this.list, 0);
							}
							if(this.list.children().length > 1 ){
								this.callArrowIndex(this.list, this.countArrow);
								if(key == 38 || key == 40){
									Array.prototype.map.call(this.list.children(), (element:Element)=> {
										ml.$(element).removeClass("selected");
									})
								}
								if(key == 40){		
									ml.$(this.list.children()[this.countArrow]).addClass("selected");
									this.scrollToAnchor((this.list.children()[this.countArrow] as HTMLLIElement), this.list);								
								}
								
								if(key == 38){
									ml.$(this.list.children()[this.countArrow]).addClass("selected");
									this.scrollToAnchor((this.list.children()[this.countArrow] as HTMLLIElement), this.list);
								}
							}
							if(key == 13){
								this.toast(this.list.children()[this.countArrow])
								this.backdrop.hide()
								this.scrollToAnchor((this.list.children()[this.countArrow] as HTMLLIElement), this.list);
								this.resetOnSelect(this.list, this.list.children()[this.countArrow], this.countArrow);
							}
							
						}
						// if((<any>e.target).value !="" && key != 37 || key != 39){
						// 	console.log((<any>e.target).value)
						// 	this.scrollToAnchor(this.list.children()[this.countArrow], this.list);
						// }
						if(!this.list.children()[0].classList.contains("ml-blueprint-select-disabled") && this.list.css('display') == 'none'){
							if( key == 13 ){
								this.text = "",
								this.backdrop.hide()
							}
						}
						if((<any>e.target).value !=""){
							this.keyUpBoolean = true;
						}
						ml.$(this.input).focus()
						this.clickEachItem(this.list, this.clickCountLi)
					})
					ml.$(this.input).on("keydown", (e:JQueryEventObject):void=>{
						if(this.list.children().length != 1){
							this.upDownFunc(this.list, <any>e);
						}
					})
					ml.$(this.input).on("focus", (e:JQueryEventObject):void=>{
						this.clickCountLi = 0
					});
					ml.$(this.button).on('click',(e):void=>{
						if(this.input.val() == '' || this.closeOnSelectBoolean){
							ml.$(this.list).children().detach();
							this.listItems();
							this.input.val('')
						}
						// this.clickEachItem(this.list)
						this.backdrop.show()
						this.input.focus()
						this.callArrowIndex(this.list, this.countArrow);
						this.scrollToAnchor((this.list.children()[this.countArrow] as HTMLLIElement), this.list)
					})	

					const intents:string[] = ['primary','success','warning','danger', 'default'];
					intents.forEach(intent => {
						ml.$(this.list).removeClass(`ml-blueprint-select-${intent}`)
					}) 
				
					if(options.intent){
						ml.$(this.list).addClass(`ml-blueprint-select-${options.intent}`)
					}
				
					this.resetBoolean = (options.resetOnSelect)? true:false;
					(options.isShow)?  this.mainOmnibar.show() :  this.mainOmnibar.hide();

					ml.$(document).on('keydown', (e:JQueryEventObject):void=>{
						if(e.keyCode == 27){
							let indexEsc = 0;
							this.backdrop.hide()
                            Array.prototype.map.call(this.list.children(), (elem, index)=>{
                                if(elem.classList.contains("selected")){
                                    indexEsc = index;
                                }
                            })
                            this.countArrow = indexEsc;
                        }
					})
					ml.$(document).on('keyup', (e:JQueryEventObject):void=>{
						
						if(e.shiftKey && e.which == 79){
							this.backdrop.show()
							this.input.focus()
						}
						obs(this.text || '')
					})
					ml.$(document).on('click',()=>{
						obs(this.text || '')
					})
					ml.$(this.backdrop).on('click',(event: JQueryEventObject):void=>{
						if(!ml.$(event.target).parentsUntil().hasClass('ml-blueprint-overlay-content')){
							if(this.list.children().length > 0 ){
								this.backdrop.hide();
								// if((this.input[0] as HTMLInputElement).value !=""){
								// 	this.countArrow = 0;
								// }
								this.callArrowIndex(this.list, this.countArrow);
								this.clickInputOpen = true;
							}
						}	
					})
					this.obsNewValue = (newValue)=>{
						this.input.val("")
						this.list.hide()
						ml.$(this.list).children().detach()
						this.listItems();
						this.backdrop.hide()
						ml.$('.ml-blueprint-select-menu').children().map((id,value) => {
							ml.$(value).removeClass('selected');
							if((value as HTMLElement).innerText.trim().toLowerCase() == newValue.toLowerCase()){
								this.countArrow = Number(ml.$(value).attr('data-index'))
								ml.$(value).addClass('selected')
								ml.$(this.contentDiv).on('click',()=>{
									this.scrollToAnchor((ml.$('.ml-blueprint-select-menu').children()[this.countArrow] as HTMLLIElement), ml.$('.ml-blueprint-select-menu'));
								})
								this.backdrop.hide()
							}
						})
					}
					this.objectsToDispose.push(
						obs.subscribe((newValue) => {
							if (obs() == null || newValue.toLowerCase() != String(this.text).toLowerCase()) {
								this.obsNewValue(newValue)
								this.toast(this.list.children()[this.countArrow])
							}
							obs(this.text)
						})
					);
					this.obsNewValue(obs())

				});

			}
		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPOmnibar.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPOmnibar {

		/**
		 * Props for omnibar component
		 * @typedef IOmnibarProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface IOmnibarProps extends BPBase.IControlProps{
			/**
			 * Items for the omnibar
			 * @typedef items[]
			 * @property {string} label - Item text
			 * @property {any} value - Item value
			 */
			items?: any[] ,
			/**
			 * Show/Hide omnibar
			 * @type {boolean}
			 */
			isShow?: boolean;
			/**
			 * Show/Hide omnibar on keypress
			 * @type {boolean}
			 */
			resetOnSelect?: boolean;
			/**
			 * Intent color for the omnibar
			 * @type {ctrl.BPBase.Intent}
			 */
			intent?: ctrl.BPBase.Intent;
			/**
			 * The name of the value field in the array of items passed to the Omnibar
			 * @type {string}
			 */
			valueField?: string,
			/**
			 * The name of the label field in the array of items passed to the Omnibar
			 * @type {string}
			 */
			labelField?: string,
			/**
			 * Custom sorting function for the data inside the Omnibar
			 * @function
			 * @param {any} a - First item for comparison
			 * @param {any} b - Second item for comparison 
			 * @return {number}
			 */
			sortFunction?: (a: any, b: any) => number,
			selectedText?: string,

		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IOmnibarProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			items?: any[] | mdash.IScopePath,
			isShow?: boolean | mdash.IScopePath,
			intent?: ctrl.BPBase.Intent | mdash.IScopePath,
			resetOnSelect?: boolean | mdash.IScopePath,
			selectedText?: string | mdash.IScopePath,
			valueField?: string | mdash.IScopePath,
			labelField?: string | mdash.IScopePath,
			sortFunction?: (a: any, b: any) => number
		}
	}

	mdash.registerControl(BPOmnibar,'dashboard.components.controls', BPOmnibar.meta);
}