namespace dashboard.components.controls {
	import mdash = ml.ui.dashboard;

	export class BPPanelStack extends mdash.BaseControl<BPPanelStack.IOptionsDefinition> {
		public static type: string = 'BPPanelStack';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Panel Stack',
			shortDescription: "The panelStack is a container component that itself contains multiple panel groups.",
			categories: ["Layout"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPPanelStack.builder().options(BPPanelStack.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPPanelStack.IOptions> = {
			'BPPanelStack': {
				options: {
					label: 'Progress',
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		
        private button:JQuery;
        private panelStack:JQuery;
		private panelStackView:JQuery;
        private panelStackHeader:JQuery;
        private panelStackContents:JQuery;
        private panelStacHeading:JQuery;
        private panelStacHeaderBack:JQuery;
        private panelBackText:string;
		private arr: string[];

		constructor(id: string, definition: mdash.IControlDefinition<BPPanelStack.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
			this.container.classList.add('namespace-blueprint-panelStack')
			this.container.classList.add('namespace-blueprint-button')
			this.panelStack = ml.$('<div class="ml-blueprint-panel-stack"></div>')
			this.panelStackView = ml.$('<div class="ml-blueprint-panel-stack-view"></div>')
			this.panelStackHeader = ml.$('<div class="ml-blueprint-panel-stack-header"></div>')
			this.panelStacHeading = ml.$('<div class="ml-blueprint-heading"></div>')
			this.panelStacHeaderBack = ml.$("<span class='ml-blueprint-panel-stack-headering-back'></span>")
			this.panelStackHeader.append(this.panelStacHeaderBack, this.panelStacHeading, '<span></span>')
			this.button = ml.$('<button class="ml-blueprint-button">Open new panel</button>')
			this.panelStackContents = ml.$(`<div class="ml-blueprint-panel-stack-contents"></div>`)
			this.panelStack.append(this.panelStackView)
			this.panelStackView.append(this.panelStackHeader, this.panelStackContents)
			this.contentDiv.append(this.panelStack);	
			this.panelStackContents.append(this.button)
		}

		protected validateDefinition(): boolean {


			if (!super.validateDefinition())
				return false;

			return true;
		}

		protected init() {
			super.init();

			var obs = this.registerScopeObservable(this.definition.name, null, false);

			if (this.isDOMLoaded) {

				this.arr = [];
				var i:number = Number(this.definition.options.panelNumber);
				
				var panelUpdate = ()=>{
					this.panelStacHeading.text(`Panel ${i}`)
					this.arr.push(this.panelStacHeading.text())
					this.panelStacHeaderBack.html(`<button class="ml-blueprint-button"><i class="mlicon-MapLarge-icons_arrow-left"></i></button>`)
					this.panelBackText = this.arr[i-2]
					ml.$(this.panelStacHeaderBack).children('button').append(this.panelBackText)
					if(i == 1){
						this.panelStacHeaderBack.html('<span></span>')
					}
				}
				
				this.button.on('click',()=>{
					i++;
					panelUpdate()
				})
				ml.$(this.panelStacHeaderBack).on('click',()=>{
					i--;
					panelUpdate()
				})
				panelUpdate()

				this.watchDefinitionOptions((options: BPPanelStack.IOptions) => {

					this.panelStacHeading.text(options.heading)

					const intents:string[] = ['primary','success','warning','danger', 'default'];
						intents.forEach(intent => {
							ml.$(this.button).removeClass(`ml-blueprint-button-intent-${intent}`)
						}) 

					ml.$(document).on('click', '.ml-blueprint-panel-stack-contents .ml-blueprint-button', ()=>{
						this.panelStackView.clone().addClass("cloneAnim").appendTo(this.panelStack)
				
					})

					if(options.intent){
						ml.$(this.button).addClass(`ml-blueprint-button-intent-${options.intent}`)
					}

				});
			}
			
		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPPanelStack.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPPanelStack {

		/**
		 * Props for Panel Stack
		 * @typedef IPanelStackProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IActionProps
		 * @augments BPBase.IAppearance
		 */
		export interface IPanelStackProps extends BPBase.IActionProps, BPBase.IAppearance{
			/**
			 * Panel stack heading text
			 * @type {string}
			 */
			heading?: string;
			/**
			 * Panel number
			 * @type {number}
			 */
			panelNumber?: number;
		}
		
		export interface IOptions extends mdash.IControlOptionsWithTransforms, IPanelStackProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			intent?: ctrl.BPBase.Intent | mdash.IScopePath;
			heading?: string | mdash.IScopePath;
			panelNumber?: number | mdash.IScopePath;
		}
	}
	
	mdash.registerControl(BPPanelStack,'dashboard.components.controls', BPPanelStack.meta);
}
