namespace dashboard.components.controls {

    import mdash = ml.ui.dashboard;

    export class BPEditableText extends mdash.BaseControl<BPEditableText.IOptionsDefinition> {
        public static type: string = 'BPEditableText';

        public static meta: mdash.IControlMeta = {
            status: mdash.ControlReleaseStatus.ALPHA,
            level: mdash.ControlLevel.BASIC,
            friendlyName: 'Editable Text',
            shortDescription: "EditableText appears as normal UI text but transforms into a text input field when the user focuses it.",
            categories: ["Input"],
            childSupport: mdash.ChildSupport.MULTIPLE
        };
        public static initialOptions: mdash.IDashboardInitialOptions<BPEditableText.IOptions> = {
            'BPEditableText': {
                options: {
                    outputTransforms: []
                }
            }
        };
        public static configSchema: mdash.IComponentConfigSchema = {
            settings: [
                {
                    path: 'validators',
                    label: 'Validators',
                    type: mdash.IComponentConfigSettingTypes.VALIDATOR,
                    isArray: true
                },
                {
                    path: 'outputTransforms',
                    label: 'Transforms',
                    type: mdash.IComponentConfigSettingTypes.TRANSFORM,
                    isArray: true,
                    transformSources: ['Text']
                },
            ]
        }
        private inputAndSpan:HTMLElement;
        private coommonArea:HTMLElement;
        private textareaAndSpan: HTMLElement;
        private textAreaHeight:number = 0;
        private AllSelected:boolean = false;
        private newMultiLine:boolean = false;
        private defaultColorIntent:string;
        private maxLength:any;
        private inputValue:string ="Edit input...";
        private textAreaValue:string = "Edit textarea...";

        constructor(id: string, definition: mdash.IControlDefinition<BPEditableText.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
            super(id, definition, container, scope, parent);

        }

        public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
            return BPEditableText.builder().options(BPEditableText.initialOptions.GenericOptions.options).toJSON();
        }

        public static builder() {
            return new mdash.ControlDefinitionBuilderWithChildren<BPEditableText.IOptionsDefinition>(this.type);
        }

        public build() {

            super.build();
            this.container.classList.add('namespace-blueprint-EditableText');
            this.coommonArea  = ml.create("div", "bpEditable", this.contentDiv[0]);
            this.inputAndSpan = document.createElement("div");
            this.inputAndSpan.className = "inputWrap";
            this.inputAndSpan.innerHTML = `
                <input class="ml-blueprint-edit-input">
                <span></span>
            `;
            
            (this.textareaAndSpan as HTMLElement) = document.createElement("div");
            this.textareaAndSpan.className = "textareaWrap";
            this.textareaAndSpan.innerHTML =`
                <textarea rows="3" class="ml-blueprint-edit-textarea"></textarea>
                <span></span>
            `;
            this.coommonArea.classList.add("ml-blueprint-Area");
            let elementArr = [this.inputAndSpan , this.textareaAndSpan];

            setTimeout(()=>{
                this.textAreaHeight = this.findJSFunction('textarea', this.textareaAndSpan).getBoundingClientRect().height;
            }, 0)
            for(let key in elementArr){
                this.coommonArea.appendChild(elementArr[key])
            }
        }

        protected validateDefinition(): boolean {

            if (!super.validateDefinition())
                return false;

            return true;

        }
        
        // protected findJSFunction(selector, parentElement){
        //     let empChild = [], manyElement = [];
        //     Array.prototype.map.call((parentElement.children as HTMLElement), (elem:Element)=>empChild.push(elem))
        //     empChild.find((element)=>{
        //         if(element.classList.contains(selector)){
        //             manyElement.push(element)
        //         }
        //         else if(element.tagName == selector.toUpperCase()){
        //             manyElement.push(element)
        //         }
        //         else if(element.id == selector){
        //             manyElement.push(element)
        //             return element
        //         }
        //     });
            
        //     return manyElement;
        // }
        protected findJSFunction(selector, parentElement){
			var empChild = [], manyElement = [], result = null;
			function callElement(pseudoSelector, parentSelector){
					Array.prototype.map.call((parentSelector.children as HTMLElement), (elem:Element)=>empChild.push(elem))
					empChild.find((element)=>{
						if(element.classList.contains(pseudoSelector)){
						manyElement.push(element)
					}
					else if(element.tagName == pseudoSelector.toUpperCase()){
						manyElement.push(element)
					}
					else if(element.id == pseudoSelector){
						manyElement.push(element)
						return element
					}
				});
				if(manyElement[0] == undefined){
					Array.prototype.map.call(parentSelector.children, element=>{
						callElement(selector, element)
					})
				}
				return manyElement[0]
			}
			result = callElement(selector, parentElement);
			return result;
		}
        protected inputData(inputPlace, obs):void{
            let input:HTMLInputElement = this.findJSFunction('input', this.inputAndSpan),
            spanWithInput:HTMLSpanElement = this.findJSFunction('span', this.inputAndSpan),
            inputPlaceholder:string = inputPlace;
          
            function Input():void{
                this.__init.call(this)
            };
            (Input.prototype as object) = { 
                constructor:Input,
                __init:function(){
                    setTimeout(()=>{
                        this.__inputStyle();
                    }, 0)
                },
                __inputStyle:():void=>{
                    input.style.height = spanWithInput.getBoundingClientRect().height +"px";
                    input.style.width = spanWithInput.getBoundingClientRect().width +"px";
                },
                __focusoutInput:(e:Event):void=>{
                    spanWithInput.style.visibility = "visible"
                    input.style.visibility = "hidden";
                    input.style.position = "absolute";
                    if(input.value == ""){
                        spanWithInput.classList.remove("colorBlack");
                        spanWithInput.innerText = inputPlaceholder;
                    }
                    else{
                        if(this.defaultColorIntent == "default"){
                            spanWithInput.classList.add("colorBlack");
                        }
                        spanWithInput.innerText = input.value;
                    }
                },
                __focusInput:(e:Event):void=>{
                    input.style.visibility = "visible";
                    if(!this.AllSelected){
                        setTimeout(()=>{
                            input.setSelectionRange(input.value.length, input.value.length);   
                        }, 10)
                    }
                    spanWithInput.style.visibility = "hidden";
                    input.setAttribute("placeholder", inputPlaceholder)    
                    input.setAttribute("maxLength", this.maxLength) 
                },
                __keyInput:(e:Event):void=>{
                    spanWithInput.innerText = input.value;
                    input.style.width = spanWithInput.clientWidth + 'px';
                    if((e.target as HTMLInputElement).value == ""){
                        input.setAttribute("placeholder", inputPlaceholder);
                        spanWithInput.innerText = inputPlaceholder;
                        _inputEvent.__inputStyle();
                    }
                    else{
                        spanWithInput.innerText = input.value;
                    }
                },
            }
            var _inputEvent = new Input();
            input.addEventListener("focus", (e:KeyboardEvent)=>{
                _inputEvent.__focusInput(e as Event);
            })
            input.addEventListener("focusout", (e:KeyboardEvent)=>{
                _inputEvent.__focusoutInput(e as Event);
            })
            input.addEventListener("input", (e:KeyboardEvent)=>{
                _inputEvent.__keyInput(e as Event);
                let obsval = obs();
                obsval[0] = (<any>e.target).value;
                obs(obsval);
            })
            input.addEventListener("keydown", (e:KeyboardEvent)=>{
                var key:number = e.keyCode || e.charCode;
                if(key == 13 && this.newMultiLine){
                    input.blur()
                }
                if((e.target as HTMLInputElement).value == ""){
                    spanWithInput.innerText = inputPlaceholder;
                }
                else{
                    spanWithInput.innerText = input.value;
                }
            })
            spanWithInput.addEventListener("click", ():void=>{
                spanWithInput.style.visibility = "hidden"
                input.style.visibility = "visible";
                input.focus();
                if(this.AllSelected && input.value !=""){
                    input.select();
                }
                else{
                    input.setSelectionRange(0,0);
                }
            })
        }
        protected textareaData(textPlace, obs){
            let textarea:HTMLTextAreaElement = this.findJSFunction('textarea', this.textareaAndSpan),
            spanWithTextarea:HTMLSpanElement = this.findJSFunction('span', this.textareaAndSpan),
            textareaPlaceholder:string = textPlace;
            function Textarea():void{
                this.__init.call(this)
            };
            (Textarea.prototype as object) = { 
                constructor:Textarea,
                __init:function():void{
                    setTimeout(()=>{

                    }, 0)
                },
                __textAreaFocus:():void=>{
                    textarea.scrollTop = textarea.scrollHeight;
                    spanWithTextarea.style.visibility = "hidden";
                    if(!this.AllSelected){
                        setTimeout(()=>{
                            textarea.setSelectionRange(textarea.value.length, textarea.value.length)   
                        }, 10)
                    }
                    textarea.setAttribute("placeholder", textareaPlaceholder)  
                    textarea.setAttribute("maxLength", this.maxLength) 
                },
                __textAreaFocusOut:():void=>{
                    spanWithTextarea.style.visibility = "visible";
                    spanWithTextarea.scrollTop = 0;
                    textarea.style.visibility = "hidden";
                    spanWithTextarea.style.height = textarea.getBoundingClientRect().height+"px" 
                    textarea.removeAttribute("placeholder")  
                },
                __textAreaInput:(e:Event):void=>{
                    textarea.scrollTop = (e.target as HTMLElement).scrollHeight;
                    (e.target as HTMLElement).style.height = 'auto';
                    (e.target as HTMLElement).style.height = (e.target as HTMLElement).scrollHeight + "px";
                    // if(e.target.scrollHeight > this.textAreaHeight && e.target.scrollHeight < 150){
                    //     textarea.style.overflow = "hidden"
                    // }
                    // else{
                    //     textarea.style.overflow = "auto"
                    // }
                    if((e.target as HTMLTextAreaElement).value == ""){
                        spanWithTextarea.classList.remove("colorBlack");
                        textarea.setAttribute("placeholder", textareaPlaceholder);
                        spanWithTextarea.innerText = textareaPlaceholder;
                    }
                    else{
                        if(this.defaultColorIntent== "default"){
                            spanWithTextarea.classList.add("colorBlack");
                        }
                        spanWithTextarea.innerText = textarea.value;
                    }
                    
                }
            }
            var _textAreaEvent = new Textarea()
            textarea.addEventListener("keydown", (e:KeyboardEvent)=>{
                var key:number = e.keyCode || e.charCode;
                if(key == 13 && this.newMultiLine){
                    textarea.blur()
                }
            })
            textarea.addEventListener("input", (e:KeyboardEvent)=>{
                _textAreaEvent.__textAreaInput(e)
                let obsval = obs();
                obsval[1] = (<any>e.target).value;
                obs(obsval);
            })
            textarea.addEventListener("focusout", (e:KeyboardEvent)=>{
                _textAreaEvent.__textAreaFocusOut(e); 
            })
            textarea.addEventListener("focus", (e:KeyboardEvent)=>{
                _textAreaEvent.__textAreaFocus(e)
            })
            spanWithTextarea.addEventListener("click", ()=>{
                spanWithTextarea.style.visibility = "hidden"
                textarea.style.visibility = "visible";
                textarea.focus();
                if(this.AllSelected && textarea.value !=""){
                    textarea.select();
                }
                else{
                    textarea.setSelectionRange(0,0);
                }
            })
          
        }
        protected init() {
            super.init();
            var obs = this.registerScopeObservable(this.definition.name, ['',''], false);

            this.objectsToDispose.push(
				obs.subscribe(newValue => {
                    let obsVal = obs();
					if(Array.isArray(newValue))
					{
						if(newValue.length == 1)
						{
                            obsVal[0] = newValue[0];
                            this.findJSFunction('input', this.inputAndSpan).value = newValue[0];
                            obs(obsVal);
						}
						else if(newValue.length > 1)
						{
                            obsVal = [...newValue];
                            this.findJSFunction('input', this.inputAndSpan).value = newValue[0];
                            this.findJSFunction('textarea', this.textareaAndSpan).value = newValue[1];
                            obs(obsVal);
						}
					}
					else
					{
                        obsVal[0] = newValue;
                        this.findJSFunction('input', this.inputAndSpan).value = newValue;
                        obs(obsVal);
					}

				})
			);
            
            if (this.isDOMLoaded) {
                this.watchDefinitionOptions((options: BPEditableText.IOptions) => {
                   
                    var that:this = this;
                    this.coommonArea.className = "ml-blueprint-Area";
                    if(options.intent){
                        this.defaultColorIntent = options.intent;
                        if(this.findJSFunction('input', this.inputAndSpan).value !=""){
                            this.findJSFunction('span', this.inputAndSpan).classList.add("colorBlack");
                        }
                        if(this.findJSFunction('textarea', this.textareaAndSpan).value !=""){
                            this.findJSFunction('span', this.textareaAndSpan).classList.add("colorBlack");
                        }
                        this.coommonArea.classList.add('ml-blueprint-Area-'+options.intent);
                    }


                    this.newMultiLine = (options.breakNewMultiLine)?true:false; 
                    this.AllSelected = (options.selected_value)?true:false; 
                    this.maxLength = (options.MaxLength)?options.MaxLength:undefined;
                    if(options.placeholder_input){
                        this.inputValue = options.placeholder_input;
                    }
                    if(this.findJSFunction('input', this.inputAndSpan).value == ''){
                        this.findJSFunction('span', this.inputAndSpan).innerHTML = this.inputValue 
                    }
                    if(options.placeholder_textarea){
                        this.textAreaValue = options.placeholder_textarea;
                    }
                    if(this.findJSFunction('textarea', this.textareaAndSpan).value == ''){
                        this.findJSFunction('span', this.textareaAndSpan).innerHTML = this.textAreaValue 
                    }
                    this.inputData(this.inputValue, obs);
                    this.textareaData(this.textAreaValue, obs)
                    
                });
            }

        }

        protected cleanup() {
            super.cleanup();

        }
    }

    export namespace BPEditableText {

        /**
         * Interface for Editable Text
         * @typedef IEditableText
         * @interface
         * @readonly
         */
        export interface IEditableText {
            /**
             * Select the value inside input
             * @type {boolean}
             */
            selected_value?: boolean,
            /**
             * Placeholder text inside input
             * @type {string}
             */
            placeholder_input?: string,
            /**
             * Placeholder text inside textarea
             * @type {string}
             */
            placeholder_textarea?: string,
            /**
             * Intent color of the textarea
             * @type {string}
             */
            intent?: string,
            /**
             * Press Enter to unfocus the text inputs instead of making a new line
             * @type {boolean}
             */
            breakNewMultiLine?:boolean,
            /**
             * Maximum length of text in text inputs
             * @type {number}
             */
            MaxLength?: number
        }

        export interface IOptions extends IEditableText, mdash.IControlOptionsWithTransforms {
        }

        export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            selected_value?: boolean | mdash.IScopePath, 
            intent?: string | mdash.IScopePath
            placeholder_input?: string | mdash.IScopePath,
            placeholder_textarea?: string | mdash.IScopePath,
            breakNewMultiLine?: boolean | mdash.IScopePath,
            MaxLength?: number | mdash.IScopePath,
        }
    }

    mdash.registerControl(BPEditableText, 'dashboard.components.controls', BPEditableText.meta);
}