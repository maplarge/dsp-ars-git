namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPMenu extends mdash.BaseControl<BPMenu.IOptionsDefinition> {
		public static type: string = 'BPMenu';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Menu',
			shortDescription: "The class menu-component is a choice that is used to group menu items and submenus both visually and functionally.",
			categories: ["Visualization"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPMenu.builder().options(BPMenu.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPMenu.IOptions> = {
			'BPMenu': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		constructor(id: string, definition: mdash.IControlDefinition<BPMenu.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

			//this is where you might create/initialize class variables of observables
		}

		private _menuContainer: JQuery;
		private _ParentMenuList: JQuery;
		private _openListButton: JQuery;
		private _optionModal: boolean = false;

		
		protected positionParentUl (){
			let _buttPosition = (this._openListButton)[0].getBoundingClientRect();
			let _width = window.innerWidth
			|| document.documentElement.clientWidth
			|| document.body.clientWidth,
			leftPositionParentUL = 0,
			originTransform =null;
			if(_buttPosition.right +this._ParentMenuList[0].offsetWidth+15 < _width){
				if(this._ParentMenuList[0].classList.contains('ml-blueprint-positionLeftUl')){
					this._ParentMenuList[0].classList.remove('ml-blueprint-positionLeftUl')
				}
				leftPositionParentUL = _buttPosition.left + _buttPosition.width + 15;
				originTransform = "top left";
			}
			else{
				this._ParentMenuList[0].classList.add('ml-blueprint-positionLeftUl')
				leftPositionParentUL = _buttPosition.left - this._ParentMenuList[0].offsetWidth - 15;
				originTransform = "top right";
			}
			ml.$(this._ParentMenuList).css({
				left:leftPositionParentUL +"px",
				top:_buttPosition.top+"px",
				transformOrigin:originTransform
			})
		}
		protected __buildChildMenuList(childMenuList, _li){
			var _ul = (document.createElement("ul") as HTMLUListElement);
			_ul.className = `ml-blueprint-MenuFirstLevel`;
			this.ForEach(childMenuList, _ul)
			_li.appendChild(_ul);
		}
		protected ForEach(childMenuList, _ul){
			setTimeout(() => {
				this.positionLeft(_ul)
			}, 400);
			Array.prototype.map.call(childMenuList, (el, key)=>{
				let _liSub = (document.createElement("li") as HTMLLIElement);
				if(childMenuList[key].hasOwnProperty('isDivider') && childMenuList[key].isDivider){
					_liSub.classList.add("ml-blueprint-divider");
					_liSub.innerHTML = "";
				}
				else{
					
					if(childMenuList[key].hasOwnProperty('onClick')){
						_liSub.setAttribute("data-click", childMenuList[key].onClick)
					}
					if(childMenuList[key].hasOwnProperty('children')){
						childMenuList[key].rightIcon = "mlicon-MapLarge-icons_layer-collapsed subMenuCaret";
						this.__buildChildMenuList(childMenuList[key].children, _liSub); 
					}
					let _a = (document.createElement("a") as HTMLElement),
						_isLeftIcon = (childMenuList[key].leftIcon)?`<span class="${childMenuList[key].leftIcon}"></span>`:"",
						_isShortCuts = (childMenuList[key].shortCuts !="")?childMenuList[key].shortCuts:"",
						_isRightIcon = (childMenuList[key].rightIcon || childMenuList[key].shortCuts)?`<span class="${childMenuList[key].rightIcon}">${_isShortCuts}</span>`:"",
						_isTitle = (childMenuList[key].isTitle)?"isTitleTrue":"";
						_a.className = _isTitle;
						_a.innerHTML =`<p>${_isLeftIcon} ${childMenuList[key].text}</p> ${_isRightIcon}`;
					_liSub.appendChild(_a);

					if(childMenuList[key].hasOwnProperty('disabled') && childMenuList[key].disabled){
						_liSub.classList.add("ml-blueprint-disabled");
					}
					if((childMenuList[key].hasOwnProperty('urlHref') && childMenuList[key].urlHref != "") && !childMenuList[key].disabled){
						let _target = (childMenuList[key].targetBlank)?"_blank":"";
						_a.setAttribute('href', `${childMenuList[key].urlHref}`);
						_a.setAttribute('target', `${_target}`);
						_liSub.appendChild(_a); 
					}
					_liSub.classList.add(`ml-blueprint-${childMenuList[key].intent}`);
					_liSub.addEventListener("mouseover", (e:Event)=>{
						(e.currentTarget as HTMLElement).classList.add('ml-blueprint-popover_hover', "ml-blueprint-backgroundHover");
					});
					_liSub.addEventListener("mouseleave", (e:Event)=>{
						let _tar = (e.currentTarget as HTMLElement);
						_tar.classList.remove('ml-blueprint-backgroundHover');
						setTimeout(()=>{
							_tar.classList.remove('ml-blueprint-popover_hover');
						}, 0);
					});
				}
				_liSub.addEventListener("click", (e:Event)=>{
					if( !(e.currentTarget as HTMLElement).classList.contains("ml-blueprint-disabled")
					 && !childMenuList[key].hasOwnProperty('children') && !childMenuList[key].isTitle){
						 Array.prototype.map.call(this.getParents(_liSub, document.querySelector('.ml-blueprint-ParentMenuList')), (el, index)=>{
							 if(el.tagName == "UL"){
								 this._optionModal = false;
								if(el.classList.contains('ml-blueprint-openListCommon')){
									el.classList.remove('ml-blueprint-openListCommon');
								}
								else{
									el.classList.add('ml-blueprint-parentHidden');
								}
							}
						})
						document.querySelectorAll('.ml-blueprint-popover_hover').forEach(elem=>{
							elem.classList.remove('ml-blueprint-popover_hover');
						})
					}
					e.stopPropagation()
				})
				_ul.appendChild(_liSub);
			})
		}
		protected getParents(el:HTMLElement, parentSelector /* optional */) {
			// If no parentSelector defined will bubble up all the way to *document*
			if (parentSelector === undefined) {
				parentSelector = document;
			}
			var parents:{}[] = [];
			var p = el.parentNode;
			while (p !== parentSelector) {
				var o = p;
				parents.push(o);
				p = o.parentNode;
			}
			parents.push(parentSelector); // Push that parentSelector you wanted to stop at
			return parents;
		}
		protected winResize(){
			this.positionParentUl();
			document.querySelectorAll('.ml-blueprint-MenuFirstLevel').forEach((element:HTMLElement) => {
				this.positionLeft(element)
			})
		}
		protected positionLeft(element:HTMLElement){
			if(element.classList.contains('ml-blueprint-positionLeft')){
				element.classList.remove('ml-blueprint-positionLeft')
			}	
			let _width = window.innerWidth
				|| document.documentElement.clientWidth
				|| document.body.clientWidth;
			if(element.offsetWidth + element.getBoundingClientRect().left > _width){
				element.classList.add('ml-blueprint-positionLeft')
			}
		}
		public build() {
			super.build();

			this._menuContainer = ml.$('<div class="ml-blueprint-menu-container"></div>').appendTo(this.contentDiv);
			this.container.classList.add('namespace-blueprint-menu');
			this._ParentMenuList = ml.$(`
				<ul class="ml-blueprint-ParentMenuList"></ul>
			`).appendTo('body');
			this._openListButton = ml.$(`
				<button class="ml-blueprint-openList"><i class="mlicon-external-link-symbol"></i> Open in...</button>
			`).appendTo(this._menuContainer)
			
			window.addEventListener('resize', ()=>{
				this.winResize()
			})

		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;
			return true;
		}

		protected init() {
			super.init();

			if(this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPMenu.IOptions) => {
					this.positionParentUl();
					this._openListButton[0].style.fontSize = (options.textLarge)?"20px":"14px",
					document.addEventListener("click", (e:Event)=>{
						if((e.target as HTMLElement).closest(".ml-blueprint-openList")){
							this.positionParentUl();
							ml.$(this._ParentMenuList).children().detach();
							this.ForEach(options.menuItems, this._ParentMenuList[0])
							this._ParentMenuList[0].classList.toggle("ml-blueprint-openListCommon")
							this._optionModal = true;
							if(options.textLarge){
								ml.$(this._ParentMenuList).children().find("p").css("fontSize", "20px")
							}
						}
						else if(this._optionModal && (e.target as HTMLElement).closest('.ml-blueprint-ParentMenuList') == null){
							this._ParentMenuList[0].classList.remove("ml-blueprint-openListCommon")
							this._optionModal = false;
						}
					})


				});
			}
			//called twice
			//1. To initialze the control but before it is loaded in the DOM (when this.isDOMLoaded == false)
			//2. After the control has been created in the DOM (when this.isDOMLoaded == true)
		}

		protected cleanup() {
			super.cleanup();

			//called just before the control gets destroyed
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPMenu.IOptionsDefinition>(this.type);
        }
        
    }
    export namespace BPMenu {
		/**
		 * Interface for building a Menu
		 * @typedef DataTreeMenu
		 * @interface
		 * @readonly
		 */
		export interface DataTreeMenu{
			/**
			 * Icon CSS class for the left side of the menu item
			 * @type {string}
			 */
			leftIcon?:string,
			/**
			 * Icon CSS class for the right side of the menu item
			 * @type {string}
			 */
			rightIcon?:string,
			/**
			 * Display shortcuts information for the menu item
			 * @type {string}
			 */
			shortCuts?:string,
			/**
			 * Text of the menu item
			 * @type {string}
			 */
			text?:string,
			/**
			 * Enable/Disable click interaction
			 * @type {boolean}
			 */
			onClick?:boolean,
			/**
			 * Determine if the menu item is a title or not
			 * @type {boolean}
			 */
			isTitle?:boolean,
			/**
			 * Disable interactions with the menu item
			 * @type {boolean}
			 */
			disabled?:boolean,
			/**
			 * URL link for the menu item
			 * @type {string}
			 */
			urlHref?:string,
			/**
			 * Turn the menu item into a divider
			 * @type {string}
			 */
			isDivider?:boolean,
			/**
			 * Intent color for the menu item
			 * @type {ctrl.BPBase.Intent}
			 */
			intent?:ctrl.BPBase.Intent,
			/**
			 * Open menu link in a new tab
			 * @type {boolean}
			 */
			targetBlank?:boolean,
			/**
			 * Add a submenu with properties
			 * @type {DataTreeMenu[]}
			 */
			children?: DataTreeMenu[],
			// 	/**
			//  * Event handler for click event on buttons
			//  * @function
			//  * @param {IScope} scope - The current scope
			//  * @param {MouseEvent} event - Click event object
			//  * @event click
			//  * @returns {void}
			//  */
			// onClick?: (scope: mdash.IScope, event: MouseEvent) => void ;
		}

		/**
		 * Menu properties
		 * @typedef IMenuProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IActionProps
		 * @augments BPBase.IAppearance
		 */
		export interface IMenuProps extends BPBase.IActionProps, BPBase.IAppearance{
			/**
			 * Make text large
			 * @type {boolean}
			 */
			textLarge?:boolean,
			/**
			 * Menu structure object
			 * @type {ctrl.BPMenu.DataTreeMenu[]}
			 */
			menuItems?:ctrl.BPMenu.DataTreeMenu[],
        }
		
		
		export interface IOptions extends mdash.IControlOptionsWithTransforms, IMenuProps {
			//mirrors IOptionsDefinition, but contains real values, whereas IOptionsDefinition can contain path's to values (scopePath)
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			//options that can be passed to the control when created
			textLarge?:boolean | mdash.IScopePath,
			menuItems?:ctrl.BPMenu.DataTreeMenu[] | mdash.IScopePath,
			onClick?: (scope: mdash.IScope, event: MouseEvent) => void | mdash.IScopePath
		}
	}
	mdash.registerControl(BPMenu,'dashboard.components.controls', BPMenu.meta);
}


