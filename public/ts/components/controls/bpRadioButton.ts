namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPRadioButton extends mdash.BaseInputControl<BPRadioButton.IOptionsDefinition> {
		public static type: string = 'BPRadioButton';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Radio Button',
			shortDescription: "A radio button is a two-states button that can be either checked or unchecked",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPRadioButton.builder().options(BPRadioButton.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPRadioButton.IOptions> = {
			'BPRadioButton': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		private radio: HTMLInputElement;
		private labelContainer: HTMLElement;
		private fancyRadio: HTMLElement;

		constructor(id: string, definition: mdash.IControlDefinition<BPRadioButton.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
			this.labelContainer = document.createElement('label');
			this.radio = document.createElement('input');
			this.fancyRadio = document.createElement('span');
			this.radio.classList.add('ml-blueprint-radio-button');
			this.labelContainer.appendChild(this.radio);
			this.labelContainer.appendChild(this.fancyRadio);
			this.radio.setAttribute('type', 'radio');
			this.fancyRadio.classList.add('ml-blueprint-radio-button-fancy');
			this.labelContainer.classList.add('ml-blueprint-radio-button-label');
			this.container.classList.add('namespace-blueprint-radio-button');

			this.contentDiv[0].appendChild(this.labelContainer);

		}

		/**
		 * initialize the radio button listener to update the observable when it's checked or unchecked
		 * @param valueObs observable variable
		 */
		private initializeListeners(valueObs: mdash.RefCountedObservable<any>) {
			var radio = this.contentDiv.find('input:radio');
			radio
				.off('change')//remove any previous change event listeners
				.on('change', (event: JQueryEventObject) => { 
					if (radio.is(':checked') != valueObs() || valueObs() == null) {
						valueObs(radio.is(":checked")); //if new value of radio is different than current value of the control, update the value of the control
					}
				});

			//objectsToDispose is an array of listeners that get destroyed when the control is destroyed
			//add listeners to this.objectsToDispose in order to prevent memory leaks
			this.objectsToDispose.push(
			//listen and respond to changes of the value of the control and update the radio accordingly
				valueObs.subscribe(newValue => {
					if (valueObs() == null || this.contentDiv.find(`input:radio[value=${newValue}]`)) {
						radio.prop('checked', false);
						this.contentDiv.find(`input:radio[value=${newValue}]`).prop('checked', true);
					}
				})
			);
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, false, false);
			this.initializeListeners(obs);
			this.initTransforms();
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPRadioButton.IOptions) => {
                
                    if(options.name)
                    {
						this.radio.setAttribute('name', options.name);
                    }

                    if(options.label)
                    {                      
						this.radio.setAttribute('value', options.label);
						this.contentDiv.find('.ml-blueprint-radio-button-left-align').detach();
						this.contentDiv.find('.ml-blueprint-radio-button-span').detach();
						
						if(options.align && options.align == BPBase.Alignment.Right)
						{
							this.labelContainer.insertAdjacentHTML('afterbegin', `<span class='ml-blueprint-radio-button-span'>${options.label}</span>`);
							this.labelContainer.style.display = 'block';
						}
						else
						{
							this.contentDiv[0].insertAdjacentHTML('afterbegin', `<span class='ml-blueprint-radio-button-left-align'>${options.label}</span>`);
							this.labelContainer.style.display = 'inline';
						}
					}
					
                    options.checked ? this.radio.setAttribute('checked', 'checked') : this.radio.removeAttribute('checked');

                    options.disabled ? this.radio.setAttribute('disabled', 'disabled') : this.radio.removeAttribute('disabled');

                    options.inline ? this.labelContainer.classList.add('ml-blueprint-radio-button-inline') : 
									 this.labelContainer.classList.remove('ml-blueprint-radio-button-inline');

					if(options.large)
					{
						this.labelContainer.classList.add('ml-blueprint-radio-button-large');
						this.contentDiv.find('.ml-blueprint-radio-button-left-align').addClass('ml-blueprint-radio-button-label-large');
					}
					else
					{
						this.labelContainer.classList.remove('ml-blueprint-radio-button-large');
						this.contentDiv.find('.ml-blueprint-radio-button-left-align').removeClass('ml-blueprint-radio-button-label-large');
					} 
                    
					if(options.onChange){
						this.radio.onchange = () => {
							options.onChange(`${this.radio.checked}`, this.scope); 
						}
					}
					else
					{
						this.radio.onchange = null;
					}
					
					if(ml.util.isNotNullUndefinedOrEmpty(options.value))
					{
						this.radio.setAttribute('value', options.value);
					}
				});
			}

		}

		protected cleanup() {
			this.radio.onchange = null;
			this.contentDiv.find('input:radio').off('change');
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPRadioButton.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPRadioButton {

		/**
		 * Interface for working with radio buttons
		 * @typedef IRadioButtonProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IControlProps
		 */
		export interface IRadioButtonProps extends BPBase.IControlProps{
			/**
			 * Name of the radio button
			 * @type {string}
			 */
			name?: string,
			/**
			 * Value of the radio button
			 * @type {any}
			 */
			value?: any
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IRadioButtonProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            name?: string | mdash.IScopePath,
            align?: BPBase.Alignment | mdash.IScopePath,
			checked?: boolean | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
			inline?: boolean | mdash.IScopePath,
			label?: string | mdash.IScopePath,
			value?: any | mdash.IScopePath,
			large?: boolean | mdash.IScopePath,
			onChange?: (value: string, scope: mdash.IScope) => void          
		}
	}

	mdash.registerControl(BPRadioButton,'dashboard.components.controls', BPRadioButton.meta);
}