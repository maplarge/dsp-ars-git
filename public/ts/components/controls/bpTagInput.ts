namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPTagInput extends mdash.BaseControl<BPTagInput.IOptionsDefinition> {
		public static type: string = 'BPTagInput';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Tag Input',
			shortDescription: "Tag inputs render Tags inside an input, followed by an actual text input.",
			categories: ["Input", "Visualization"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPTagInput.builder().options(BPTagInput.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPTagInput.IOptions> = {
			'BPTagInput': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
        }

        protected tagInput: JQuery;
        protected enterTags: JQuery;
		protected enterTagInput: JQuery;
		protected text: JQuery;
		protected removeIcon: JQuery;
		protected resetButton: JQuery;
		protected removeButoon: JQuery;
		protected once: boolean  = false;
		protected IconOnce: boolean  = false;
		protected triggered: boolean = false;
		protected removeLength: any;
		protected removeTag: any;
		protected arr: any;
		protected obj: any;

        
		constructor(id: string, definition: mdash.IControlDefinition<BPTagInput.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		public build() {
            super.build();
			this.container.classList.add('namespace-blueprint-tagComp');
            this.container.classList.add('namespace-blueprint-tagInput');
            this.tagInput = ml.$('<div></div>');
            this.contentDiv.append(this.tagInput);            
            this.tagInput.addClass('ml-blueprint-tagInput');
            this.enterTags = ml.$('<div></div>');
			this.enterTags.addClass('ml-blueprint-enterTags');
			this.tagInput.append(this.enterTags);
			this.removeButoon = ml.$('<button class="ml-blueprint-removeTag"><i class="mlicon-MapLarge-icons_close-delete"></i></button>');
            this.tagInput.append(this.removeButoon);        
            this.enterTagInput = ml.$('<input>');
            this.enterTagInput.addClass('ml-blueprint-enterTagInput');
			this.enterTags.append(this.enterTagInput);
			this.resetButton = ml.$('<button class="ml-blueprint-resetTag"><i class="ml-blueprint-icon ml-icon-refresh"></i></button>');
		}

		protected addPlaceholder = (options) => {
			if(this.arr.length == 0) {
				this.enterTagInput.attr('placeholder', options);
			}			
		}

		protected removeSingleTag(tag, options) {
			this.arr.splice(tag.index(), 1);
			ml.$(tag).detach();
			if(options){
				this.addPlaceholder(options);
			}			
		};

		protected removePlaceholder = () => {
			if(this.arr.length > 0) {
				this.enterTagInput.removeAttr('placeholder');
			}
		}

		protected obsItems = (obs, triggered) => {
			let itemNames = [];
			ml.$.each(this.arr, function(id, item){
				itemNames[id] = item.value;
			})
			if(!triggered)
			{
				obs(itemNames);
			}
		}
		
		protected textPush(obs, triggered){
			this.arr.push(this.obj);
			this.enterTagInput.before(this.text);
			this.obsItems(obs, triggered);
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}

		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, [], false);

			this.objectsToDispose.push(
				obs.subscribe(newValue => {
					if(ml.util.isNullUndefinedOrEmpty(newValue))
					{
						this.triggered = true;
						this.removeButoon.click();
					}
					else if(typeof newValue == 'string' && newValue != this.arr[0] && ml.util.isNotNullUndefinedOrEmpty(newValue))
					{
						this.triggered = true;
						this.obj = { value: newValue };
						this.removeButoon.click();
						const e = ml.$.Event('keydown');
						e.which = 13;
						this.enterTagInput.trigger(e);
					}
					else if(Array.isArray(newValue) && newValue.length != this.arr.length)
					{
						this.triggered = true;
						this.removeButoon.click();
						newValue.forEach(item => {
							this.obj = { value: item.label || item };
							const e = ml.$.Event('keydown');
							e.which = 13;
							this.enterTagInput.trigger(e);
						})
					}
					else if(Array.isArray(newValue) && newValue.length == this.arr.length)
					{
						for(let i = 0; i < this.arr.length; i++)
						{
							if(this.arr[i].value != newValue[i])
							{
								this.triggered = true;
								this.removeButoon.click();
								newValue.forEach(item => {
									this.obj = { value: item.label || item };
									const e = ml.$.Event('keydown');
									e.which = 13;
									this.enterTagInput.trigger(e);
								})
								break;
							}
						}
					}
					this.triggered = false;
				})
			)

			if (this.isDOMLoaded) {

				this.arr = this.definition.options.onAdd;
				this.obj = {
					value: this.enterTagInput.val()
				};              
				
				this.tagInput.click(() => {
					this.enterTagInput.focus();
				}) 			
				
				this.watchDefinitionOptions((options: BPTagInput.IOptions) => {

					var itemNames: string[] = []

					this.obsItems(obs, this.triggered);

					const intents: string[] = ['primary','success','warning','danger', 'default'];
					intents.forEach(intent => {
						ml.$('.ml-blueprint-enterTags .ml-blueprint-tagComp').removeClass(`ml-blueprint-tagComp-${intent} `);						
					})
			
					
					this.enterTagInput.off();

					ml.$(this.enterTagInput).on('keydown',  (e) => {
						var text = ml.$('.ml-blueprint-enterTags .ml-blueprint-tagComp').last()
						var active = 'ml-blueprint-tagComp-active'
						if(e.keyCode == 8){
							if(this.enterTagInput.val() == ''){
								if(text.hasClass(active)){
									ml.$('.ml-blueprint-tagComp').last().addClass(active);
									text.detach();
									this.arr.splice(text.index(),1);
									this.obsItems(obs, this.triggered)
									if(options.placeholder) {
										if(this.arr.length === 0)
										{
											this.addPlaceholder(options.placeholder);
										}
									}
								}
								else {
									text.addClass(active);
								}
							}
							
						}
						else {
							text.removeClass(active);
						}
					})

					if(options.placeholder) 
					{						
						this.addPlaceholder (options.placeholder);
						this.removePlaceholder();						
					}					

					if (options.onAdd)
					{	
						if(!this.once){

							options.onAdd.forEach((opt) => {
	
								this.text = ml.$(`<span class="ml-blueprint-tagComp-default ml-blueprint-tagComp">${opt.value}</span>`);
								this.enterTags.prepend(this.text);
								
								if(options.onRemove) {
									let text = this.text
									this.removeIcon = ml.$('<button class="mlicon-MapLarge-icons_close-delete"></button>');
									this.removeIcon.addClass('ml-blueprint-remove');
									this.text.append(this.removeIcon);								
									this.removeIcon.on('click', ()=> {this.removeSingleTag(text, options.placeholder);this.obsItems(obs, this.triggered)})
									
								}								
							})
							this.once = true
						}
						this.enterTagInput.on( "keydown", (event) => {
							this.enterTagInput.attr('value', this.enterTagInput.val())
							if(event.which == 13) {
								if(this.enterTagInput.val() == '' && !this.triggered){
									return;
								}
								else {
									if(!this.triggered)
									{
										this.text = ml.$(`<span class="ml-blueprint-tagComp"></span>`).text(this.enterTagInput.val());
									}
									else
									{
										this.text = ml.$(`<span class="ml-blueprint-tagComp"></span>`).text(this.obj.value);
									}
									this.resetButton.detach()
									this.tagInput.append(this.removeButoon);
									if(!this.triggered) {
										this.obj.value = this.enterTagInput.val()
									}
									this.textPush(obs, this.triggered);
									this.obj = { value: '' };
								}
								if(options.placeholder)  {
									this.removePlaceholder ()								
								}
							
								this.enterTagInput.attr('value', '')

								if(options.onRemove) {
									let text = this.text
									this.removeIcon = ml.$('<button class="mlicon-MapLarge-icons_close-delete"></button>');
									this.removeIcon.addClass('ml-blueprint-remove');
									this.text.append(this.removeIcon);								
									this.removeIcon.on('click', ()=> {this.removeSingleTag(text, options.placeholder);this.obsItems(obs, this.triggered)});								
								}
								if(options.large)
								{
									this.text.addClass('ml-blueprint-tagComp-large');
								}								
								if(options.intent)
								{								
									this.text.addClass(`ml-blueprint-tagComp-${options.intent}`);
								}
								if(options.minimal)
								{
									this.text.addClass('ml-blueprint-tagComp-minimal');							
								}								
							}
						});
						if(options.addOnBlur) {						
							let thisInput = this.enterTagInput;

							this.tagInput.click(e => {
								if(ml.$(e.currentTarget).hasClass('ml-blueprint-tagInput')){
									ml.$('.ml-blueprint-enterTagInput').off('blur');
									e.preventDefault();
									e.stopPropagation();
								}							
							})
							ml.$('body').off('click')
							ml.$('body').click(e => {
								if(!ml.$(e.target).hasClass('ml-blueprint-tagInput') || ml.$(e.target).closest('.ml-blueprint-tagInput').length == 0){
									ml.$('.ml-blueprint-enterTagInput').trigger('blur');
									thisInput.attr('value', thisInput.val())
								
									if(thisInput.val() == ''){
										return;
									}
									else {										
										this.text = ml.$(`<span class="ml-blueprint-tagComp">${thisInput.val()}</span>`);
										this.resetButton.detach()
										this.tagInput.append(this.removeButoon);
										this.obj.value = thisInput.val()
										this.textPush(obs, this.triggered);
										this.obj = { value: '' };
									}
									if(options.placeholder)  {
										this.removePlaceholder();					
									}	
									thisInput.attr('value', '')
		
									if(options.onRemove) {
										let text = this.text;
										this.removeIcon = ml.$('<button class="mlicon-MapLarge-icons_close-delete"></button>');
										this.removeIcon.addClass('ml-blueprint-remove');
										this.text.append(this.removeIcon);
										this.removeIcon.on('click', ()=> {this.removeSingleTag(text, options.placeholder);this.obsItems(obs, this.triggered)});
									}
									if(options.large)
									{
										this.text.addClass('ml-blueprint-tagComp-large');
									}								
									if(options.intent)
									{
										this.text.addClass(`ml-blueprint-tagComp-${options.intent}`);
									}
									if(options.minimal)
									{
										this.text.addClass('ml-blueprint-tagComp-minimal');							
									}								
								}						
							})
						}					
						this.removeButoon.off()
						this.removeButoon.on('click',()=> {
							if(this.arr.length !== 0) {
								this.removeTag = this.contentDiv.find('.ml-blueprint-enterTags .ml-blueprint-tagComp').detach();
								this.removeLength = this.arr.splice(0);
								this.obsItems(obs, this.triggered)
								this.removeButoon.detach();
								this.tagInput.append(this.resetButton);
							}

							if(options.placeholder)  {
								this.addPlaceholder (options.placeholder);
								this.removePlaceholder ();
							};
							
						});
						this.resetButton.off()
						this.resetButton.on('click', () => {
							this.enterTagInput.before(this.removeTag);
							this.resetButton.detach();
							this.tagInput.append(this.removeButoon);
							this.arr = [...this.removeLength];
							this.obsItems(obs, this.triggered)
							this.removeLength = [];
							this.removePlaceholder ();
							for(var i = 0; i<this.removeTag.length; i++) {
								const intents = ['primary','success','warning','danger', 'default'];
								intents.forEach(intent => {
									ml.$(this.removeTag[i]).removeClass(`ml-blueprint-tagComp-${intent} `);									
								})
								ml.$(this.removeTag[i]).removeClass('ml-blueprint-tagComp-large');
								ml.$(this.removeTag[i]).removeClass('ml-blueprint-tagComp-minimal');
								if(options.large)
								{									
									ml.$(this.removeTag[i]).addClass('ml-blueprint-tagComp-large');
								}
								if(options.minimal)
								{
									ml.$(this.removeTag[i]).addClass('ml-blueprint-tagComp-minimal');
								}
								ml.$(this.removeTag[i]).addClass(`ml-blueprint-tagComp-${options.intent}`);
							}
							
						})									
					}
					if(options.icon) 
					{	
						if(!this.IconOnce) 
						{
							this.tagInput.prepend(`<i class="ml-blueprint-icon ml-icon-${options.icon}"></i>`);
							this.IconOnce = true;
						}
					}				
				
					if(options.fill)
                    {
                        this.tagInput.addClass('ml-blueprint-tagInput-fill');
					}
					else {
                        this.tagInput.removeClass('ml-blueprint-tagInput-fill');
					}
					if (options.disabled)
					{
						this.removeButoon.off();
						this.resetButton.off();
						this.tagInput.addClass('ml-blueprint-tagInput-disabled');
						this.enterTagInput.addClass('ml-blueprint-tagInput-disabled');
						this.enterTagInput.attr('disabled', 'true');
						this.enterTags.addClass('ml-blueprint-tagInput-disabled');
						ml.$('.ml-blueprint-removeTag').addClass('ml-blueprint-tagInput-disabled');
						ml.$('.ml-blueprint-removeTag').attr('disabled', 'true');
						ml.$('.ml-blueprint-tagInput .ml-blueprint-remove').hide();

					}
					else {
						this.tagInput.removeClass('ml-blueprint-tagInput-disabled');
						this.enterTagInput.removeClass('ml-blueprint-tagInput-disabled');
						this.enterTagInput.removeAttr('disabled');
						this.enterTags.removeClass('ml-blueprint-tagInput-disabled');
						ml.$('.ml-blueprint-removeTag').removeClass('ml-blueprint-tagInput-disabled');
						ml.$('.ml-blueprint-removeTag').removeAttr('disabled');
						ml.$('.ml-blueprint-tagInput .ml-blueprint-remove').show()
					}

					if(options.large)
					{
						ml.$('.ml-blueprint-enterTags .ml-blueprint-tagComp').addClass('ml-blueprint-tagComp-large');
						this.tagInput.addClass('ml-blueprint-tagInput-large');
					}
					else 
					{
						ml.$('.ml-blueprint-enterTags .ml-blueprint-tagComp').removeClass('ml-blueprint-tagComp-large');
						this.tagInput.removeClass('ml-blueprint-tagInput-large');
					}
					if(options.intent)
					{
						ml.$('.ml-blueprint-enterTags .ml-blueprint-tagComp').addClass(`ml-blueprint-tagComp-${options.intent}`);
					}
					
					if(options.minimal)
					{
						ml.$('.ml-blueprint-enterTags .ml-blueprint-tagComp').addClass('ml-blueprint-tagComp-minimal');							
					}
					else
					{
						ml.$('.ml-blueprint-enterTags .ml-blueprint-tagComp').removeClass('ml-blueprint-tagComp-minimal');
					}
					this.enterTagInput.focusin(() => {
						this.enterTagInput.closest('.ml-blueprint-tagInput').addClass('ml-blueprint-tagInputBoxShadow');
					});
					this.enterTagInput.focusout(() => {
						this.enterTagInput.closest('.ml-blueprint-tagInput').removeClass('ml-blueprint-tagInputBoxShadow');
					});					
				});

			}
		}

		protected cleanup() {
			super.cleanup();

		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPTagInput.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPTagInput {

		/**
		 * Tag Input properties
		 * @typedef ITagProps
		 * @readonly
		 * @interface
		 * @augments ctrl.BPBase.IControlProps
		 */
		export interface ITagProps extends ctrl.BPBase.IControlProps{
			/**
			 * Intent color of the tags added inside tag input
			 * @type {ctrl.BPBase.Intent}
			 */
			intent?: ctrl.BPBase.Intent,
			/**
			 * Array of objects with Tag information
			 * @type {ctrl.BPBase.IOptionProps[]}
			 */
			onAdd?: ctrl.BPBase.IOptionProps[],
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ITagProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			intent?: ctrl.BPBase.Intent | mdash.IScopePath,
			onAdd?: ctrl.BPBase.IOptionProps[] | mdash.IScopePath,
			icon?: string | mdash.IScopePath,
			minimal?: boolean | mdash.IScopePath,
			disabled?:  boolean | mdash.IScopePath,
			placeholder?:  string | mdash.IScopePath,
			large?:  boolean | mdash.IScopePath,
			fill?:  boolean | mdash.IScopePath,
			onRemove?: (Event: MouseEvent) => void | mdash.IScopePath,
			addOnBlur?: (Event: MouseEvent) => void | mdash.IScopePath,
		}
	}

	mdash.registerControl(BPTagInput,'dashboard.component.controls', BPTagInput.meta);
}