namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPLabel extends mdash.BaseControl<BPLabel.IOptionsDefinition> {
		public static type: string = 'BPLabel';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Label',
			shortDescription: "Labels enhance the usability of your forms.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPLabel.builder().options(BPLabel.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPLabel.IOptions> = {
			'BPLabel': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected label: HTMLElement;
		protected input: HTMLElement;


		constructor(id: string, definition: mdash.IControlDefinition<BPLabel.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}


		public build() {
			super.build();
			this.container.classList.add('namespace-blueprint-label-input');
			this.label = ml.create("label", "ml-BPLabel", this.contentDiv[0]);
			this.label.classList.add('ml-blueprint-label');
			this.input = ml.create("input", "ml-BPLabel", this.contentDiv[0]);
			this.input.classList.add('ml-blueprint-label-input')
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
				return false;

			return true;
		}


		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			this.objectsToDispose.push(
				obs.subscribe(newValue => {
					if(newValue != this.label.textContent)
					{
						this.label.textContent = newValue;
					}
				})
			)

			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPLabel.IOptions) => {

					if (options.label) {
						ml.$(this.label).text(options.label);
						obs(ml.$(this.label).text())
					}
					if (ml.util.isNotEmptyString(options.placeholder)) {
						this.input.setAttribute("placeholder", options.placeholder);
					}
					if (options.disabled) {
						this.input.setAttribute('disabled', 'disabled');
						this.input.classList.add('ml-blueprint-disabled');
						this.label.classList.add('ml-blueprint-label-disabled')
					}
					else {
						this.input.removeAttribute('disabled');
						this.input.classList.remove('ml-blueprint-disabled');
						this.label.classList.remove('ml-blueprint-label-disabled');
					}
					if (options.inline) {
						this.label.classList.add('ml-blueprint-display-inline');
					}
					else {
						this.label.classList.remove('ml-blueprint-display-inline')
					}
				});
			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPLabel.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPLabel {

		/**
		 * Label properties
		 * @typedef ILabelProps
		 * @interface
		 * @readonly
		 * @augments {BPBase.IControlProps}
		 */
		export interface ILabelProps extends BPBase.IControlProps {
			/**
			 * Text of the label
			 * @type {string}
			 */
			label?: string,
			/**
			 * Placeholder of the input
			 * @type {string}
			 */
			placeholder?: string,
			/**
			 * Enable or disable the component interactions
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Inline style for Label
			 * @type {boolean}
			 */
			inline?: boolean,
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ILabelProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			label?: string | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
			inline?: boolean | mdash.IScopePath,
			placeholder?: string | mdash.IScopePath,
		}
	}

	mdash.registerControl(BPLabel, 'dashboard.components.controls', BPLabel.meta);
}