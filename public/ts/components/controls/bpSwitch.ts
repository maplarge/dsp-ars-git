/// <reference path="./bpCheckbox.ts" />
namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export namespace BPSwitch {
		export interface IOptionsDefinition extends BPCheckbox.IOptionsDefinition { }
		export interface IOptions extends BPCheckbox.IOptions { }
	}

	export class BPSwitch <T extends BPSwitch.IOptionsDefinition> extends BPCheckbox<BPSwitch.IOptionsDefinition> {
		public static type: string = 'BPSwitch';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.BETA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Switch',
			shortDescription: "A switch is simply an alternate appearance for a checkbox that simulates on/off instead of checked/unchecked.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
            };
            protected node: HTMLElement;
            protected switch: HTMLElement;
		protected text: HTMLElement;

		constructor(id: string, definition: mdash.IControlDefinition<BPSwitch.IOptionsDefinition>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		public build() { 
                  super.build();
                  this.node = ml.create("label", "ml-BPSwitch",  this.contentDiv[0])
                  this.node.classList.add('ml-blueprint-switch-label')
                  this.checkbox.classList.add('ml-blueprint-switch');
                  this.checkbox.classList.remove('ml-blueprint-checkbox')
                  this.container.classList.add('namespace-blueprint-switch');
			this.switch = ml.create("span", "ml-BPSwitch", this.contentDiv[0]);
                  this.switch.classList.add('ml-blueprint-switch-slider')
                  this.node.appendChild(this.checkbox)
                  this.node.appendChild(this.switch)
                  this.text  = ml.create("span", "ml-BPSwitch",  this.contentDiv[0])
			this.text.classList.add('ml-blueprint-text') 
            }

		protected validateDefinition(): boolean { return super.validateDefinition(); }

		protected init() { 
                  super.init();

                  if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPCheckbox.IOptions) => {

                              if(options.label)
                              {
						this.text.textContent =  options.label;
						this.contentDiv.find('label.ml-blueprint-checkbox-label').detach();
						this.contentDiv.find('.ml-blueprint-checkbox-left-align').detach();                 
                              }
                              
                        });
                  }
            }

		protected cleanup() { super.cleanup(); }

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPSwitch.IOptionsDefinition>(BPSwitch.type);
		}
	}
	mdash.registerControl(BPSwitch, 'dashboard.components.controls', BPSwitch.meta);
}   