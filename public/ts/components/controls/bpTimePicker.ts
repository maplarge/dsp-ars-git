namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPTimePicker extends mdash.BaseControl<BPTimePicker.IOptionsDefinition> {
		public static type: string = 'BPTimePicker';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Time Picker',
			shortDescription: "Time Picker is a component used to quickly select a time, or time value in an input field.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPTimePicker.builder().options(BPTimePicker.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPTimePicker.IOptions> = {
			'BPTimePicker': {
				options: {
					outputTransforms: [],
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}
		
		protected timePicker: HTMLElement;
		protected arrowUp: HTMLElement;
		protected inputRow: HTMLElement;
		protected hours: HTMLElement;
		protected minute: HTMLElement;
		protected second: HTMLElement;
		protected milisecond: HTMLElement;
		protected arrowDown: HTMLElement;
		protected arrowUpButton: HTMLElement;
		protected arrowDownButton: HTMLElement;
		protected Am_Pm: HTMLElement;
		protected select: HTMLElement;
		protected switchArrowMax: boolean =  false;
		protected switchArrowMin: boolean = false;
		protected checkOne: boolean = false;
		
		
		constructor(id: string, definition: mdash.IControlDefinition<BPTimePicker.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();

			this.container.classList.add('namespace-blueprint-TimePicker');
			this.timePicker = ml.create("div", "ml-BPDatePicker", this.contentDiv[0]);
			this.timePicker.classList.add('ml-blueprint-timePicker');
			this.inputRow = ml.create("div", "ml-BPDatePicker", this.contentDiv[0]);
			this.inputRow.classList.add('ml-blueprint-input-row');
			this.timePicker.appendChild(this.inputRow);
		}

		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
			return false;
			
			return true;
		}
		
		protected generateMinutes(): void {
			this.hours = ml.create("input", "ml-hours", this.inputRow);
			ml.$(this.hours).after('<span id="colon1">:</span');
			ml.$(this.hours).attr('type', 'number');
			ml.$(this.hours).val('0');
			ml.$(this.hours).attr('value', Number(ml.$(this.hours).val()));

			this.minute = ml.create("input", "ml-minute", this.inputRow);
			ml.$(this.minute).attr('type', 'number');
			ml.$(this.minute).val('00');
			ml.$(this.minute).attr('value', Number(ml.$(this.minute).val()));
		}
		protected generateSeconds(): void{
			this.generateMinutes();
			this.second = ml.create("input", "ml-second", this.inputRow);
			ml.$(this.second).before('<span id="colon2">:</span');
			ml.$(this.second).attr('type', 'number');
			ml.$(this.second).val('00');
			ml.$(this.second).attr('value', Number(ml.$(this.second).val()));
		}

		protected generateMilliseconds(): void{
			this.generateSeconds();
			this.milisecond = ml.create("input", "ml-milisecond", this.inputRow);
			ml.$(this.milisecond).before('<span id="dot">.</span>');
			ml.$(this.milisecond).attr('type', 'number');
			ml.$(this.milisecond).val('000');
			ml.$(this.milisecond).attr('value', Number(ml.$(this.milisecond).val()));
		}

		protected leadingZerosHours(input: any, e: any, num: Number): void {
			if(!isNaN(input.value) && input.value.length === 0 && e.which != 8 && e.which !=46) {
				input.value = num + input.value;
			}
		}
		protected leadingZerosSecond(input: any, e: any): void {
			if(!isNaN(input.value) && input.value.length === 1 && e.which != 8 && e.which !=46) {
				input.value = '0' + input.value;
			}
			if(!isNaN(input.value) && input.value.length === 0 && e.which != 8 && e.which !=46) {
				input.value = '00' + input.value;
			}
		}
		protected leadingZeros(input: any, e: any): void {
			if(!isNaN(input.value) && input.value.length <= 1 && e.which != 8 && e.which !=46) {
				input.value = '00' + input.value;
			}
			if(!isNaN(input.value) && input.value.length === 2 && e.which != 8 && e.which !=46) {
				input.value = '0' + input.value;															
			}						
		}

		protected  ZeroMinutes(): void {
			ml.$(this.hours).on('change', (e) => this.leadingZerosHours(this.hours, e, 0));
			ml.$(document).ready( (e) => this.leadingZerosHours(this.hours, e, 0));
			
			ml.$(this.minute).on('change', (e) => this.leadingZerosSecond(this.minute, e));
			ml.$(document).ready( (e) => this.leadingZerosSecond(this.minute, e));
		}

		protected ZeroSecond(): void {
			this.ZeroMinutes();
			ml.$(this.second).on('change', (e) => this.leadingZerosSecond(this.second, e));
			ml.$(document).ready( (e) => this.leadingZerosSecond(this.second, e));
		}

		protected ZeroMilisecond(): void {
			this.ZeroSecond();
			ml.$(this.milisecond).on('change', (e) => this.leadingZeros(this.milisecond, e));
			ml.$(document).ready( (e) => this.leadingZeros(this.milisecond, e));
		}

		protected CreateArrow(count): void {
			this.arrowUpButton = ml.create('span', 'ml-BPTimePicker', this.arrowUp);			
			this.arrowUpButton.innerHTML = `<i id=up-${count} class="ml-blueprint-arrow-button mlicon-arrowup"></i>`;				
			this.arrowDownButton = ml.create('span', 'ml-BPTimePicker', this.arrowDown);
			this.arrowDownButton.innerHTML = `<i id=down-${count} class="ml-blueprint-arrow-button mlicon-arrowdown"></i>`;
		}

		protected arrowIncrement(input: HTMLElement, max:number, min: number): void {
			ml.$(input).val(Number(ml.$(input).val()) + 1)
			if(ml.$(input).val() == max) {														
				ml.$(input).val(min);
			}
			ml.$(input).attr('value', Number(ml.$(input).val()));

		}

		protected arrowDecrement(input: HTMLElement, min: number, max: number): void {
			ml.$(input).val(Number(ml.$(input).val()) - 1)
			if(ml.$(input).val() == min) {
				ml.$(input).val(max);
			}
			ml.$(input).attr('value', Number(ml.$(input).val()));
		}

		protected minuteLock(max, input, num): void{
			if(Number(ml.$(this.hours).val()) === Number(max)) {
				ml.$(input).val(num)
			}
		}

		protected obsValue(option) {
			let minute = `${ml.$(this.hours).val()} : ${ml.$(this.minute).val()}`,
			second  = ` : ${ml.$(this.second).val()}`,
			milisecond =  `. ${ml.$(this.milisecond).val()}` ;
			if(option == BPBase.Precision.Minute)return minute;
			if(option == BPBase.Precision.Second)return minute + second;
			if(option == BPBase.Precision.Milisecond)return minute + second + milisecond;
		}
		
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, '', false);
			
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPTimePicker.IOptions) => {
					ml.$(this.arrowUp).detach();
					ml.$(this.arrowDown).detach();
					ml.$(this.Am_Pm).detach();
					ml.$(this.hours).detach();
					ml.$(this.minute).detach();
					ml.$(this.second).detach();
					ml.$(this.milisecond).detach();
					ml.$('#colon1').detach();
					ml.$('#colon2').detach();
					ml.$('#dot').detach();					

				if(options.Precision == BPBase.Precision.Minute)
				{				
					this.generateMinutes();
				}

				if(options.Precision == BPBase.Precision.Second)
				{
					this.generateSeconds();
				}

				if(options.Precision == BPBase.Precision.Milisecond)
				{
					this.generateMilliseconds();
				}

				
				function select(input): void {
					ml.$(input).select();
				}
							
				if(options.selectAllOnFocus)
				{
					ml.$(this.hours).on('click', () => select(ml.$(this.hours)));
					ml.$(this.minute).on('click', () => select(ml.$(this.minute)));
					ml.$(this.second).on('click', () => select(ml.$(this.second)));
					ml.$(this.milisecond).on('click', () => select(ml.$(this.milisecond)));
				}
				
				function maxmin(max :number, min :number, _self: JQuery): void {
					var value: any = _self.val();
					
					if ((value !== '') && (value.indexOf('.') === -1)) {
						_self.val(Math.max(Math.min(value, max), min));
					}
					ml.$(_self).attr('value', ml.$(_self).val());
				};

				function updown(input: HTMLElement, max:number, min:number, currentMax:number, currentMin:number, e: JQueryEventObject): void {
					if(Number(ml.$(input).val()) == currentMax) {
						if(e.which === 38) {
							ml.$(input).val(min);
						}
					}
					
					if(ml.$(input).val() == currentMin) {
						if(e.which === 40) {
							ml.$(input).val(max);
						}
					}
				}

				function inputBlur (input: HTMLElement, e: JQueryEventObject): void{
					if(e.which === 13) {
						input.blur();
					}
				}
				
				function validate(e: JQueryEventObject) {
					var charCode: number = e.keyCode? e.keyCode : e.charCode
					if ((charCode === 190) || (charCode === 110) || (charCode === 189) || (charCode === 109) || (charCode === 107) || (charCode === 187) || (charCode === 45) || (charCode === 86)) {
							return false;
						}
				};							

				if(options.Precision == BPBase.Precision.Minute)
				{
					this.ZeroMinutes();
				}

				if(options.Precision == BPBase.Precision.Second)
				{
					this.ZeroSecond();
				}
				
				if(options.Precision == BPBase.Precision.Milisecond)
				{
					this.ZeroMilisecond();
				}


		
				ml.$(this.hours).on('input', () => {
					maxmin(23, 0,ml.$(this.hours));
					if(ml.$(this.hours).val() == ''){ml.$(this.hours).attr('value', 0)};
				});
				ml.$(this.minute).on('input', () => {maxmin(59, 0,ml.$(this.minute));
					if(ml.$(this.minute).val() == ''){ml.$(this.minute).attr('value', 0)};
				});
				ml.$(this.second).on('input', () => {maxmin(59, 0,ml.$(this.second));
					if(ml.$(this.second).val() == ''){ml.$(this.second).attr('value', 0)};
				});
				ml.$(this.milisecond).on('input', () => {
					maxmin(999, 0,ml.$(this.milisecond));
					if(ml.$(this.milisecond).val() == ''){ml.$(this.milisecond).attr('value', 0)};
				});
				
				ml.$(this.hours).on('keydown', validate);
				ml.$(this.minute).on('keydown', validate);
				ml.$(this.second).on('keydown', validate);
				ml.$(this.milisecond).on('keydown', validate);
				
				ml.$(this.hours).on('keydown', (e) => {
					inputBlur(this.hours, e);					
				});
				ml.$(this.minute).on('keydown', (e) => {inputBlur(this.minute, e)});
				ml.$(this.second).on('keydown', (e) => {inputBlur(this.second, e)});
				ml.$(this.milisecond).on('keydown', (e) => {inputBlur(this.milisecond, e)});
				
				if(!options.maxTime && !options.minTime) {ml.$(this.hours).on('keydown', (e) => updown(this.hours, 24, -1, 23, 0, e));}
				
				ml.$(this.minute).on('keydown', (e) => updown(this.minute, 60, -1, 59, 0, e));
				ml.$(this.second).on('keydown', (e) => updown(this.second, 60, -1, 59, 0, e));
				ml.$(this.milisecond).on('keydown', (e) => updown(this.milisecond, 1000, -1, 999, 0, e));
				if(options.useAmPm)
				{
					this.Am_Pm = ml.create("div", "ml-Am_Pm", this.timePicker);
					this.Am_Pm.classList.add('ml-blueprint-Am-Pm')
					this.select = ml.create("select", "ml-select", this.Am_Pm);
					let am: HTMLElement = ml.create("option", "ml-option", this.select);
					let pm: HTMLElement = ml.create("option", "ml-option", this.select);
					am.textContent = 'AM';
					pm.textContent = 'PM';
					let arrowTop: HTMLElement = ml.create("i", "ml-Arrow", this.Am_Pm);
					arrowTop.classList.add('mlicon-MapLarge-icons_layer-expanded', 'ml-blueprint-arrowUp');
					let arrowbottom: HTMLElement = ml.create("i", "ml-Arrow", this.Am_Pm);
					arrowbottom.classList.add('mlicon-MapLarge-icons_layer-expanded', 'ml-blueprint-arrowBottom');

					ml.$(this.hours).on('input', () => maxmin(12, 1,ml.$(this.hours)));
					if(!options.maxTime && !options.minTime) {ml.$(this.hours).on('keydown', (e) => updown(this.hours, 13, 0, 12, 1, e))};
					if(!options.maxTime && !options.minTime) {ml.$(this.hours).val('12')};
				}				
				else {
					ml.$(this.Am_Pm).detach();
				}
	
				if(options.maxTime)
				{
					var maxHours: number = options.maxTime;
					var switchTrueFalse: boolean = true;
					var maximumHours: number = maxHours;
			
					if(options.useAmPm)
					{
						if(maxHours >= 13) {
							var MaxAmPm: number = maxHours - 12;
							maxHours = maxHours - 12;
							ml.$(this.hours).val(MaxAmPm);	
						}
						
						if(maxHours === 0)
						{
							ml.$(this.hours).val(12);
						}
					}

					ml.$(this.hours).on('input', () => {
						if(options.minTime){
							if(ml.$(this.hours).val() == maxHours) {ml.$(this.hours).attr('max', maxHours)};
							if (ml.$(this.hours).val() < maxHours) {ml.$(this.hours).removeAttr('max')};
						}
						else {
						ml.$(this.hours).attr('max', maxHours)
						}
					})					
					
					if(!options.minTime && maxHours  == 0 && ml.$(this.hours).val() == 0) {
						ml.$(this.hours).attr('max', maxHours);
					}
					var switchTrueFalse: boolean = true;

					ml.$(this.hours).on('keydown',(e) => {
						
						if(e.which == 40){
							if(maxHours  == 0 && ml.$(this.hours).val() == 0 && options.minTime) {
								ml.$(this.hours).removeAttr('max');
							}
						}

						if(maxHours == 23 && ml.$(this.hours).val() == 23 )
						{
							if(e.which === 38)
							{
								return false;
							}
						}
						else if(ml.$(this.hours).val() == 23) {
							if(e.which === 38) {
								ml.$(this.hours).val(-1);									
							}
						}
					})

					let hoursLock: Function = (input): void => {
						if(Number(ml.$(input).val()) > 0)
						{
							if(switchTrueFalse && maxHours > 0){								
								maxHours = maxHours - 1;
								switchTrueFalse = false;
								
							}
							ml.$(this.hours).attr('max', maxHours);
						}

						else if(Number(ml.$(input).val()) === 0)
						{
							if(switchTrueFalse == false){
								maxHours = maxHours + 1;
								switchTrueFalse = true;
							}
							ml.$(this.hours).attr('max', maximumHours);
						}
					}
				
					let hoursUnLock: Function = (hours, input): void => {
						if(Number(ml.$(hours).val()) == maximumHours)
						{
							ml.$(input).attr('max', 0);
						}
						else {
							ml.$(input).removeAttr('max');
						}
					}

					ml.$(this.minute).on('input', () => {hoursLock(this.minute); this.minuteLock(maximumHours, this.minute, '00');});
					ml.$(this.hours).on('input', () => hoursUnLock(this.hours, this.minute));

					if(options.Precision == BPBase.Precision.Second)
					{
						ml.$(this.second).on('input', () => {hoursLock(this.second); this.minuteLock(maximumHours, this.second, '00');});
						ml.$(this.hours).on('input', () => hoursUnLock(this.hours, this.second));
					}

					if(options.Precision == BPBase.Precision.Milisecond)
					{
						ml.$(this.second).on('input', () => {hoursLock(this.second); this.minuteLock(maximumHours, this.second, '00');});
						ml.$(this.hours).on('input', () => hoursUnLock(this.hours, this.second));
						ml.$(this.milisecond).on('input', () => {hoursLock(this.milisecond); this.minuteLock(maximumHours, this.milisecond, '000');});
						ml.$(this.hours).on('input', () => hoursUnLock(this.hours, this.milisecond));
					}
				}

				if(options.minTime)
				{
					var minHours: number = options.minTime;				
					ml.$(this.hours).attr('min', minHours)
					ml.$(this.hours).val(minHours);
					if(options.useAmPm)
					{
						if(minHours >= 13) {
							var MinAmPm: number = minHours - 12;
							minHours = minHours -12
							ml.$(this.hours).val(MinAmPm);	
							ml.$(this.hours).attr('min', minHours)

						}
						if(minHours === 0) ml.$(this.hours).val(12);						
					}

					ml.$(this.hours).on('keydown', (e) => {
						if(options.maxTime){

							if(minHours == 0 && ml.$(this.hours).val() == 0)
							{
								if(e.which === 40) 
								{
									return false;
								}
							}	
							else if(ml.$(this.hours).val() ==  0) {
								if(e.which === 40) {								
									ml.$(this.hours).val(24);
								}
							}
						}					
					})
				
					ml.$(this.hours).on('input', () => {
						if(ml.$(this.hours).val() == minHours) {ml.$(this.hours).attr('min', minHours)};
						if(options.maxTime) {
							if (ml.$(this.hours).val() != minHours) {ml.$(this.hours).removeAttr('min')};
						}						
					})

					if(options.maxTime) {
						if(maxHours == minHours) ml.$(this.hours).attr('max', maxHours);
					}					
				}
				ml.$(this.hours).on('keydown', ()=>{
					ml.$(this.hours).blur(()=>{						
						if(!options.maxTime) {
							if(Number(ml.$(this.hours).val()) < Number(minHours)){
								ml.$(this.hours).val(minHours);	
							}
						}
						
						if(!options.minTime) {
							if(Number(ml.$(this.hours).val()) > Number(maxHours)){
								ml.$(this.hours).val(maxHours);
								ml.$(this.hours).attr('max', maxHours);
							}
						}

						if(options.minTime && options.maxTime) {
							if(Number(ml.$(this.hours).val()) < minHours && Number(ml.$(this.hours).val()) > maxHours || minHours === maxHours){
								ml.$(this.hours).val(minHours);
							}

							if(minHours < maxHours)
							{
								if(Number(ml.$(this.hours).val()) < Number(minHours)){
									ml.$(this.hours).val(minHours);	
								}
								if(Number(ml.$(this.hours).val()) > Number(maxHours)){
									ml.$(this.hours).val(maxHours);
									ml.$(this.hours).attr('max', maxHours);
								}
							}
						}
					})					
				})
				
				if(options.showArrowButtons)
				{					
					this.arrowUp = ml.create("div", "ml-BPTimePicker", this.contentDiv[0]);
					this.arrowUp.classList.add('ml-blueprint-arrowUp-row');
					this.inputRow.insertAdjacentElement('beforebegin', this.arrowUp);
					this.arrowDown = ml.create("div", "ml-BPTimePicker", this.contentDiv[0]);
					this.arrowDown.classList.add('ml-blueprint-arrowDown-row');
					this.timePicker.appendChild(this.arrowDown);
					if(options.Precision == BPBase.Precision.Minute)
					{
						for(let i: number = 0; i < 2; i++) {
							this.CreateArrow(i);
						}

						ml.$('#up-0').on('click', () => this.arrowIncrement(this.hours, 24, 0));
						ml.$('#down-0').on('click', () => this.arrowDecrement(this.hours, -1, 23));
						ml.$('#up-1').on('click', () => this.arrowIncrement(this.minute, 60, 0));
						ml.$('#down-1').on('click', () => this.arrowDecrement(this.minute, -1, 59));

						document.getElementById('up-1').addEventListener('click', (e) => this.leadingZerosSecond(this.minute, e));
						document.getElementById('down-1').addEventListener('click', (e) => this.leadingZerosSecond(this.minute, e));
					}
					if(options.Precision == BPBase.Precision.Second)
					{
						for(let i: number = 0; i < 3; i++){
							this.CreateArrow(i);
						}
						
						ml.$('#up-0').on('click', () => this.arrowIncrement(this.hours, 24, 0));
						ml.$('#down-0').on('click', () => this.arrowDecrement(this.hours, -1, 23));
						ml.$('#up-1').on('click', () => this.arrowIncrement(this.minute, 60, 0));
						ml.$('#down-1').on('click', () => this.arrowDecrement(this.minute, -1, 59));
						ml.$('#up-2').on('click', () => this.arrowIncrement(this.second, 60, 0));
						ml.$('#down-2').on('click', () => this.arrowDecrement(this.second, -1, 59));
						document.getElementById('up-1').addEventListener('click', (e) => this.leadingZerosSecond(this.minute, e));
						document.getElementById('down-1').addEventListener('click', (e) => this.leadingZerosSecond(this.minute, e));	
						document.getElementById('up-2').addEventListener('click', (e) => this.leadingZerosSecond(this.second, e));
						document.getElementById('down-2').addEventListener('click', (e) => this.leadingZerosSecond(this.second, e));						
					}
					if(options.Precision == BPBase.Precision.Milisecond)
					{
						for(let i: number = 0; i < 4; i++){
							this.CreateArrow(i)
						}
						ml.$('#up-0').on('click', () => {
							this.arrowIncrement(this.hours, 24, 0);
						});
						ml.$('#down-0').on('click', () => this.arrowDecrement(this.hours, -1, 23));

						ml.$('#up-1').on('click', (e) => {

							if(!options.maxTime) {
								this.arrowIncrement(this.minute, 60, 0);
							}
							else if(options.maxTime && Number(ml.$(this.hours).val()) != maximumHours) {
								this.arrowIncrement(this.minute, 60, 0);
							}
							this.leadingZerosSecond(this.minute, e)
							
						});

						ml.$('#down-1').on('click', (e) => {
							if(!options.maxTime) {
								this.arrowDecrement(this.minute, -1, 59)
							}
							else if(options.maxTime && Number(ml.$(this.hours).val()) != maximumHours) {
								this.arrowDecrement(this.minute, -1, 59)
							}
							this.leadingZerosSecond(this.minute, e)
						});

						ml.$('#up-2').on('click', (e) => {
							if(!options.maxTime) {
								this.arrowIncrement(this.second, 60, 0)
							}
							else if(options.maxTime && Number(ml.$(this.hours).val()) != maximumHours) {
								this.arrowIncrement(this.second, 60, 0)
							}
							this.leadingZerosSecond(this.second, e)
						});

						ml.$('#down-2').on('click', (e) => {
							if(!options.maxTime) {
								this.arrowDecrement(this.second, -1, 59)
							}
							else if(options.maxTime && Number(ml.$(this.hours).val()) != maximumHours) {
								this.arrowDecrement(this.second, -1, 59)
							}
							this.leadingZerosSecond(this.second, e)
						});


						ml.$('#up-3').on('click', (e) => {
							if(!options.maxTime) {
								this.arrowIncrement(this.milisecond, 1000, 0)
							}
							else if(options.maxTime && Number(ml.$(this.hours).val()) != maximumHours) {
								this.arrowIncrement(this.milisecond, 1000, 0)
							}
							this.leadingZeros(this.milisecond, e)
						});

						ml.$('#down-3').on('click', (e) => {
							if(!options.maxTime) {
								this.arrowDecrement(this.milisecond, -1, 999)
							}
							else if(options.maxTime && Number(ml.$(this.hours).val()) != maximumHours) {
								this.arrowDecrement(this.milisecond, -1, 999)
							}

							this.leadingZeros(this.milisecond, e)
						});
					}

					if(options.useAmPm) {
						ml.$('#up-0').off().on('click', () => this.arrowIncrement(this.hours, 13, 1));
						ml.$('#down-0').off().on('click', () => this.arrowDecrement(this.hours,0, 12));
					}
					
					if(options.minTime) {

						ml.$('#up-0').off().on('click', () => {
							if(!options.maxTime && Number(ml.$(this.hours).val()) !=23) {
								
								if(options.useAmPm) {
									this.arrowIncrement(this.hours, 12, 0);
								} else {
									this.arrowIncrement(this.hours, 24, 0);									
								}
							}
							
						});

						ml.$('#down-0').off().on('click', () => {
							if(options.minTime && Number(ml.$(this.hours).val()) != minHours) {
								this.arrowDecrement(this.hours, -1, 23);
							}							
						});												
					}

					if(options.maxTime)
					{	
						ml.$('#up-1').on('click', () => {
							if(Number(ml.$(this.minute).val()) != 0) {
								if(!this.checkOne){	maxHours -= 1; this.checkOne = true;}
							}
							if(Number(ml.$(this.minute).val()) == 0) {
								if(this.checkOne == true){ maxHours += 1; this.checkOne = false;}
							}
						})

						ml.$('#down-1').on('click', () => {
							if(Number(ml.$(this.minute).val()) == 0) {
								if(this.checkOne == true){ maxHours += 1; this.checkOne = false;}
							}
							if(Number(ml.$(this.minute).val()) != 0) {
								if(!this.checkOne){	maxHours -= 1; this.checkOne = true;}
							}
						})

						ml.$('#up-2').on('click', () => {
							if(Number(ml.$(this.second).val()) != 0) {
								if(!this.checkOne){	maxHours -= 1; this.checkOne = true;}
							}
							if(Number(ml.$(this.second).val()) == 0) {
								if(this.checkOne == true){ maxHours += 1; this.checkOne = false;}
							}
						})

						ml.$('#down-2').on('click', ()=>{
							if(Number(ml.$(this.second).val()) == 0) {
								if(this.checkOne == true){ maxHours += 1; this.checkOne = false;}
							}
							if(Number(ml.$(this.second).val()) != 0) {
								if(!this.checkOne){	maxHours -= 1; this.checkOne = true;}
							}
						})

						ml.$('#up-3').on('click', ()=>{
							if(Number(ml.$(this.milisecond).val()) != 0) {
								if(!this.checkOne){	maxHours -= 1; this.checkOne = true;}
							}
							if(Number(ml.$(this.milisecond).val()) == 0) {
								if(this.checkOne == true){ maxHours += 1; this.checkOne = false;}
							}
						})

						ml.$('#down-3').on('click', ()=>{
							if(Number(ml.$(this.milisecond).val()) == 0) {
								if(this.checkOne == true){ maxHours += 1; this.checkOne = false;}
							}
							if(Number(ml.$(this.milisecond).val()) != 0) {
								if(!this.checkOne){	maxHours -= 1; this.checkOne = true;}
							}							
						})

						ml.$('#up-0').off().on('click', () => {							
							if(Number(ml.$(this.hours).val()) != maxHours) {
								
								if(options.useAmPm) {
									this.arrowIncrement(this.hours, 12, 0);
								} else {
									this.arrowIncrement(this.hours, 24, 0);									
								}
							}
							if(Number(ml.$(this.hours).val()) == maximumHours)
							{
								ml.$(this.minute).attr('max', 0);
								if(options.Precision = BPBase.Precision.Second) ml.$(this.second).attr('max', 0);
								if(options.Precision = BPBase.Precision.Milisecond) ml.$(this.milisecond).attr('max', 0);
							}
							if(options.minTime) {
								if(ml.$(this.hours).val() == maxHours) ml.$(this.hours).attr('max', maxHours);								
							}

							else {
								ml.$(this.hours).attr('max', maxHours);							
							}

							if(options.minTime) {
								if (Number(ml.$(this.hours).val()) != minHours) {ml.$(this.hours).removeAttr('min')};
							}
						});

						ml.$('#down-0').off().on('click', () => {
							if(!options.minTime && Number(ml.$(this.hours).val()) !=0) {
								this.arrowDecrement(this.hours, -1, 23);
							}
							if(options.minTime && Number(ml.$(this.hours).val()) != minHours) {
								if(options.useAmPm) {
									this.arrowDecrement(this.hours, 0, 12);

								} else {
									this.arrowDecrement(this.hours, -1, 23);								
								}

							}
							if(Number(ml.$(this.hours).val()) != maximumHours)
							{						
								ml.$(this.minute).removeAttr('max');
								if(options.Precision = BPBase.Precision.Second)ml.$(this.second).removeAttr('max');
								if(options.Precision = BPBase.Precision.Milisecond)ml.$(this.milisecond).removeAttr('max');
							}
							if(Number(ml.$(this.hours).val()) == minHours) {ml.$(this.hours).attr('min', minHours)};
							if (Number(ml.$(this.hours).val()) < maxHours) ml.$(this.hours).removeAttr('max');
						});	
					}
				}
				else 
				{
					ml.$(this.arrowUp).detach();
					ml.$(this.arrowDown).detach();
				}

				obs(this.obsValue(options.Precision));
				
				ml.$('.ml-blueprint-arrow-button').on('click', ()=>obs(this.obsValue(options.Precision)))
				
				ml.$(this.inputRow).find('input').on('change', ()=>obs(this.obsValue(options.Precision)))
			
				if(options.defaultValue)
				{
					let time: Date = options.defaultValue;
					var hours: number = time.getHours();
					var minute: number = time.getMinutes();
					var second: number = time.getSeconds();
					var milisecond: number = time.getMilliseconds();
					ml.$(this.hours).val(hours);
					ml.$(this.minute).val(minute);
					ml.$(this.second).val(second);
					ml.$(this.milisecond).val(milisecond);
					if(options.useAmPm)
					{
						if(hours >= 13) {
							let ampm: number = hours - 12;
							ml.$(this.hours).val(ampm);
						}

						if(hours === 0)
						{
							ml.$(this.hours).val(12);
						}
					}
				}

				if(options.disabled)
				{
					this.timePicker.classList.add('ml-blueprint-disabled');
					if(options.useAmPm)
					{
						this.select.setAttribute('disabled', 'true');
					}

					// this lines disable click event on arrows and passes disabled attribute
					if(options.Precision == BPBase.Precision.Minute)
					{
						ml.$('#up-0').off('click');
						ml.$('#down-0').off('click');
						ml.$('#up-1').off('click');
						ml.$('#down-1').off('click');
						this.hours.setAttribute('disabled', `${options.disabled}`);
						this.minute.setAttribute('disabled', `${options.disabled}`);
					}

					if(options.Precision == BPBase.Precision.Second)
					{
						ml.$('#up-0').off('click');
						ml.$('#down-0').off('click');
						ml.$('#up-1').off('click');
						ml.$('#down-1').off('click');
						ml.$('#up-2').off('click');
						ml.$('#down-2').off('click');
						this.hours.setAttribute('disabled', `${options.disabled}`);
						this.minute.setAttribute('disabled', `${options.disabled}`);
						this.second.setAttribute('disabled', `${options.disabled}`);
					}

					if(options.Precision == BPBase.Precision.Milisecond)
					{
						ml.$('#up-0').off('click');
						ml.$('#down-0').off('click');
						ml.$('#up-1').off('click');
						ml.$('#down-1').off('click');
						ml.$('#up-2').off('click');
						ml.$('#down-2').off('click');
						ml.$('#up-3').off('click');
						ml.$('#down-3').off('click');
						this.hours.setAttribute('disabled', `${options.disabled}`);
						this.minute.setAttribute('disabled', `${options.disabled}`);
						this.second.setAttribute('disabled', `${options.disabled}`);
						this.milisecond.setAttribute('disabled', `${options.disabled}`);
					}
					if(options.useAmPm) {
						ml.$('#up-0').off();
						ml.$('#down-0').off();
					}
				}
				else {
					this.timePicker.classList.remove('ml-blueprint-disabled');
					if(options.Precision == BPBase.Precision.Minute)
					{
						this.hours.removeAttribute('disabled');
						this.minute.removeAttribute('disabled');
					}

					if(options.Precision == BPBase.Precision.Second)
					{
						this.hours.removeAttribute('disabled');
						this.minute.removeAttribute('disabled');
						this.second.removeAttribute('disabled');
					}

					if(options.Precision == BPBase.Precision.Milisecond)
					{
						this.hours.removeAttribute('disabled');
						this.minute.removeAttribute('disabled');
						this.second.removeAttribute('disabled');
						this.milisecond.removeAttribute('disabled');
					}
				}			
			});
			}
		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPTimePicker.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPTimePicker {

		export interface ITimePicker extends BPBase.ITimePickerProps, BPBase.IControlProps{
	
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ITimePicker{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			Precision?: BPBase.Precision | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath,
			showArrowButtons?: boolean | mdash.IScopePath,
			selectAllOnFocus?: boolean | mdash.IScopePath,
			defaultValue?: Date | mdash.IScopePath,
			useAmPm?: boolean | mdash.IScopePath,
			minTime?: number | mdash.IScopePath,
			maxTime?: number | mdash.IScopePath,
		}
	}

	mdash.registerControl(BPTimePicker,'dashboard.components.controls', BPTimePicker.meta);
}