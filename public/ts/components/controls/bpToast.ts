namespace dashboard.components.controls {
	import mdash = ml.ui.dashboard;
	
	export class BPToast extends mdash.BaseControl<BPToast.IOptionsDefinition> {
		public static type: string = 'BPToast';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Toast',
			shortDescription: "The toast component is a non-disruptive message that appears at the bottom of the interface to provide quick.",
			categories: ["Visualization","Layout"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPToast.builder().options(BPToast.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPToast.IOptions> = {
			'BPToast': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected ModalBlock: JQuery;
		protected ButtonsBlock: JQuery;
		protected ButtonsObj: JQuery;
		protected UploadButton: JQuery;
		protected UploadProgress: JQuery;
		protected ModalSuccess: JQuery;
		protected ModalDanger: JQuery;
		protected ModalPrimary: JQuery;
		protected ModalWarning: JQuery;
		protected ModalEachReplaceAnother: JQuery;
		protected ModalGreenReplaceWarning: JQuery;
		protected indexClass: any = 0;
		protected ButtonsArray:any[];
		protected IntervalProgres:any = {};
		protected BoolInterval:boolean = true;
		protected DetacheItem:HTMLElement;
		protected DeleteItemFunctions:any = {};
		protected CloseButton:Function;
		protected commonButtons:HTMLElement;
		protected ESCKeypressBollean:boolean = false;

		constructor(id: string, definition: mdash.IControlDefinition<BPToast.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
			
			this.container.classList.add('namespace-blueprint-toast')
			this.commonButtons = ml.create("div", "ml-BPToast",  this.contentDiv[0]) as HTMLElement;
			this.ModalBlock = ml.$(`<div class="ml-blueprint-ModalSection"></div>`);
			this.commonButtons.classList.add('ml-blueprint-buttonsWrap');
			this.UploadButton = ml.$(`
				<button class="ml-blueprint-upluadFile">Upload file...</button>
			`);
			
		}
		
		protected validateDefinition(): boolean {

			if (!super.validateDefinition())
			return false;

			return true;
		}

		protected init() {
			super.init();

			var obs = this.registerScopeObservable(this.definition.name, null, false);
			
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPToast.IOptions) => {
					ml.$(this.ButtonsBlock).detach()
					
					this.ModalBlock.appendTo('body');
					ml.$(this.UploadProgress).detach();
					this.ButtonsBlock = ml.$(`
						<p class="ml-blueprint-buttonsLine"></p>
					`);
					
					var self = this;
					var Modal = {
						__ReplaceModal:(tag:Element, type:string, IntentIndex: number):void=>{
							self.ModalBlock.children().map((ind:number, el:HTMLElement)=>{
								
								if(el == tag){
									if(type=="success"){
										self.ModalEachReplaceAnother = ml.$(`
											<div class="ml-blueprint-ModalSuccessReplaceRed" style="margin-top:-40px; opacity:0">
												<span class="mlicon-button14"></span>
											<div class="ml-blueprint-middleUndo">
												${options.btnData[IntentIndex].replaceItemText}
											</div>
												<a href="javascript:void(0)" class="mlicon-MapLarge-icons_close-delete ml-blueprint-detacheItem"></a>
											</div>
										`);
									}
									else if(type=="warning"){
										self.ModalEachReplaceAnother = ml.$(`
											<div class="ml-blueprint-ModalWarningReplaceWhite" style="margin-top:-40px; opacity:0">
											<div class="ml-blueprint-middleUndo">
											${options.btnData[IntentIndex].replaceItemText}
											</div>
												<a href="javascript:void(0)" class="mlicon-MapLarge-icons_close-delete ml-blueprint-detacheItem"></a>
											</div>
										`);
									}
									let thisItemReplace = self.ModalEachReplaceAnother as JQuery;
									self.ModalBlock.prepend(self.ModalEachReplaceAnother);
									Modal.__ModalItemsStyleAndRemove(thisItemReplace);
									self.DeleteItemFunctions.__callDelete(ind, el);
									self.CloseButton();
								}
							});
							Modal.__OnMouseEnter();
							Modal.__OnMouseLeave();
						},
						__OnMouseEnter:():void=>{
							self.ModalBlock.children().on('mouseover', (event:Event)=>{
								if((<Element>event.currentTarget).closest('.ml-blueprint-ModalSection') && (options.escClear != false)){
									(<Element>event.currentTarget).classList.add('ml-blueprint-ModalStopDetach');
								}
							});
						},
						__OnMouseLeave:():void=>{
							self.ModalBlock.children().on('mouseleave', (event:JQueryEventObject)=>{
								(<Element>event.currentTarget).classList.remove('ml-blueprint-ModalStopDetach');
								setTimeout(()=>{
									if(ml.$((<Element>event.currentTarget)).hasClass("ml-blueprint-ModalStopDetach")){
										return false;
									}
									else{
										ml.$((<Element>event.currentTarget)).css({
											visibility:"hidden",
											opacity:0,
											marginTop:"15px",
											top:"-5px",
											zIndex:"-1",
											position:"relative"
										})
										setTimeout(()=>{
											ml.$((<Element>event.currentTarget)).detach();
										}, 400);
									}
								}, options.timeout);
							})
						},
						__ModalItemsStyleAndRemove:(thisItem:JQuery):void=>{
							setTimeout(()=>{
								Array.prototype.map.call(this.ModalBlock.children(), (element, index)=>{
									ml.$(element).css({marginTop:15+'px', opacity:1, position:"relative"})
								})
							}, 100)
							setTimeout(()=>{
								if(this.ESCKeypressBollean == false){
									if(ml.$(thisItem).hasClass("ml-blueprint-ModalStopDetach")){
										return false;
									}
									else{
										ml.$(thisItem).css({
											visibility:"hidden",
											opacity:0,
											marginTop:"15px",
											top:"-5px",
											zIndex:"-1",
											position:"relative"
										})
										setTimeout(()=>{
											ml.$(thisItem).detach();
										}, 50);
									}
								}
							}, options.timeout);
						},
						__Success:function(num:number){
							self.ModalSuccess = ml.$(`
								<div class="ml-blueprint-ModalSuccess" style="margin-top:-40px; opacity:0">
									<span class="mlicon-MapLarge-icons_check"></span>
									<div class="ml-blueprint-middleUndo">
									${options.btnData[num].modalText}
										<a href="javascript:void(0)" class="ml-blueprint-undo ml-blueprint-ReplaceModal">Undo</a>
									</div>
									<a href="javascript:void(0)" class="mlicon-MapLarge-icons_close-delete ml-blueprint-detacheItem"></a>
								</div>
							`);
							let thisItem = self.ModalSuccess as JQuery;
							self.ModalBlock.prepend(self.ModalSuccess);
							this.__ModalItemsStyleAndRemove(thisItem);
							self.CloseButton();
							(document.querySelector('.ml-blueprint-ReplaceModal') as HTMLInputElement).addEventListener('click', function(e:Event){
								Modal.__ReplaceModal((<HTMLInputElement>e.target).offsetParent, "success", num);
							})
						},
						__Danger:function(num:number){
							self.ModalDanger = ml.$(`
								<div class="ml-blueprint-ModalDanger" style="margin-top:-40px; opacity:0">
									<span class="mlicon-warning17"></span>
									<div class="ml-blueprint-middleUndo">
										${options.btnData[num].modalText}
										<a href="javascript:void(0)" class="ml-blueprint-undo ml-blueprint-ReplaceModal">Retry</a>
									</div>
									<a href="javascript:void(0)" class="mlicon-MapLarge-icons_close-delete ml-blueprint-detacheItem"></a>
								</div>
							`);
							let thisItem = self.ModalDanger as JQuery;
							self.ModalBlock.prepend(self.ModalDanger);
							this.__ModalItemsStyleAndRemove(thisItem);
							self.CloseButton();
							(document.querySelector('.ml-blueprint-ReplaceModal') as HTMLInputElement).addEventListener('click', function(e:Event){
								self.ModalBlock.children().map((ind, el)=>{							
									if(<HTMLInputElement>el == (<HTMLInputElement>e.target).offsetParent){
										(<HTMLInputElement>e.target).offsetParent.remove()
										Modal.__Danger(2);
									}
								})
							})
						},				
						__Primary:function(num:number){
							self.ModalPrimary = ml.$(`
								<div class="ml-blueprint-ModalPrimary" style="margin-top:-40px; opacity:0">
									<div class="ml-blueprint-middleUndo">
										<span>
											${options.btnData[num].modalText} <i>Toasty.</i>
										</span>
										<a class="ml-blueprint-undo" href="https://www.google.com/search?q=toast&source=lnms&tbm=isch" target="_blank">Yum.</a>
									</div>
									<a href="javascript:void(0)" class="mlicon-MapLarge-icons_close-delete ml-blueprint-detacheItem"></a>
								</div>
							`);
							let thisItem = self.ModalPrimary as JQuery;
							self.ModalBlock.prepend(self.ModalPrimary);
							this.__ModalItemsStyleAndRemove(thisItem);
							self.CloseButton();
						},
						__Warning:function(num:number){
							self.ModalWarning = ml.$(`
								<div class="ml-blueprint-ModalWarning" style="margin-top:-40px; opacity:0">
									<span class="mlicon-selectshape"></span>
									<div class="ml-blueprint-middleUndo">
									${options.btnData[num].modalText}
										<a href="javascript:void(0)" class="ml-blueprint-undo ml-blueprint-ReplaceModal">Adieu</a>
									</div>
									<a href="javascript:void(0)" class="mlicon-MapLarge-icons_close-delete ml-blueprint-detacheItem"></a>
								</div>
							`);
							let thisItem = self.ModalWarning as JQuery;
							self.ModalBlock.prepend(self.ModalWarning);
							this.__ModalItemsStyleAndRemove(thisItem);
							self.CloseButton();
							(document.querySelector('.ml-blueprint-ReplaceModal') as HTMLInputElement).addEventListener('click', function(e:Event){
								Modal.__ReplaceModal((<HTMLInputElement>e.target).offsetParent, "warning", num);
							})
						}
					};
					


					// Create Button
					Array.prototype.map.call(options.btnData, (element:JQuery, index:number)=>{
						this.ButtonsObj = ml.$(`<button data-id=${index} class="ml-blueprint-toast-${element.intent}">${element.name}</button>`);
						this.ButtonsBlock.append(this.ButtonsObj)
						ml.$(this.ButtonsObj).on('click', (e:Event)=>{
							switch(ml.$(e.target).attr('data-id')){
								case "0":Modal.__Primary(0);
									break;
								case "1":Modal.__Success(1);
									break;
								case "2":Modal.__Danger(2);
									break;
								case "3":Modal.__Warning(3);
									break;
								default:
								break;
							}
							Modal.__OnMouseEnter();
							Modal.__OnMouseLeave();
							if(options.autoFocus){
								Array.prototype.map.call(this.ModalBlock.children(), (element:HTMLInputElement)=>{
									element.setAttribute("tabindex", "-1");
								})
								this.ModalBlock.children()[0].setAttribute("tabindex", "0")
								this.ModalBlock.children()[0].focus()
							}
						})
					})
					// -------------------------

					
					// Close button click Delete Or Restore Object for all Modals
					Object.assign(this.DeleteItemFunctions, {
						__styles:()=>{
							return {
								deleteBeforeItems:{
									visibility:"hidden",
									opacity:"0",
									marginTop:"15px",
									position:"relative"
								},
								deleteLastItem:{
									visibility:"hidden",
									opacity:"0",
									height:"0",
									overflow:"hidden",
									padding:"0 10px",
									marginTop:"0",
									top:"25px",
									position:"relative"
								},
								deleteRestoreItems:{
									visibility:"visible",
									opacity:"1",
									height:"auto",
									padding:"5px 10px	",
									marginTop:"15px",
									top:"0",
									position:"relative"
								}
							}
						},
						__CheckLastItem:function(ind:number, elem:HTMLInputElement){
							return  (ind == self.ModalBlock.children().length-1)?
									Object.assign(elem.style, this.__styles().deleteBeforeItems):
									Object.assign(elem.style, this.__styles().deleteLastItem);
						},
						__callDelete:function(ind:number, el:HTMLInputElement):void{
							this.__CheckLastItem(ind, el);
							setTimeout(()=>el.remove(), 400)
						},
						__callDeleteRestore:function(ind: number, el: HTMLInputElement):void{
							this.__CheckLastItem(ind, el);
							setTimeout(()=>Object.assign(el.style, this.__styles().deleteRestoreItems), 1000)
						}
					});
					// -----------------------------

					// Close Button with the all Modals bind to this.DeleteItemFunctions Object
					this.CloseButton = (e)=>{
						this.DetacheItem = document.querySelector('.ml-blueprint-detacheItem') as HTMLInputElement;
						this.DetacheItem.addEventListener('click', (elem:Event)=>{
							let boolRetsor = false;
							this.DetacheItem.style.boxShadow = "0"
							this.ModalBlock.children().map((ind:number, el:HTMLInputElement)=>{
								if(el == (<HTMLInputElement>elem.target).offsetParent){
									Array.prototype.map.call((<HTMLInputElement>elem.target).offsetParent.children, (element:JQuery)=>{
										// Uploade Modals Delete if
										// if click event target is equal upload progress Bar any items
										if(element.classList.contains('ml-blueprint-uploadProgress')){
											boolRetsor = true;
											if(element.children[0].classList.contains("ml-blueprint-hasBeenEnded")){
												this.DeleteItemFunctions.__callDelete(ind, el)
											}
											else{
												this.DeleteItemFunctions.__callDeleteRestore(ind, el);
											}
										}
										// ------------------------
									})
									// Left Buttons
									// if click event target does't equal upload progress Bar any items 
									if(!boolRetsor){
										this.DeleteItemFunctions.__callDelete(ind, el)
									}
									// --------------------------
								}
							})
						})
					}
					// -------------------------
					// Upload Button

					const uploadFunc = (objElem):void =>{
						this.UploadProgress = ml.$(`
							<div class="ml-blueprint-modalProgress" style="margin-top:-40px; opacity:0">
								<span class="mlicon-tableupload"></span>
								<div class="ml-blueprint-uploadProgress">
									<span class="ml-blueprint-animateProgress ml-blueprint-animateBar${objElem.indexElem}"></span>
								</div>
								<a href="javascript:void(0)" class="mlicon-MapLarge-icons_close-delete ml-blueprint-detacheItem"></a>
							</div>
						`);
						this.ModalBlock.prepend(this.UploadProgress);
						

						let thisItem: JQuery = this.UploadProgress;
						Modal.__ModalItemsStyleAndRemove(thisItem);

						// Delete This Item with close Click
						this.CloseButton();
						// --------------------------

						// ProgressBar width Animation
						this.IntervalProgres['some'+objElem.indexElem] = {
							['width'+objElem.indexElem]:0,
							['boolean'+objElem.indexElem]:true,
							readyCall:function():void{
								if(this['boolean'+objElem.indexElem]){
									this['boolean'+objElem.indexElem] = false;
									let clearAnim = setInterval(()=>{
										this['width'+objElem.indexElem]+=20;
										if(this['width'+objElem.indexElem]<=100){
											ml.$(`.ml-blueprint-animateBar${objElem.indexElem}`).css({width:this['width'+objElem.indexElem]+"%"})
										}
										if(this['width'+objElem.indexElem] > 100){
											ml.$(`.ml-blueprint-animateBar${objElem.indexElem}`).addClass('ml-blueprint-hasBeenEnded')
											ml.$(`.ml-blueprint-animateBar${objElem.indexElem}`).css({
												background:"none",
												backgroundColor: 'rgb(15, 153, 96)'
											})
											clearInterval(clearAnim);
										}
									}, objElem.speedInter)
								}
							}
						}
						for(let key in this.IntervalProgres){
							this.IntervalProgres[key].readyCall()
						}
						Modal.__OnMouseEnter();
						Modal.__OnMouseLeave();
						// ----------------------------

						if(options.autoFocus){
							Array.prototype.map.call(this.ModalBlock.children(), (element:HTMLInputElement)=>{
								element.setAttribute("tabindex", "-1");
							})
							this.ModalBlock.children()[0].setAttribute("tabindex", "0")
							this.ModalBlock.children()[0].focus()
						}
					}

					this.ButtonsBlock.appendTo(this.commonButtons);
					
					this.UploadButton.appendTo(this.commonButtons);
					this.UploadButton.off().on('click', ()=>{
						this.indexClass+=1;				
						uploadFunc({indexElem:this.indexClass, speedInter:500});
						if(options.autoFocus){
							this.ModalBlock.children()[0].focus()
						}
					})
					if(options.escClear){
						ml.$('.namespace-blueprint-toast').on('keydown', (e)=>{
							if(e.keyCode == 27 && this.ModalBlock.children().length > 0){
								this.ESCKeypressBollean = true;
								let elementCount = this.ModalBlock.children().length;
								var removeWithESC = setInterval(()=>{
									elementCount--;
									// if Common modal Items has any Img Upload Item
									if(this.ModalBlock.children()[elementCount].classList.contains('ml-blueprint-modalProgress')){
										if(ml.$(this.ModalBlock.children()[elementCount]).find('.ml-blueprint-uploadProgress').children()[0].classList.contains("ml-blueprint-hasBeenEnded")){
											this.DeleteItemFunctions.__callDelete(elementCount, this.ModalBlock.children()[elementCount])
										}
										else{
											this.DeleteItemFunctions.__callDeleteRestore(elementCount, this.ModalBlock.children()[elementCount]);
										}
										if(elementCount == 0){
											this.ESCKeypressBollean = false;
											clearInterval(removeWithESC);
										}
									}
									// ------------------------
									// Anything else do this function left items 
									else{
										ml.$(this.ModalBlock.children()[elementCount]).css({
											visibility:"hidden",
											opacity:0,
											marginTop:"15px",
											top:"-5px",
											zIndex:"-1",
											position:"relative"
										})
										setTimeout(()=>{
											ml.$(this.ModalBlock.children()[elementCount]).detach();
										}, 150);
										if(elementCount == 0){	
											this.ESCKeypressBollean = false;
											clearInterval(removeWithESC);
										}
									}
									// -------------------------------
								}, 100)
							}
						})
					}
					else{
						ml.$('.namespace-blueprint-toast').off('keydown')
					}
					
					if(options.modalPosition){
						this.ModalBlock.attr('class', "ml-blueprint-ModalSection")
						switch(options.modalPosition){
							case BPToast.AlignmentSelect.TopCenter:this.ModalBlock.addClass('ml-blueprint-alignTopCenter');
								break;
							case BPToast.AlignmentSelect.TopLeft:this.ModalBlock.addClass('ml-blueprint-alignTopLeft');
								break;
							case BPToast.AlignmentSelect.TopRight:this.ModalBlock.addClass('ml-blueprint-alignTopRight');
								break;
							case BPToast.AlignmentSelect.BottomLeft:this.ModalBlock.addClass('ml-blueprint-alignBottomLeft');
								break;
							case BPToast.AlignmentSelect.BottomRight:this.ModalBlock.addClass('ml-blueprint-alignBottomRight');
								break;
							case BPToast.AlignmentSelect.BottomCenter:this.ModalBlock.addClass('ml-blueprint-alignBottomCenter');
								break;
							default:this.ModalBlock.addClass('ml-blueprint-alignTopCenter');
								break;
						}
						
					}
			
				});
			}

		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPToast.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPToast {
		
		/**
		 * Alignment enumeration for Toasts
		 * @readonly
		 * @enum
		 */
		export enum AlignmentSelect{
			/** Place toast at bottom left */
			BottomLeft = 'AlignBottomLeft',
			/** Place toast at bottom center */
			BottomCenter = 'AlignBottomCenter',
			/** Place toast at bottom right */
			BottomRight = 'AlignBottomRight',
			/** Place toast at top left */
			TopLeft = 'AlignTopLeft',
			/** Place toast at top center */
			TopCenter = 'AlignTopCenter',
			/** Place toast at top right */
			TopRight = 'AlignTopRight',
		}

		/**
		 * Interface for Toast
		 * @typedef IToastProps
		 * @interface
		 * @readonly
		 * @augments BPBase.IActionProps
		 * @augments BPBase.IAppearance
		 */
		export interface IToastProps extends BPBase.IActionProps, BPBase.IAppearance{
			/**
			 * Intent color for the Toast
			 * @type {ctrl.BPBase.Intent}
			 */
			intent?:ctrl.BPBase.Intent,
			/**
			 * Position of the toast modal on the screen
			 * @type {AlignmentSelect}
			 */
			modalPosition?:AlignmentSelect,
			/**
			 * Name of the toast modal
			 * @type {string}
			 */
			name?: string,
			/**
			 * Automatically place the focus on the toast modal
			 * @type {string}
			 */
			autoFocus?:boolean,
			/**
			 * Button objects that create toasts
			 * @type {ctrl.BPBase.IToastButtonProps[]}
			 */
			btnData?: ctrl.BPBase.IToastButtonProps[],
			/**
			 * Clear the toast by pressing ESC
			 * @type {boolean}
			 */
			escClear?:boolean,
			/**
			 * The number of milliseconds after which the toast automatically closes
			 * @type {number}
			 */
			timeout?:number,
			/**
			 * Text inside the toast modal
			 * @type {string}
			 */
			modalText?:string,
			/**
			 * Text to replace the modal toast text with after an interaction inside it happens
			 * @type {string}
			 */
			replaceItemText?:string,
		}
		
		
		export interface IOptions extends mdash.IControlOptionsWithTransforms, IToastProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			name?:string | mdash.IScopePath,
			btnData?: ctrl.BPBase.IToastButtonProps[] | mdash.IScopePath,
			intent?:string | BPBase.Intent | mdash.IScopePath,
			modalPosition?:string | BPToast.AlignmentSelect | mdash.IScopePath,
			autoFocus?:boolean | mdash.IScopePath,
			timeout?:number | mdash.IScopePath,
			escClear?:boolean | mdash.IScopePath,
			modalText?:string | mdash.IScopePath,
			replaceItemText?:string | mdash.IScopePath,
		}
	}
	mdash.registerControl(BPToast,'dashboard.components.controls', BPToast.meta);
}