namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPDateInput extends mdash.BaseControl<BPDateInput.IOptionsDefinition> {
		public static type: string = 'BPDateInput';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Date Input',
			shortDescription: "The DateInput represents an input field that recognizes and formats scheduling values such as dates.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.SINGLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPDateInput.builder().options(BPDateInput.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPDateInput.IOptions> = {
			'BPDateInput': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		constructor(id: string, definition: mdash.IControlDefinition<BPDateInput.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		private dateInputDate: JQuery;
		private dateInputText: JQuery;
		private observer: MutationObserver;

		public build() {
			super.build();
			this.container.classList.add('namespace-blueprint-dateinput');
			//this is where you might create dom elements
			//elements are not yet added to the dom at this point though
		}

		protected validateDefinition(): boolean {

			//run any validation against the defintion (options passed into the control) that you might require

			if (!super.validateDefinition())
				return false;

			return true;
		}

		/**
		 * set a new date
		 * @param obs observable variable
		 */
		protected setDate(obs: mdash.RefCountedObservable<string>): void
		{
			this.dateInputDate.css('background', 'rgb(19, 124, 189)');
			this.dateInputText.css('color','#fff');
			obs(this.contentDiv.find('.namespace-blueprint-textInput input').val());
		}

		/**
		 * clear the date
		 * @param obs observable variable
		 */
		protected clearDate(obs: mdash.RefCountedObservable<string>): void
		{
			this.dateInputDate.css('background', 'rgba(138,155,168,.2)');
			this.dateInputText.css('color','#000');
			this.dateInputText.text('no date');
			obs('');
		}

		/**
		 * format date depending on the props
		 * @param options options object with props
		 */
		protected formatDate(options: BPDateInput.IOptions){
			let dateText: string = this.contentDiv.find('.namespace-blueprint-datePicker').find('.ml-blueprint-select-date').text();
			this.dateInputText.text(dateText);
			const parsedDate: moment.Moment = ml.moment(dateText);

			if(options.Precision == BPBase.Precision.Second || options.Precision == BPBase.Precision.Milisecond)
			{
				// grab the seconds from the timepicker's input for seconds and update the time value
				parsedDate.seconds(parseInt(this.contentDiv.find('.namespace-blueprint-datePicker .ml-blueprint-timePicker input').eq(2).val()));
			}

			if(parsedDate)
			{
				if(options.dateFormat)
				{
					switch(options.dateFormat){
						case BPDateInput.DateFormat.JS_DATE:
							this.contentDiv.find('.namespace-blueprint-textInput input').val(parsedDate.format('MM/D/YYYY'));
							break;
						case BPDateInput.DateFormat.MONTH_DAY_YEAR:
							this.contentDiv.find('.namespace-blueprint-textInput input').val(ml.moment(parsedDate).format('MM/DD/YYYY'));
							break;
						case BPDateInput.DateFormat.YEAR_MONTH_DAY:
							this.contentDiv.find('.namespace-blueprint-textInput input').val(ml.moment(parsedDate).format('YYYY-MM-DD'));
							break;
						case BPDateInput.DateFormat.YEAR_MONTH_DAY_TIME:
							this.contentDiv.find('.namespace-blueprint-textInput input').val(ml.moment(parsedDate).format('YYYY-MM-DD HH:mm:ss'));
							break;
					}
				}
				else
				{
					this.contentDiv.find('.namespace-blueprint-textInput input').val(parsedDate.format('MM/D/YYYY'));
				}
			}
			else
			{
				this.contentDiv.find('.namespace-blueprint-textInput input').val('');
			}
			
			return parsedDate;
		}
		
		/**
		 * setup the mutation observer to observe date changes caused by the datepicker
		 * @param options options object with props
		 * @param obs observable variable
		 */
		protected setupObserver(options: BPDateInput.IOptions, obs: mdash.RefCountedObservable<string>){
			if(this.observer) this.observer.disconnect();
			
			this.observer = new MutationObserver(() => {
				// put formatted date on date change
				const date: moment.Moment = this.formatDate(options);

				// the ~~ turns undefined,null,NaN,strings and other non-numeric values into 0. 
				// The ~ JS operator itself takes the value, turns it into a 0 and does -x+1 operation on it(makes it -1),
				// ~~ reverses the operation(first ~ makes it -1,second one turns it to 0),
				// but it doesn't reverse the conversion from non-number type to number
				const dateIsNotTheSame: boolean = ~~ml.moment(date).date() != ~~ml.moment(obs()).date();
				const minutesAreTheSame: boolean = ~~ml.moment(date).minutes() == ~~ml.moment(obs()).minutes();
				const secondsAreTheSame: boolean = ~~ml.moment(date).seconds() == ~~ml.moment(obs()).seconds();
				const hoursAreTheSame: boolean = ~~ml.moment(date).hours() == ~~ml.moment(obs()).hours();
				
				// if close on selection is active and the changed date is not the same as in the obs()
				if(options.closeOnSelection && dateIsNotTheSame)
				{
					this.contentDiv.find('.namespace-blueprint-datePicker').hide();
				}

				// if the date is valid - set it,otherwise clear it
				if(date) 
				{
					this.setDate(obs);
				} 
				else 
				{
					this.contentDiv.find('.namespace-blueprint-textInput input').val('');
					this.clearDate(obs);
				}  
				
				// if the user clicked on the same selected date but didn't change the time with timepicker
				if(!dateIsNotTheSame && minutesAreTheSame && secondsAreTheSame && hoursAreTheSame)
				{
					this.contentDiv.find('.namespace-blueprint-textInput input').val('');
					this.contentDiv.find('.namespace-blueprint-datePicker').find('.mlui-datepicker-current-day').removeClass('mlui-datepicker-current-day');
					this.clearDate(obs);
				}
			});
	
			this.observer.observe(this.contentDiv.find('.ml-blueprint-date-text')[0], {
				subtree: true,
				childList: true
			});
		}
		
		/**
		 * handle events for date input
		 * @param options options object with props
		 * @param obs observable variable
		 */
		protected setupEventListeners(options: BPDateInput.IOptions, obs: mdash.RefCountedObservable<string>){
			const that: this = this;
	
			this.contentDiv.find('.namespace-blueprint-textInput input').off('click').on('click', () => {
				this.contentDiv.find('.namespace-blueprint-datePicker').show();
			});
	
			function hideOnClick(e){
				if(e.srcElement.classList.contains('ml-dash-content')){
					that.contentDiv.find('.namespace-blueprint-datePicker').hide();
				}
			}
			
			document.body.removeEventListener('click', hideOnClick);
			document.body.addEventListener('click', hideOnClick);

			this.setupObserver(options, obs);
		}

		/**
		 * destroys and re-adds children, makes the neccessary styling or attribute corrections for date input
		 * @param options options object with props
		 */
		protected resetOrInitialize(options: BPDateInput.IOptions): void{
			this.contentDiv.empty();
			this.destroyChildren(true);

			let placeholder: string = '';

			if(options.dateFormat){
				switch(options.dateFormat){
					case BPDateInput.DateFormat.JS_DATE:
						placeholder = "JS Date";
						break;
					case BPDateInput.DateFormat.MONTH_DAY_YEAR:
						placeholder = "MM/DD/YYYY (moment)";
						break;
					case BPDateInput.DateFormat.YEAR_MONTH_DAY:
						placeholder = "YYYY-MM-DD (moment)";
						break;
					case BPDateInput.DateFormat.YEAR_MONTH_DAY_TIME:
						placeholder = "YYYY-MM-DD HH:mm:ss (moment)";
						break;
				}
			}
			else
			{
				placeholder = 'JS Date';
			}

			this.addChild(0, BPTextInput.builder().options({
				type: 'text',
				readonly: true,
				disabled: options.disabled,
				placeholder
			}).toJSON());
			
			this.addChild(1, BPDatePicker.builder().options({
				Precision: options.Precision,
				max_date: options.max_date,
				min_date: options.min_date,
				reverseMonthAndYearMenus: options.reverseMonthAndYearMenus,
				year_range: options.year_range,
			}).toJSON());
	
			this.dateInputDate = ml.$('<span class="ml-blueprint-dateinput-date"></span>').appendTo(this.contentDiv);
			this.dateInputText = ml.$('<span class="ml-blueprint-date-text"></span>').appendTo(this.dateInputDate);
			this.dateInputText.text('no date');
			
			this.contentDiv.find('.namespace-blueprint-textInput').css({
				width: 'unset',
				height: 'unset'
			});

			this.contentDiv.find('.namespace-blueprint-datePicker').css({
				width: 'unset',
				height: 'unset'
			});

			this.contentDiv.find('.namespace-blueprint-textInput .ml-blueprint-textInput').css('margin','auto');
			this.contentDiv.find('.namespace-blueprint-datePicker .ml-blueprint-parentDiv').css('margin','auto');
	
			this.contentDiv.find('.namespace-blueprint-datePicker').hide();
			this.contentDiv.find('.namespace-blueprint-datePicker').find('.ml-blueprint-select-date').hide();
		}

		protected init() {
			super.init();

			var obs = this.registerScopeObservable(this.definition.name, '', false);

			this.watchDefinitionOptions((options: BPDateInput.IOptions) => {
				this.resetOrInitialize(options);
				this.setupEventListeners(options, obs);
			})

			//called twice
			//1. To initialze the control but before it is loaded in the DOM (when this.isDOMLoaded == false)
			//2. After the control has been created in the DOM (when this.isDOMLoaded == true)
		}

		protected cleanup() {
			super.cleanup();
			this.observer.disconnect();
			//called just before the control gets destroyed
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPDateInput.IOptionsDefinition>(this.type);
        }
    }
    export namespace BPDateInput {

		/**
		 * Enumeration for Date Formatting.
		 * @readonly
		 * @enum {string}
		 */
		export enum DateFormat{
			/** Native JavaScript Date Format*/
			JS_DATE = 'JS_DATE',
			/** MM/DD/YYYY Date format with Moment.js */
			MONTH_DAY_YEAR = 'MONTH_DAY_YEAR',
			/** YYYY-MM-DD Date format with Moment.js */
			YEAR_MONTH_DAY = 'YEAR_MONTH_DAY',
			/** YYYY-MM-DD HH:mm:ss Date format with Moment.js */
			YEAR_MONTH_DAY_TIME = 'YEAR_MONTH_DAY_TIME'
		}

		/**
		 * Interface for working with Date Input
		 * @typedef IDateInput
		 * @readonly
		 * @interface
		 */
		export interface IDateInput{
			/**
			 * Close the calendar when a date is selected
			 * @type {boolean}
			 */
			closeOnSelection?: boolean,
			/**
			 * Disable interactions in the component
			 * @type {boolean}
			 */
			disabled?: boolean,
			/**
			 * Specify the format for the Date inside the input field and the observable
			 * @type {DateFormat}
			 */
			dateFormat?: DateFormat
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms,BPDatePicker.IOptions, IDateInput {
			//mirrors IOptionsDefinition, but contains real values, whereas IOptionsDefinition can contain path's to values (scopePath)
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms, BPDatePicker.IOptionsDefinition {
			//options that can be passed to the control when created
			closeOnSelection?: boolean | mdash.IScopePath,
			dateFormat?: DateFormat | mdash.IScopePath,
			disabled?: boolean | mdash.IScopePath
		}
	}
	mdash.registerControl(BPDateInput,'dashboard.components.controls', BPDateInput.meta);
}