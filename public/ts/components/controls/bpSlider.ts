namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPSlider<T extends BPSlider.IOptionsDefinition> extends mdash.BaseInputControl<T> {
		public static type: string = 'BPSlider';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Slider',
			shortDescription: "The slider component performs an action or a series of actions to communicate with other embedded components when altering the slider position",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPSlider.builder().options(BPSlider.initialOptions.GenericOptions.options).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPSlider.IOptions> = {
			'BPSlider': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		protected slider: JQuery;
		protected handleLabel: JQuery;
		protected handleAppended: boolean = false;
		protected observer: MutationObserver;

		constructor(id: string, definition: mdash.IControlDefinition<T>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);

		}

		public build() {
			super.build();
			
			this.slider = ml.$("<div class='ml-blueprint-slider'></div>").slider();
			this.handleLabel = ml.$(`<span class='ml-blueprint-slider-handle-label'></span>`);
			
			this.contentDiv.append(this.slider);
			
			this.container.className += " namespace-blueprint-slider";
			
		}
		
		protected validateDefinition(): boolean {
			
			if (!super.validateDefinition())
				return false;
			
			return true;
		}

		/**
		 * insert a data-id attribute inside each handle on the slider track
		 * that way we can track and manage the handles easier
		 */
		protected giveIdentityToHandles(): void{
			this.contentDiv.find('.mlui-slider-handle').each((index, elem) => {
				ml.$(elem).attr('data-id',index);
			})
		}
		
		/**
		 * Update the slider on observable change
		 * @param {RefCountedObservable} valueObs valueObs - Observable
		 */
		protected initializeListeners(valueObs: mdash.RefCountedObservable<any>): void {

			//objectsToDispose is an array of listeners that get destroyed when the control is destroyed
			//add listeners to this.objectsToDispose in order to prevent memory leaks
			this.objectsToDispose.push(
				valueObs.subscribe(newValue => {
					if (valueObs() == null || Number(newValue) != this.slider.slider('value')) {
						this.contentDiv.find('.ml-blueprint-slider-handle-label').text(newValue);
						this.slider.slider('value', Number(newValue));
					}
				})
			);
		}

		/**
		 * Find the closest handle to drag it to that label
		 * @param {JQueryEventObject} e - The event object of the triggered event,in this case - the label on which the event happened
		 * @param {HTMLElement} handle - The handle which is affected
		 * @param {RefCountedObservable} obs - The observable
		 * @param {boolean} vertical - If the orientation of the slider is vertical or not
		 */
		protected dragClosestHandleToLabel(e: JQueryEventObject, handle: HTMLElement, obs: mdash.RefCountedObservable<any>, 
				vertical: boolean): void{
			const labelValue: number = Number(ml.$(e.srcElement).text());
			const labelLeft: string = e.srcElement['style'].left;
			const labelTop: string = e.srcElement['style'].top;
			const sliderValues: number[] = this.slider.slider('values');
			const sliderValue: number = this.slider.slider('value');

			if((sliderValue || sliderValue == 0) && sliderValues.length == 0)
			{
				this.slider.slider('value', labelValue);
				obs(labelValue);
			}
			else
			{
				// do it for multiple handles
				let handleIndex: number = Number(ml.$(handle).attr('data-id'));

				this.slider.slider('values', handleIndex, labelValue);
				const obsVals: number[] = obs();
				obsVals[handleIndex] = labelValue;
				obs(obsVals);
			}
			setTimeout(() => {
				ml.$(handle).find('.ml-blueprint-slider-handle-label').text(labelValue);
				vertical ? handle.style.top = labelTop : handle.style.left = labelLeft;
			}, 1);
		}
		
		/**
		 * Dynamically generate vertical or horizontal labels by using template strings and expressions extensively
		 * @param {string} orientation - The orientation of the slider
		 * @param {number} min - the minimum of the slider
		 * @param {number} max - the maximum of the slider
		 * @param {number[]} percentages - the percentages to generate the labels at
		 */
		protected conditionalGeneration(orientation: string, min: number, max: number, percentages?: number[]): void {
			// boundary is used to correctly calculate the value by using a percentage
			const boundary: number = max - min;

			// at first, we append the min value on the track
			this.slider.append(`<span class="ml-blueprint-slider-label ml-blueprint-slider-label-${orientation}" 
					   style="${orientation == 'vertical' ? 'top: 100%' : 'left: 0%'}">${+min.toFixed(1)}</span>`);
					   
			// then,if the user supplies an array of percentages
			// we take those and generate the correct values at their correct positions
			if(percentages)
			{
				for(let i = 0; i < percentages.length; i++)
				{
					// we calculate the value from it's percentage
					// then we use conditions to determine if the label should be
					// placed in vertical or horizontal orientation
					const value: number = (boundary * percentages[i] / 100) + min;
					const positioning: string = orientation === 'vertical' ? `top: ` : `left: `;
					const calculation: string = orientation === 'vertical' ? `${100 - percentages[i]}%` : `${percentages[i]}%`;
					const style: string = positioning + calculation;
					
					this.slider.append(`<span class="ml-blueprint-slider-label ml-blueprint-slider-label-${orientation}" style="${style}">${+value.toFixed(1)}</span>`);
				}
			}

			// after at least the min value is appended, we append the max value as well
			this.slider.append(`<span class="ml-blueprint-slider-label ml-blueprint-slider-label-${orientation}" 
				style="${orientation == 'vertical' ? 'top: 0%' : 'left: 99%'}">${+max.toFixed(1)}</span>`);
		}

		/**
		 * it's used to correct the slider's values so the obs,slider and label values are all the same
		 * @param obs observable variable
		 */
		protected observeAttributeChanges(obs: mdash.RefCountedObservable<any>): void 
		{
			if(this.observer) this.observer.disconnect(); // disconnect the observer if it already exists
			const handle: HTMLElement = this.contentDiv.find('.mlui-slider-handle')[0];
			// create a new MutationObserver instance with a callback and assign it to the class variable
			this.observer = new MutationObserver(() => {
				this.slider.slider('value', Number(handle.textContent));
				obs(Number(handle.textContent));
			});
			// attach the observer to every handle
			this.observer.observe(handle, {attributes: true});
		}

		/**
		 * append the tooltip of the handle
		 * @param options the options object with current parameters
		 * @param obs observable variable
		 */
		protected initializeHandleTooltip(options: BPSlider.IOptions, obs: mdash.RefCountedObservable<any>): void{
			this.slider.slider('option', 'value', obs() || options.min || 0);
			if(!this.handleAppended){
				this.contentDiv.find('.mlui-slider-handle').append(this.handleLabel);
				this.handleAppended = true;
			}
			this.handleLabel.text(obs());
			//obs(options.value);
		}
		
		/**
		 * Generates labels at given percentages
		 * @param vertical Is the slider vertically oriented or not
		 * @param min minimum value of the slider
		 * @param max maximum value of the slider
		 * @param percentages percentages to generate the labels at
		 */
		protected generateLabels(vertical: boolean, min: number, max: number, percentages?: number[]): void{
			
			// detach any existing labels before generating new ones
			this.contentDiv.find('.ml-blueprint-slider-label-vertical, .ml-blueprint-slider-label-horizontal').detach();
			
			vertical ? this.conditionalGeneration('vertical', min, max, percentages) : 
						this.conditionalGeneration('horizontal', min, max, percentages);
		}

		/**
		 * made to be overriden by Range Slider and Multiple Range Slider
		 * @param options the options object with props
		 * @param obs Observable variable
		 */
		protected initializeSliderOptions(options: BPSlider.IOptions, obs: mdash.RefCountedObservable<any>): Object{
			return {
				// using ternary and logic operators because if/else take longer to write
				disabled: Boolean(options.disabled),
				range: options.showTrackFill ? "min" : false,
				min: options.min || 0,
				max: options.max || ~~options.min + 100,
				orientation: options.vertical ? 'vertical' : 'horizontal',
				step: options.step || 1,
				value: obs() || 0,
				slide: (event: JQueryEventObject, { handle, value }): void => {
					// update the observable value on change
					obs(value);
					// if the slide event source was a generated label
					if(event.srcElement.classList.contains('ml-blueprint-slider-label'))
					{
						this.dragClosestHandleToLabel(event, handle, obs, options.vertical);
					}
					this.handleLabel.text(value);
					if(options.vertical)
					{
						// vertical mode of the slider with MapLarge version of jQuery UI is broken
						// (when you drag the handle,only the blue line moves up and down,the handle stays)
						// (without the showTrackFill, nothing moves)
						// jQuery UI's Slider handle moves up and down by
						// adding or subtracting a percentage from the bottom css parameter
						// it works perfectly here (https://jqueryui.com/slider/#slider-vertical)
						// but it wasn't working in our extension pages
						// After experimenting with it I found that the slider handle does react to top parameter
						// the line below takes a "reverse" value of the bottom
						// assigns it to the top so it properly handles the drag events
						// (without subtracting from 100 the handle does inverse movement)
						handle.style.top = 100 - parseFloat(handle.style.bottom) + '%';
					}
				}
			}

		}

		/**
		 * Sets up the labels for the slider handles
		 * @param options options object with props
		 * @param obs observable variable
		 */
		protected initializeLabels(options: BPSlider.IOptions, obs: mdash.RefCountedObservable<any>): void {
			if(options.vertical)
			{
				// for some reason,the blue bar only takes 25% of the width in vertical mode
				// I just put it at 100% so it fills the whole div
				this.contentDiv.find('.mlui-slider-range').css('width', '100%');
				//if (ml.util.isNotNullUndefinedOrEmpty(obs()))
				//{
					// a functionality of the slider is broken in vertical mode
					// when you give it a default or initial value,only the blue bar gets to the spot correctly
					// these lines also put the handle on the right spot and fix the problem
					this.initializeHandleTooltip(options, obs);
					this.handleLabel.addClass('ml-blueprint-slider-handle-label-vertical');
					this.contentDiv.find('.mlui-slider-handle')[0].style.top = 100 - parseFloat(this.contentDiv.find('.mlui-slider-handle')[0].style.bottom) + '%';
				//}
			}
			else
			{
				// set the values and the label when it's horizontal
				this.handleLabel.removeClass('ml-blueprint-slider-handle-label-vertical');
				this.initializeHandleTooltip(options, obs);
				
				// when switching back and forth between vertical and horizontal modes dynamically
				// the handle positioning and the track bar display incorrectly
				// these are css fixes
				this.contentDiv.find('.mlui-slider-handle').css('top', '-5px');
				this.contentDiv.find('.mlui-slider-range').css('height', '100%');
			}
		}

		protected init() {
			super.init();
			
			var obs = this.registerScopeObservable(this.definition.name, 0, false);
			this.initializeListeners(obs);
			this.initTransforms();
			if(this.isDOMLoaded)
			{
				this.watchDefinitionOptions((options: BPSlider.IOptions) => {
					
					// initialize the slider with options
					this.slider.slider(this.initializeSliderOptions(options, obs));
					this.initializeLabels(options, obs);
					this.generateLabels(Boolean(options.vertical), this.slider.slider('option', 'min'),this.slider.slider('option', 'max'), options.labelPercentages);
					this.giveIdentityToHandles();
					this.observeAttributeChanges(obs);
				})
			}
			}
			
		protected cleanup() {
			super.cleanup();
			if(this.observer) this.observer.disconnect();			
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPSlider.IOptionsDefinition>(this.type);
		}
    }
    
    export namespace BPSlider {

		/**
		 * Interface provides properties for the Slider component
		 * @typedef ISliderProps
		 * @interface
		 * @readonly
		 * @augments BPBase.ISliderBaseProps
		 */
		export interface ISliderProps extends BPBase.ISliderBaseProps{
			/**
			 * Percentages to generate the slider labels at
			 * @type {number[]}
			 */
			labelPercentages?: number[],
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, ISliderProps {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            disabled?: boolean | mdash.IScopePath,
			max?: number | mdash.IScopePath,
			min?: number | mdash.IScopePath,
			showTrackFill?: boolean | mdash.IScopePath,
			step?: number | mdash.IScopePath,
			vertical?: boolean | mdash.IScopePath,
			labelPercentages?: number[] | mdash.IScopePath,
		}
	}

	mdash.registerControl(BPSlider,'dasboard.component.controls', BPSlider.meta);

}