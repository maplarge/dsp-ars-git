namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;
	
	export class BPCollapse extends mdash.controls.CompositeControl<BPCollapse.IOptions, BPCollapse.IOptionsDefinition> {
		public static type: string = 'BPCollapse';
		
		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Collapsable Button',
			shortDescription: "A dashboard control that can hide or display child controls. The collapseable content is displayed inline and will push subsequent elements around.",
			categories: ["Layout"],
			childSupport: mdash.ChildSupport.MULTIPLE
		};

		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPCollapse.builder().options({
				showText: "Show", hideText: "Hide"
			}).toJSON();
		}
		
		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

		constructor(id: string, definition: mdash.IControlDefinition<BPCollapse.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}

		private insideScope: mdash.IScope;

		/**
		 * @internal
		 * 
		 * Explicitly sets the container than any control children will be created in.
		 */
		protected getChildContainer(index:number, definition: mdash.IControlDefinition<any>): HTMLElement { 
			return this.contentDiv.find('.ml-blueprint-collapse-container')[0];
		}

		/**
		 * Generate the structure for Collapse
		 * @param options Collapse component options object
		 */
		protected getControlLayout(options: BPCollapse.IOptions): mdash.IControlDefinition<any> {
			return mdash.controls.ScopeDefaults.builder()
				.options({
					defaults: [
						{ path: "isCollapseOpen", value: options.isOpen },
						{ path: "buttonShowText", value: options.showText },
						{ path: "buttonHideText", value: options.hideText },
						{ path: "transitionDuration", value: options.transitionDuration }
					]
				}).children([
					ctrl.BPButton.builder().options({
						intent: ctrl.BPBase.Intent.Default,
						onClick: (scope) => {
							if(this.getScopeValue("isCollapseOpen", scope))
							{
								this.contentDiv.find('.ml-blueprint-collapse-container').slideUp(this.getScopeValue("transitionDuration", scope));
							}
							else
							{
								this.contentDiv.find('.ml-blueprint-collapse-container').slideDown(this.getScopeValue("transitionDuration", scope));
							}
							this.setScopeValue("isCollapseOpen", !this.getScopeValue("isCollapseOpen", scope), scope);
						},
						label: { $scopePath: (scope) => {
								// assign button's scope to a class member so the onOptionsChanged can also update the scope values
								this.insideScope = scope;
								return this.getScopeValue("isCollapseOpen", scope) ? this.getScopeValue("buttonHideText", scope) : 
																	this.getScopeValue("buttonShowText", scope);
							}
						}	
					}),
					mdash.controls.Html.builder().options({
						html: "<div class='ml-blueprint-collapse-container'></div>",
					})
				]).toJSON();
		}
		
		/**
		 * @inheritdoc
		 */
		protected validateDefinition(): boolean {
			if (!super.validateDefinition())
				return false;
			return true;
		}
		
		/**
		 * @inheritDoc
		 */
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, false, false);
			
			if(this.isDOMLoaded)
			{
				this.container.classList.add('namespace-blueprint-collapse');
				this.createChildren();
			}
		}
		
		/**
		 * @inheritdoc 
		 */
		protected onOptionsChanged(options: BPCollapse.IOptions): void{
			if(options.isOpen)
			{
				this.contentDiv.find('.ml-blueprint-collapse-container').show(options.transitionDuration);
			}
			else
			{
				this.contentDiv.find('.ml-blueprint-collapse-container').hide(options.transitionDuration);
			}
			// update scope variables whenever options change externally
			this.setScopeValue("isCollapseOpen", options.isOpen, this.insideScope);
			this.setScopeValue("buttonShowText", options.showText, this.insideScope);
			this.setScopeValue("buttonHideText", options.hideText, this.insideScope);
			this.setScopeValue("transitionDuration", options.transitionDuration, this.insideScope);
		}

		/**
		 * @inheritdoc
		 */
		protected cleanup() {
			super.cleanup();
		}
		
		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPCollapse.IOptionsDefinition>(this.type);
		}
	}
	export namespace BPCollapse {

		/**
		 * Interface for collapse properties
		 * @typedef ICollapseProps
		 * @interface
		 * @readonly
		 */
		export interface ICollapseProps{
			/**
			 * Is the collapse open or not
			 * @type {boolean}
			 */
			isOpen?: boolean,
			/**
			 * Text for button when the collapse is hidden
			 * @type {string}
			 */
			showText: string,
			/**
			 * Text for button when the collapse is shown
			 * @type {string}
			 */
			hideText: string,
			/**
			 * Duration of the show/hide animation
			 * @type {number}
			 */
			transitionDuration?: number
		}

		export interface IOptions extends ICollapseProps, mdash.IControlOptionsWithTransforms {
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
			isOpen?: boolean | mdash.IScopePath,
			showText: string | mdash.IScopePath,
			hideText: string | mdash.IScopePath,
			transitionDuration?: number | mdash.IScopePath
		}
	}

	mdash.registerControl(BPCollapse,'dashboard.components.controls', BPCollapse.meta);
}