namespace dashboard.components.controls {

    import mdash = ml.ui.dashboard;

    export class BPOverflow extends mdash.BaseControl<BPOverflow.IOptionsDefinition> {
        public static type: string = 'BPOverflow';

        public static meta: mdash.IControlMeta = {
            status: mdash.ControlReleaseStatus.ALPHA,
            level: mdash.ControlLevel.BASIC,
            friendlyName: 'Overflow List',
            shortDescription: "OverflowList takes a generic list of items and renders as many items as can fit inside itself.",
            categories: ["Visualization"],
            childSupport: mdash.ChildSupport.MULTIPLE
        };
        public static initialOptions: mdash.IDashboardInitialOptions<BPOverflow.IOptions> = {
            'BPOverflow': {
                options: {
                    outputTransforms: []
                }
            }
        };
        public static configSchema: mdash.IComponentConfigSchema = {
            settings: [
                {
                    path: 'validators',
                    label: 'Validators',
                    type: mdash.IComponentConfigSettingTypes.VALIDATOR,
                    isArray: true
                },
                {
                    path: 'outputTransforms',
                    label: 'Transforms',
                    type: mdash.IComponentConfigSettingTypes.TRANSFORM,
                    isArray: true,
                    transformSources: ['Text']
                },
            ]
        }
        private div: JQuery;
        private div_inner: JQuery;
        private ul: JQuery;
        private link: JQuery;
        private DottedUl: HTMLUListElement;


        constructor(id: string, definition: mdash.IControlDefinition<BPOverflow.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
            super(id, definition, container, scope, parent);

        }

        public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
            return BPOverflow.builder().options(BPOverflow.initialOptions.GenericOptions.options).children(children).toJSON();
        }

        public static builder() {
            return new mdash.ControlDefinitionBuilderWithChildren<BPOverflow.IOptionsDefinition>(this.type);
        }

        public build() {
            super.build();
            this.container.classList.add('namespace-blueprint-overflowList');
            this.ul = ml.$(`<ul class = 'ml-blueprint-ul'></ul>`);
            this.div = ml.$(`<div class="ml-blueprint-default-contanier"></div>`);
            this.div_inner = ml.$(`<div class="ml-blueprint-inner-div"></div>`);
            this.div_inner.append(this.ul);
            this.div.append(this.div_inner);

            this.contentDiv.append(this.div);
            this.DottedUl = document.createElement("ul");
            this.DottedUl.className = "popover";
            ml.$('li', this.div).not(':last').append('<i></i>')

            var popover = document.querySelector('.ml-blueprint-popover')

            document.addEventListener("click", (e: Event) => {
                // if(e.target.classList.closest())
                if (!e.srcElement.closest('.ml-blueprint-icon') && !e.srcElement.closest('.ml-blueprint-popover')) {
                    document.querySelector('.ml-blueprint-popover').classList.remove('ml-blueprint-active-popover')

                }
                else {

                    if (document.querySelector('.ml-blueprint-popover').classList.contains('ml-blueprint-active-popover')) {
                        document.querySelector('.ml-blueprint-popover').classList.remove('ml-blueprint-active-popover')
                    }
                    else {
                        document.querySelector('.ml-blueprint-popover').classList.add('ml-blueprint-active-popover')
                        this.elementHover(document.querySelector('.ml-blueprint-popover'))
                    }
                }
            })
        }
        protected  elementHover(element): void {
            Array.prototype.map.call(element.children, (elem)=>{
                elem.addEventListener('mouseenter', function () {
                    if(ml.$(elem).children().find('a')[0] != undefined){
                        ml.$(elem).addClass('ml-hover');
                    }
                })
            })

        }

        protected validateDefinition(): boolean {

            if (!super.validateDefinition())
                return false;

            return true;
        }

        protected init() {
            super.init();
            var obs = this.registerScopeObservable(this.definition.name, false, false);

            if (this.isDOMLoaded) {

                this.watchDefinitionOptions((options: BPOverflow.IOptions) => {

                        ml.$(this.ul).children().detach();

                        Array.prototype.map.call(options.link_data, (element, index) => {

                            if (element.href) {
                                if (index == 0) {
                                    this.link = ml.$(`<li class="ml-blueprint-link href">
                                       <b><a class="ml-blueprint-span-overflow ">
                                            ${element.label}
                                           </a> </b>
                                        </li>`);
                                    ml.$(this.ul).prepend(this.link)
                                } else {
                                    this.link = ml.$(`<li class="ml-blueprint-link href">
                                       <a class="ml-blueprint-span-overflow">
                                            ${element.label}
                                           </a> 
                                        </li>`);
                                    ml.$(this.ul).prepend(this.link)
                                }
                            } else {
                                if (index == 0) {
                                    this.link = ml.$(`<li class="ml-blueprint-link">
                                       <b><span class="ml-blueprint-span-overflow">
                                            ${element.label}
                                           </span> </b>
                                        </li>`);
                                    ml.$(this.ul).prepend(this.link)
                                } else {
                                    this.link = ml.$(`<li class="ml-blueprint-link">
                                       <span class="ml-blueprint-span-overflow">
                                            ${element.label}
                                           </span> 
                                        </li>`);
                                    ml.$(this.ul).prepend(this.link)
                                }
                            }
                        });

                        var icon;
                        if (options.icon) {
                            icon = `<i class="ml-blueprint-icon  ${options.icon}"></i>`
                        } else {
                            icon = `<i class="ml-blueprint-icon  ${options.icon}"></i>`
                        }

                        let items = document.querySelectorAll('.ml-blueprint-link');

                        function elementsWidthsSum(index?: number) {
                            let sumWidth: number = 0;
                            if (index) {
                                for (let i = 0; i < index; i++) {
                                    sumWidth += items[i].getBoundingClientRect().width
                                }
                            } else {
                                for (let i = 0; i < items.length; i++) {
                                    sumWidth += items[i].getBoundingClientRect().width
                                }
                            }
                            return sumWidth;

                        }


                        function getAbsalutWidth(wiidthPercent: number): number {
                               if(wiidthPercent >100)  {
                                   getAbsalutWidth(98);
                               }
                                ml.$('.ml-blueprint-inner-div').css('width', `${wiidthPercent}%`);
                                return document.querySelector('.ml-blueprint-inner-div')
                                    .getBoundingClientRect().width;


                        }

                        function checkValidation(): void {

                            var valid = elementsWidthsSum() - document.querySelector('.ml-blueprint-inner-div')
                                .getBoundingClientRect().width;

                            if (valid < -50) {
                                ml.$('#icon').css('display', 'inline');
                                ml.$(this.ul).children("li:nth-child(1)").css('display', 'none');
                            } else {
                                // ml.$('#icon').css('display', 'none');
                                ml.$(this.ul).children("li:nth-child(1)").css('display', 'none');
                            }


                        }

                        function checkValidationEnd(): void {

                            var valid = elementsWidthsSum() - document.querySelector('.ml-blueprint-inner-div')
                                .getBoundingClientRect().width;

                            if (valid > -44) {
                                ml.$(this.ul).children(`li:nth-child(${items.length-1})`).css('display', 'none');
                            } else {
                                //  ml.$('#icon').css('display', 'none');
                                ml.$('#icon').css('display', 'none');
                                ml.$(this.ul).children(`li:nth-child(${items.length})`).css('display', 'none');
                            }

                        }

                        var ul = new Array();
                        var sizeEnd = new Object();
                        var sizeStart1 = new Object();

                        for (let i = items.length; i > 0; i--) {
                            sizeEnd[`size_${i}`] = elementsWidthsSum(i);
                        }

                        for (let i = 0; i < items.length + 1; i++) {
                            sizeStart1[`size_${i}`] = elementsWidthsSum((items.length) - i);
                        }

                        switch (options.position) {
                            case BPBase.Position.Start:

                                ml.$(this.ul).prepend(`<li id="icon"  class="ml-blueprint-link">
                                           <span  class="ml-blueprint-span-overflow">
                                               ${icon}
                                           </span>  
                                            <ul class ="ml-blueprint-popover"></ul>
                                        </li>`);

                                if (options.width <= 100) {
                                    checkValidation();
                                } else {
                                    ml.$(this.ul).children(`li:nth-child(2)`).css('display', 'none');
                                }

                                for (let i = 0; i < items.length + 1; i++) {
                                    if (i == 5) var distance = 70;
                                    else var distance = 110;
                                    if (i == items.length - 1) {

                                        if (getAbsalutWidth(options.width) < sizeStart1[`size_${i}`] + distance) {
                                            ml.$(this.ul).children(`li:nth-child(${i + 2})`)
                                                .css('display', 'none');
                                            if (ml.$(this.ul).children(`li:nth-child(${i + 2})`).hasClass('href')) {
                                                ul.push('<a class="ml-href" href="#"><i class="mlicon-document162"></i>'
                                                    + ml.$(this.ul).children(`li:nth-child(${i + 2})`).text() + '</a>');
                                            } else {
                                                ul.push('<span  style="color: darkgrey"><i  class="mlicon-document162" style="color: darkgrey"></i>'
                                                    + ml.$(this.ul).children(`li:nth-child(${i + 2})`).text() + '</span>');
                                            }

                                        }
                                    } else {
                                        if (getAbsalutWidth(options.width) < sizeStart1[`size_${i}`] + distance) {
                                            ml.$(this.ul).children(`li:nth-child(${i + 2})`)
                                                .css('display', 'none');
                                            if (ml.$(this.ul).children(`li:nth-child(${i + 2})`).hasClass('href')) {
                                                ul.push('<a class="ml-href" href="#"><i class="mlicon-load"></i>'
                                                    + ml.$(this.ul).children(`li:nth-child(${i + 2})`).text() + '</a>');
                                            } else {
                                                ul.push('<span  style="color: darkgrey"><i  class="mlicon-load" style="color: darkgrey"></i>'
                                                    + ml.$(this.ul).children(`li:nth-child(${i + 2})`).text() + '</span>');
                                            }

                                        }
                                    }

                                }
                                for (let i = 0; i < ul.length - 1; i++) {
                                    ml.$('.ml-blueprint-popover').prepend(`<li ><span class="ml-blueprint-pop-element">${ul[i]}</span></li>`)

                                }


                                if (ml.$(this.ul).children(`li:nth-child(${items.length+1})`).css('display')=="none") {
                                    ml.$('#icon').addClass('last');
                                } else {
                                    ml.$('#icon').removeClass('last');
                                }

                                break;
                            case BPBase.Position.End:
                                ml.$(this.ul).append(`<li id="icon" class="ml-blueprint-link">
                                           <span class="ml-blueprint-span-overflow">
                                               ${icon}
                                           </span>  
                                           <ul class ="ml-blueprint-popover"></ul>
                                        </li>`);
                                ml.$('.ml-blueprint-popover').addClass('end-position');

                                checkValidationEnd();

                                for (let i = items.length + 1; i >= 0; i--) {
                                    if (i == items.length) {
                                        if (getAbsalutWidth(options.width) < sizeEnd[`size_${i}`] + 70) {
                                            ml.$(this.ul).children(`li:nth-child(${i})`)
                                                .css('display', 'none');

                                            if (ml.$(this.ul).children(`li:nth-child(${i})`).hasClass('href')) {
                                                ul.push('<a class="ml-href" href="#"><i class="mlicon-document162"></i>'
                                                    + ml.$(this.ul).children(`li:nth-child(${i})`).text() + '</a>');
                                            } else {
                                                ul.push('<span  style="color: darkgrey"><i style="color: darkgrey" class="mlicon-document162"></i>'
                                                    + ml.$(this.ul).children(`li:nth-child(${i})`).text() + '</span>');
                                            }

                                        }
                                    } else {
                                        if (getAbsalutWidth(options.width) < sizeEnd[`size_${i}`] + 70) {
                                            ml.$(this.ul).children(`li:nth-child(${i})`)
                                                .css('display', 'none');

                                            if (ml.$(this.ul).children(`li:nth-child(${i})`).hasClass('href')) {
                                                ul.push('<a class="ml-href" href="#"><i class="mlicon-load"></i>'
                                                    + ml.$(this.ul).children(`li:nth-child(${i})`).text() + '</a>');
                                            } else {
                                                ul.push('<span  style="color: darkgrey"><i style="color: darkgrey" class="mlicon-load"></i>'
                                                    + ml.$(this.ul).children(`li:nth-child(${i})`).text() + '</span>');
                                            }

                                        }
                                    }
                                }

                                Array.prototype.map.call(ul, (element, index) => {
                                    ml.$('.ml-blueprint-popover').prepend(`<li ><span class="ml-blueprint-pop-element">${element}</span></li>`)
                                });


                                if (ml.$('#icon').css('display') == 'none') {
                                    ml.$('li.ml-blueprint-link:nth-last-child(2)').addClass('last')
                                } else {
                                    ml.$('li.ml-blueprint-link:nth-last-child(2)').removeClass('last')
                                }

                                break;
                            default:
                                break;

                        }


                    }
                );

            }

        }

        protected cleanup() {
            super.cleanup();
        }
    }

    export namespace BPOverflow {

        /**
         * Overflow component properties interface
         * @typedef IOverflowProps
         * @interface
         * @readonly
         * @augments BPBase.IControlProps
         */
        export interface IOverflowProps extends BPBase.IControlProps {
            /**
             * Width of the container
             * @type {number}
             */
            width?: number;
            /**
             * Icon class of the first element
             * @type {string}
             */
            icon?: string;
            /**
             * Object with links displayed on he overflow list
             * @type {ctrl.BPBase.IControlProps[]}
             */
            link_data?: ctrl.BPBase.IControlProps[];
            /**
             * 
             * @type {Position}
             */
            position?: ctrl.BPBase.Position;
        }

        export interface IOptions extends mdash.IControlOptionsWithTransforms, IOverflowProps {
        }

        export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            width?: number | mdash.IScopePath;
            link_data?: ctrl.BPBase.IControlProps[] | mdash.IScopePath;
            icon?: string | mdash.IScopePath;
            position?: ctrl.BPBase.Position | mdash.IScopePath;


        }
    }

    mdash.registerControl(BPOverflow, 'dashboard.components.controls', BPOverflow.meta);
}