namespace dashboard.components.controls {

	import mdash = ml.ui.dashboard;

	export class BPControl extends mdash.BaseInputControl<BPControl.IOptionsDefinition> {
		public static type: string = 'BPControl';

		public static meta: mdash.IControlMeta = {
			status: mdash.ControlReleaseStatus.ALPHA,
			level: mdash.ControlLevel.BASIC,
			friendlyName: 'Control',
			shortDescription: "Control draws itself on the form or on another control.",
			categories: ["Input"],
			childSupport: mdash.ChildSupport.NONE
		};
		public static getInitialConfiguration(parentType: string, children: mdash.IControlDefinition<any>[]) {
			return BPControl.builder().options(BPControl.initialOptions.GenericOptions.options).children(children).toJSON();
		}

		public static initialOptions: mdash.IDashboardInitialOptions<BPControl.IOptions> = {
			'BPControl': {
				options: {
					outputTransforms: []
				}
			}
		};

		public static configSchema: mdash.IComponentConfigSchema = {
			settings: [
				{ path: 'validators', label: 'Validators', type: mdash.IComponentConfigSettingTypes.VALIDATOR, isArray: true },
				{ path: 'outputTransforms', label: 'Transforms', type: mdash.IComponentConfigSettingTypes.TRANSFORM, isArray: true, transformSources: ['Text'] },
			]
		}

        private controlGroup: JQuery;
		private htmlSelect: JQuery;
		private textInput: JQuery;
		private button: JQuery;

		constructor(id: string, definition: mdash.IControlDefinition<BPControl.IOptions>, container: HTMLElement, scope: mdash.IScope, parent: mdash.BaseControl<any>) {
			super(id, definition, container, scope, parent);
		}


		public build() {
			super.build();
			this.container.classList.add('namespace-blueprint-control-group');
			this.container.classList.add('namespace-blueprint-button');
			this.controlGroup = ml.$('<div class="ml-blueprint-control-group"></div>')
			this.contentDiv.append(this.controlGroup);  
			this.htmlSelect = ml.$(`<div class="ml-blueprint-html-select"><select></select><i></i></div>`)
			this.textInput = ml.$('<div class="ml-blueprint-input-group"><input class="ml-blueprint-input" /></div>')
			this.button = ml.$('<button class="ml-blueprint-control-button"></button>')
			this.controlGroup.append(this.textInput)
		}
		
		protected validateDefinition(): boolean {
			if (!super.validateDefinition())
				return false;

			return true;
		}
		
		
		protected init() {
			super.init();
			var obs = this.registerScopeObservable(this.definition.name, [], false);
			this.initTransforms()
			
					
			if (this.isDOMLoaded) {
				this.watchDefinitionOptions((options: BPControl.IOptions) => {
					ml.$('.ml-blueprint-control-group .ml-blueprint-html-select i').addClass('mlicon-MapLarge-icons_dropdown')

					function obsI(htmlSelect, textInput){
						let obsItems = []
						obsItems[0] = htmlSelect.val();
						obsItems[1] = textInput.val();
						obs(obsItems)
					}

					ml.$('select',this.htmlSelect).children().detach()
					
					ml.$('input',this.textInput).attr('placeholder', options.placeholder)
					
				
					this.button.html('<i></i>')

					if(options.vertical){
						this.controlGroup.addClass('ml-blueprint-vertical')
					}
					else{
						this.controlGroup.removeClass('ml-blueprint-vertical')
					}
					if(options.fill){
						this.controlGroup.addClass('ml-blueprint-fill')
					}
					else{
						this.controlGroup.removeClass('ml-blueprint-fill')
					}
					if(String(options.left) == 'select' || String(options.right) == 'select'){
						if(options.selectOption){
							ml.$.each(options.selectOption, (id, item)=>{
								ml.$('select',this.htmlSelect).append(`<option value="${item.label}">${item.label}</option>`)
							})
							obsI(ml.$('select',this.htmlSelect), ml.$('input',this.textInput))
						}
						else{
							ml.$('select option',this.htmlSelect).detach()
						}
					}
					if(String(options.left) == 'select'){
						this.controlGroup.prepend(this.htmlSelect)
					}
					if(String(options.right) == 'select'){
						this.controlGroup.append(this.htmlSelect)
					}
					else if(String(options.left) != 'select' && String(options.right) != 'select'){
						ml.$('select',this.htmlSelect).detach()
					}

					if(options.inputIcon){
						this.textInput.addClass('ml-blueprint-input-icon') 
						this.textInput.prepend(`<span><i class="mlicon-${options.inputIcon}"></i></span>`)
					}

					if(String(options.left) == 'button' || String(options.right) == 'button'){
						if(options.icon){
							ml.$('i',this.button).addClass(`mlicon-${options.icon}`)
						}
						else{
							ml.$('i',this.button).removeClass()
						}
						if(options.buttonText){
							this.button.append(`${options.buttonText}`)
						}
						else{
							ml.$(this.button).detach()
						}
					}

					if(String(options.left) == 'button'){
						ml.$(this.controlGroup).prepend(this.button)
					}
					if(String(options.right) == 'button'){
						ml.$(this.controlGroup).append(this.button)
					}
					else if(String(options.left) != 'button' && String(options.right) != 'button'){
						this.button.detach()
					}
					
					if(options.inputValue){
						ml.$('input',this.textInput).val(options.inputValue)
						obsI(ml.$('select',this.htmlSelect), ml.$('input',this.textInput))
					}
					else{
						ml.$('input',this.textInput).removeAttr('value')
					}

					if(options.intent){
						this.button.addClass(`ml-blueprint-button-intent-${options.intent}`)
					}
					else{
						this.button.removeClass(`ml-blueprint-button-intent-${options.intent}`)
					}

					ml.$('input',this.textInput).on('keyup',()=>{
						obsI(ml.$('select',this.htmlSelect), ml.$('input',this.textInput))
					})

					ml.$('select',this.htmlSelect).on('change',()=>{
						obsI(ml.$('select',this.htmlSelect), ml.$('input',this.textInput))
					})

				
				})
			}
		}

		protected cleanup() {
			super.cleanup();
		}

		public static builder() {
			return new mdash.ControlDefinitionBuilderWithChildren<BPControl.IOptionsDefinition>(this.type);
		}
	}

	export namespace BPControl {

		/**
		 * Interface to work with Controls 
		 * @typedef IControlProps
		 * @readonly
		 * @interface
		 * @augments BPBase.IControlProps
		 */
		export interface IControlProps extends BPBase.IControlProps{
			/**
			 * Options for select
			 * @type {string[]}
			 */
			selectOption?: {label:string,value:string}[];
			/**
			 * Orientation of the control (vertical/horizontal)
			 * @type {boolean}
			 */
			vertical?: boolean;
			/**
			 * Value to be placed inside control
			 * @type {string}
			 */
			inputValue?: string;
			/**
			 * Icon of the control
			 * @type {string}
			 */
			inputIcon?: string;
			/**
			 * Text for the button
			 * @type {string}
			 */
			buttonText?: string;
			/**
			 * Intent color of the control
			 * @type {ctrl.BPBase.Intent}
			 */
			intent?: ctrl.BPBase.Intent;
			/**
			 * Control type for the left side
			 * @type {ctrl.BPBase.ControlGroup}
			 */
			left?: ctrl.BPBase.ControlGroup;
			/**
			 * Control type for the right side
			 * @type {ctrl.BPBase.ControlGroup}
			 */
			right?: ctrl.BPBase.ControlGroup;
		}

		export interface IOptions extends mdash.IControlOptionsWithTransforms, IControlProps{
		}

		export interface IOptionsDefinition extends mdash.IControlOptionsWithTransforms {
            icon?: string | mdash.IScopePath;
            inputIcon?: string | mdash.IScopePath;
            vertical?: boolean | mdash.IScopePath;
			fill?: boolean | mdash.IScopePath;
			left?: ctrl.BPBase.ControlGroup | mdash.IScopePath,
			right?: ctrl.BPBase.ControlGroup | mdash.IScopePath,
            placeholder?: string | mdash.IScopePath;
            inputValue?: string | mdash.IScopePath;
			selectOption?: {label:string,value:string}[] | mdash.IScopePath;
			buttonText?: string | mdash.IScopePath;
			intent?: ctrl.BPBase.Intent | mdash.IScopePath;
		}
	}

	mdash.registerControl(BPControl,'dashboard.components.controls', BPControl.meta);
}