"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var r = express.Router();
/* GET home page. */
r.get('/', function (req, res, next) {
    res.render('dashboards/main', { title: 'MapLarge' });
});
r.get('/docs', function (req, res, next) {
    res.render('_index', { title: 'MapLarge' });
});
r.get('/examples', function (req, res, next) {
    res.render('examples/_index');
});
r.get('/examples/:templatename', function (req, res, next) {
    res.render('examples/' + req.params.templatename);
});
r.get('/controls/:templatename', function (req, res, next) {
    res.render('exp/' + req.params.templatename);
});
r.get('/dashboards', function (req, res) {
    res.render('dashboards/main');
});
exports.router = r;
