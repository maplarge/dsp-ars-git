"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app = require("./app");
var http = require("http");
var log = require("node-pretty-log");
var port = process.env.PORT || 8000;
var mlJSUrl = "http://qa.maplarge.com/js";
//var file = fs.createWriteStream("public/lib/MapLargeAPI.js");
//var request = http.get(mlJSUrl, function(response) {
//    log('Info', 'Getting TS from MapLarge');
//    response.pipe(file);
//    file.on('finish', () => log("Info", 'Finished Writting ML file'));
//});
http.createServer(app.App).listen(port, function () {
    log('info', 'Server Listening at ' + port);
});
