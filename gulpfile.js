const gulp = require('gulp'),
	concat = require('gulp-concat'),
	order = require('gulp-order'),
	less = require('gulp-less'),
	path = require('path'),
	log = require('node-pretty-log'),
	request = require('request'),
	merge = require('merge2'),
	source = require('vinyl-source-stream'),
	buffer = require('gulp-buffer'),
	ts = require("gulp-typescript"),
	tsProject = ts.createProject("public/ts/tsconfig.json");

var jssources = [
	'./public/lib/prism.js',
	'./public/lib/MapLargeAPI.js',
	'./public/scripts/MapLarge.Server.d.ts',
	//'./public/scripts/lib/codemirror/codemirror.js',
	'./public/scripts/mlCodeMirror.js',
	'./public/lib/codemirror/mode/javascript/javascript.js',
	'./public/lib/codemirror/mode/xml/xml.js',
	'./public/lib/codemirror/mode/css/css.js',
	'./public/lib/codemirror/mode/vb/vb.js',
	'./public/lib/codemirror/mode/htmlmixed/htmlmixed.js',
	'./public/scripts/*.js',
	'./public/scripts/**/*.js',
];

var styleSources = [
	'./public/lib/**/*.css',
	'./public/styles/*.css',
	'./public/styles/**/*.css',
	'./public/styles/less/**/*.less',
	'./public/ts/**/*.less',
	'./public/ts/**/styles/*.less',
];

var buildStyles = () => {
	log('info', "gulp - compiling Styles");
	return gulp.src(styleSources)
		.pipe(less({
			paths: [path.join(__dirname, 'less', 'includes')]
		}))
		.pipe(concat("style.css"))
		.pipe(gulp.dest('./public/dist/css'));
}

var buildJs = () => {
	log('info', "gulp - compiling Scripts")

	return gulp.src(jssources)
		.pipe(buffer())
		.pipe(concat('scripts.js'))
		.pipe(gulp.dest('./public/dist/scripts'));
}



var watchjs = () => {
	buildJs();
	gulp.watch(jssources.concat(['./gulpfile.js']), ['build-js']);
}



var watchstyles = () => {
	buildStyles();
	gulp.watch(styleSources.concat(['./gulpfile.js']), ['build-styles']);
}

var compileTS = () => {
	log('info', "gulp - compiling TypeScript");
	return tsProject.src()
		.pipe(tsProject())
		.js.pipe(gulp.dest("public/scripts"));
};

gulp.task('build-js', () => buildJs());

gulp.task('build-styles', () => buildStyles());

gulp.task('style-watch', () => watchstyles());

gulp.task('watch-js', () => watchjs());

gulp.task('compile', () => compileTS());

gulp.task('watch', () => {
	watchstyles();
	watchjs();
});

gulp.task('build', ['compile'], () => {
	buildJs();
	buildStyles();
});